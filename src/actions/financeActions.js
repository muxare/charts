import * as types from './actionTypes';
import {beginAjaxCall, ajaxCallError} from './ajaxActions';

let headers = new Headers({
    'Access-Control-Allow-Origin': '*'
});

const fetchConfig = () => {
   return {
       method: 'GET',
       headers: headers,
       mode: 'cors',
       cache: 'default'
    };
};


export function receiveCompanyAnalysis(ticker, data) {
    return {
        type: types.RECEIVE_COMPANY_ANALYSIS,
        payload: {
            ticker: ticker,
            data: data
        }
    };
}
export function receiveCompaniesAnalysis(data) {
    return {
        type: types.RECEIVE_COMPANIES_ANALYSIS,
        payload: data
    };
}
export function getCompaniesAnalysis() {
    return dispatch => {
        dispatch(beginAjaxCall());
        return fetch(`http://localhost:56411/api/analysis/company`, fetchConfig())
            .then(response => response.json())
            .then(json => dispatch(receiveCompaniesAnalysis(json)));
    };
}

export function getCompanyAnalysis(ticker) {
    return dispatch => {
        dispatch(beginAjaxCall());
        return fetch(`http://localhost:56411/api/analysis/${ticker}/`, fetchConfig())
            .then(response => response.json())
            .then(json => dispatch(receiveCompanyAnalysis(ticker, json)));
    };
}

