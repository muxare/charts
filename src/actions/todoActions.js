import * as types from './actionTypes';

/*
 * other constants
 */

export const VisibilityFilters = {
    SHOW_ALL: "SHOW_ALL",
    SHOW_COMPLETED: "SHOW_COMPLETED",
    SHOW_ACTIVE: "SHOW_ACTIVE",
};


/*
 * action creators
 */



// Toto action acreators
export function addTodo(text) {
    return { type: types.ADD_TODO, value: text}
}

export function toggleTodo(index) {
    return { type: types.TOGGLE_TODO, value: index}
}

export function setVisibilityFilter(filter) {
    return { type: types.SET_VISIBILITY_FILTER, value: filter}
}
