import * as types from '../actions/actionTypes';

const initialState = {
    companies: [],
    chartData: {}
};
const finance = (state = initialState, action) => {
    // console.log(state);
    // console.log(action);
    switch (action.type) {
        case types.LOAD_COMPANIES_ANALYSIS_SUCCESS:
            // console.log(action.payload);
            return state;

        case types.LOAD_COMPANIES_ANALYSIS:
            // console.log(action.payload);
            return state;

        case types.RECEIVE_COMPANIES_ANALYSIS:
            //console.log(action.payload);
            return {
                ...state,
                companies: action.payload,
            };

        case types.LOAD_COMPANY_ANALYSIS_SUCCESS:
            //console.log(action.payload);
            return state;

        case types.LOAD_COMPANY_ANALYSIS:
            //console.log(action.payload);
            return state;

        case types.RECEIVE_COMPANY_ANALYSIS:
            //console.log(action.payload);
            //const index = Object.values(state.chartData).findIndex((i, o) => o.ticker === action.payload.ticker);
            return {
                ...state,
                chartData: {
                    ...state.chartData,
                    [action.payload.ticker]: action.payload.data,
                }
            };

        default:
            //console.log(action.payload);
            return state;
    }

};

export default finance;
