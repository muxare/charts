import { ADD_USER } from '../actions/todoActions';

const usersReducer = (state, action) => {
    switch (action.type) {
        case ADD_USER:
            return state;

        case "UPDATE_USER":
            return state;

        case "REMOVE_USER":
            return state;

        default:
            return state;
    }

};

export default usersReducer;
