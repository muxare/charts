import { combineReducers } from 'redux';
import todos from './todosReducer';
import finance from './financeReducer';

const todoApp = combineReducers({
    todos,
    finance
});

export default todoApp;