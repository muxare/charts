import initialState from './todoInitialState';
import { ADD_TODO, TOGGLE_TODO } from '../actions/actionTypes';

const todos = (state = initialState.todos, action) => {
    console.log(action);
    switch (action.type) {
        case ADD_TODO:
            return [
                ...state,
                {
                    text: action.value,
                    completed: false
                }
            ];

        case TOGGLE_TODO:
            return [
                ...state.slice(0,action.value),
                {
                    text: state[action.value].text,
                    completed: !state[action.value].completed
                },
                ...state.slice(action.value+1)
            ];

        default:
            return state;
    }

};

export default todos;