import React from 'react';
import Table, {Column} from '../common/Table';
import dataRows from '../../data/users.json';

class UsersPage extends React.Component {
    constructor(props) {
        super(props);

        this.onRowClick = this.onRowClick.bind(this);
    }
    onRowClick(e) {
        debugger;
        console.log(e);
    }
    render() {
        return (
            <Table rows={dataRows} onRowClick={this.onRowClick}>
                <Column dataKey="name" label="Name" onClick={e => console.log("mu")} />
                <Column dataKey="username" label="User Name" />
                <Column dataKey="email" label="E-mail" />
                <Column dataKey="phone" label="Phone Number" />
            </Table>
        );
    }
}

export default UsersPage;

