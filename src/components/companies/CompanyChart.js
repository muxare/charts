import React from 'react';
import { connect } from 'react-redux';
import CandleStickChart from '../common/CandleStickChart';

class CompanyChart extends React.Component {
    render() {
        return (
            <CandleStickChart />
        );
    }
}
const mapStateToProps = (state) => {

};

const mapDispatchToProps = (dispatch) => {

};

export default connect()(CompanyChart);

