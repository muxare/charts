import React from 'react';
import Table, {Column} from '../common/Table';
import dataRows from '../../data/users.json';

class CompaniesPage extends React.Component {
    render() {
        return (
            <Table rows={dataRows}>
                <Column dataKey="name" label="Name" />
                <Column dataKey="username" label="User Name" />
                <Column dataKey="email" label="E-mail" />
                <Column dataKey="phone" label="Phone Number" />
            </Table>
        );
    }
}

export default UsersPage;

