import React from 'react';
import { Route, Switch, Link } from 'react-router-dom';
import BarchartPage from './BarchartPage';
import CalendarViewPage from './CalendarViewPage';
import HistogramPade from './HistogramPage';
import LinechartPage from './LinechartPage';
import PiechartPage from './PiechartPage';
import RadialTidyTreePage from './RadialTidyTreePage';
import CandleStickChart from './CandleStickChartPage';

const ChartsPage = (props) => (
    <div>
        <nav className="navbar navbar-default navbar-fixed-bottom">
            <ul className="nav navbar-nav">
                <li><Link to="/charts/barchart">Bar Chart</Link></li>
                <li><Link to="/charts/calendar">Calendar View</Link></li>
                <li><Link to="/charts/histogram">Histogram</Link></li>
                <li><Link to="/charts/linechart">Line Chart</Link></li>
                <li><Link to="/charts/piechart">Pie Chart</Link></li>
                <li><Link to="/charts/radialtree">Radial Tidy Tree</Link></li>
                <li><Link to="/charts/candlestick">Candle Stick Chart</Link></li>
            </ul>
        </nav>
        <Switch>
            <Route path="/charts/barchart" component={BarchartPage} />
            <Route path="/charts/calendar" component={CalendarViewPage} />
            <Route path="/charts/histogram" component={HistogramPade} />
            <Route path="/charts/linechart" component={LinechartPage} />
            <Route path="/charts/piechart" component={PiechartPage} />
            <Route path="/charts/radialtree" component={RadialTidyTreePage} />
            <Route path="/charts/candlestick" component={CandleStickChart} />
        </Switch>
    </div>
);

export default ChartsPage;