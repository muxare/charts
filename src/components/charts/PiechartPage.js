import React from 'react';
//import Dimensions from 'react-dimensions';
import PieChart from '../common/Piechart';

const state = {
    dataPieChart: [
        { label: '<5', value: 2704659 },
        { label: '5-13', value: 4499890 },
        { label: '4-171', value: 2159981 },
        { label: '8-241', value: 3853788 },
        { label: '5-442', value: 14106543 },
        { label: '5-644', value: 8819342 },
        { label: '65≥', value: 612463 },
    ]
};

const width = 600;
const height = 600;
class PiechartPage  extends React.Component {
    render() {
        //const { containerWidth, containerHeight} = this.props;

        return (
            <div>
                <h1>Pie Chart Page</h1>
                <PieChart
                    data={state.dataPieChart}
                    width={width}
                    height={height}
                />
            </div>
        );
    }
}


//export default Dimensions()(PiechartPage);
export default PiechartPage;
