import React from 'react';
import LineChart from '../common/Linechart';
import {LineChartData} from '../../data/lineChartData';

const LinechartPage = () => (
    <div>
        <h1>Line Chart Page</h1>
        <LineChart data={LineChartData} />
    </div>
);

export default LinechartPage;
