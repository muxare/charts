import React from 'react';
import CalendarView, {CalendarViewOrig} from '../common/CalendarView';
import data from '../../data/dji.csv.js';

const CalendarViesPage = () => (
    <div>
        <h1>Calendar View Page</h1>
        <CalendarView data={data} />
        <CalendarViewOrig />
    </div>
);

export default CalendarViesPage;
