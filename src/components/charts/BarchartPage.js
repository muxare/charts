import React from 'react';
import BarChart from '../common/Barchart';
//import BarChart from '../../../node_modules/charts/Barchart';

const state = {
    dataBarChart: [
        { salesperson: 'Bob', sales: 33 },
        { salesperson: 'Robin', sales: 12 },
        { salesperson: 'Anne', sales: 41 },
        { salesperson: 'Mark', sales: 16 },
        { salesperson: 'Joe', sales: 59 },
        { salesperson: 'Eve', sales: 38 },
        { salesperson: 'Karen', sales: 21 },
        { salesperson: 'Kirsty', sales: 25 },
        { salesperson: 'Chris', sales: 30 },
        { salesperson: 'Lisa', sales: 47 },
        { salesperson: 'Tom', sales: 5 },
        { salesperson: 'Stacy', sales: 20 },
        { salesperson: 'Charles', sales: 13 },
        { salesperson: 'Mary', sales: 29 }
    ]
};

class BarchartPage extends React.Component {
    render() {
        return (
            <div>
                <h1>Bar Chart Page</h1>
                <BarChart data={state.dataBarChart} />
            </div>
        );
    }
}

export default BarchartPage;
