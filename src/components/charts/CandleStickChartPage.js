import React from 'react';
import CandleStickChart, { CandleStickChartOrig } from '../common/CandleStickChart';
import { CandleStickChartData } from '../../data/stockQuotes.json.js';

const CandleStickChartPage = () => (
    <div>
        <h1>Candle Stick Chart</h1>
        <CandleStickChart
            data={CandleStickChartData.slice(0, 30)}
        />
        <CandleStickChartOrig />
    </div>
);

export default CandleStickChartPage;
