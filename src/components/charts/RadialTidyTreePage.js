import React from 'react';
import RadialTidyTree, {RadialTidyTreeOrig} from '../common/RadialTidyTree';
import {RadialTidyTreeData} from '../../data/flare.json.js';

const RadialTidyTreeePage = () => (
    <div>
        <h1>Radial Tidy Tree Page</h1>
        <RadialTidyTree data={RadialTidyTreeData} />
        <RadialTidyTreeOrig />
    </div>
);

export default RadialTidyTreeePage;
