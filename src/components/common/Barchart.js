import React from 'react';
import './Barchart.css';
import { scaleBand, scaleLinear } from 'd3-scale';
import { max } from 'd3-array';
import XYAxis from './x-y-axis';

// set the dimensions and margins of the graph
const margin = {top: 20, right: 20, bottom: 30, left: 40},
    width = 960 - margin.left - margin.right,
    height = 500 - margin.top - margin.bottom;

// set the ranges
const x = scaleBand().range([0, width]).padding(0.1);
const y = scaleLinear().range([height, 0]);


// https://bl.ocks.org/d3noob/bdf28027e0ce70bd132edc64f1dd7ea4
class BarChart extends React.Component {
    render() {
        const { data } = this.props;
        x.domain(data.map(function(d) { return d.salesperson; }));
        y.domain([0, max(data, function(d) { return d.sales; })]);
        const scales = { xScale: x, yScale: y };
        const axisProps = {height: height, margin: margin};
        return (
            <svg width={width + margin.left + margin.right} height={height + margin.top + margin.bottom}>
                <g transform={`translate(${margin.left}, ${margin.top})`}>
                    {data.map((d, i) => (
                        <rect
                            key={ i }
                            className="bar"
                            x={ x(d.salesperson) }
                            y={ y(d.sales) }
                            width={ x.bandwidth() }
                            height={ height - y(d.sales) }
                        />
                    ))}
                </g>
                <XYAxis {...axisProps} {...scales} />
            </svg>
        );
    }
}
export default BarChart;

export const BarChartOrig = () => (
    <svg width="960" height="500">
        <g transform="translate(40,20)">
            <rect className="bar" x="6.3829787234042215" width="57.4468085106383" y="198.30508474576274"
    height="251.69491525423726"/>
            <rect className="bar" x="70.21276595744678" width="57.4468085106383" y="358.47457627118644"
    height="91.52542372881356"/>
            <rect className="bar" x="134.04255319148933" width="57.4468085106383" y="137.28813559322032"
    height="312.7118644067797"/>
            <rect className="bar" x="197.8723404255319" width="57.4468085106383" y="327.96610169491527"
    height="122.03389830508473"/>
            <rect className="bar" x="261.70212765957444" width="57.4468085106383" y="0" height="450"/>
            <rect className="bar" x="325.531914893617" width="57.4468085106383" y="160.16949152542378"
    height="289.8305084745762"/>
            <rect className="bar" x="389.36170212765956" width="57.4468085106383" y="289.8305084745763"
    height="160.16949152542372"/>
            <rect className="bar" x="453.1914893617021" width="57.4468085106383" y="259.3220338983051"
    height="190.6779661016949"/>
            <rect className="bar" x="517.0212765957447" width="57.4468085106383" y="221.18644067796612"
    height="228.81355932203388"/>
            <rect className="bar" x="580.8510638297872" width="57.4468085106383" y="91.52542372881356"
    height="358.47457627118644"/>
            <rect className="bar" x="644.6808510638298" width="57.4468085106383" y="411.864406779661"
    height="38.13559322033899"/>
            <rect className="bar" x="708.5106382978723" width="57.4468085106383" y="297.45762711864404"
    height="152.54237288135596"/>
            <rect className="bar" x="772.3404255319149" width="57.4468085106383" y="350.8474576271186"
    height="99.15254237288138"/>
            <rect className="bar" x="836.1702127659574" width="57.4468085106383" y="228.81355932203388"
    height="221.18644067796612"/>
            <g transform="translate(0,450)" style={{ fill: "none", fontSize:"10px", fontFamily:"sans-serif", textAnchor:"middle"}}>
                <path className="domain" stroke="#000" d="M0.5,6V0.5H900.5V6"/>
                <g className="tick" opacity="1" transform="translate(35.106382978723374,0)">
                    <line stroke="#000" y2="6" x1="0.5" x2="0.5"/>
                    <text fill="#000" y="9" x="0.5" dy="0.71em">Bob</text>
                </g>
                <g className="tick" opacity="1" transform="translate(98.93617021276593,0)">
                    <line stroke="#000" y2="6" x1="0.5" x2="0.5"/>
                    <text fill="#000" y="9" x="0.5" dy="0.71em">Robin</text>
                </g>
                <g className="tick" opacity="1" transform="translate(162.76595744680847,0)">
                    <line stroke="#000" y2="6" x1="0.5" x2="0.5"/>
                    <text fill="#000" y="9" x="0.5" dy="0.71em">Anne</text>
                </g>
                <g className="tick" opacity="1" transform="translate(226.59574468085103,0)">
                    <line stroke="#000" y2="6" x1="0.5" x2="0.5"/>
                    <text fill="#000" y="9" x="0.5" dy="0.71em">Mark</text>
                </g>
                <g className="tick" opacity="1" transform="translate(290.4255319148936,0)">
                    <line stroke="#000" y2="6" x1="0.5" x2="0.5"/>
                    <text fill="#000" y="9" x="0.5" dy="0.71em">Joe</text>
                </g>
                <g className="tick" opacity="1" transform="translate(354.25531914893617,0)">
                    <line stroke="#000" y2="6" x1="0.5" x2="0.5"/>
                    <text fill="#000" y="9" x="0.5" dy="0.71em">Eve</text>
                </g>
                <g className="tick" opacity="1" transform="translate(418.0851063829787,0)">
                    <line stroke="#000" y2="6" x1="0.5" x2="0.5"/>
                    <text fill="#000" y="9" x="0.5" dy="0.71em">Karen</text>
                </g>
                <g className="tick" opacity="1" transform="translate(481.9148936170213,0)">
                    <line stroke="#000" y2="6" x1="0.5" x2="0.5"/>
                    <text fill="#000" y="9" x="0.5" dy="0.71em">Kirsty</text>
                </g>
                <g className="tick" opacity="1" transform="translate(545.7446808510638,0)">
                    <line stroke="#000" y2="6" x1="0.5" x2="0.5"/>
                    <text fill="#000" y="9" x="0.5" dy="0.71em">Chris</text>
                </g>
                <g className="tick" opacity="1" transform="translate(609.5744680851063,0)">
                    <line stroke="#000" y2="6" x1="0.5" x2="0.5"/>
                    <text fill="#000" y="9" x="0.5" dy="0.71em">Lisa</text>
                </g>
                <g className="tick" opacity="1" transform="translate(673.4042553191489,0)">
                    <line stroke="#000" y2="6" x1="0.5" x2="0.5"/>
                    <text fill="#000" y="9" x="0.5" dy="0.71em">Tom</text>
                </g>
                <g className="tick" opacity="1" transform="translate(737.2340425531914,0)">
                    <line stroke="#000" y2="6" x1="0.5" x2="0.5"/>
                    <text fill="#000" y="9" x="0.5" dy="0.71em">Stacy</text>
                </g>
                <g className="tick" opacity="1" transform="translate(801.063829787234,0)">
                    <line stroke="#000" y2="6" x1="0.5" x2="0.5"/>
                    <text fill="#000" y="9" x="0.5" dy="0.71em">Charles</text>
                </g>
                <g className="tick" opacity="1" transform="translate(864.8936170212766,0)">
                    <line stroke="#000" y2="6" x1="0.5" x2="0.5"/>
                    <text fill="#000" y="9" x="0.5" dy="0.71em">Mary</text>
                </g>
            </g>
            <g fill="none" style={{ fontSize: "10px", fontFamily: "sans-serif", textAnchor: "end"}}>
                <path className="domain" stroke="#000" d="M-6,450.5H0.5V0.5H-6"/>
                <g className="tick" opacity="1" transform="translate(0,450)">
                    <line stroke="#000" x2="-6" y1="0.5" y2="0.5"/>
                    <text fill="#000" x="-9" y="0.5" dy="0.32em">0</text>
                </g>
                <g className="tick" opacity="1" transform="translate(0,411.864406779661)">
                    <line stroke="#000" x2="-6" y1="0.5" y2="0.5"/>
                    <text fill="#000" x="-9" y="0.5" dy="0.32em">5</text>
                </g>
                <g className="tick" opacity="1" transform="translate(0,373.728813559322)">
                    <line stroke="#000" x2="-6" y1="0.5" y2="0.5"/>
                    <text fill="#000" x="-9" y="0.5" dy="0.32em">10</text>
                </g>
                <g className="tick" opacity="1" transform="translate(0,335.5932203389831)">
                    <line stroke="#000" x2="-6" y1="0.5" y2="0.5"/>
                    <text fill="#000" x="-9" y="0.5" dy="0.32em">15</text>
                </g>
                <g className="tick" opacity="1" transform="translate(0,297.45762711864404)">
                    <line stroke="#000" x2="-6" y1="0.5" y2="0.5"/>
                    <text fill="#000" x="-9" y="0.5" dy="0.32em">20</text>
                </g>
                <g className="tick" opacity="1" transform="translate(0,259.3220338983051)">
                    <line stroke="#000" x2="-6" y1="0.5" y2="0.5"/>
                    <text fill="#000" x="-9" y="0.5" dy="0.32em">25</text>
                </g>
                <g className="tick" opacity="1" transform="translate(0,221.18644067796612)">
                    <line stroke="#000" x2="-6" y1="0.5" y2="0.5"/>
                    <text fill="#000" x="-9" y="0.5" dy="0.32em">30</text>
                </g>
                <g className="tick" opacity="1" transform="translate(0,183.05084745762713)">
                    <line stroke="#000" x2="-6" y1="0.5" y2="0.5"/>
                    <text fill="#000" x="-9" y="0.5" dy="0.32em">35</text>
                </g>
                <g className="tick" opacity="1" transform="translate(0,144.91525423728814)">
                    <line stroke="#000" x2="-6" y1="0.5" y2="0.5"/>
                    <text fill="#000" x="-9" y="0.5" dy="0.32em">40</text>
                </g>
                <g className="tick" opacity="1" transform="translate(0,106.77966101694915)">
                    <line stroke="#000" x2="-6" y1="0.5" y2="0.5"/>
                    <text fill="#000" x="-9" y="0.5" dy="0.32em">45</text>
                </g>
                <g className="tick" opacity="1" transform="translate(0,68.64406779661016)">
                    <line stroke="#000" x2="-6" y1="0.5" y2="0.5"/>
                    <text fill="#000" x="-9" y="0.5" dy="0.32em">50</text>
                </g>
                <g className="tick" opacity="1" transform="translate(0,30.508474576271226)">
                    <line stroke="#000" x2="-6" y1="0.5" y2="0.5"/>
                    <text fill="#000" x="-9" y="0.5" dy="0.32em">55</text>
                </g>
            </g>
        </g>
    </svg>
);

