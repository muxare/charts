import React from 'react';
import PropTypes from 'prop-types';

export default class Table extends React.Component {
    constructor(props) {
        super(props);

        const sortableColumn = props.children.find(c => c.props.sortable === true);
        this.state = {
            visibleRows: props.rows,
            sort: {
                key: sortableColumn !== undefined ? sortableColumn.props.dataKey : '',
                order: "asc"
            }
        };

        this.pick = this.pick.bind(this);
        this.renderHeader = this.renderHeader.bind(this);
        this.onSort = this.onSort.bind(this);
    }
    pick(o, ...fields) {
        return fields.reduce((a, x) => {
            if(o.hasOwnProperty(x)) a[x] = o[x];
            return a;
        }, {});
    }
    renderHeader() {
        return (
            <tr>
                {this.props.children.map(c => {
                    return (
                        <th
                            key={c.props.dataKey}
                            onClick={(e) => this.onSort(e, c.props.dataKey)}
                        >
                            {c.props.label}
                        </th>
                    )
                })}
            </tr>
        );
    }
    onSort(e, sortKey) {
        this.setState({
            sort: {
                key: sortKey,
                order: this.state.sort.order === "asc" ? "desc" : "asc"
            }
        });
    }
    render() {
        //const rows = this.state.visibleRows.sort((a,b) => this.state.sort.order === "asc" ? a[this.state.sort.key] < b[this.state.sort.key] : a[this.state.sort.key] > b[this.state.sort.key]);
        const {rows, onRowClick, children} = this.props;
        const columnNames = children.map(c => c.props.dataKey);
        const columnDataCallbacks = Object.assign(...children.map(c => ({[c.props.dataKey]: c.props.rowDataCallback})));
        //const columnNames = children.map(c => c.props.dataKey);

        return (
            <table className="table table-stripped table-hover" style={{textAlign: "left"}}>
                <thead>
                {this.renderHeader()}
                </thead>
                <tbody>
                {rows.map((row, i) =>
                    <TableRow
                        key={i}
                        row={ this.pick(row, ...columnNames) }
                        onClick={e => onRowClick(e, row)}
                        dataCallbacks={columnDataCallbacks}
                    />
                )}
                </tbody>
            </table>

        );
    }
}

Table.propTypes = {
    rows: PropTypes.array.isRequired
};


// Todo: Support child of type CellContent for custom formatting if cell data
export const Column = ({dataKey, label, sortable, onClick, rowDataCallback}) => {
    return <th onClick={onClick}>{label}</th>;
};
Column.propTypes = {
    dataKey: PropTypes.string,
    label: PropTypes.string,
    sortable: PropTypes.bool,
    onClick: PropTypes.func,
    rowDataCallback: PropTypes.func
};

const TableRow = ({row, onClick, dataCallbacks}) => (
    <tr
        className=""
        onClick={onClick}
    >
        {Object.keys(row).map(cellKey =>
            <TableCell
                key={cellKey}
                data={dataCallbacks[cellKey] !== undefined ? dataCallbacks[cellKey](row[cellKey]) : row[cellKey]}
            />
        )}
    </tr>
);
TableRow.propTypes = {
    row: PropTypes.object,
    onClick: PropTypes.func,
    rowDataCallback: PropTypes.object
};

class TableCell extends React.Component {
    render() {
        const { data } = this.props;
        return (
            <td key={this.key}>{data}</td>
        );
    }
}
TableCell.propTypes = {
    data: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ])
};
