import React from 'react';
import './Histogram.css';
import { range, histogram, max } from 'd3-array';
import { randomBates } from 'd3-random';
import { format } from 'd3-format';
import { scaleLinear } from 'd3-scale';
import {XAxis} from "./axis";

const data = range(1000).map(randomBates(10));
const formatCount = format(",.0f");

const margin = {top: 20, right: 20, bottom: 30, left: 50};
const widthSvg = 960;
const heightSvg = 500;
const width = 960 - margin.left - margin.right;
const height = 500 - margin.top - margin.bottom;

const x = scaleLinear().rangeRound([0, width]);
const bins = histogram().domain(x.domain()).thresholds(x.ticks(20))(data);
const y = scaleLinear().domain([0, max(bins, function(d) { return d.length; })]).range([height, 0]);

class Histogram extends React.Component {
    render() {
        return (
            <svg width={widthSvg} height={heightSvg}>
                <g transform={`translate(${margin.left}, ${margin.top})`}>
                    { bins.map((bin, i) =>
                        <g key={i} className="bar" transform={`translate(${x(bin.x0)}, ${y(bin.length)})`}>
                            <rect x={1} width={x(bins[0].x1) - x(bins[0].x0) - 1} height={height - y(bin.length)} />
                            <text dy=".75em" y="6" x={(x(bins[0].x1) - x(bins[0].x0)) / 2} textAnchor="middle">{formatCount(bin.length)}</text>
                        </g>) }
                    <XAxis
                        scale={x}
                        translate={`translate(0, ${height})`}
                    />
                </g>
            </svg>
        );
    }
}
export default Histogram;

export const HistogramOrig = () => (
    <svg width="960" height="500">
        <g transform="translate(30,10)">
            <g className="bar" transform="translate(0,460)">
                <rect x="1" width="44" height="0"/>
                <text dy=".75em" y="6" x="22.5" textAnchor="middle">0</text>
            </g>
            <g className="bar" transform="translate(45,460)">
                <rect x="1" width="44" height="0"/>
                <text dy=".75em" y="6" x="22.5" textAnchor="middle">0</text>
            </g>
            <g className="bar" transform="translate(90,460)">
                <rect x="1" width="44" height="0"/>
                <text dy=".75em" y="6" x="22.5" textAnchor="middle">0</text>
            </g>
            <g className="bar" transform="translate(135,460)">
                <rect x="1" width="44" height="0"/>
                <text dy=".75em" y="6" x="22.5" textAnchor="middle">0</text>
            </g>
            <g className="bar" transform="translate(180,455.4679802955665)">
                <rect x="1" width="44" height="4.5320197044334805"/>
                <text dy=".75em" y="6" x="22.5" textAnchor="middle">2</text>
            </g>
            <g className="bar" transform="translate(225,435.0738916256158)">
                <rect x="1" width="44" height="24.926108374384228"/>
                <text dy=".75em" y="6" x="22.5" textAnchor="middle">11</text>
            </g>
            <g className="bar" transform="translate(270,371.6256157635468)">
                <rect x="1" width="44" height="88.37438423645318"/>
                <text dy=".75em" y="6" x="22.5" textAnchor="middle">39</text>
            </g>
            <g className="bar" transform="translate(315,222.06896551724137)">
                <rect x="1" width="44" height="237.93103448275863"/>
                <text dy=".75em" y="6" x="22.5" textAnchor="middle">105</text>
            </g>
            <g className="bar" transform="translate(360,111.0344827586207)">
                <rect x="1" width="44" height="348.9655172413793"/>
                <text dy=".75em" y="6" x="22.5" textAnchor="middle">154</text>
            </g>
            <g className="bar" transform="translate(405,18.12807881773398)">
                <rect x="1" width="44" height="441.871921182266"/>
                <text dy=".75em" y="6" x="22.5" textAnchor="middle">195</text>
            </g>
            <g className="bar" transform="translate(450,0)">
                <rect x="1" width="44" height="460"/>
                <text dy=".75em" y="6" x="22.5" textAnchor="middle">203</text>
            </g>
            <g className="bar" transform="translate(495,72.51231527093597)">
                <rect x="1" width="44" height="387.48768472906403"/>
                <text dy=".75em" y="6" x="22.5" textAnchor="middle">171</text>
            </g>
            <g className="bar" transform="translate(540,287.78325123152706)">
                <rect x="1" width="44" height="172.21674876847294"/>
                <text dy=".75em" y="6" x="22.5" textAnchor="middle">76</text>
            </g>
            <g className="bar" transform="translate(585,376.1576354679803)">
                <rect x="1" width="44" height="83.8423645320197"/>
                <text dy=".75em" y="6" x="22.5" textAnchor="middle">37</text>
            </g>
            <g className="bar" transform="translate(630,446.4039408866995)">
                <rect x="1" width="44" height="13.596059113300498"/>
                <text dy=".75em" y="6" x="22.5" textAnchor="middle">6</text>
            </g>
            <g className="bar" transform="translate(675,457.73399014778323)">
                <rect x="1" width="44" height="2.2660098522167686"/>
                <text dy=".75em" y="6" x="22.5" textAnchor="middle">1</text>
            </g>
            <g className="bar" transform="translate(720,460)">
                <rect x="1" width="44" height="0"/>
                <text dy=".75em" y="6" x="22.5" textAnchor="middle">0</text>
            </g>
            <g className="bar" transform="translate(765,460)">
                <rect x="1" width="44" height="0"/>
                <text dy=".75em" y="6" x="22.5" textAnchor="middle">0</text>
            </g>
            <g className="bar" transform="translate(810,460)">
                <rect x="1" width="44" height="0"/>
                <text dy=".75em" y="6" x="22.5" textAnchor="middle">0</text>
            </g>
            <g className="bar" transform="translate(855,460)">
                <rect x="1" width="44" height="0"/>
                <text dy=".75em" y="6" x="22.5" textAnchor="middle">0</text>
            </g>
            <g className="bar" transform="translate(900,460)">
                <rect x="1" width="44" height="0"/>
                <text dy=".75em" y="6" x="22.5" textAnchor="middle">0</text>
            </g>
            <g className="axis axis--x" transform="translate(0,460)" fill="none" fontSize="10" fontFamily="sans-serif"
               textAnchor="middle">
                <path className="domain" stroke="#000" d="M0.5,6V0.5H900.5V6"/>
                <g className="tick" opacity="1" transform="translate(0,0)">
                    <line stroke="#000" y2="6" x1="0.5" x2="0.5"/>
                    <text fill="#000" y="9" x="0.5" dy="0.71em">0.0</text>
                </g>
                <g className="tick" opacity="1" transform="translate(90,0)">
                    <line stroke="#000" y2="6" x1="0.5" x2="0.5"/>
                    <text fill="#000" y="9" x="0.5" dy="0.71em">0.1</text>
                </g>
                <g className="tick" opacity="1" transform="translate(180,0)">
                    <line stroke="#000" y2="6" x1="0.5" x2="0.5"/>
                    <text fill="#000" y="9" x="0.5" dy="0.71em">0.2</text>
                </g>
                <g className="tick" opacity="1" transform="translate(270,0)">
                    <line stroke="#000" y2="6" x1="0.5" x2="0.5"/>
                    <text fill="#000" y="9" x="0.5" dy="0.71em">0.3</text>
                </g>
                <g className="tick" opacity="1" transform="translate(360,0)">
                    <line stroke="#000" y2="6" x1="0.5" x2="0.5"/>
                    <text fill="#000" y="9" x="0.5" dy="0.71em">0.4</text>
                </g>
                <g className="tick" opacity="1" transform="translate(450,0)">
                    <line stroke="#000" y2="6" x1="0.5" x2="0.5"/>
                    <text fill="#000" y="9" x="0.5" dy="0.71em">0.5</text>
                </g>
                <g className="tick" opacity="1" transform="translate(540,0)">
                    <line stroke="#000" y2="6" x1="0.5" x2="0.5"/>
                    <text fill="#000" y="9" x="0.5" dy="0.71em">0.6</text>
                </g>
                <g className="tick" opacity="1" transform="translate(630,0)">
                    <line stroke="#000" y2="6" x1="0.5" x2="0.5"/>
                    <text fill="#000" y="9" x="0.5" dy="0.71em">0.7</text>
                </g>
                <g className="tick" opacity="1" transform="translate(720,0)">
                    <line stroke="#000" y2="6" x1="0.5" x2="0.5"/>
                    <text fill="#000" y="9" x="0.5" dy="0.71em">0.8</text>
                </g>
                <g className="tick" opacity="1" transform="translate(810,0)">
                    <line stroke="#000" y2="6" x1="0.5" x2="0.5"/>
                    <text fill="#000" y="9" x="0.5" dy="0.71em">0.9</text>
                </g>
                <g className="tick" opacity="1" transform="translate(900,0)">
                    <line stroke="#000" y2="6" x1="0.5" x2="0.5"/>
                    <text fill="#000" y="9" x="0.5" dy="0.71em">1.0</text>
                </g>
            </g>
        </g>
    </svg>
);

