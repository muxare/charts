import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions/todoActions';

class Todo extends React.Component {
    render() {
        return (
            <div className="container">
                <div className="form-group">
                    <input className="form-control" ref={node => {this.input = node}} />
                    <button
                        className="input-control"
                        onClick={() => {
                            this.props.dispatch(actions.addTodo(this.input.value));
                            this.input.value = '';
                        }}
                    >
                        Add
                    </button>
                </div>
                <ul>
                    {this.props.todos.map((d, i) =>
                        <li
                            className="list-group-item"
                            key={i}
                            onClick={(e) => {
                                this.props.dispatch(actions.toggleTodo(i))
                            }}
                            style={{backgroundColor: d.completed ? "black" : ""}}
                        >
                            {d.text}
                        </li>
                    )}
                </ul>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        todos: state.todos
    };
};

export default connect(mapStateToProps)(Todo);
