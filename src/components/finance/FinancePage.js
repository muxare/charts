import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions/financeActions';
import CandleStickChart from '../common/CandleStickChart';
import Table, {Column} from '../common/Table';
import {dataRowsPayload} from '../../data/financeDataPayload.json';

class FinancePage extends React.Component {
    constructor(props) {
        super(props);

        this.handleRowClickEvent = this.handleRowClickEvent.bind(this);
    }
    handleRowClickEvent(e, row) {
        //this.props.dispatch(actions.getCompanyAnalysis(row.Ticker));
        console.log(e, row);
    }
    componentDidMount() {
        //this.props.dispatch(actions.getCompanyAnalysis("SWED-A.ST"));
        //this.props.dispatch(actions.getCompaniesAnalysis());
        //this.props.dispatch(actions.receiveCompaniesAnalysis(FinanceDataPayload))
    }
    render() {
        const {dataRows, data} = this.props;
        //const technicalIndicators = data.map();

        return (
            <div>
                <h1>Candle Stick Chart</h1>
                <Table
                    rows={dataRowsPayload}
                    onRowClick={this.handleRowClickEvent}
                >
                    <Column dataKey="Name" label="Company Name" />
                    <Column dataKey="Ticker" label="Ticker" />
                    <Column dataKey="AverageTradeVolume" label="Average Trade Vol" sortable={true} rowDataCallback={(data) => {return data;}} />
                    <Column dataKey="TechnicalIndicatorScore" label="Analysis Score" sortable={true} />
                    <Column dataKey="TechnicalIndiactors" label="Technical Indicators" sortable={true} rowDataCallback={(data) => {return Object.keys(data).filter(k => data[k]).map(k => k)}} />
                </Table>
                {/*<CandleStickChart*/}
                    {/*data={data}*/}
                {/*/>*/}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        dataRows: state.finance.companies,
        data: state.finance.chartData
    };
};

export default connect(mapStateToProps)(FinancePage);
