import React from 'react';
import { Route, Switch } from 'react-router-dom';
import ChartsPage from './charts/ChartsPage';
import Todo from './todo/TodoPage';
import Users from './users/UsersPage';
import Finance from './finance/FinancePage';

const Main = () => (
    <main className="">
        <Switch>
            <Route path="/charts" component={ChartsPage} />
            <Route path="/todo" component={Todo} />
            <Route path="/users" component={Users} />
            <Route path="/finance" component={Finance} />
        </Switch>
    </main>
);

export default Main;
