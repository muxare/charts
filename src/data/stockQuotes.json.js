export const CandleStickChartData = [
    {
        Date: "2017-03-14",
        Open: 22.57,
        High: 22.620001,
        Low: 22.51,
        Close: 22.57,
        Volume: 1664600,
        AdjClose: 22.57
    },
    {
        Date: "2017-03-13",
        Open: 22.66,
        High: 22.780001,
        Low: 22.549999,
        Close: 22.639999,
        Volume: 1629600,
        AdjClose: 22.639999
    },
    {
        Date: "2017-03-10",
        Open: 22.51,
        High: 22.66,
        Low: 22.469999,
        Close: 22.639999,
        Volume: 2832500,
        AdjClose: 22.639999
    },
    {
        Date: "2017-03-09",
        Open: 22.309999,
        High: 22.389999,
        Low: 22.24,
        Close: 22.360001,
        Volume: 1684000,
        AdjClose: 22.360001
    },
    {
        Date: "2017-03-08",
        Open: 22.57,
        High: 22.68,
        Low: 22.52,
        Close: 22.540001,
        Volume: 1581400,
        AdjClose: 22.540001
    },
    {
        Date: "2017-03-07",
        Open: 22.549999,
        High: 22.639999,
        Low: 22.49,
        Close: 22.610001,
        Volume: 1381200,
        AdjClose: 22.610001
    },
    {
        Date: "2017-03-06",
        Open: 22.58,
        High: 22.59,
        Low: 22.49,
        Close: 22.57,
        Volume: 1781900,
        AdjClose: 22.57
    },
    {
        Date: "2017-03-03",
        Open: 22.73,
        High: 22.799999,
        Low: 22.6,
        Close: 22.780001,
        Volume: 906000,
        AdjClose: 22.780001
    },
    {
        Date: "2017-03-02",
        Open: 22.780001,
        High: 22.82,
        Low: 22.67,
        Close: 22.68,
        Volume: 1127900,
        AdjClose: 22.68
    },
    {
        Date: "2017-03-01",
        Open: 22.77,
        High: 23.02,
        Low: 22.77,
        Close: 22.98,
        Volume: 1584500,
        AdjClose: 22.98
    },
    {
        Date: "2017-02-28",
        Open: 22.58,
        High: 22.690001,
        Low: 22.530001,
        Close: 22.549999,
        Volume: 2198100,
        AdjClose: 22.549999
    },
    {
        Date: "2017-02-27",
        Open: 22.440001,
        High: 22.559999,
        Low: 22.4,
        Close: 22.5,
        Volume: 1951600,
        AdjClose: 22.5
    },
    {
        Date: "2017-02-24",
        Open: 22.389999,
        High: 22.57,
        Low: 22.370001,
        Close: 22.549999,
        Volume: 1411500,
        AdjClose: 22.549999
    },
    {
        Date: "2017-02-23",
        Open: 22.889999,
        High: 22.92,
        Low: 22.75,
        Close: 22.780001,
        Volume: 2524700,
        AdjClose: 22.780001
    },
    {
        Date: "2017-02-22",
        Open: 22.83,
        High: 22.91,
        Low: 22.73,
        Close: 22.82,
        Volume: 2328100,
        AdjClose: 22.82
    },
    {
        Date: "2017-02-21",
        Open: 22.799999,
        High: 23.01,
        Low: 22.790001,
        Close: 22.969999,
        Volume: 1849100,
        AdjClose: 22.969999
    },
    {
        Date: "2017-02-17",
        Open: 22.74,
        High: 22.83,
        Low: 22.690001,
        Close: 22.82,
        Volume: 1859700,
        AdjClose: 22.82
    },
    {
        Date: "2017-02-16",
        Open: 22.959999,
        High: 23.030001,
        Low: 22.940001,
        Close: 23.01,
        Volume: 1159500,
        AdjClose: 23.01
    },
    {
        Date: "2017-02-15",
        Open: 22.68,
        High: 22.940001,
        Low: 22.68,
        Close: 22.93,
        Volume: 1987500,
        AdjClose: 22.93
    },
    {
        Date: "2017-02-14",
        Open: 22.92,
        High: 22.92,
        Low: 22.709999,
        Close: 22.82,
        Volume: 1221900,
        AdjClose: 22.82
    },
    {
        Date: "2017-02-13",
        Open: 22.940001,
        High: 22.99,
        Low: 22.860001,
        Close: 22.940001,
        Volume: 2421400,
        AdjClose: 22.940001
    },
    {
        Date: "2017-02-10",
        Open: 22.889999,
        High: 22.959999,
        Low: 22.559999,
        Close: 22.879999,
        Volume: 4645700,
        AdjClose: 22.879999
    },
    {
        Date: "2017-02-09",
        Open: 22.610001,
        High: 22.879999,
        Low: 22.610001,
        Close: 22.77,
        Volume: 2047200,
        AdjClose: 22.77
    },
    {
        Date: "2017-02-08",
        Open: 22.709999,
        High: 22.719999,
        Low: 22.57,
        Close: 22.610001,
        Volume: 3654600,
        AdjClose: 22.610001
    },
    {
        Date: "2017-02-07",
        Open: 23.43,
        High: 23.52,
        Low: 23.389999,
        Close: 23.450001,
        Volume: 1698000,
        AdjClose: 23.450001
    },
    {
        Date: "2017-02-06",
        Open: 23.33,
        High: 23.4,
        Low: 23.27,
        Close: 23.370001,
        Volume: 1818300,
        AdjClose: 23.370001
    },
    {
        Date: "2017-02-03",
        Open: 23.58,
        High: 23.700001,
        Low: 23.559999,
        Close: 23.629999,
        Volume: 2991500,
        AdjClose: 23.629999
    },
    {
        Date: "2017-02-02",
        Open: 23.690001,
        High: 23.73,
        Low: 23.629999,
        Close: 23.67,
        Volume: 1530100,
        AdjClose: 23.67
    },
    {
        Date: "2017-02-01",
        Open: 23.82,
        High: 23.879999,
        Low: 23.620001,
        Close: 23.75,
        Volume: 1640500,
        AdjClose: 23.75
    },
    {
        Date: "2017-01-31",
        Open: 23.799999,
        High: 23.85,
        Low: 23.65,
        Close: 23.82,
        Volume: 3540700,
        AdjClose: 23.82
    },
    {
        Date: "2017-01-30",
        Open: 23.629999,
        High: 23.809999,
        Low: 23.58,
        Close: 23.799999,
        Volume: 2626100,
        AdjClose: 23.799999
    },
    {
        Date: "2017-01-27",
        Open: 23.82,
        High: 23.860001,
        Low: 23.74,
        Close: 23.83,
        Volume: 4281100,
        AdjClose: 23.83
    },
    {
        Date: "2017-01-26",
        Open: 23.85,
        High: 23.9,
        Low: 23.68,
        Close: 23.799999,
        Volume: 5779600,
        AdjClose: 23.799999
    },
    {
        Date: "2017-01-25",
        Open: 23.41,
        High: 23.58,
        Low: 23.389999,
        Close: 23.5,
        Volume: 2191400,
        AdjClose: 23.5
    },
    {
        Date: "2017-01-24",
        Open: 22.870001,
        High: 23.01,
        Low: 22.860001,
        Close: 22.950001,
        Volume: 1745200,
        AdjClose: 22.950001
    },
    {
        Date: "2017-01-23",
        Open: 22.74,
        High: 22.809999,
        Low: 22.629999,
        Close: 22.780001,
        Volume: 1621900,
        AdjClose: 22.780001
    },
    {
        Date: "2017-01-20",
        Open: 22.73,
        High: 22.799999,
        Low: 22.690001,
        Close: 22.780001,
        Volume: 1367500,
        AdjClose: 22.780001
    },
    {
        Date: "2017-01-19",
        Open: 22.48,
        High: 22.58,
        Low: 22.389999,
        Close: 22.559999,
        Volume: 1619800,
        AdjClose: 22.559999
    },
    {
        Date: "2017-01-18",
        Open: 22.34,
        High: 22.469999,
        Low: 22.33,
        Close: 22.41,
        Volume: 2777000,
        AdjClose: 22.41
    },
    {
        Date: "2017-01-17",
        Open: 22.389999,
        High: 22.49,
        Low: 22.309999,
        Close: 22.33,
        Volume: 7203800,
        AdjClose: 22.33
    },
    {
        Date: "2017-01-13",
        Open: 22.459999,
        High: 22.51,
        Low: 22.370001,
        Close: 22.48,
        Volume: 2241900,
        AdjClose: 22.48
    },
    {
        Date: "2017-01-12",
        Open: 22.290001,
        High: 22.33,
        Low: 22.15,
        Close: 22.26,
        Volume: 3055100,
        AdjClose: 22.26
    },
    {
        Date: "2017-01-11",
        Open: 22.1,
        High: 22.32,
        Low: 22.08,
        Close: 22.280001,
        Volume: 3608300,
        AdjClose: 22.280001
    },
    {
        Date: "2017-01-10",
        Open: 21.68,
        High: 22.129999,
        Low: 21.68,
        Close: 22.1,
        Volume: 3251500,
        AdjClose: 22.1
    },
    {
        Date: "2017-01-09",
        Open: 21.41,
        High: 21.549999,
        Low: 21.379999,
        Close: 21.5,
        Volume: 1359100,
        AdjClose: 21.5
    },
    {
        Date: "2017-01-06",
        Open: 21.620001,
        High: 21.68,
        Low: 21.559999,
        Close: 21.6,
        Volume: 1115500,
        AdjClose: 21.6
    },
    {
        Date: "2017-01-05",
        Open: 21.35,
        High: 21.58,
        Low: 21.35,
        Close: 21.559999,
        Volume: 1515100,
        AdjClose: 21.559999
    },
    {
        Date: "2017-01-04",
        Open: 21.15,
        High: 21.379999,
        Low: 21.139999,
        Close: 21.370001,
        Volume: 2329600,
        AdjClose: 21.370001
    },
    {
        Date: "2017-01-03",
        Open: 21.139999,
        High: 21.280001,
        Low: 21.110001,
        Close: 21.280001,
        Volume: 2009000,
        AdjClose: 21.280001
    },
    {
        Date: "2016-12-30",
        Open: 21.26,
        High: 21.27,
        Low: 21.040001,
        Close: 21.07,
        Volume: 1386900,
        AdjClose: 21.07
    },
    {
        Date: "2016-12-29",
        Open: 21.139999,
        High: 21.17,
        Low: 21.07,
        Close: 21.08,
        Volume: 1472900,
        AdjClose: 21.08
    },
    {
        Date: "2016-12-28",
        Open: 21.23,
        High: 21.23,
        Low: 21.01,
        Close: 21.040001,
        Volume: 1310200,
        AdjClose: 21.040001
    },
    {
        Date: "2016-12-27",
        Open: 21.040001,
        High: 21.129999,
        Low: 21.02,
        Close: 21.08,
        Volume: 675500,
        AdjClose: 21.08
    },
    {
        Date: "2016-12-23",
        Open: 21.1,
        High: 21.129999,
        Low: 21.030001,
        Close: 21.040001,
        Volume: 1019100,
        AdjClose: 21.040001
    },
    {
        Date: "2016-12-22",
        Open: 21.120001,
        High: 21.23,
        Low: 21.08,
        Close: 21.190001,
        Volume: 3475200,
        AdjClose: 21.190001
    },
    {
        Date: "2016-12-21",
        Open: 21.18,
        High: 21.18,
        Low: 21,
        Close: 21.049999,
        Volume: 3108200,
        AdjClose: 21.049999
    },
    {
        Date: "2016-12-20",
        Open: 21.07,
        High: 21.18,
        Low: 21.059999,
        Close: 21.18,
        Volume: 4064700,
        AdjClose: 21.18
    },
    {
        Date: "2016-12-19",
        Open: 21.049999,
        High: 21.139999,
        Low: 20.98,
        Close: 21.139999,
        Volume: 2806600,
        AdjClose: 21.139999
    },
    {
        Date: "2016-12-16",
        Open: 20.93,
        High: 21.110001,
        Low: 20.93,
        Close: 20.959999,
        Volume: 1206800,
        AdjClose: 20.959999
    },
    {
        Date: "2016-12-15",
        Open: 20.860001,
        High: 20.98,
        Low: 20.799999,
        Close: 20.969999,
        Volume: 1950600,
        AdjClose: 20.969999
    },
    {
        Date: "2016-12-14",
        Open: 21.360001,
        High: 21.450001,
        Low: 20.93,
        Close: 20.950001,
        Volume: 3610900,
        AdjClose: 20.950001
    },
    {
        Date: "2016-12-13",
        Open: 21.200001,
        High: 21.379999,
        Low: 21.120001,
        Close: 21.280001,
        Volume: 2516600,
        AdjClose: 21.280001
    },
    {
        Date: "2016-12-12",
        Open: 21.200001,
        High: 21.35,
        Low: 21.17,
        Close: 21.26,
        Volume: 4889000,
        AdjClose: 21.26
    },
    {
        Date: "2016-12-09",
        Open: 20.870001,
        High: 21.030001,
        Low: 20.870001,
        Close: 20.98,
        Volume: 1090000,
        AdjClose: 20.98
    },
    {
        Date: "2016-12-08",
        Open: 20.889999,
        High: 21.01,
        Low: 20.860001,
        Close: 20.92,
        Volume: 1561000,
        AdjClose: 20.92
    },
    {
        Date: "2016-12-07",
        Open: 20.889999,
        High: 21.139999,
        Low: 20.860001,
        Close: 21.1,
        Volume: 2104000,
        AdjClose: 21.1
    },
    {
        Date: "2016-12-06",
        Open: 20.889999,
        High: 21.07,
        Low: 20.879999,
        Close: 21.040001,
        Volume: 1335300,
        AdjClose: 21.040001
    },
    {
        Date: "2016-12-05",
        Open: 21.040001,
        High: 21.09,
        Low: 20.959999,
        Close: 21.07,
        Volume: 1430500,
        AdjClose: 21.07
    },
    {
        Date: "2016-12-02",
        Open: 20.629999,
        High: 20.73,
        Low: 20.540001,
        Close: 20.610001,
        Volume: 1420500,
        AdjClose: 20.610001
    },
    {
        Date: "2016-12-01",
        Open: 20.450001,
        High: 20.709999,
        Low: 20.43,
        Close: 20.59,
        Volume: 1872900,
        AdjClose: 20.59
    },
    {
        Date: "2016-11-30",
        Open: 20.41,
        High: 20.49,
        Low: 20.34,
        Close: 20.43,
        Volume: 3131500,
        AdjClose: 20.43
    },
    {
        Date: "2016-11-29",
        Open: 20.280001,
        High: 20.48,
        Low: 20.26,
        Close: 20.379999,
        Volume: 889100,
        AdjClose: 20.379999
    },
    {
        Date: "2016-11-28",
        Open: 20.48,
        High: 20.530001,
        Low: 20.389999,
        Close: 20.4,
        Volume: 1053000,
        AdjClose: 20.4
    },
    {
        Date: "2016-11-25",
        Open: 20.610001,
        High: 20.709999,
        Low: 20.6,
        Close: 20.700001,
        Volume: 678500,
        AdjClose: 20.700001
    },
    {
        Date: "2016-11-23",
        Open: 20.5,
        High: 20.620001,
        Low: 20.48,
        Close: 20.59,
        Volume: 1699800,
        AdjClose: 20.59
    },
    {
        Date: "2016-11-22",
        Open: 20.77,
        High: 20.780001,
        Low: 20.639999,
        Close: 20.75,
        Volume: 1904300,
        AdjClose: 20.75
    },
    {
        Date: "2016-11-21",
        Open: 20.66,
        High: 20.75,
        Low: 20.629999,
        Close: 20.68,
        Volume: 1699800,
        AdjClose: 20.68
    },
    {
        Date: "2016-11-18",
        Open: 20.629999,
        High: 20.74,
        Low: 20.610001,
        Close: 20.690001,
        Volume: 1775100,
        AdjClose: 20.690001
    },
    {
        Date: "2016-11-17",
        Open: 20.940001,
        High: 21,
        Low: 20.860001,
        Close: 20.9,
        Volume: 1174100,
        AdjClose: 20.9
    },
    {
        Date: "2016-11-16",
        Open: 21.01,
        High: 21.08,
        Low: 20.870001,
        Close: 20.879999,
        Volume: 1388900,
        AdjClose: 20.879999
    },
    {
        Date: "2016-11-15",
        Open: 20.92,
        High: 21.219999,
        Low: 20.91,
        Close: 21.219999,
        Volume: 2533600,
        AdjClose: 21.219999
    },
    {
        Date: "2016-11-14",
        Open: 21.07,
        High: 21.18,
        Low: 21.02,
        Close: 21.16,
        Volume: 1901500,
        AdjClose: 21.16
    },
    {
        Date: "2016-11-11",
        Open: 21.4,
        High: 21.43,
        Low: 21.16,
        Close: 21.379999,
        Volume: 2079200,
        AdjClose: 21.379999
    },
    {
        Date: "2016-11-10",
        Open: 21.190001,
        High: 21.41,
        Low: 21.09,
        Close: 21.360001,
        Volume: 3434600,
        AdjClose: 21.360001
    },
    {
        Date: "2016-11-09",
        Open: 21.09,
        High: 21.370001,
        Low: 21,
        Close: 21.27,
        Volume: 3151400,
        AdjClose: 21.27
    },
    {
        Date: "2016-11-08",
        Open: 20.65,
        High: 20.92,
        Low: 20.65,
        Close: 20.9,
        Volume: 1856700,
        AdjClose: 20.9
    },
    {
        Date: "2016-11-07",
        Open: 20.85,
        High: 20.9,
        Low: 20.799999,
        Close: 20.889999,
        Volume: 1262000,
        AdjClose: 20.889999
    },
    {
        Date: "2016-11-04",
        Open: 20.41,
        High: 20.57,
        Low: 20.309999,
        Close: 20.4,
        Volume: 1621000,
        AdjClose: 20.4
    },
    {
        Date: "2016-11-03",
        Open: 20.6,
        High: 20.6,
        Low: 20.4,
        Close: 20.43,
        Volume: 1478300,
        AdjClose: 20.43
    },
    {
        Date: "2016-11-02",
        Open: 20.459999,
        High: 20.58,
        Low: 20.440001,
        Close: 20.450001,
        Volume: 1426700,
        AdjClose: 20.450001
    },
    {
        Date: "2016-11-01",
        Open: 20.629999,
        High: 20.639999,
        Low: 20.450001,
        Close: 20.52,
        Volume: 2001600,
        AdjClose: 20.52
    },
    {
        Date: "2016-10-31",
        Open: 20.6,
        High: 20.67,
        Low: 20.549999,
        Close: 20.65,
        Volume: 2119200,
        AdjClose: 20.65
    },
    {
        Date: "2016-10-28",
        Open: 20.49,
        High: 20.629999,
        Low: 20.459999,
        Close: 20.620001,
        Volume: 3182300,
        AdjClose: 20.620001
    },
    {
        Date: "2016-10-27",
        Open: 20.700001,
        High: 20.75,
        Low: 20.49,
        Close: 20.530001,
        Volume: 5638900,
        AdjClose: 20.530001
    },
    {
        Date: "2016-10-26",
        Open: 22.219999,
        High: 22.23,
        Low: 22.08,
        Close: 22.08,
        Volume: 1548200,
        AdjClose: 22.08
    },
    {
        Date: "2016-10-25",
        Open: 22.129999,
        High: 22.17,
        Low: 22.059999,
        Close: 22.120001,
        Volume: 1714900,
        AdjClose: 22.120001
    },
    {
        Date: "2016-10-24",
        Open: 22.32,
        High: 22.389999,
        Low: 22.209999,
        Close: 22.290001,
        Volume: 1274800,
        AdjClose: 22.290001
    },
    {
        Date: "2016-10-21",
        Open: 22.209999,
        High: 22.360001,
        Low: 22.200001,
        Close: 22.35,
        Volume: 1717000,
        AdjClose: 22.35
    },
    {
        Date: "2016-10-20",
        Open: 22.33,
        High: 22.450001,
        Low: 22.290001,
        Close: 22.370001,
        Volume: 1402900,
        AdjClose: 22.370001
    },
    {
        Date: "2016-10-19",
        Open: 22.41,
        High: 22.52,
        Low: 22.4,
        Close: 22.450001,
        Volume: 2931200,
        AdjClose: 22.450001
    },
    {
        Date: "2016-10-18",
        Open: 22.4,
        High: 22.5,
        Low: 22.32,
        Close: 22.450001,
        Volume: 1597200,
        AdjClose: 22.450001
    },
    {
        Date: "2016-10-17",
        Open: 22.33,
        High: 22.42,
        Low: 22.290001,
        Close: 22.33,
        Volume: 1019000,
        AdjClose: 22.33
    },
    {
        Date: "2016-10-14",
        Open: 22.41,
        High: 22.5,
        Low: 22.33,
        Close: 22.34,
        Volume: 1110600,
        AdjClose: 22.34
    },
    {
        Date: "2016-10-13",
        Open: 22.09,
        High: 22.33,
        Low: 22.049999,
        Close: 22.25,
        Volume: 1531300,
        AdjClose: 22.25
    },
    {
        Date: "2016-10-12",
        Open: 22.379999,
        High: 22.440001,
        Low: 22.290001,
        Close: 22.389999,
        Volume: 996500,
        AdjClose: 22.389999
    },
    {
        Date: "2016-10-11",
        Open: 22.709999,
        High: 22.719999,
        Low: 22.299999,
        Close: 22.370001,
        Volume: 2284500,
        AdjClose: 22.370001
    },
    {
        Date: "2016-10-10",
        Open: 22.799999,
        High: 22.870001,
        Low: 22.77,
        Close: 22.780001,
        Volume: 1075200,
        AdjClose: 22.780001
    },
    {
        Date: "2016-10-07",
        Open: 22.879999,
        High: 22.889999,
        Low: 22.58,
        Close: 22.719999,
        Volume: 1784100,
        AdjClose: 22.719999
    },
    {
        Date: "2016-10-06",
        Open: 22.879999,
        High: 22.92,
        Low: 22.790001,
        Close: 22.879999,
        Volume: 1264000,
        AdjClose: 22.879999
    },
    {
        Date: "2016-10-05",
        Open: 22.66,
        High: 22.790001,
        Low: 22.59,
        Close: 22.76,
        Volume: 1265000,
        AdjClose: 22.76
    },
    {
        Date: "2016-10-04",
        Open: 22.620001,
        High: 22.74,
        Low: 22.5,
        Close: 22.57,
        Volume: 1926800,
        AdjClose: 22.57
    },
    {
        Date: "2016-10-03",
        Open: 22.58,
        High: 22.66,
        Low: 22.48,
        Close: 22.52,
        Volume: 1491500,
        AdjClose: 22.52
    },
    {
        Date: "2016-09-30",
        Open: 22.280001,
        High: 22.549999,
        Low: 22.27,
        Close: 22.51,
        Volume: 2473400,
        AdjClose: 22.51
    },
    {
        Date: "2016-09-29",
        Open: 22.49,
        High: 22.6,
        Low: 22.290001,
        Close: 22.379999,
        Volume: 2525500,
        AdjClose: 22.379999
    },
    {
        Date: "2016-09-28",
        Open: 22.49,
        High: 22.709999,
        Low: 22.43,
        Close: 22.700001,
        Volume: 1634300,
        AdjClose: 22.700001
    },
    {
        Date: "2016-09-27",
        Open: 22.309999,
        High: 22.530001,
        Low: 22.280001,
        Close: 22.49,
        Volume: 1232700,
        AdjClose: 22.49
    },
    {
        Date: "2016-09-26",
        Open: 22.6,
        High: 22.610001,
        Low: 22.49,
        Close: 22.5,
        Volume: 1202400,
        AdjClose: 22.5
    },
    {
        Date: "2016-09-23",
        Open: 22.690001,
        High: 22.790001,
        Low: 22.620001,
        Close: 22.65,
        Volume: 1936800,
        AdjClose: 22.65
    },
    {
        Date: "2016-09-22",
        Open: 22.85,
        High: 22.879999,
        Low: 22.75,
        Close: 22.809999,
        Volume: 1123300,
        AdjClose: 22.809999
    },
    {
        Date: "2016-09-21",
        Open: 22.389999,
        High: 22.52,
        Low: 22.23,
        Close: 22.51,
        Volume: 1450300,
        AdjClose: 22.51
    },
    {
        Date: "2016-09-20",
        Open: 22.309999,
        High: 22.309999,
        Low: 22.120001,
        Close: 22.129999,
        Volume: 1306900,
        AdjClose: 22.129999
    },
    {
        Date: "2016-09-19",
        Open: 21.99,
        High: 22.120001,
        Low: 21.950001,
        Close: 21.98,
        Volume: 1473900,
        AdjClose: 21.98
    },
    {
        Date: "2016-09-16",
        Open: 22.01,
        High: 22.059999,
        Low: 21.870001,
        Close: 21.950001,
        Volume: 2274400,
        AdjClose: 21.950001
    },
    {
        Date: "2016-09-15",
        Open: 22.049999,
        High: 22.309999,
        Low: 22,
        Close: 22.25,
        Volume: 1991600,
        AdjClose: 22.25
    },
    {
        Date: "2016-09-14",
        Open: 22.280001,
        High: 22.33,
        Low: 22.02,
        Close: 22.07,
        Volume: 4370800,
        AdjClose: 22.07
    },
    {
        Date: "2016-09-13",
        Open: 22.41,
        High: 22.48,
        Low: 22.120001,
        Close: 22.24,
        Volume: 2838700,
        AdjClose: 22.24
    },
    {
        Date: "2016-09-12",
        Open: 22.02,
        High: 22.440001,
        Low: 21.99,
        Close: 22.389999,
        Volume: 1940900,
        AdjClose: 22.389999
    },
    {
        Date: "2016-09-09",
        Open: 22.57,
        High: 22.58,
        Low: 22.17,
        Close: 22.17,
        Volume: 1837700,
        AdjClose: 22.17
    },
    {
        Date: "2016-09-08",
        Open: 22.620001,
        High: 22.780001,
        Low: 22.610001,
        Close: 22.65,
        Volume: 1836700,
        AdjClose: 22.65
    },
    {
        Date: "2016-09-07",
        Open: 22.629999,
        High: 22.68,
        Low: 22.540001,
        Close: 22.6,
        Volume: 1111600,
        AdjClose: 22.6
    },
    {
        Date: "2016-09-06",
        Open: 22.459999,
        High: 22.59,
        Low: 22.34,
        Close: 22.540001,
        Volume: 3144100,
        AdjClose: 22.540001
    },
    {
        Date: "2016-09-02",
        Open: 22.15,
        High: 22.25,
        Low: 22.09,
        Close: 22.209999,
        Volume: 1923000,
        AdjClose: 22.209999
    },
    {
        Date: "2016-09-01",
        Open: 21.639999,
        High: 22.1,
        Low: 21.59,
        Close: 22.07,
        Volume: 2812300,
        AdjClose: 22.07
    },
    {
        Date: "2016-08-31",
        Open: 21.719999,
        High: 21.77,
        Low: 21.6,
        Close: 21.67,
        Volume: 940800,
        AdjClose: 21.67
    },
    {
        Date: "2016-08-30",
        Open: 21.879999,
        High: 21.91,
        Low: 21.709999,
        Close: 21.76,
        Volume: 987200,
        AdjClose: 21.76
    },
    {
        Date: "2016-08-29",
        Open: 21.809999,
        High: 21.940001,
        Low: 21.799999,
        Close: 21.91,
        Volume: 931000,
        AdjClose: 21.91
    },
    {
        Date: "2016-08-26",
        Open: 22.030001,
        High: 22.26,
        Low: 21.76,
        Close: 21.84,
        Volume: 1455400,
        AdjClose: 21.84
    },
    {
        Date: "2016-08-25",
        Open: 21.93,
        High: 22,
        Low: 21.93,
        Close: 21.940001,
        Volume: 643600,
        AdjClose: 21.940001
    },
    {
        Date: "2016-08-24",
        Open: 22.049999,
        High: 22.08,
        Low: 21.959999,
        Close: 21.99,
        Volume: 585700,
        AdjClose: 21.99
    },
    {
        Date: "2016-08-23",
        Open: 22.129999,
        High: 22.190001,
        Low: 22.059999,
        Close: 22.07,
        Volume: 846100,
        AdjClose: 22.07
    },
    {
        Date: "2016-08-22",
        Open: 21.92,
        High: 22.01,
        Low: 21.870001,
        Close: 21.950001,
        Volume: 1089500,
        AdjClose: 21.950001
    },
    {
        Date: "2016-08-19",
        Open: 21.99,
        High: 22.09,
        Low: 21.9,
        Close: 22.059999,
        Volume: 1240600,
        AdjClose: 22.059999
    },
    {
        Date: "2016-08-18",
        Open: 22.23,
        High: 22.360001,
        Low: 22.209999,
        Close: 22.360001,
        Volume: 1122200,
        AdjClose: 22.360001
    },
    {
        Date: "2016-08-17",
        Open: 22.049999,
        High: 22.1,
        Low: 21.91,
        Close: 22.059999,
        Volume: 1413300,
        AdjClose: 22.059999
    },
    {
        Date: "2016-08-16",
        Open: 22.07,
        High: 22.16,
        Low: 22.030001,
        Close: 22.049999,
        Volume: 1156700,
        AdjClose: 22.049999
    },
    {
        Date: "2016-08-15",
        Open: 22.049999,
        High: 22.16,
        Low: 22.049999,
        Close: 22.129999,
        Volume: 1253900,
        AdjClose: 22.129999
    },
    {
        Date: "2016-08-12",
        Open: 22.120001,
        High: 22.15,
        Low: 22.02,
        Close: 22.07,
        Volume: 1067900,
        AdjClose: 22.07
    },
    {
        Date: "2016-08-11",
        Open: 22.02,
        High: 22.08,
        Low: 21.950001,
        Close: 22.02,
        Volume: 1074100,
        AdjClose: 22.02
    },
    {
        Date: "2016-08-10",
        Open: 21.889999,
        High: 21.940001,
        Low: 21.84,
        Close: 21.860001,
        Volume: 1715100,
        AdjClose: 21.860001
    },
    {
        Date: "2016-08-09",
        Open: 21.549999,
        High: 21.639999,
        Low: 21.52,
        Close: 21.559999,
        Volume: 862900,
        AdjClose: 21.559999
    },
    {
        Date: "2016-08-08",
        Open: 21.33,
        High: 21.41,
        Low: 21.32,
        Close: 21.389999,
        Volume: 1067500,
        AdjClose: 21.389999
    },
    {
        Date: "2016-08-05",
        Open: 21.190001,
        High: 21.32,
        Low: 21.190001,
        Close: 21.299999,
        Volume: 1034600,
        AdjClose: 21.299999
    },
    {
        Date: "2016-08-04",
        Open: 21.18,
        High: 21.299999,
        Low: 21.139999,
        Close: 21.23,
        Volume: 898200,
        AdjClose: 21.23
    },
    {
        Date: "2016-08-03",
        Open: 20.950001,
        High: 21.02,
        Low: 20.940001,
        Close: 21,
        Volume: 1250300,
        AdjClose: 21
    },
    {
        Date: "2016-08-02",
        Open: 21.209999,
        High: 21.209999,
        Low: 21,
        Close: 21.08,
        Volume: 1542900,
        AdjClose: 21.08
    },
    {
        Date: "2016-08-01",
        Open: 21.02,
        High: 21.16,
        Low: 20.950001,
        Close: 21.07,
        Volume: 1180800,
        AdjClose: 21.07
    },
    {
        Date: "2016-07-29",
        Open: 21.24,
        High: 21.379999,
        Low: 21.17,
        Close: 21.25,
        Volume: 1697200,
        AdjClose: 21.25
    },
    {
        Date: "2016-07-28",
        Open: 21.040001,
        High: 21.120001,
        Low: 20.98,
        Close: 21.07,
        Volume: 1853500,
        AdjClose: 21.07
    },
    {
        Date: "2016-07-27",
        Open: 20.91,
        High: 20.99,
        Low: 20.799999,
        Close: 20.940001,
        Volume: 1601800,
        AdjClose: 20.940001
    },
    {
        Date: "2016-07-26",
        Open: 20.84,
        High: 20.99,
        Low: 20.780001,
        Close: 20.969999,
        Volume: 1391600,
        AdjClose: 20.969999
    },
    {
        Date: "2016-07-25",
        Open: 20.860001,
        High: 20.879999,
        Low: 20.75,
        Close: 20.84,
        Volume: 1120100,
        AdjClose: 20.84
    },
    {
        Date: "2016-07-22",
        Open: 20.950001,
        High: 20.959999,
        Low: 20.74,
        Close: 20.91,
        Volume: 2299600,
        AdjClose: 20.91
    },
    {
        Date: "2016-07-21",
        Open: 20.65,
        High: 20.860001,
        Low: 20.610001,
        Close: 20.700001,
        Volume: 2994700,
        AdjClose: 20.700001
    },
    {
        Date: "2016-07-20",
        Open: 20.200001,
        High: 20.35,
        Low: 20.110001,
        Close: 20.24,
        Volume: 1843100,
        AdjClose: 20.24
    },
    {
        Date: "2016-07-19",
        Open: 20,
        High: 20.18,
        Low: 19.950001,
        Close: 20.110001,
        Volume: 2072700,
        AdjClose: 20.110001
    },
    {
        Date: "2016-07-18",
        Open: 19.969999,
        High: 20.049999,
        Low: 19.9,
        Close: 19.93,
        Volume: 1567000,
        AdjClose: 19.93
    },
    {
        Date: "2016-07-15",
        Open: 19.950001,
        High: 20.02,
        Low: 19.870001,
        Close: 20,
        Volume: 1588100,
        AdjClose: 20
    },
    {
        Date: "2016-07-14",
        Open: 20,
        High: 20.09,
        Low: 19.91,
        Close: 19.950001,
        Volume: 3024600,
        AdjClose: 19.950001
    },
    {
        Date: "2016-07-13",
        Open: 19.690001,
        High: 19.719999,
        Low: 19.559999,
        Close: 19.629999,
        Volume: 1352200,
        AdjClose: 19.629999
    },
    {
        Date: "2016-07-12",
        Open: 19.51,
        High: 19.59,
        Low: 19.49,
        Close: 19.530001,
        Volume: 2246400,
        AdjClose: 19.530001
    },
    {
        Date: "2016-07-11",
        Open: 19.370001,
        High: 19.459999,
        Low: 19.34,
        Close: 19.360001,
        Volume: 1746100,
        AdjClose: 19.360001
    },
    {
        Date: "2016-07-08",
        Open: 18.75,
        High: 18.93,
        Low: 18.719999,
        Close: 18.9,
        Volume: 1528000,
        AdjClose: 18.9
    },
    {
        Date: "2016-07-07",
        Open: 19.440001,
        High: 19.549999,
        Low: 19.290001,
        Close: 19.370001,
        Volume: 1740100,
        AdjClose: 18.608001
    },
    {
        Date: "2016-07-06",
        Open: 19.16,
        High: 19.360001,
        Low: 19.02,
        Close: 19.34,
        Volume: 2143800,
        AdjClose: 18.57918
    },
    {
        Date: "2016-07-05",
        Open: 19.5,
        High: 19.540001,
        Low: 19.26,
        Close: 19.309999,
        Volume: 1575400,
        AdjClose: 18.55036
    },
    {
        Date: "2016-07-01",
        Open: 19.860001,
        High: 19.950001,
        Low: 19.84,
        Close: 19.91,
        Volume: 1714600,
        AdjClose: 19.126757
    },
    {
        Date: "2016-06-30",
        Open: 19.469999,
        High: 19.84,
        Low: 19.450001,
        Close: 19.83,
        Volume: 2444400,
        AdjClose: 19.049904
    },
    {
        Date: "2016-06-29",
        Open: 19.26,
        High: 19.49,
        Low: 19.23,
        Close: 19.440001,
        Volume: 2341700,
        AdjClose: 18.675247
    },
    {
        Date: "2016-06-28",
        Open: 19.309999,
        High: 19.4,
        Low: 19.139999,
        Close: 19.33,
        Volume: 2701300,
        AdjClose: 18.569573
    },
    {
        Date: "2016-06-27",
        Open: 19.530001,
        High: 19.530001,
        Low: 19.08,
        Close: 19.290001,
        Volume: 2879100,
        AdjClose: 18.531148
    },
    {
        Date: "2016-06-24",
        Open: 20,
        High: 20.389999,
        Low: 19.84,
        Close: 19.879999,
        Volume: 4213000,
        AdjClose: 19.097936
    },
    {
        Date: "2016-06-23",
        Open: 21.540001,
        High: 21.639999,
        Low: 21.389999,
        Close: 21.620001,
        Volume: 1349100,
        AdjClose: 20.769488
    },
    {
        Date: "2016-06-22",
        Open: 21.360001,
        High: 21.42,
        Low: 21.24,
        Close: 21.26,
        Volume: 1328500,
        AdjClose: 20.423649
    },
    {
        Date: "2016-06-21",
        Open: 21.200001,
        High: 21.360001,
        Low: 21.07,
        Close: 21.290001,
        Volume: 1651100,
        AdjClose: 20.45247
    },
    {
        Date: "2016-06-20",
        Open: 20.98,
        High: 21.08,
        Low: 20.92,
        Close: 20.950001,
        Volume: 1066500,
        AdjClose: 20.125845
    },
    {
        Date: "2016-06-17",
        Open: 20.5,
        High: 20.6,
        Low: 20.389999,
        Close: 20.549999,
        Volume: 2582600,
        AdjClose: 19.741579
    },
    {
        Date: "2016-06-16",
        Open: 20.110001,
        High: 20.35,
        Low: 19.889999,
        Close: 20.309999,
        Volume: 2029800,
        AdjClose: 19.511021
    },
    {
        Date: "2016-06-15",
        Open: 20.4,
        High: 20.549999,
        Low: 20.299999,
        Close: 20.33,
        Volume: 2056700,
        AdjClose: 19.530234
    },
    {
        Date: "2016-06-14",
        Open: 20.190001,
        High: 20.24,
        Low: 20.07,
        Close: 20.18,
        Volume: 2289300,
        AdjClose: 19.386136
    },
    {
        Date: "2016-06-13",
        Open: 20.25,
        High: 20.41,
        Low: 20.23,
        Close: 20.27,
        Volume: 2563500,
        AdjClose: 19.472595
    },
    {
        Date: "2016-06-10",
        Open: 20.780001,
        High: 20.85,
        Low: 20.59,
        Close: 20.68,
        Volume: 1662000,
        AdjClose: 19.866466
    },
    {
        Date: "2016-06-09",
        Open: 21.030001,
        High: 21.16,
        Low: 21.030001,
        Close: 21.139999,
        Volume: 1805600,
        AdjClose: 20.308369
    },
    {
        Date: "2016-06-08",
        Open: 21.540001,
        High: 21.57,
        Low: 21.43,
        Close: 21.469999,
        Volume: 1293100,
        AdjClose: 20.625387
    },
    {
        Date: "2016-06-07",
        Open: 21.5,
        High: 21.559999,
        Low: 21.469999,
        Close: 21.469999,
        Volume: 1773200,
        AdjClose: 20.625387
    },
    {
        Date: "2016-06-06",
        Open: 21.200001,
        High: 21.309999,
        Low: 21.120001,
        Close: 21.219999,
        Volume: 1671400,
        AdjClose: 20.385222
    },
    {
        Date: "2016-06-03",
        Open: 20.940001,
        High: 20.99,
        Low: 20.84,
        Close: 20.950001,
        Volume: 1162600,
        AdjClose: 20.125845
    },
    {
        Date: "2016-06-02",
        Open: 20.76,
        High: 20.860001,
        Low: 20.709999,
        Close: 20.84,
        Volume: 1491700,
        AdjClose: 20.020172
    },
    {
        Date: "2016-06-01",
        Open: 20.73,
        High: 20.879999,
        Low: 20.67,
        Close: 20.879999,
        Volume: 1130500,
        AdjClose: 20.058597
    },
    {
        Date: "2016-05-31",
        Open: 20.870001,
        High: 20.99,
        Low: 20.700001,
        Close: 20.780001,
        Volume: 2493200,
        AdjClose: 19.962532
    },
    {
        Date: "2016-05-27",
        Open: 21.040001,
        High: 21.09,
        Low: 20.959999,
        Close: 20.99,
        Volume: 1070400,
        AdjClose: 20.16427
    },
    {
        Date: "2016-05-26",
        Open: 21.030001,
        High: 21.07,
        Low: 20.9,
        Close: 20.92,
        Volume: 1232900,
        AdjClose: 20.097024
    },
    {
        Date: "2016-05-25",
        Open: 20.67,
        High: 20.76,
        Low: 20.65,
        Close: 20.690001,
        Volume: 1475400,
        AdjClose: 19.876073
    },
    {
        Date: "2016-05-24",
        Open: 20.42,
        High: 20.59,
        Low: 20.42,
        Close: 20.540001,
        Volume: 1613600,
        AdjClose: 19.731974
    },
    {
        Date: "2016-05-23",
        Open: 20.459999,
        High: 20.57,
        Low: 20.42,
        Close: 20.5,
        Volume: 1802500,
        AdjClose: 19.693547
    },
    {
        Date: "2016-05-20",
        Open: 20.389999,
        High: 20.450001,
        Low: 20.32,
        Close: 20.34,
        Volume: 1143600,
        AdjClose: 19.539841
    },
    {
        Date: "2016-05-19",
        Open: 20.17,
        High: 20.26,
        Low: 20.1,
        Close: 20.23,
        Volume: 1591200,
        AdjClose: 19.434168
    },
    {
        Date: "2016-05-18",
        Open: 20.35,
        High: 20.549999,
        Low: 20.24,
        Close: 20.360001,
        Volume: 2298500,
        AdjClose: 19.559055
    },
    {
        Date: "2016-05-17",
        Open: 20.43,
        High: 20.559999,
        Low: 20.27,
        Close: 20.32,
        Volume: 1704300,
        AdjClose: 19.520627
    },
    {
        Date: "2016-05-16",
        Open: 20.27,
        High: 20.440001,
        Low: 20.25,
        Close: 20.34,
        Volume: 2002000,
        AdjClose: 19.539841
    },
    {
        Date: "2016-05-13",
        Open: 20.32,
        High: 20.469999,
        Low: 20.129999,
        Close: 20.200001,
        Volume: 1943400,
        AdjClose: 19.405349
    },
    {
        Date: "2016-05-12",
        Open: 20.9,
        High: 20.91,
        Low: 20.58,
        Close: 20.709999,
        Volume: 1616800,
        AdjClose: 19.895285
    },
    {
        Date: "2016-05-11",
        Open: 20.75,
        High: 20.85,
        Low: 20.66,
        Close: 20.700001,
        Volume: 1316400,
        AdjClose: 19.88568
    },
    {
        Date: "2016-05-10",
        Open: 20.450001,
        High: 20.709999,
        Low: 20.450001,
        Close: 20.690001,
        Volume: 1673600,
        AdjClose: 19.876073
    },
    {
        Date: "2016-05-09",
        Open: 20.530001,
        High: 20.549999,
        Low: 20.389999,
        Close: 20.4,
        Volume: 2850300,
        AdjClose: 19.59748
    },
    {
        Date: "2016-05-06",
        Open: 20.5,
        High: 20.629999,
        Low: 20.48,
        Close: 20.6,
        Volume: 2595500,
        AdjClose: 19.789613
    },
    {
        Date: "2016-05-05",
        Open: 20.780001,
        High: 20.780001,
        Low: 20.48,
        Close: 20.540001,
        Volume: 1223600,
        AdjClose: 19.731974
    },
    {
        Date: "2016-05-04",
        Open: 20.889999,
        High: 20.93,
        Low: 20.629999,
        Close: 20.690001,
        Volume: 1865900,
        AdjClose: 19.876073
    },
    {
        Date: "2016-05-03",
        Open: 21.15,
        High: 21.16,
        Low: 20.969999,
        Close: 21.02,
        Volume: 2401600,
        AdjClose: 20.193091
    },
    {
        Date: "2016-05-02",
        Open: 21.18,
        High: 21.25,
        Low: 21.09,
        Close: 21.23,
        Volume: 2071200,
        AdjClose: 20.394829
    },
    {
        Date: "2016-04-29",
        Open: 21.120001,
        High: 21.219999,
        Low: 21.01,
        Close: 21.110001,
        Volume: 2951900,
        AdjClose: 20.27955
    },
    {
        Date: "2016-04-28",
        Open: 20.940001,
        High: 21.120001,
        Low: 20.870001,
        Close: 20.92,
        Volume: 1368100,
        AdjClose: 20.097024
    },
    {
        Date: "2016-04-27",
        Open: 20.98,
        High: 21.129999,
        Low: 20.9,
        Close: 21.08,
        Volume: 1226900,
        AdjClose: 20.25073
    },
    {
        Date: "2016-04-26",
        Open: 20.879999,
        High: 20.950001,
        Low: 20.83,
        Close: 20.91,
        Volume: 1818600,
        AdjClose: 20.087418
    },
    {
        Date: "2016-04-25",
        Open: 20.620001,
        High: 20.68,
        Low: 20.540001,
        Close: 20.59,
        Volume: 1589600,
        AdjClose: 19.780006
    },
    {
        Date: "2016-04-22",
        Open: 20.74,
        High: 20.809999,
        Low: 20.690001,
        Close: 20.780001,
        Volume: 1295000,
        AdjClose: 19.962532
    },
    {
        Date: "2016-04-21",
        Open: 20.700001,
        High: 20.84,
        Low: 20.620001,
        Close: 20.690001,
        Volume: 2702000,
        AdjClose: 19.876073
    },
    {
        Date: "2016-04-20",
        Open: 20.85,
        High: 21.030001,
        Low: 20.809999,
        Close: 20.879999,
        Volume: 3170300,
        AdjClose: 20.058597
    },
    {
        Date: "2016-04-19",
        Open: 20.280001,
        High: 20.370001,
        Low: 20.200001,
        Close: 20.26,
        Volume: 1827900,
        AdjClose: 19.462988
    },
    {
        Date: "2016-04-18",
        Open: 19.74,
        High: 20.02,
        Low: 19.74,
        Close: 19.99,
        Volume: 1586600,
        AdjClose: 19.203609
    },
    {
        Date: "2016-04-15",
        Open: 19.780001,
        High: 19.9,
        Low: 19.76,
        Close: 19.790001,
        Volume: 1523000,
        AdjClose: 19.011478
    },
    {
        Date: "2016-04-14",
        Open: 19.870001,
        High: 19.91,
        Low: 19.799999,
        Close: 19.85,
        Volume: 905700,
        AdjClose: 19.069118
    },
    {
        Date: "2016-04-13",
        Open: 19.77,
        High: 19.790001,
        Low: 19.67,
        Close: 19.780001,
        Volume: 1087000,
        AdjClose: 19.001872
    },
    {
        Date: "2016-04-12",
        Open: 19.540001,
        High: 19.76,
        Low: 19.469999,
        Close: 19.709999,
        Volume: 1523500,
        AdjClose: 18.934624
    },
    {
        Date: "2016-04-11",
        Open: 19.459999,
        High: 19.57,
        Low: 19.389999,
        Close: 19.389999,
        Volume: 1307900,
        AdjClose: 18.627213
    },
    {
        Date: "2016-04-08",
        Open: 19.24,
        High: 19.459999,
        Low: 19.23,
        Close: 19.370001,
        Volume: 1218600,
        AdjClose: 18.608001
    },
    {
        Date: "2016-04-07",
        Open: 19.040001,
        High: 19.129999,
        Low: 18.93,
        Close: 18.98,
        Volume: 1570300,
        AdjClose: 18.233342
    },
    {
        Date: "2016-04-06",
        Open: 19.02,
        High: 19.219999,
        Low: 18.959999,
        Close: 19.200001,
        Volume: 2760500,
        AdjClose: 18.444688
    },
    {
        Date: "2016-04-05",
        Open: 19.049999,
        High: 19.18,
        Low: 19.01,
        Close: 19.09,
        Volume: 1675100,
        AdjClose: 18.339015
    },
    {
        Date: "2016-04-04",
        Open: 19.540001,
        High: 19.58,
        Low: 19.33,
        Close: 19.389999,
        Volume: 1822400,
        AdjClose: 18.627213
    },
    {
        Date: "2016-04-01",
        Open: 19.02,
        High: 19.290001,
        Low: 19.01,
        Close: 19.27,
        Volume: 1691500,
        AdjClose: 18.511934
    },
    {
        Date: "2016-03-31",
        Open: 19.459999,
        High: 19.52,
        Low: 19.379999,
        Close: 19.42,
        Volume: 1552000,
        AdjClose: 18.656033
    },
    {
        Date: "2016-03-30",
        Open: 19.559999,
        High: 19.67,
        Low: 19.51,
        Close: 19.58,
        Volume: 1827200,
        AdjClose: 18.809739
    },
    {
        Date: "2016-03-29",
        Open: 18.889999,
        High: 19.190001,
        Low: 18.84,
        Close: 19.17,
        Volume: 959900,
        AdjClose: 18.415868
    },
    {
        Date: "2016-03-28",
        Open: 18.950001,
        High: 19.040001,
        Low: 18.93,
        Close: 18.98,
        Volume: 606400,
        AdjClose: 18.233342
    },
    {
        Date: "2016-03-24",
        Open: 18.799999,
        High: 18.940001,
        Low: 18.780001,
        Close: 18.93,
        Volume: 1313100,
        AdjClose: 18.18531
    },
    {
        Date: "2016-03-23",
        Open: 19.200001,
        High: 19.200001,
        Low: 19.07,
        Close: 19.09,
        Volume: 1305800,
        AdjClose: 18.339015
    },
    {
        Date: "2016-03-22",
        Open: 19.16,
        High: 19.299999,
        Low: 19.120001,
        Close: 19.24,
        Volume: 1251700,
        AdjClose: 18.483114
    },
    {
        Date: "2016-03-21",
        Open: 19.209999,
        High: 19.360001,
        Low: 19.190001,
        Close: 19.24,
        Volume: 2730400,
        AdjClose: 18.483114
    },
    {
        Date: "2016-03-18",
        Open: 19.48,
        High: 19.530001,
        Low: 19.379999,
        Close: 19.450001,
        Volume: 2104100,
        AdjClose: 18.684854
    },
    {
        Date: "2016-03-17",
        Open: 19.120001,
        High: 19.48,
        Low: 19.059999,
        Close: 19.42,
        Volume: 1923800,
        AdjClose: 18.656033
    },
    {
        Date: "2016-03-16",
        Open: 18.5,
        High: 18.950001,
        Low: 18.5,
        Close: 18.91,
        Volume: 1800500,
        AdjClose: 18.166096
    },
    {
        Date: "2016-03-15",
        Open: 18.41,
        High: 18.41,
        Low: 18.290001,
        Close: 18.35,
        Volume: 1169600,
        AdjClose: 17.628126
    },
    {
        Date: "2016-03-14",
        Open: 18.469999,
        High: 18.549999,
        Low: 18.41,
        Close: 18.5,
        Volume: 1151900,
        AdjClose: 17.772225
    },
    {
        Date: "2016-03-11",
        Open: 18.530001,
        High: 18.690001,
        Low: 18.49,
        Close: 18.67,
        Volume: 1912000,
        AdjClose: 17.935537
    },
    {
        Date: "2016-03-10",
        Open: 18.32,
        High: 18.459999,
        Low: 18.09,
        Close: 18.24,
        Volume: 2440300,
        AdjClose: 17.522453
    },
    {
        Date: "2016-03-09",
        Open: 18.190001,
        High: 18.290001,
        Low: 18.129999,
        Close: 18.209999,
        Volume: 1220300,
        AdjClose: 17.493633
    },
    {
        Date: "2016-03-08",
        Open: 18.4,
        High: 18.42,
        Low: 18.200001,
        Close: 18.219999,
        Volume: 1689500,
        AdjClose: 17.503239
    },
    {
        Date: "2016-03-07",
        Open: 18.24,
        High: 18.49,
        Low: 18.23,
        Close: 18.459999,
        Volume: 1246400,
        AdjClose: 17.733798
    },
    {
        Date: "2016-03-04",
        Open: 18.290001,
        High: 18.49,
        Low: 18.23,
        Close: 18.389999,
        Volume: 2423900,
        AdjClose: 17.666552
    },
    {
        Date: "2016-03-03",
        Open: 18.360001,
        High: 18.51,
        Low: 18.360001,
        Close: 18.51,
        Volume: 1271500,
        AdjClose: 17.781832
    },
    {
        Date: "2016-03-02",
        Open: 18.02,
        High: 18.24,
        Low: 17.969999,
        Close: 18.219999,
        Volume: 1934100,
        AdjClose: 17.503239
    },
    {
        Date: "2016-03-01",
        Open: 17.969999,
        High: 18.219999,
        Low: 17.92,
        Close: 18.200001,
        Volume: 1371600,
        AdjClose: 17.484028
    },
    {
        Date: "2016-02-29",
        Open: 17.85,
        High: 18,
        Low: 17.799999,
        Close: 17.799999,
        Volume: 1572500,
        AdjClose: 17.099762
    },
    {
        Date: "2016-02-26",
        Open: 17.870001,
        High: 17.92,
        Low: 17.74,
        Close: 17.799999,
        Volume: 1155100,
        AdjClose: 17.099762
    },
    {
        Date: "2016-02-25",
        Open: 17.65,
        High: 17.82,
        Low: 17.610001,
        Close: 17.809999,
        Volume: 1502300,
        AdjClose: 17.109369
    },
    {
        Date: "2016-02-24",
        Open: 17.35,
        High: 17.709999,
        Low: 17.26,
        Close: 17.690001,
        Volume: 1547900,
        AdjClose: 16.99409
    },
    {
        Date: "2016-02-23",
        Open: 17.790001,
        High: 17.870001,
        Low: 17.620001,
        Close: 17.65,
        Volume: 1461600,
        AdjClose: 16.955663
    },
    {
        Date: "2016-02-22",
        Open: 17.809999,
        High: 17.93,
        Low: 17.799999,
        Close: 17.889999,
        Volume: 1664500,
        AdjClose: 17.186221
    },
    {
        Date: "2016-02-19",
        Open: 17.780001,
        High: 17.9,
        Low: 17.719999,
        Close: 17.860001,
        Volume: 1485000,
        AdjClose: 17.157403
    },
    {
        Date: "2016-02-18",
        Open: 17.969999,
        High: 17.969999,
        Low: 17.83,
        Close: 17.870001,
        Volume: 2096000,
        AdjClose: 17.16701
    },
    {
        Date: "2016-02-17",
        Open: 17.719999,
        High: 17.93,
        Low: 17.68,
        Close: 17.860001,
        Volume: 2620200,
        AdjClose: 17.157403
    },
    {
        Date: "2016-02-16",
        Open: 17.41,
        High: 17.51,
        Low: 17.23,
        Close: 17.48,
        Volume: 2383800,
        AdjClose: 16.792351
    },
    {
        Date: "2016-02-12",
        Open: 16.799999,
        High: 17.059999,
        Low: 16.709999,
        Close: 17.030001,
        Volume: 2286700,
        AdjClose: 16.360054
    },
    {
        Date: "2016-02-11",
        Open: 16.690001,
        High: 16.809999,
        Low: 16.52,
        Close: 16.67,
        Volume: 2685900,
        AdjClose: 16.014216
    },
    {
        Date: "2016-02-10",
        Open: 17.08,
        High: 17.139999,
        Low: 16.889999,
        Close: 16.91,
        Volume: 2087400,
        AdjClose: 16.244774
    },
    {
        Date: "2016-02-09",
        Open: 16.84,
        High: 17.07,
        Low: 16.790001,
        Close: 16.98,
        Volume: 6043300,
        AdjClose: 16.31202
    },
    {
        Date: "2016-02-08",
        Open: 17.219999,
        High: 17.27,
        Low: 16.98,
        Close: 17.15,
        Volume: 6445400,
        AdjClose: 16.475333
    },
    {
        Date: "2016-02-05",
        Open: 17.700001,
        High: 17.82,
        Low: 17.559999,
        Close: 17.6,
        Volume: 5023100,
        AdjClose: 16.907631
    },
    {
        Date: "2016-02-04",
        Open: 17.219999,
        High: 17.66,
        Low: 17.200001,
        Close: 17.629999,
        Volume: 5710700,
        AdjClose: 16.936449
    },
    {
        Date: "2016-02-03",
        Open: 17.049999,
        High: 17.280001,
        Low: 16.84,
        Close: 17.23,
        Volume: 7229800,
        AdjClose: 16.552185
    },
    {
        Date: "2016-02-02",
        Open: 16.85,
        High: 16.99,
        Low: 16.719999,
        Close: 16.76,
        Volume: 5517100,
        AdjClose: 16.100675
    },
    {
        Date: "2016-02-01",
        Open: 17.08,
        High: 17.280001,
        Low: 17,
        Close: 17.219999,
        Volume: 2385400,
        AdjClose: 16.542579
    },
    {
        Date: "2016-01-29",
        Open: 17.059999,
        High: 17.309999,
        Low: 17.02,
        Close: 17.299999,
        Volume: 2117600,
        AdjClose: 16.619431
    },
    {
        Date: "2016-01-28",
        Open: 17.120001,
        High: 17.16,
        Low: 16.83,
        Close: 16.93,
        Volume: 2863100,
        AdjClose: 16.263988
    },
    {
        Date: "2016-01-27",
        Open: 16.66,
        High: 17.16,
        Low: 16.66,
        Close: 16.9,
        Volume: 3393200,
        AdjClose: 16.235167
    },
    {
        Date: "2016-01-26",
        Open: 16.51,
        High: 16.690001,
        Low: 16.48,
        Close: 16.67,
        Volume: 2392100,
        AdjClose: 16.014216
    },
    {
        Date: "2016-01-25",
        Open: 16.360001,
        High: 16.5,
        Low: 16.290001,
        Close: 16.32,
        Volume: 2857600,
        AdjClose: 15.677984
    },
    {
        Date: "2016-01-22",
        Open: 16.620001,
        High: 16.690001,
        Low: 16.35,
        Close: 16.540001,
        Volume: 2119200,
        AdjClose: 15.889331
    },
    {
        Date: "2016-01-21",
        Open: 15.96,
        High: 16.200001,
        Low: 15.83,
        Close: 16.09,
        Volume: 3170000,
        AdjClose: 15.457033
    },
    {
        Date: "2016-01-20",
        Open: 16.120001,
        High: 16.18,
        Low: 15.74,
        Close: 16.09,
        Volume: 3981000,
        AdjClose: 15.457033
    },
    {
        Date: "2016-01-19",
        Open: 16.379999,
        High: 16.459999,
        Low: 16.129999,
        Close: 16.26,
        Volume: 3243200,
        AdjClose: 15.620345
    },
    {
        Date: "2016-01-15",
        Open: 16.129999,
        High: 16.280001,
        Low: 15.98,
        Close: 16.059999,
        Volume: 3139700,
        AdjClose: 15.428212
    },
    {
        Date: "2016-01-14",
        Open: 16.57,
        High: 16.82,
        Low: 16.41,
        Close: 16.73,
        Volume: 3928500,
        AdjClose: 16.071855
    },
    {
        Date: "2016-01-13",
        Open: 16.709999,
        High: 16.790001,
        Low: 16.26,
        Close: 16.299999,
        Volume: 2607100,
        AdjClose: 15.658771
    },
    {
        Date: "2016-01-12",
        Open: 16.59,
        High: 16.690001,
        Low: 16.440001,
        Close: 16.66,
        Volume: 4156200,
        AdjClose: 16.004609
    },
    {
        Date: "2016-01-11",
        Open: 16.48,
        High: 16.5,
        Low: 16.200001,
        Close: 16.309999,
        Volume: 2848600,
        AdjClose: 15.668377
    },
    {
        Date: "2016-01-08",
        Open: 16.549999,
        High: 16.59,
        Low: 16.27,
        Close: 16.309999,
        Volume: 3517700,
        AdjClose: 15.668377
    },
    {
        Date: "2016-01-07",
        Open: 16.65,
        High: 16.790001,
        Low: 16.57,
        Close: 16.6,
        Volume: 2422400,
        AdjClose: 15.94697
    },
    {
        Date: "2016-01-06",
        Open: 16.959999,
        High: 17.040001,
        Low: 16.83,
        Close: 16.92,
        Volume: 2570100,
        AdjClose: 16.254381
    },
    {
        Date: "2016-01-05",
        Open: 17.23,
        High: 17.309999,
        Low: 17.09,
        Close: 17.209999,
        Volume: 2477800,
        AdjClose: 16.532972
    },
    {
        Date: "2016-01-04",
        Open: 17.43,
        High: 17.530001,
        Low: 17.209999,
        Close: 17.52,
        Volume: 2313200,
        AdjClose: 16.830778
    },
    {
        Date: "2015-12-31",
        Open: 17.889999,
        High: 17.9,
        Low: 17.73,
        Close: 17.73,
        Volume: 989400,
        AdjClose: 17.032516
    },
    {
        Date: "2015-12-30",
        Open: 18.110001,
        High: 18.200001,
        Low: 18.07,
        Close: 18.08,
        Volume: 1290200,
        AdjClose: 17.368747
    },
    {
        Date: "2015-12-29",
        Open: 18.16,
        High: 18.299999,
        Low: 18.16,
        Close: 18.23,
        Volume: 1414900,
        AdjClose: 17.512846
    },
    {
        Date: "2015-12-28",
        Open: 18.27,
        High: 18.27,
        Low: 18.09,
        Close: 18.16,
        Volume: 1849600,
        AdjClose: 17.4456
    },
    {
        Date: "2015-12-24",
        Open: 18.030001,
        High: 18.18,
        Low: 18.030001,
        Close: 18.129999,
        Volume: 528400,
        AdjClose: 17.41678
    },
    {
        Date: "2015-12-23",
        Open: 17.93,
        High: 18.08,
        Low: 17.870001,
        Close: 18.07,
        Volume: 1790300,
        AdjClose: 17.359141
    },
    {
        Date: "2015-12-22",
        Open: 17.59,
        High: 17.780001,
        Low: 17.540001,
        Close: 17.74,
        Volume: 1684300,
        AdjClose: 17.042123
    },
    {
        Date: "2015-12-21",
        Open: 17.620001,
        High: 17.65,
        Low: 17.459999,
        Close: 17.549999,
        Volume: 2414200,
        AdjClose: 16.859597
    },
    {
        Date: "2015-12-18",
        Open: 17.59,
        High: 17.639999,
        Low: 17.4,
        Close: 17.48,
        Volume: 2845400,
        AdjClose: 16.792351
    },
    {
        Date: "2015-12-17",
        Open: 17.99,
        High: 18,
        Low: 17.709999,
        Close: 17.709999,
        Volume: 2658300,
        AdjClose: 17.013302
    },
    {
        Date: "2015-12-16",
        Open: 18,
        High: 18.120001,
        Low: 17.82,
        Close: 18.08,
        Volume: 4163100,
        AdjClose: 17.368747
    },
    {
        Date: "2015-12-15",
        Open: 17.969999,
        High: 18.059999,
        Low: 17.870001,
        Close: 17.91,
        Volume: 3933100,
        AdjClose: 17.205435
    },
    {
        Date: "2015-12-14",
        Open: 17.99,
        High: 18.040001,
        Low: 17.780001,
        Close: 17.870001,
        Volume: 5580800,
        AdjClose: 17.16701
    },
    {
        Date: "2015-12-11",
        Open: 17.9,
        High: 17.98,
        Low: 17.77,
        Close: 17.780001,
        Volume: 3384200,
        AdjClose: 17.08055
    },
    {
        Date: "2015-12-10",
        Open: 18.17,
        High: 18.200001,
        Low: 18.01,
        Close: 18.040001,
        Volume: 2625500,
        AdjClose: 17.330322
    },
    {
        Date: "2015-12-09",
        Open: 18.209999,
        High: 18.48,
        Low: 18.129999,
        Close: 18.24,
        Volume: 2715400,
        AdjClose: 17.522453
    },
    {
        Date: "2015-12-08",
        Open: 18.219999,
        High: 18.33,
        Low: 18.1,
        Close: 18.219999,
        Volume: 2855800,
        AdjClose: 17.503239
    },
    {
        Date: "2015-12-07",
        Open: 18.59,
        High: 18.59,
        Low: 18.43,
        Close: 18.49,
        Volume: 1911100,
        AdjClose: 17.762618
    },
    {
        Date: "2015-12-04",
        Open: 18.530001,
        High: 18.75,
        Low: 18.51,
        Close: 18.73,
        Volume: 2273400,
        AdjClose: 17.993177
    },
    {
        Date: "2015-12-03",
        Open: 18.83,
        High: 18.83,
        Low: 18.5,
        Close: 18.6,
        Volume: 3705300,
        AdjClose: 17.868292
    },
    {
        Date: "2015-12-02",
        Open: 18.459999,
        High: 18.59,
        Low: 18.450001,
        Close: 18.51,
        Volume: 1552600,
        AdjClose: 17.781832
    },
    {
        Date: "2015-12-01",
        Open: 18.74,
        High: 18.780001,
        Low: 18.48,
        Close: 18.6,
        Volume: 2627800,
        AdjClose: 17.868292
    },
    {
        Date: "2015-11-30",
        Open: 18.83,
        High: 19.02,
        Low: 18.780001,
        Close: 18.82,
        Volume: 2879300,
        AdjClose: 18.079636
    },
    {
        Date: "2015-11-27",
        Open: 18.690001,
        High: 18.75,
        Low: 18.610001,
        Close: 18.68,
        Volume: 970300,
        AdjClose: 17.945144
    },
    {
        Date: "2015-11-25",
        Open: 18.809999,
        High: 18.870001,
        Low: 18.719999,
        Close: 18.76,
        Volume: 1229200,
        AdjClose: 18.021997
    },
    {
        Date: "2015-11-24",
        Open: 18.59,
        High: 18.74,
        Low: 18.540001,
        Close: 18.700001,
        Volume: 2110400,
        AdjClose: 17.964358
    },
    {
        Date: "2015-11-23",
        Open: 18.6,
        High: 18.67,
        Low: 18.42,
        Close: 18.469999,
        Volume: 1872500,
        AdjClose: 17.743405
    },
    {
        Date: "2015-11-20",
        Open: 18.969999,
        High: 18.98,
        Low: 18.77,
        Close: 18.780001,
        Volume: 1222800,
        AdjClose: 18.041211
    },
    {
        Date: "2015-11-19",
        Open: 18.68,
        High: 18.84,
        Low: 18.67,
        Close: 18.83,
        Volume: 1630500,
        AdjClose: 18.089243
    },
    {
        Date: "2015-11-18",
        Open: 18.67,
        High: 18.77,
        Low: 18.59,
        Close: 18.75,
        Volume: 1619900,
        AdjClose: 18.01239
    },
    {
        Date: "2015-11-17",
        Open: 18.639999,
        High: 18.709999,
        Low: 18.48,
        Close: 18.52,
        Volume: 2021400,
        AdjClose: 17.791439
    },
    {
        Date: "2015-11-16",
        Open: 18.290001,
        High: 18.52,
        Low: 18.25,
        Close: 18.5,
        Volume: 2433900,
        AdjClose: 17.772225
    },
    {
        Date: "2015-11-13",
        Open: 18.34,
        High: 18.42,
        Low: 18.25,
        Close: 18.290001,
        Volume: 1608700,
        AdjClose: 17.570487
    },
    {
        Date: "2015-11-12",
        Open: 18.530001,
        High: 18.719999,
        Low: 18.49,
        Close: 18.5,
        Volume: 1367000,
        AdjClose: 17.772225
    },
    {
        Date: "2015-11-11",
        Open: 18.93,
        High: 18.950001,
        Low: 18.799999,
        Close: 18.82,
        Volume: 1009500,
        AdjClose: 18.079636
    },
    {
        Date: "2015-11-10",
        Open: 18.73,
        High: 18.84,
        Low: 18.65,
        Close: 18.799999,
        Volume: 1409700,
        AdjClose: 18.060423
    },
    {
        Date: "2015-11-09",
        Open: 19.17,
        High: 19.209999,
        Low: 18.860001,
        Close: 18.92,
        Volume: 2534500,
        AdjClose: 18.175703
    },
    {
        Date: "2015-11-06",
        Open: 19.129999,
        High: 19.209999,
        Low: 19,
        Close: 19.15,
        Volume: 2341600,
        AdjClose: 18.396654
    },
    {
        Date: "2015-11-05",
        Open: 19.18,
        High: 19.200001,
        Low: 19.02,
        Close: 19.09,
        Volume: 1716000,
        AdjClose: 18.339015
    },
    {
        Date: "2015-11-04",
        Open: 19.27,
        High: 19.27,
        Low: 19.07,
        Close: 19.139999,
        Volume: 1950300,
        AdjClose: 18.387047
    },
    {
        Date: "2015-11-03",
        Open: 18.99,
        High: 19.139999,
        Low: 18.93,
        Close: 19.049999,
        Volume: 1151300,
        AdjClose: 18.300588
    },
    {
        Date: "2015-11-02",
        Open: 19.110001,
        High: 19.15,
        Low: 19.01,
        Close: 19.1,
        Volume: 1595600,
        AdjClose: 18.348622
    },
    {
        Date: "2015-10-30",
        Open: 18.870001,
        High: 18.98,
        Low: 18.809999,
        Close: 18.879999,
        Volume: 2124200,
        AdjClose: 18.137275
    },
    {
        Date: "2015-10-29",
        Open: 18.68,
        High: 18.77,
        Low: 18.65,
        Close: 18.719999,
        Volume: 1815400,
        AdjClose: 17.98357
    },
    {
        Date: "2015-10-28",
        Open: 18.67,
        High: 18.940001,
        Low: 18.58,
        Close: 18.76,
        Volume: 3563700,
        AdjClose: 18.021997
    },
    {
        Date: "2015-10-27",
        Open: 18.52,
        High: 18.57,
        Low: 18.32,
        Close: 18.33,
        Volume: 3663300,
        AdjClose: 17.608913
    },
    {
        Date: "2015-10-26",
        Open: 18.709999,
        High: 18.74,
        Low: 18.450001,
        Close: 18.469999,
        Volume: 1656900,
        AdjClose: 17.743405
    },
    {
        Date: "2015-10-23",
        Open: 18.65,
        High: 18.67,
        Low: 18.469999,
        Close: 18.6,
        Volume: 4806600,
        AdjClose: 17.868292
    },
    {
        Date: "2015-10-22",
        Open: 17.9,
        High: 18.27,
        Low: 17.870001,
        Close: 18.16,
        Volume: 4407200,
        AdjClose: 17.4456
    },
    {
        Date: "2015-10-21",
        Open: 18.309999,
        High: 18.34,
        Low: 18.01,
        Close: 18.030001,
        Volume: 2948700,
        AdjClose: 17.320715
    },
    {
        Date: "2015-10-20",
        Open: 17.940001,
        High: 18.219999,
        Low: 17.93,
        Close: 18.139999,
        Volume: 3401400,
        AdjClose: 17.426387
    },
    {
        Date: "2015-10-19",
        Open: 17.92,
        High: 17.98,
        Low: 17.83,
        Close: 17.870001,
        Volume: 3600400,
        AdjClose: 17.16701
    },
    {
        Date: "2015-10-16",
        Open: 18.219999,
        High: 18.299999,
        Low: 18.049999,
        Close: 18.26,
        Volume: 2202100,
        AdjClose: 17.541667
    },
    {
        Date: "2015-10-15",
        Open: 18.43,
        High: 18.67,
        Low: 18.4,
        Close: 18.66,
        Volume: 1711600,
        AdjClose: 17.925931
    },
    {
        Date: "2015-10-14",
        Open: 18.549999,
        High: 18.620001,
        Low: 18.49,
        Close: 18.549999,
        Volume: 1531600,
        AdjClose: 17.820257
    },
    {
        Date: "2015-10-13",
        Open: 18.450001,
        High: 18.620001,
        Low: 18.440001,
        Close: 18.49,
        Volume: 1232500,
        AdjClose: 17.762618
    },
    {
        Date: "2015-10-12",
        Open: 18.76,
        High: 18.82,
        Low: 18.700001,
        Close: 18.74,
        Volume: 981500,
        AdjClose: 18.002783
    },
    {
        Date: "2015-10-09",
        Open: 19,
        High: 19.120001,
        Low: 18.870001,
        Close: 18.959999,
        Volume: 1874900,
        AdjClose: 18.214128
    },
    {
        Date: "2015-10-08",
        Open: 18.42,
        High: 18.75,
        Low: 18.41,
        Close: 18.709999,
        Volume: 1769700,
        AdjClose: 17.973963
    },
    {
        Date: "2015-10-07",
        Open: 18.530001,
        High: 18.620001,
        Low: 18.309999,
        Close: 18.450001,
        Volume: 1890100,
        AdjClose: 17.724193
    },
    {
        Date: "2015-10-06",
        Open: 18.17,
        High: 18.360001,
        Low: 18.16,
        Close: 18.290001,
        Volume: 2200000,
        AdjClose: 17.570487
    },
    {
        Date: "2015-10-05",
        Open: 17.940001,
        High: 18.25,
        Low: 17.92,
        Close: 18.209999,
        Volume: 2800800,
        AdjClose: 17.493633
    },
    {
        Date: "2015-10-02",
        Open: 17.58,
        High: 17.879999,
        Low: 17.5,
        Close: 17.870001,
        Volume: 1886000,
        AdjClose: 17.16701
    },
    {
        Date: "2015-10-01",
        Open: 17.719999,
        High: 17.77,
        Low: 17.41,
        Close: 17.540001,
        Volume: 1833800,
        AdjClose: 16.849992
    },
    {
        Date: "2015-09-30",
        Open: 17.67,
        High: 17.74,
        Low: 17.51,
        Close: 17.67,
        Volume: 1755500,
        AdjClose: 16.974877
    },
    {
        Date: "2015-09-29",
        Open: 17.43,
        High: 17.459999,
        Low: 17.280001,
        Close: 17.370001,
        Volume: 2700300,
        AdjClose: 16.686679
    },
    {
        Date: "2015-09-28",
        Open: 17.360001,
        High: 17.389999,
        Low: 17.129999,
        Close: 17.139999,
        Volume: 3004700,
        AdjClose: 16.465726
    },
    {
        Date: "2015-09-25",
        Open: 17.43,
        High: 17.450001,
        Low: 17.209999,
        Close: 17.27,
        Volume: 2572800,
        AdjClose: 16.590613
    },
    {
        Date: "2015-09-24",
        Open: 17.18,
        High: 17.42,
        Low: 17.15,
        Close: 17.360001,
        Volume: 3827100,
        AdjClose: 16.677072
    },
    {
        Date: "2015-09-23",
        Open: 17.629999,
        High: 17.629999,
        Low: 17.299999,
        Close: 17.360001,
        Volume: 1853000,
        AdjClose: 16.677072
    },
    {
        Date: "2015-09-22",
        Open: 17.75,
        High: 17.780001,
        Low: 17.559999,
        Close: 17.700001,
        Volume: 2851700,
        AdjClose: 17.003697
    },
    {
        Date: "2015-09-21",
        Open: 18.110001,
        High: 18.23,
        Low: 18.07,
        Close: 18.129999,
        Volume: 3917300,
        AdjClose: 17.41678
    },
    {
        Date: "2015-09-18",
        Open: 18.34,
        High: 18.41,
        Low: 18.139999,
        Close: 18.17,
        Volume: 3953100,
        AdjClose: 17.455207
    },
    {
        Date: "2015-09-17",
        Open: 18.52,
        High: 18.879999,
        Low: 18.52,
        Close: 18.67,
        Volume: 2931500,
        AdjClose: 17.935537
    },
    {
        Date: "2015-09-16",
        Open: 18.700001,
        High: 18.83,
        Low: 18.690001,
        Close: 18.82,
        Volume: 1981900,
        AdjClose: 18.079636
    },
    {
        Date: "2015-09-15",
        Open: 18.469999,
        High: 18.65,
        Low: 18.43,
        Close: 18.620001,
        Volume: 1935800,
        AdjClose: 17.887505
    },
    {
        Date: "2015-09-14",
        Open: 18.6,
        High: 18.6,
        Low: 18.41,
        Close: 18.51,
        Volume: 2276200,
        AdjClose: 17.781832
    },
    {
        Date: "2015-09-11",
        Open: 18.58,
        High: 18.799999,
        Low: 18.559999,
        Close: 18.799999,
        Volume: 1378600,
        AdjClose: 18.060423
    },
    {
        Date: "2015-09-10",
        Open: 18.629999,
        High: 18.77,
        Low: 18.49,
        Close: 18.67,
        Volume: 1744800,
        AdjClose: 17.935537
    },
    {
        Date: "2015-09-09",
        Open: 19.120001,
        High: 19.139999,
        Low: 18.700001,
        Close: 18.73,
        Volume: 1745200,
        AdjClose: 17.993177
    },
    {
        Date: "2015-09-08",
        Open: 19.040001,
        High: 19.09,
        Low: 18.879999,
        Close: 19.030001,
        Volume: 2417300,
        AdjClose: 18.281376
    },
    {
        Date: "2015-09-04",
        Open: 18.309999,
        High: 18.4,
        Low: 18.219999,
        Close: 18.33,
        Volume: 1906100,
        AdjClose: 17.608913
    },
    {
        Date: "2015-09-03",
        Open: 18.83,
        High: 19,
        Low: 18.76,
        Close: 18.84,
        Volume: 1954000,
        AdjClose: 18.09885
    },
    {
        Date: "2015-09-02",
        Open: 18.92,
        High: 18.940001,
        Low: 18.610001,
        Close: 18.809999,
        Volume: 1800000,
        AdjClose: 18.070029
    },
    {
        Date: "2015-09-01",
        Open: 19.09,
        High: 19.15,
        Low: 18.83,
        Close: 18.91,
        Volume: 2976400,
        AdjClose: 18.166096
    },
    {
        Date: "2015-08-31",
        Open: 19.120001,
        High: 19.370001,
        Low: 19.07,
        Close: 19.309999,
        Volume: 1276600,
        AdjClose: 18.55036
    },
    {
        Date: "2015-08-28",
        Open: 19.16,
        High: 19.360001,
        Low: 19.139999,
        Close: 19.33,
        Volume: 2185800,
        AdjClose: 18.569573
    },
    {
        Date: "2015-08-27",
        Open: 19.08,
        High: 19.42,
        Low: 19.02,
        Close: 19.32,
        Volume: 3823700,
        AdjClose: 18.559967
    },
    {
        Date: "2015-08-26",
        Open: 19.049999,
        High: 19.08,
        Low: 18.68,
        Close: 19.07,
        Volume: 2864600,
        AdjClose: 18.319801
    },
    {
        Date: "2015-08-25",
        Open: 19.200001,
        High: 19.23,
        Low: 18.610001,
        Close: 18.620001,
        Volume: 3287600,
        AdjClose: 17.887505
    },
    {
        Date: "2015-08-24",
        Open: 18.68,
        High: 19.219999,
        Low: 18.42,
        Close: 18.709999,
        Volume: 5190100,
        AdjClose: 17.973963
    },
    {
        Date: "2015-08-21",
        Open: 19.389999,
        High: 19.42,
        Low: 18.969999,
        Close: 18.99,
        Volume: 2646500,
        AdjClose: 18.242949
    },
    {
        Date: "2015-08-20",
        Open: 19.65,
        High: 19.66,
        Low: 19.290001,
        Close: 19.290001,
        Volume: 2135300,
        AdjClose: 18.531148
    },
    {
        Date: "2015-08-19",
        Open: 19.620001,
        High: 19.83,
        Low: 19.58,
        Close: 19.700001,
        Volume: 2374900,
        AdjClose: 18.925019
    },
    {
        Date: "2015-08-18",
        Open: 19.92,
        High: 19.969999,
        Low: 19.84,
        Close: 19.879999,
        Volume: 885500,
        AdjClose: 19.097936
    },
    {
        Date: "2015-08-17",
        Open: 19.959999,
        High: 20.07,
        Low: 19.9,
        Close: 20.07,
        Volume: 818800,
        AdjClose: 19.280462
    },
    {
        Date: "2015-08-14",
        Open: 19.98,
        High: 20.059999,
        Low: 19.91,
        Close: 20.02,
        Volume: 1182100,
        AdjClose: 19.23243
    },
    {
        Date: "2015-08-13",
        Open: 20.16,
        High: 20.18,
        Low: 20.040001,
        Close: 20.049999,
        Volume: 1982600,
        AdjClose: 19.261249
    },
    {
        Date: "2015-08-12",
        Open: 20.110001,
        High: 20.33,
        Low: 20.049999,
        Close: 20.32,
        Volume: 2639700,
        AdjClose: 19.520627
    },
    {
        Date: "2015-08-11",
        Open: 20.549999,
        High: 20.549999,
        Low: 20.309999,
        Close: 20.35,
        Volume: 2053100,
        AdjClose: 19.549448
    },
    {
        Date: "2015-08-10",
        Open: 20.49,
        High: 20.82,
        Low: 20.49,
        Close: 20.809999,
        Volume: 2130100,
        AdjClose: 19.991351
    },
    {
        Date: "2015-08-07",
        Open: 20.42,
        High: 20.58,
        Low: 20.389999,
        Close: 20.5,
        Volume: 1460300,
        AdjClose: 19.693547
    },
    {
        Date: "2015-08-06",
        Open: 20.41,
        High: 20.450001,
        Low: 20.33,
        Close: 20.389999,
        Volume: 1327600,
        AdjClose: 19.587873
    },
    {
        Date: "2015-08-05",
        Open: 20.33,
        High: 20.549999,
        Low: 20.309999,
        Close: 20.370001,
        Volume: 2868100,
        AdjClose: 19.568662
    },
    {
        Date: "2015-08-04",
        Open: 20.17,
        High: 20.26,
        Low: 20.02,
        Close: 20.040001,
        Volume: 1422700,
        AdjClose: 19.251644
    },
    {
        Date: "2015-08-03",
        Open: 20.23,
        High: 20.23,
        Low: 20.040001,
        Close: 20.1,
        Volume: 1398300,
        AdjClose: 19.309283
    },
    {
        Date: "2015-07-31",
        Open: 20.389999,
        High: 20.41,
        Low: 20.25,
        Close: 20.280001,
        Volume: 1517100,
        AdjClose: 19.482202
    },
    {
        Date: "2015-07-30",
        Open: 20.040001,
        High: 20.08,
        Low: 19.950001,
        Close: 20.040001,
        Volume: 1218400,
        AdjClose: 19.251644
    },
    {
        Date: "2015-07-29",
        Open: 19.809999,
        High: 20.059999,
        Low: 19.799999,
        Close: 19.940001,
        Volume: 1413400,
        AdjClose: 19.155577
    },
    {
        Date: "2015-07-28",
        Open: 19.709999,
        High: 19.9,
        Low: 19.67,
        Close: 19.870001,
        Volume: 1868700,
        AdjClose: 19.088331
    },
    {
        Date: "2015-07-27",
        Open: 19.889999,
        High: 19.889999,
        Low: 19.639999,
        Close: 19.68,
        Volume: 2211000,
        AdjClose: 18.905805
    },
    {
        Date: "2015-07-24",
        Open: 20.360001,
        High: 20.379999,
        Low: 19.969999,
        Close: 20,
        Volume: 2708600,
        AdjClose: 19.213216
    },
    {
        Date: "2015-07-23",
        Open: 21.08,
        High: 21.110001,
        Low: 20.58,
        Close: 20.65,
        Volume: 4017200,
        AdjClose: 19.664727
    },
    {
        Date: "2015-07-22",
        Open: 20.219999,
        High: 20.52,
        Low: 20.209999,
        Close: 20.389999,
        Volume: 2991600,
        AdjClose: 19.417132
    },
    {
        Date: "2015-07-21",
        Open: 20.74,
        High: 20.809999,
        Low: 20.620001,
        Close: 20.639999,
        Volume: 1600900,
        AdjClose: 19.655203
    },
    {
        Date: "2015-07-20",
        Open: 20.709999,
        High: 20.790001,
        Low: 20.66,
        Close: 20.709999,
        Volume: 1135900,
        AdjClose: 19.721863
    },
    {
        Date: "2015-07-17",
        Open: 20.690001,
        High: 20.700001,
        Low: 20.6,
        Close: 20.65,
        Volume: 1311300,
        AdjClose: 19.664727
    },
    {
        Date: "2015-07-16",
        Open: 20.85,
        High: 20.889999,
        Low: 20.74,
        Close: 20.77,
        Volume: 1559400,
        AdjClose: 19.779002
    },
    {
        Date: "2015-07-15",
        Open: 20.48,
        High: 20.58,
        Low: 20.42,
        Close: 20.52,
        Volume: 1500200,
        AdjClose: 19.54093
    },
    {
        Date: "2015-07-14",
        Open: 20.940001,
        High: 21,
        Low: 20.82,
        Close: 20.879999,
        Volume: 1699700,
        AdjClose: 19.883752
    },
    {
        Date: "2015-07-13",
        Open: 20.76,
        High: 20.84,
        Low: 20.709999,
        Close: 20.76,
        Volume: 1447400,
        AdjClose: 19.769479
    },
    {
        Date: "2015-07-10",
        Open: 20.75,
        High: 20.75,
        Low: 20.5,
        Close: 20.59,
        Volume: 1866000,
        AdjClose: 19.60759
    },
    {
        Date: "2015-07-09",
        Open: 20.24,
        High: 20.299999,
        Low: 20,
        Close: 20.01,
        Volume: 2577200,
        AdjClose: 19.055263
    },
    {
        Date: "2015-07-08",
        Open: 20.27,
        High: 20.33,
        Low: 20.110001,
        Close: 20.129999,
        Volume: 2786100,
        AdjClose: 19.169537
    },
    {
        Date: "2015-07-07",
        Open: 20.16,
        High: 20.51,
        Low: 19.959999,
        Close: 20.459999,
        Volume: 5526000,
        AdjClose: 19.483792
    },
    {
        Date: "2015-07-06",
        Open: 20.4,
        High: 20.620001,
        Low: 20.360001,
        Close: 20.52,
        Volume: 2277800,
        AdjClose: 19.54093
    },
    {
        Date: "2015-07-02",
        Open: 20.889999,
        High: 20.93,
        Low: 20.73,
        Close: 20.799999,
        Volume: 1748800,
        AdjClose: 19.807569
    },
    {
        Date: "2015-07-01",
        Open: 21.09,
        High: 21.190001,
        Low: 20.780001,
        Close: 20.870001,
        Volume: 1930700,
        AdjClose: 19.874231
    },
    {
        Date: "2015-06-30",
        Open: 21.290001,
        High: 21.33,
        Low: 20.76,
        Close: 20.879999,
        Volume: 4374500,
        AdjClose: 19.883752
    },
    {
        Date: "2015-06-29",
        Open: 21.5,
        High: 21.639999,
        Low: 21.25,
        Close: 21.290001,
        Volume: 2864700,
        AdjClose: 20.274191
    },
    {
        Date: "2015-06-26",
        Open: 21.58,
        High: 21.870001,
        Low: 21.58,
        Close: 21.870001,
        Volume: 4590600,
        AdjClose: 20.826518
    },
    {
        Date: "2015-06-25",
        Open: 21.639999,
        High: 21.709999,
        Low: 21.5,
        Close: 21.530001,
        Volume: 2337300,
        AdjClose: 20.50274
    },
    {
        Date: "2015-06-24",
        Open: 21.82,
        High: 21.959999,
        Low: 21.719999,
        Close: 21.73,
        Volume: 2124100,
        AdjClose: 20.693196
    },
    {
        Date: "2015-06-23",
        Open: 22.27,
        High: 22.35,
        Low: 22.18,
        Close: 22.280001,
        Volume: 1709700,
        AdjClose: 21.216955
    },
    {
        Date: "2015-06-22",
        Open: 22.610001,
        High: 22.620001,
        Low: 22.41,
        Close: 22.43,
        Volume: 1152600,
        AdjClose: 21.359798
    },
    {
        Date: "2015-06-19",
        Open: 22.48,
        High: 22.5,
        Low: 22.26,
        Close: 22.309999,
        Volume: 2128300,
        AdjClose: 21.245523
    },
    {
        Date: "2015-06-18",
        Open: 21.98,
        High: 22.309999,
        Low: 21.969999,
        Close: 22.139999,
        Volume: 2360600,
        AdjClose: 21.083634
    },
    {
        Date: "2015-06-17",
        Open: 22.709999,
        High: 22.75,
        Low: 22.09,
        Close: 22.280001,
        Volume: 3969300,
        AdjClose: 21.216955
    },
    {
        Date: "2015-06-16",
        Open: 22.6,
        High: 22.65,
        Low: 22.469999,
        Close: 22.59,
        Volume: 1960100,
        AdjClose: 21.512164
    },
    {
        Date: "2015-06-15",
        Open: 22.360001,
        High: 22.530001,
        Low: 22.33,
        Close: 22.51,
        Volume: 1264100,
        AdjClose: 21.435981
    },
    {
        Date: "2015-06-12",
        Open: 22.74,
        High: 22.860001,
        Low: 22.67,
        Close: 22.77,
        Volume: 1271100,
        AdjClose: 21.683576
    },
    {
        Date: "2015-06-11",
        Open: 22.74,
        High: 22.76,
        Low: 22.559999,
        Close: 22.700001,
        Volume: 1275500,
        AdjClose: 21.616916
    },
    {
        Date: "2015-06-10",
        Open: 22.92,
        High: 23.059999,
        Low: 22.879999,
        Close: 22.940001,
        Volume: 1464700,
        AdjClose: 21.845465
    },
    {
        Date: "2015-06-09",
        Open: 22.709999,
        High: 22.76,
        Low: 22.610001,
        Close: 22.610001,
        Volume: 1183600,
        AdjClose: 21.53121
    },
    {
        Date: "2015-06-08",
        Open: 22.66,
        High: 22.75,
        Low: 22.59,
        Close: 22.67,
        Volume: 1578300,
        AdjClose: 21.588347
    },
    {
        Date: "2015-06-05",
        Open: 23.139999,
        High: 23.139999,
        Low: 22.549999,
        Close: 22.799999,
        Volume: 3635400,
        AdjClose: 21.712143
    },
    {
        Date: "2015-06-04",
        Open: 23.59,
        High: 23.66,
        Low: 23.07,
        Close: 23.139999,
        Volume: 7951300,
        AdjClose: 22.035921
    },
    {
        Date: "2015-06-03",
        Open: 22.24,
        High: 22.450001,
        Low: 22.219999,
        Close: 22.34,
        Volume: 1657900,
        AdjClose: 21.274092
    },
    {
        Date: "2015-06-02",
        Open: 22.01,
        High: 22.129999,
        Low: 21.98,
        Close: 22.030001,
        Volume: 1426300,
        AdjClose: 20.978884
    },
    {
        Date: "2015-06-01",
        Open: 21.860001,
        High: 21.879999,
        Low: 21.620001,
        Close: 21.67,
        Volume: 1420200,
        AdjClose: 20.63606
    },
    {
        Date: "2015-05-29",
        Open: 21.940001,
        High: 22.08,
        Low: 21.74,
        Close: 21.84,
        Volume: 1931300,
        AdjClose: 20.797949
    },
    {
        Date: "2015-05-28",
        Open: 21.889999,
        High: 21.99,
        Low: 21.860001,
        Close: 21.98,
        Volume: 1571300,
        AdjClose: 20.931268
    },
    {
        Date: "2015-05-27",
        Open: 21.77,
        High: 21.950001,
        Low: 21.76,
        Close: 21.9,
        Volume: 2052500,
        AdjClose: 20.855085
    },
    {
        Date: "2015-05-26",
        Open: 21.76,
        High: 21.77,
        Low: 21.52,
        Close: 21.57,
        Volume: 1075500,
        AdjClose: 20.540831
    },
    {
        Date: "2015-05-22",
        Open: 21.68,
        High: 21.76,
        Low: 21.65,
        Close: 21.68,
        Volume: 892800,
        AdjClose: 20.645583
    },
    {
        Date: "2015-05-21",
        Open: 21.889999,
        High: 21.99,
        Low: 21.860001,
        Close: 21.93,
        Volume: 970100,
        AdjClose: 20.883655
    },
    {
        Date: "2015-05-20",
        Open: 21.629999,
        High: 21.82,
        Low: 21.629999,
        Close: 21.77,
        Volume: 1234400,
        AdjClose: 20.731289
    },
    {
        Date: "2015-05-19",
        Open: 21.91,
        High: 21.92,
        Low: 21.700001,
        Close: 21.76,
        Volume: 2926300,
        AdjClose: 20.721766
    },
    {
        Date: "2015-05-18",
        Open: 22.280001,
        High: 22.290001,
        Low: 22.09,
        Close: 22.15,
        Volume: 1587700,
        AdjClose: 21.093157
    },
    {
        Date: "2015-05-15",
        Open: 22.09,
        High: 22.23,
        Low: 22.030001,
        Close: 22.209999,
        Volume: 979800,
        AdjClose: 21.150294
    },
    {
        Date: "2015-05-14",
        Open: 22.209999,
        High: 22.27,
        Low: 22.1,
        Close: 22.190001,
        Volume: 1845100,
        AdjClose: 21.131249
    },
    {
        Date: "2015-05-13",
        Open: 21.99,
        High: 22.17,
        Low: 21.969999,
        Close: 22,
        Volume: 1553000,
        AdjClose: 20.950314
    },
    {
        Date: "2015-05-12",
        Open: 21.76,
        High: 21.860001,
        Low: 21.719999,
        Close: 21.74,
        Volume: 1594800,
        AdjClose: 20.70272
    },
    {
        Date: "2015-05-11",
        Open: 21.879999,
        High: 21.959999,
        Low: 21.790001,
        Close: 21.82,
        Volume: 1706500,
        AdjClose: 20.778902
    },
    {
        Date: "2015-05-08",
        Open: 21.889999,
        High: 21.98,
        Low: 21.85,
        Close: 21.889999,
        Volume: 1907700,
        AdjClose: 20.845562
    },
    {
        Date: "2015-05-07",
        Open: 21.469999,
        High: 21.5,
        Low: 21.32,
        Close: 21.35,
        Volume: 1307100,
        AdjClose: 20.331328
    },
    {
        Date: "2015-05-06",
        Open: 21.639999,
        High: 21.700001,
        Low: 21.4,
        Close: 21.469999,
        Volume: 1953100,
        AdjClose: 20.445602
    },
    {
        Date: "2015-05-05",
        Open: 21.57,
        High: 21.620001,
        Low: 21.33,
        Close: 21.34,
        Volume: 1642100,
        AdjClose: 20.321805
    },
    {
        Date: "2015-05-04",
        Open: 21.290001,
        High: 21.42,
        Low: 21.280001,
        Close: 21.4,
        Volume: 2025500,
        AdjClose: 20.378942
    },
    {
        Date: "2015-05-01",
        Open: 22.030001,
        High: 22.030001,
        Low: 21.799999,
        Close: 21.940001,
        Volume: 1507300,
        AdjClose: 20.32752
    },
    {
        Date: "2015-04-30",
        Open: 21.59,
        High: 21.889999,
        Low: 21.58,
        Close: 21.77,
        Volume: 3701500,
        AdjClose: 20.170014
    },
    {
        Date: "2015-04-29",
        Open: 22.139999,
        High: 22.309999,
        Low: 22,
        Close: 22.200001,
        Volume: 3005100,
        AdjClose: 20.568411
    },
    {
        Date: "2015-04-28",
        Open: 21.82,
        High: 22.09,
        Low: 21.780001,
        Close: 22.059999,
        Volume: 3142100,
        AdjClose: 20.438699
    },
    {
        Date: "2015-04-27",
        Open: 21.9,
        High: 22.15,
        Low: 21.84,
        Close: 21.969999,
        Volume: 2134300,
        AdjClose: 20.355314
    },
    {
        Date: "2015-04-24",
        Open: 21.66,
        High: 21.73,
        Low: 21.57,
        Close: 21.690001,
        Volume: 1459000,
        AdjClose: 20.095893
    },
    {
        Date: "2015-04-23",
        Open: 21.549999,
        High: 21.91,
        Low: 21.52,
        Close: 21.83,
        Volume: 1468600,
        AdjClose: 20.225603
    },
    {
        Date: "2015-04-22",
        Open: 21.870001,
        High: 21.91,
        Low: 21.780001,
        Close: 21.85,
        Volume: 1165600,
        AdjClose: 20.244134
    },
    {
        Date: "2015-04-21",
        Open: 22.1,
        High: 22.190001,
        Low: 21.959999,
        Close: 22.07,
        Volume: 1694600,
        AdjClose: 20.447964
    },
    {
        Date: "2015-04-20",
        Open: 21.9,
        High: 22.02,
        Low: 21.82,
        Close: 21.84,
        Volume: 590500,
        AdjClose: 20.234869
    },
    {
        Date: "2015-04-17",
        Open: 21.77,
        High: 21.799999,
        Low: 21.67,
        Close: 21.780001,
        Volume: 1185800,
        AdjClose: 20.179279
    },
    {
        Date: "2015-04-16",
        Open: 22.049999,
        High: 22.059999,
        Low: 21.879999,
        Close: 21.969999,
        Volume: 1586900,
        AdjClose: 20.355314
    },
    {
        Date: "2015-04-15",
        Open: 21.91,
        High: 22.129999,
        Low: 21.870001,
        Close: 22.08,
        Volume: 1997600,
        AdjClose: 20.45723
    },
    {
        Date: "2015-04-14",
        Open: 21.74,
        High: 21.84,
        Low: 21.700001,
        Close: 21.799999,
        Volume: 1598900,
        AdjClose: 20.197808
    },
    {
        Date: "2015-04-13",
        Open: 21.52,
        High: 21.6,
        Low: 21.43,
        Close: 21.48,
        Volume: 814400,
        AdjClose: 19.901326
    },
    {
        Date: "2015-04-10",
        Open: 21.5,
        High: 21.549999,
        Low: 21.42,
        Close: 21.540001,
        Volume: 647400,
        AdjClose: 19.956918
    },
    {
        Date: "2015-04-09",
        Open: 21.59,
        High: 21.709999,
        Low: 21.43,
        Close: 21.57,
        Volume: 1239000,
        AdjClose: 19.984712
    },
    {
        Date: "2015-04-08",
        Open: 21.73,
        High: 21.74,
        Low: 21.43,
        Close: 21.51,
        Volume: 1118000,
        AdjClose: 19.929122
    },
    {
        Date: "2015-04-07",
        Open: 21.76,
        High: 21.809999,
        Low: 21.559999,
        Close: 21.59,
        Volume: 1144200,
        AdjClose: 20.003242
    },
    {
        Date: "2015-04-06",
        Open: 21.48,
        High: 21.809999,
        Low: 21.450001,
        Close: 21.66,
        Volume: 1395200,
        AdjClose: 20.068098
    },
    {
        Date: "2015-04-02",
        Open: 21.48,
        High: 21.629999,
        Low: 21.450001,
        Close: 21.49,
        Volume: 1599600,
        AdjClose: 19.910592
    },
    {
        Date: "2015-04-01",
        Open: 21.379999,
        High: 21.43,
        Low: 21.23,
        Close: 21.379999,
        Volume: 1684300,
        AdjClose: 19.808675
    },
    {
        Date: "2015-03-31",
        Open: 21.15,
        High: 21.27,
        Low: 21.129999,
        Close: 21.17,
        Volume: 1230500,
        AdjClose: 19.61411
    },
    {
        Date: "2015-03-30",
        Open: 21.41,
        High: 21.49,
        Low: 21.370001,
        Close: 21.370001,
        Volume: 937800,
        AdjClose: 19.799412
    },
    {
        Date: "2015-03-27",
        Open: 21.290001,
        High: 21.32,
        Low: 21.209999,
        Close: 21.309999,
        Volume: 919700,
        AdjClose: 19.74382
    },
    {
        Date: "2015-03-26",
        Open: 21.35,
        High: 21.4,
        Low: 21.200001,
        Close: 21.27,
        Volume: 1213000,
        AdjClose: 19.706761
    },
    {
        Date: "2015-03-25",
        Open: 21.66,
        High: 21.700001,
        Low: 21.389999,
        Close: 21.4,
        Volume: 1240600,
        AdjClose: 19.827206
    },
    {
        Date: "2015-03-24",
        Open: 21.809999,
        High: 21.82,
        Low: 21.610001,
        Close: 21.639999,
        Volume: 1815000,
        AdjClose: 20.049567
    },
    {
        Date: "2015-03-23",
        Open: 21.629999,
        High: 21.67,
        Low: 21.52,
        Close: 21.549999,
        Volume: 1958400,
        AdjClose: 19.966181
    },
    {
        Date: "2015-03-20",
        Open: 21.34,
        High: 21.48,
        Low: 21.23,
        Close: 21.41,
        Volume: 3797000,
        AdjClose: 19.836471
    },
    {
        Date: "2015-03-19",
        Open: 20.9,
        High: 20.91,
        Low: 20.700001,
        Close: 20.799999,
        Volume: 1637300,
        AdjClose: 19.271303
    },
    {
        Date: "2015-03-18",
        Open: 20.540001,
        High: 21.129999,
        Low: 20.49,
        Close: 21.040001,
        Volume: 2354100,
        AdjClose: 19.493665
    },
    {
        Date: "2015-03-17",
        Open: 20.379999,
        High: 20.4,
        Low: 20.290001,
        Close: 20.389999,
        Volume: 1550000,
        AdjClose: 18.891436
    },
    {
        Date: "2015-03-16",
        Open: 20.26,
        High: 20.41,
        Low: 20.25,
        Close: 20.389999,
        Volume: 1400500,
        AdjClose: 18.891436
    },
    {
        Date: "2015-03-13",
        Open: 20.059999,
        High: 20.18,
        Low: 19.93,
        Close: 20.16,
        Volume: 1585200,
        AdjClose: 18.67834
    },
    {
        Date: "2015-03-12",
        Open: 20.17,
        High: 20.23,
        Low: 20.07,
        Close: 20.200001,
        Volume: 1615500,
        AdjClose: 18.715401
    },
    {
        Date: "2015-03-11",
        Open: 20.18,
        High: 20.26,
        Low: 20.1,
        Close: 20.17,
        Volume: 1371400,
        AdjClose: 18.687605
    },
    {
        Date: "2015-03-10",
        Open: 20.209999,
        High: 20.299999,
        Low: 20.18,
        Close: 20.200001,
        Volume: 1239100,
        AdjClose: 18.715401
    },
    {
        Date: "2015-03-09",
        Open: 20.809999,
        High: 20.82,
        Low: 20.6,
        Close: 20.75,
        Volume: 2009200,
        AdjClose: 19.224978
    },
    {
        Date: "2015-03-06",
        Open: 21,
        High: 21.02,
        Low: 20.719999,
        Close: 20.75,
        Volume: 1356400,
        AdjClose: 19.224978
    },
    {
        Date: "2015-03-05",
        Open: 21.110001,
        High: 21.17,
        Low: 20.98,
        Close: 21.049999,
        Volume: 966600,
        AdjClose: 19.502929
    },
    {
        Date: "2015-03-04",
        Open: 21.030001,
        High: 21.139999,
        Low: 20.83,
        Close: 21.139999,
        Volume: 1321400,
        AdjClose: 19.586314
    },
    {
        Date: "2015-03-03",
        Open: 21.209999,
        High: 21.33,
        Low: 21.120001,
        Close: 21.190001,
        Volume: 1014000,
        AdjClose: 19.632641
    },
    {
        Date: "2015-03-02",
        Open: 21.4,
        High: 21.51,
        Low: 21.370001,
        Close: 21.450001,
        Volume: 1476400,
        AdjClose: 19.873532
    },
    {
        Date: "2015-02-27",
        Open: 21.540001,
        High: 21.57,
        Low: 21.370001,
        Close: 21.389999,
        Volume: 1881300,
        AdjClose: 19.817941
    },
    {
        Date: "2015-02-26",
        Open: 21.389999,
        High: 21.48,
        Low: 21.34,
        Close: 21.379999,
        Volume: 968100,
        AdjClose: 19.808675
    },
    {
        Date: "2015-02-25",
        Open: 21.360001,
        High: 21.440001,
        Low: 21.290001,
        Close: 21.389999,
        Volume: 1034400,
        AdjClose: 19.817941
    },
    {
        Date: "2015-02-24",
        Open: 21.370001,
        High: 21.5,
        Low: 21.35,
        Close: 21.42,
        Volume: 1139800,
        AdjClose: 19.845737
    },
    {
        Date: "2015-02-23",
        Open: 21.440001,
        High: 21.459999,
        Low: 21.33,
        Close: 21.379999,
        Volume: 1061700,
        AdjClose: 19.808675
    },
    {
        Date: "2015-02-20",
        Open: 21.309999,
        High: 21.620001,
        Low: 21.209999,
        Close: 21.57,
        Volume: 1979300,
        AdjClose: 19.984712
    },
    {
        Date: "2015-02-19",
        Open: 21.280001,
        High: 21.33,
        Low: 21.18,
        Close: 21.200001,
        Volume: 1387700,
        AdjClose: 19.641906
    },
    {
        Date: "2015-02-18",
        Open: 21.299999,
        High: 21.379999,
        Low: 21.24,
        Close: 21.32,
        Volume: 1297700,
        AdjClose: 19.753086
    },
    {
        Date: "2015-02-17",
        Open: 21.34,
        High: 21.360001,
        Low: 21.15,
        Close: 21.24,
        Volume: 1842500,
        AdjClose: 19.678965
    },
    {
        Date: "2015-02-13",
        Open: 21.379999,
        High: 21.459999,
        Low: 21.27,
        Close: 21.34,
        Volume: 1579000,
        AdjClose: 19.771616
    },
    {
        Date: "2015-02-12",
        Open: 20.91,
        High: 21.120001,
        Low: 20.9,
        Close: 21.07,
        Volume: 1415700,
        AdjClose: 19.521459
    },
    {
        Date: "2015-02-11",
        Open: 20.790001,
        High: 20.809999,
        Low: 20.639999,
        Close: 20.76,
        Volume: 2038900,
        AdjClose: 19.234243
    },
    {
        Date: "2015-02-10",
        Open: 20.77,
        High: 20.84,
        Low: 20.66,
        Close: 20.84,
        Volume: 2504500,
        AdjClose: 19.308364
    },
    {
        Date: "2015-02-09",
        Open: 20.549999,
        High: 20.809999,
        Low: 20.549999,
        Close: 20.76,
        Volume: 5050600,
        AdjClose: 19.234243
    },
    {
        Date: "2015-02-06",
        Open: 20.52,
        High: 20.58,
        Low: 20.27,
        Close: 20.33,
        Volume: 2135400,
        AdjClose: 18.835846
    },
    {
        Date: "2015-02-05",
        Open: 20.190001,
        High: 20.440001,
        Low: 20.16,
        Close: 20.360001,
        Volume: 2581800,
        AdjClose: 18.863642
    },
    {
        Date: "2015-02-04",
        Open: 19.85,
        High: 20.059999,
        Low: 19.74,
        Close: 19.84,
        Volume: 3511000,
        AdjClose: 18.381859
    },
    {
        Date: "2015-02-03",
        Open: 19.77,
        High: 20.030001,
        Low: 19.77,
        Close: 19.950001,
        Volume: 2623600,
        AdjClose: 18.483775
    },
    {
        Date: "2015-02-02",
        Open: 19.34,
        High: 19.59,
        Low: 19.280001,
        Close: 19.549999,
        Volume: 2604300,
        AdjClose: 18.113171
    },
    {
        Date: "2015-01-30",
        Open: 19.219999,
        High: 19.34,
        Low: 19.139999,
        Close: 19.139999,
        Volume: 2333200,
        AdjClose: 17.733305
    },
    {
        Date: "2015-01-29",
        Open: 19.379999,
        High: 19.440001,
        Low: 19.25,
        Close: 19.389999,
        Volume: 2257700,
        AdjClose: 17.964931
    },
    {
        Date: "2015-01-28",
        Open: 19.52,
        High: 19.68,
        Low: 19.360001,
        Close: 19.379999,
        Volume: 2810500,
        AdjClose: 17.955666
    },
    {
        Date: "2015-01-27",
        Open: 19.620001,
        High: 19.76,
        Low: 19.58,
        Close: 19.68,
        Volume: 1485900,
        AdjClose: 18.233618
    },
    {
        Date: "2015-01-26",
        Open: 19.77,
        High: 19.92,
        Low: 19.73,
        Close: 19.84,
        Volume: 1988500,
        AdjClose: 18.381859
    },
    {
        Date: "2015-01-23",
        Open: 19.85,
        High: 19.950001,
        Low: 19.780001,
        Close: 19.799999,
        Volume: 1923200,
        AdjClose: 18.344798
    },
    {
        Date: "2015-01-22",
        Open: 20,
        High: 20.07,
        Low: 19.889999,
        Close: 20.01,
        Volume: 2188000,
        AdjClose: 18.539365
    },
    {
        Date: "2015-01-21",
        Open: 19.790001,
        High: 19.9,
        Low: 19.59,
        Close: 19.780001,
        Volume: 6499400,
        AdjClose: 18.326269
    },
    {
        Date: "2015-01-20",
        Open: 19.700001,
        High: 19.75,
        Low: 19.459999,
        Close: 19.610001,
        Volume: 2807600,
        AdjClose: 18.168763
    },
    {
        Date: "2015-01-16",
        Open: 19.93,
        High: 20.049999,
        Low: 19.57,
        Close: 19.9,
        Volume: 4130000,
        AdjClose: 18.437449
    },
    {
        Date: "2015-01-15",
        Open: 20.09,
        High: 20.209999,
        Low: 19.700001,
        Close: 20.120001,
        Volume: 6556800,
        AdjClose: 18.641281
    },
    {
        Date: "2015-01-14",
        Open: 19.440001,
        High: 19.51,
        Low: 19.26,
        Close: 19.5,
        Volume: 1562000,
        AdjClose: 18.066847
    },
    {
        Date: "2015-01-13",
        Open: 19.84,
        High: 19.93,
        Low: 19.51,
        Close: 19.65,
        Volume: 2107900,
        AdjClose: 18.205822
    },
    {
        Date: "2015-01-12",
        Open: 20.139999,
        High: 20.139999,
        Low: 19.700001,
        Close: 19.790001,
        Volume: 2597400,
        AdjClose: 18.335534
    },
    {
        Date: "2015-01-09",
        Open: 20.040001,
        High: 20.16,
        Low: 19.940001,
        Close: 20.09,
        Volume: 1626500,
        AdjClose: 18.613485
    },
    {
        Date: "2015-01-08",
        Open: 20.059999,
        High: 20.4,
        Low: 20.049999,
        Close: 20.309999,
        Volume: 2050000,
        AdjClose: 18.817315
    },
    {
        Date: "2015-01-07",
        Open: 20.09,
        High: 20.16,
        Low: 19.92,
        Close: 20.049999,
        Volume: 2439200,
        AdjClose: 18.576424
    },
    {
        Date: "2015-01-06",
        Open: 20.290001,
        High: 20.42,
        Low: 20.040001,
        Close: 20.1,
        Volume: 1772000,
        AdjClose: 18.62275
    },
    {
        Date: "2015-01-05",
        Open: 20.57,
        High: 20.6,
        Low: 20.33,
        Close: 20.379999,
        Volume: 1566400,
        AdjClose: 18.882171
    },
    {
        Date: "2015-01-02",
        Open: 21.15,
        High: 21.209999,
        Low: 20.969999,
        Close: 21.08,
        Volume: 1255500,
        AdjClose: 19.530725
    },
    {
        Date: "2014-12-31",
        Open: 21.440001,
        High: 21.440001,
        Low: 21.139999,
        Close: 21.15,
        Volume: 849400,
        AdjClose: 19.59558
    },
    {
        Date: "2014-12-30",
        Open: 21.5,
        High: 21.51,
        Low: 21.389999,
        Close: 21.4,
        Volume: 628100,
        AdjClose: 19.827206
    },
    {
        Date: "2014-12-29",
        Open: 21.530001,
        High: 21.67,
        Low: 21.5,
        Close: 21.559999,
        Volume: 935200,
        AdjClose: 19.975447
    },
    {
        Date: "2014-12-26",
        Open: 21.469999,
        High: 21.73,
        Low: 21.469999,
        Close: 21.68,
        Volume: 719900,
        AdjClose: 20.086628
    },
    {
        Date: "2014-12-24",
        Open: 21.549999,
        High: 21.66,
        Low: 21.549999,
        Close: 21.6,
        Volume: 401200,
        AdjClose: 20.012508
    },
    {
        Date: "2014-12-23",
        Open: 21.48,
        High: 21.620001,
        Low: 21.469999,
        Close: 21.57,
        Volume: 1121900,
        AdjClose: 19.984712
    },
    {
        Date: "2014-12-22",
        Open: 21.48,
        High: 21.48,
        Low: 21.360001,
        Close: 21.43,
        Volume: 1036000,
        AdjClose: 19.855002
    },
    {
        Date: "2014-12-19",
        Open: 21.219999,
        High: 21.440001,
        Low: 21.18,
        Close: 21.41,
        Volume: 2999600,
        AdjClose: 19.836471
    },
    {
        Date: "2014-12-18",
        Open: 21.07,
        High: 21.219999,
        Low: 21,
        Close: 21.219999,
        Volume: 5047100,
        AdjClose: 19.660435
    },
    {
        Date: "2014-12-17",
        Open: 20.790001,
        High: 21.040001,
        Low: 20.690001,
        Close: 20.85,
        Volume: 3043800,
        AdjClose: 19.317629
    },
    {
        Date: "2014-12-16",
        Open: 20.57,
        High: 21.040001,
        Low: 20.549999,
        Close: 20.73,
        Volume: 2138300,
        AdjClose: 19.206448
    },
    {
        Date: "2014-12-15",
        Open: 20.85,
        High: 20.889999,
        Low: 20.43,
        Close: 20.5,
        Volume: 2229400,
        AdjClose: 18.993352
    },
    {
        Date: "2014-12-12",
        Open: 21.030001,
        High: 21.1,
        Low: 20.709999,
        Close: 20.719999,
        Volume: 2006700,
        AdjClose: 19.197182
    },
    {
        Date: "2014-12-11",
        Open: 21.190001,
        High: 21.309999,
        Low: 21.049999,
        Close: 21.08,
        Volume: 1682400,
        AdjClose: 19.530725
    },
    {
        Date: "2014-12-10",
        Open: 21.389999,
        High: 21.42,
        Low: 21.1,
        Close: 21.200001,
        Volume: 1652400,
        AdjClose: 19.641906
    },
    {
        Date: "2014-12-09",
        Open: 21.42,
        High: 21.639999,
        Low: 21.41,
        Close: 21.610001,
        Volume: 1697300,
        AdjClose: 20.021773
    },
    {
        Date: "2014-12-08",
        Open: 21.68,
        High: 21.74,
        Low: 21.5,
        Close: 21.559999,
        Volume: 1671600,
        AdjClose: 19.975447
    },
    {
        Date: "2014-12-05",
        Open: 21.65,
        High: 21.83,
        Low: 21.629999,
        Close: 21.74,
        Volume: 1712000,
        AdjClose: 20.142218
    },
    {
        Date: "2014-12-04",
        Open: 21.959999,
        High: 22.15,
        Low: 21.92,
        Close: 22.030001,
        Volume: 1596100,
        AdjClose: 20.410905
    },
    {
        Date: "2014-12-03",
        Open: 21.91,
        High: 22.120001,
        Low: 21.9,
        Close: 22.1,
        Volume: 2155200,
        AdjClose: 20.47576
    },
    {
        Date: "2014-12-02",
        Open: 22.01,
        High: 22.030001,
        Low: 21.860001,
        Close: 21.969999,
        Volume: 1849400,
        AdjClose: 20.355314
    },
    {
        Date: "2014-12-01",
        Open: 22.16,
        High: 22.18,
        Low: 21.99,
        Close: 22.139999,
        Volume: 1644500,
        AdjClose: 20.512819
    },
    {
        Date: "2014-11-28",
        Open: 22.48,
        High: 22.51,
        Low: 22.42,
        Close: 22.42,
        Volume: 703300,
        AdjClose: 20.772241
    },
    {
        Date: "2014-11-26",
        Open: 22.940001,
        High: 22.959999,
        Low: 22.85,
        Close: 22.889999,
        Volume: 611600,
        AdjClose: 21.207698
    },
    {
        Date: "2014-11-25",
        Open: 22.940001,
        High: 23.059999,
        Low: 22.92,
        Close: 23.02,
        Volume: 758200,
        AdjClose: 21.328145
    },
    {
        Date: "2014-11-24",
        Open: 22.889999,
        High: 22.98,
        Low: 22.85,
        Close: 22.950001,
        Volume: 1125300,
        AdjClose: 21.26329
    },
    {
        Date: "2014-11-21",
        Open: 22.809999,
        High: 22.85,
        Low: 22.639999,
        Close: 22.76,
        Volume: 1431400,
        AdjClose: 21.087253
    },
    {
        Date: "2014-11-20",
        Open: 22.52,
        High: 22.629999,
        Low: 22.49,
        Close: 22.59,
        Volume: 1153100,
        AdjClose: 20.929747
    },
    {
        Date: "2014-11-19",
        Open: 22.67,
        High: 22.700001,
        Low: 22.5,
        Close: 22.59,
        Volume: 1175500,
        AdjClose: 20.929747
    },
    {
        Date: "2014-11-18",
        Open: 22.58,
        High: 22.67,
        Low: 22.530001,
        Close: 22.629999,
        Volume: 1550900,
        AdjClose: 20.966807
    },
    {
        Date: "2014-11-17",
        Open: 22.33,
        High: 22.35,
        Low: 22.25,
        Close: 22.280001,
        Volume: 828800,
        AdjClose: 20.642531
    },
    {
        Date: "2014-11-14",
        Open: 22.120001,
        High: 22.370001,
        Low: 22.110001,
        Close: 22.309999,
        Volume: 1464000,
        AdjClose: 20.670325
    },
    {
        Date: "2014-11-13",
        Open: 22.33,
        High: 22.5,
        Low: 21.93,
        Close: 22.07,
        Volume: 2820700,
        AdjClose: 20.447964
    },
    {
        Date: "2014-11-12",
        Open: 22.23,
        High: 22.32,
        Low: 22.200001,
        Close: 22.280001,
        Volume: 1041900,
        AdjClose: 20.642531
    },
    {
        Date: "2014-11-11",
        Open: 22.26,
        High: 22.42,
        Low: 22.23,
        Close: 22.4,
        Volume: 1001100,
        AdjClose: 20.753711
    },
    {
        Date: "2014-11-10",
        Open: 22.4,
        High: 22.440001,
        Low: 22.34,
        Close: 22.389999,
        Volume: 1206600,
        AdjClose: 20.744446
    },
    {
        Date: "2014-11-07",
        Open: 22.120001,
        High: 22.200001,
        Low: 22.07,
        Close: 22.15,
        Volume: 1316800,
        AdjClose: 20.522085
    },
    {
        Date: "2014-11-06",
        Open: 22.33,
        High: 22.35,
        Low: 22.17,
        Close: 22.190001,
        Volume: 1699000,
        AdjClose: 20.559146
    },
    {
        Date: "2014-11-05",
        Open: 22.059999,
        High: 22.16,
        Low: 21.959999,
        Close: 22.110001,
        Volume: 2673600,
        AdjClose: 20.485025
    },
    {
        Date: "2014-11-04",
        Open: 21.719999,
        High: 21.780001,
        Low: 21.629999,
        Close: 21.68,
        Volume: 1096000,
        AdjClose: 20.086628
    },
    {
        Date: "2014-11-03",
        Open: 21.9,
        High: 21.92,
        Low: 21.67,
        Close: 21.809999,
        Volume: 791100,
        AdjClose: 20.207073
    },
    {
        Date: "2014-10-31",
        Open: 21.940001,
        High: 21.959999,
        Low: 21.84,
        Close: 21.940001,
        Volume: 650600,
        AdjClose: 20.32752
    },
    {
        Date: "2014-10-30",
        Open: 21.549999,
        High: 21.780001,
        Low: 21.5,
        Close: 21.73,
        Volume: 1342800,
        AdjClose: 20.132953
    },
    {
        Date: "2014-10-29",
        Open: 21.92,
        High: 21.950001,
        Low: 21.49,
        Close: 21.57,
        Volume: 1542600,
        AdjClose: 19.984712
    },
    {
        Date: "2014-10-28",
        Open: 21.59,
        High: 21.780001,
        Low: 21.559999,
        Close: 21.77,
        Volume: 1234400,
        AdjClose: 20.170014
    },
    {
        Date: "2014-10-27",
        Open: 21.24,
        High: 21.35,
        Low: 21.15,
        Close: 21.25,
        Volume: 945100,
        AdjClose: 19.688231
    },
    {
        Date: "2014-10-24",
        Open: 21.309999,
        High: 21.48,
        Low: 21.24,
        Close: 21.43,
        Volume: 1597600,
        AdjClose: 19.855002
    },
    {
        Date: "2014-10-23",
        Open: 21.32,
        High: 21.540001,
        Low: 21.309999,
        Close: 21.389999,
        Volume: 1895600,
        AdjClose: 19.817941
    },
    {
        Date: "2014-10-22",
        Open: 21.280001,
        High: 21.4,
        Low: 21.030001,
        Close: 21.09,
        Volume: 3944700,
        AdjClose: 19.53999
    },
    {
        Date: "2014-10-21",
        Open: 20.790001,
        High: 20.98,
        Low: 20.76,
        Close: 20.969999,
        Volume: 1915500,
        AdjClose: 19.428809
    },
    {
        Date: "2014-10-20",
        Open: 20.610001,
        High: 20.67,
        Low: 20.48,
        Close: 20.639999,
        Volume: 2037000,
        AdjClose: 19.123062
    },
    {
        Date: "2014-10-17",
        Open: 20.51,
        High: 20.620001,
        Low: 20.379999,
        Close: 20.440001,
        Volume: 1668700,
        AdjClose: 18.937762
    },
    {
        Date: "2014-10-16",
        Open: 19.940001,
        High: 20.52,
        Low: 19.92,
        Close: 20.370001,
        Volume: 2330900,
        AdjClose: 18.872907
    },
    {
        Date: "2014-10-15",
        Open: 20.32,
        High: 20.51,
        Low: 20.1,
        Close: 20.469999,
        Volume: 2659000,
        AdjClose: 18.965556
    },
    {
        Date: "2014-10-14",
        Open: 20.610001,
        High: 20.799999,
        Low: 20.48,
        Close: 20.540001,
        Volume: 2039700,
        AdjClose: 19.030413
    },
    {
        Date: "2014-10-13",
        Open: 20.77,
        High: 20.809999,
        Low: 20.41,
        Close: 20.41,
        Volume: 2492400,
        AdjClose: 18.909966
    },
    {
        Date: "2014-10-10",
        Open: 20.6,
        High: 20.690001,
        Low: 20.389999,
        Close: 20.4,
        Volume: 2235200,
        AdjClose: 18.900701
    },
    {
        Date: "2014-10-09",
        Open: 21.370001,
        High: 21.41,
        Low: 20.84,
        Close: 20.889999,
        Volume: 3527100,
        AdjClose: 19.354688
    },
    {
        Date: "2014-10-08",
        Open: 21.370001,
        High: 21.66,
        Low: 21.16,
        Close: 21.620001,
        Volume: 5912200,
        AdjClose: 20.031038
    },
    {
        Date: "2014-10-07",
        Open: 21.610001,
        High: 21.690001,
        Low: 21.5,
        Close: 21.5,
        Volume: 3305700,
        AdjClose: 19.919857
    },
    {
        Date: "2014-10-06",
        Open: 21.889999,
        High: 22,
        Low: 21.76,
        Close: 21.950001,
        Volume: 3812900,
        AdjClose: 20.336785
    },
    {
        Date: "2014-10-03",
        Open: 21.57,
        High: 21.68,
        Low: 21.559999,
        Close: 21.629999,
        Volume: 1967800,
        AdjClose: 20.040302
    },
    {
        Date: "2014-10-02",
        Open: 22.049999,
        High: 22.08,
        Low: 21.700001,
        Close: 21.91,
        Volume: 1758400,
        AdjClose: 20.299724
    },
    {
        Date: "2014-10-01",
        Open: 22.32,
        High: 22.34,
        Low: 22.049999,
        Close: 22.120001,
        Volume: 1788000,
        AdjClose: 20.494291
    },
    {
        Date: "2014-09-30",
        Open: 22.370001,
        High: 22.52,
        Low: 22.309999,
        Close: 22.41,
        Volume: 1869800,
        AdjClose: 20.762976
    },
    {
        Date: "2014-09-29",
        Open: 22.530001,
        High: 22.610001,
        Low: 22.450001,
        Close: 22.52,
        Volume: 1089500,
        AdjClose: 20.864892
    },
    {
        Date: "2014-09-26",
        Open: 22.76,
        High: 22.790001,
        Low: 22.6,
        Close: 22.690001,
        Volume: 1277600,
        AdjClose: 21.022398
    },
    {
        Date: "2014-09-25",
        Open: 22.85,
        High: 22.860001,
        Low: 22.51,
        Close: 22.559999,
        Volume: 2161700,
        AdjClose: 20.901952
    },
    {
        Date: "2014-09-24",
        Open: 22.709999,
        High: 22.85,
        Low: 22.67,
        Close: 22.799999,
        Volume: 1297000,
        AdjClose: 21.124313
    },
    {
        Date: "2014-09-23",
        Open: 23,
        High: 23.08,
        Low: 22.9,
        Close: 22.969999,
        Volume: 4285000,
        AdjClose: 21.281819
    },
    {
        Date: "2014-09-22",
        Open: 23.17,
        High: 23.17,
        Low: 22.889999,
        Close: 23.01,
        Volume: 1985100,
        AdjClose: 21.31888
    },
    {
        Date: "2014-09-19",
        Open: 23.16,
        High: 23.17,
        Low: 22.940001,
        Close: 23,
        Volume: 1359000,
        AdjClose: 21.309614
    },
    {
        Date: "2014-09-18",
        Open: 23.01,
        High: 23.139999,
        Low: 22.99,
        Close: 23.08,
        Volume: 1132100,
        AdjClose: 21.383735
    },
    {
        Date: "2014-09-17",
        Open: 23,
        High: 23.209999,
        Low: 22.969999,
        Close: 23.02,
        Volume: 3013000,
        AdjClose: 21.328145
    },
    {
        Date: "2014-09-16",
        Open: 22.92,
        High: 23.18,
        Low: 22.870001,
        Close: 23.09,
        Volume: 2017900,
        AdjClose: 21.393
    },
    {
        Date: "2014-09-15",
        Open: 22.9,
        High: 22.9,
        Low: 22.75,
        Close: 22.799999,
        Volume: 1150900,
        AdjClose: 21.124313
    },
    {
        Date: "2014-09-12",
        Open: 22.879999,
        High: 22.959999,
        Low: 22.85,
        Close: 22.889999,
        Volume: 962200,
        AdjClose: 21.207698
    },
    {
        Date: "2014-09-11",
        Open: 22.790001,
        High: 22.969999,
        Low: 22.76,
        Close: 22.92,
        Volume: 2828200,
        AdjClose: 21.235494
    },
    {
        Date: "2014-09-10",
        Open: 22.889999,
        High: 23.02,
        Low: 22.85,
        Close: 23,
        Volume: 1782000,
        AdjClose: 21.309614
    },
    {
        Date: "2014-09-09",
        Open: 23.32,
        High: 23.34,
        Low: 23.01,
        Close: 23.08,
        Volume: 1379400,
        AdjClose: 21.383735
    },
    {
        Date: "2014-09-08",
        Open: 23,
        High: 23.120001,
        Low: 22.870001,
        Close: 22.91,
        Volume: 1315200,
        AdjClose: 21.226229
    },
    {
        Date: "2014-09-05",
        Open: 23.09,
        High: 23.129999,
        Low: 22.969999,
        Close: 23.1,
        Volume: 685900,
        AdjClose: 21.402265
    },
    {
        Date: "2014-09-04",
        Open: 23.309999,
        High: 23.35,
        Low: 23.02,
        Close: 23.110001,
        Volume: 1220800,
        AdjClose: 21.41153
    },
    {
        Date: "2014-09-03",
        Open: 23.450001,
        High: 23.5,
        Low: 23.27,
        Close: 23.32,
        Volume: 1858500,
        AdjClose: 21.606096
    },
    {
        Date: "2014-09-02",
        Open: 22.92,
        High: 23,
        Low: 22.860001,
        Close: 22.969999,
        Volume: 2527300,
        AdjClose: 21.281819
    },
    {
        Date: "2014-08-29",
        Open: 22.790001,
        High: 22.790001,
        Low: 22.690001,
        Close: 22.77,
        Volume: 732100,
        AdjClose: 21.096519
    },
    {
        Date: "2014-08-28",
        Open: 22.719999,
        High: 22.860001,
        Low: 22.709999,
        Close: 22.809999,
        Volume: 562300,
        AdjClose: 21.133578
    },
    {
        Date: "2014-08-27",
        Open: 23.02,
        High: 23.030001,
        Low: 22.85,
        Close: 22.879999,
        Volume: 821100,
        AdjClose: 21.198433
    },
    {
        Date: "2014-08-26",
        Open: 22.93,
        High: 23.01,
        Low: 22.91,
        Close: 22.969999,
        Volume: 1271400,
        AdjClose: 21.281819
    },
    {
        Date: "2014-08-25",
        Open: 22.809999,
        High: 22.950001,
        Low: 22.790001,
        Close: 22.870001,
        Volume: 1145700,
        AdjClose: 21.189169
    },
    {
        Date: "2014-08-22",
        Open: 22.76,
        High: 22.799999,
        Low: 22.610001,
        Close: 22.73,
        Volume: 2656000,
        AdjClose: 21.059458
    },
    {
        Date: "2014-08-21",
        Open: 22.84,
        High: 22.969999,
        Low: 22.83,
        Close: 22.9,
        Volume: 1324100,
        AdjClose: 21.216963
    },
    {
        Date: "2014-08-20",
        Open: 22.530001,
        High: 22.719999,
        Low: 22.530001,
        Close: 22.68,
        Volume: 1309900,
        AdjClose: 21.013133
    },
    {
        Date: "2014-08-19",
        Open: 22.700001,
        High: 22.809999,
        Low: 22.67,
        Close: 22.790001,
        Volume: 1476700,
        AdjClose: 21.115049
    },
    {
        Date: "2014-08-18",
        Open: 22.559999,
        High: 22.620001,
        Low: 22.51,
        Close: 22.610001,
        Volume: 979800,
        AdjClose: 20.948278
    },
    {
        Date: "2014-08-15",
        Open: 22.790001,
        High: 22.83,
        Low: 22.4,
        Close: 22.620001,
        Volume: 1246000,
        AdjClose: 20.957543
    },
    {
        Date: "2014-08-14",
        Open: 22.68,
        High: 22.690001,
        Low: 22.610001,
        Close: 22.65,
        Volume: 674300,
        AdjClose: 20.985337
    },
    {
        Date: "2014-08-13",
        Open: 22.58,
        High: 22.610001,
        Low: 22.5,
        Close: 22.52,
        Volume: 1120700,
        AdjClose: 20.864892
    },
    {
        Date: "2014-08-12",
        Open: 22.43,
        High: 22.51,
        Low: 22.41,
        Close: 22.450001,
        Volume: 688700,
        AdjClose: 20.800037
    },
    {
        Date: "2014-08-11",
        Open: 22.58,
        High: 22.629999,
        Low: 22.51,
        Close: 22.530001,
        Volume: 765000,
        AdjClose: 20.874158
    },
    {
        Date: "2014-08-08",
        Open: 22.32,
        High: 22.48,
        Low: 22.200001,
        Close: 22.469999,
        Volume: 1126900,
        AdjClose: 20.818566
    },
    {
        Date: "2014-08-07",
        Open: 22.450001,
        High: 22.5,
        Low: 22.23,
        Close: 22.27,
        Volume: 1337500,
        AdjClose: 20.633266
    },
    {
        Date: "2014-08-06",
        Open: 22.360001,
        High: 22.620001,
        Low: 22.34,
        Close: 22.540001,
        Volume: 978300,
        AdjClose: 20.883423
    },
    {
        Date: "2014-08-05",
        Open: 22.66,
        High: 22.719999,
        Low: 22.43,
        Close: 22.540001,
        Volume: 1631900,
        AdjClose: 20.883423
    },
    {
        Date: "2014-08-04",
        Open: 22.85,
        High: 22.879999,
        Low: 22.67,
        Close: 22.83,
        Volume: 1190700,
        AdjClose: 21.152108
    },
    {
        Date: "2014-08-01",
        Open: 22.889999,
        High: 23.040001,
        Low: 22.73,
        Close: 22.83,
        Volume: 1340700,
        AdjClose: 21.152108
    },
    {
        Date: "2014-07-31",
        Open: 23.17,
        High: 23.26,
        Low: 22.98,
        Close: 23,
        Volume: 1392100,
        AdjClose: 21.309614
    },
    {
        Date: "2014-07-30",
        Open: 23.48,
        High: 23.58,
        Low: 23.34,
        Close: 23.469999,
        Volume: 2188400,
        AdjClose: 21.745071
    },
    {
        Date: "2014-07-29",
        Open: 23.84,
        High: 23.889999,
        Low: 23.559999,
        Close: 23.57,
        Volume: 1916800,
        AdjClose: 21.837722
    },
    {
        Date: "2014-07-28",
        Open: 23.950001,
        High: 24.059999,
        Low: 23.879999,
        Close: 24,
        Volume: 1593500,
        AdjClose: 22.236119
    },
    {
        Date: "2014-07-25",
        Open: 23.93,
        High: 23.950001,
        Low: 23.780001,
        Close: 23.84,
        Volume: 1325400,
        AdjClose: 22.087879
    },
    {
        Date: "2014-07-24",
        Open: 24.129999,
        High: 24.15,
        Low: 23.959999,
        Close: 24.01,
        Volume: 2229000,
        AdjClose: 22.245385
    },
    {
        Date: "2014-07-23",
        Open: 23.459999,
        High: 23.84,
        Low: 23.389999,
        Close: 23.790001,
        Volume: 4451600,
        AdjClose: 22.041554
    },
    {
        Date: "2014-07-22",
        Open: 23.17,
        High: 23.51,
        Low: 23.139999,
        Close: 23.43,
        Volume: 6845900,
        AdjClose: 21.708012
    },
    {
        Date: "2014-07-21",
        Open: 22.620001,
        High: 22.889999,
        Low: 22.610001,
        Close: 22.799999,
        Volume: 1938300,
        AdjClose: 21.124313
    },
    {
        Date: "2014-07-18",
        Open: 22.67,
        High: 22.82,
        Low: 22.66,
        Close: 22.790001,
        Volume: 1287500,
        AdjClose: 21.115049
    },
    {
        Date: "2014-07-17",
        Open: 22.959999,
        High: 23.059999,
        Low: 22.77,
        Close: 22.790001,
        Volume: 2287800,
        AdjClose: 21.115049
    },
    {
        Date: "2014-07-16",
        Open: 22.940001,
        High: 23.02,
        Low: 22.860001,
        Close: 22.940001,
        Volume: 1082200,
        AdjClose: 21.254024
    },
    {
        Date: "2014-07-15",
        Open: 22.790001,
        High: 22.889999,
        Low: 22.709999,
        Close: 22.77,
        Volume: 1001100,
        AdjClose: 21.096519
    },
    {
        Date: "2014-07-14",
        Open: 23.030001,
        High: 23.040001,
        Low: 22.860001,
        Close: 22.870001,
        Volume: 1255800,
        AdjClose: 21.189169
    },
    {
        Date: "2014-07-11",
        Open: 22.73,
        High: 22.809999,
        Low: 22.66,
        Close: 22.790001,
        Volume: 863900,
        AdjClose: 21.115049
    },
    {
        Date: "2014-07-10",
        Open: 22.65,
        High: 22.809999,
        Low: 22.629999,
        Close: 22.790001,
        Volume: 2008300,
        AdjClose: 21.115049
    },
    {
        Date: "2014-07-09",
        Open: 22.860001,
        High: 23.030001,
        Low: 22.84,
        Close: 23.02,
        Volume: 1679600,
        AdjClose: 21.328145
    },
    {
        Date: "2014-07-08",
        Open: 23.030001,
        High: 23.040001,
        Low: 22.799999,
        Close: 22.85,
        Volume: 1818800,
        AdjClose: 21.170639
    },
    {
        Date: "2014-07-07",
        Open: 22.83,
        High: 23,
        Low: 22.83,
        Close: 22.93,
        Volume: 2309400,
        AdjClose: 21.244759
    },
    {
        Date: "2014-07-03",
        Open: 22.940001,
        High: 23.049999,
        Low: 22.83,
        Close: 23.02,
        Volume: 2098000,
        AdjClose: 21.328145
    },
    {
        Date: "2014-07-02",
        Open: 22.950001,
        High: 23.01,
        Low: 22.860001,
        Close: 22.93,
        Volume: 1637400,
        AdjClose: 21.244759
    },
    {
        Date: "2014-07-01",
        Open: 22.969999,
        High: 23.110001,
        Low: 22.950001,
        Close: 23.01,
        Volume: 1070200,
        AdjClose: 21.31888
    },
    {
        Date: "2014-06-30",
        Open: 22.959999,
        High: 23.049999,
        Low: 22.92,
        Close: 23.02,
        Volume: 1087700,
        AdjClose: 21.328145
    },
    {
        Date: "2014-06-27",
        Open: 22.780001,
        High: 23,
        Low: 22.77,
        Close: 22.950001,
        Volume: 1297300,
        AdjClose: 21.26329
    },
    {
        Date: "2014-06-26",
        Open: 22.690001,
        High: 22.84,
        Low: 22.58,
        Close: 22.799999,
        Volume: 1162000,
        AdjClose: 21.124313
    },
    {
        Date: "2014-06-25",
        Open: 22.780001,
        High: 22.93,
        Low: 22.780001,
        Close: 22.84,
        Volume: 1525500,
        AdjClose: 21.161374
    },
    {
        Date: "2014-06-24",
        Open: 23.01,
        High: 23.110001,
        Low: 22.9,
        Close: 22.940001,
        Volume: 1512000,
        AdjClose: 21.254024
    },
    {
        Date: "2014-06-23",
        Open: 23.02,
        High: 23.08,
        Low: 22.950001,
        Close: 23.08,
        Volume: 1000200,
        AdjClose: 21.383735
    },
    {
        Date: "2014-06-20",
        Open: 23.17,
        High: 23.24,
        Low: 23.120001,
        Close: 23.219999,
        Volume: 1505600,
        AdjClose: 21.513445
    },
    {
        Date: "2014-06-19",
        Open: 23.379999,
        High: 23.379999,
        Low: 23.25,
        Close: 23.280001,
        Volume: 849000,
        AdjClose: 21.569036
    },
    {
        Date: "2014-06-18",
        Open: 23.370001,
        High: 23.4,
        Low: 23.18,
        Close: 23.35,
        Volume: 999900,
        AdjClose: 21.633891
    },
    {
        Date: "2014-06-17",
        Open: 23.120001,
        High: 23.309999,
        Low: 23.120001,
        Close: 23.27,
        Volume: 903400,
        AdjClose: 21.559771
    },
    {
        Date: "2014-06-16",
        Open: 23.209999,
        High: 23.309999,
        Low: 23.18,
        Close: 23.27,
        Volume: 1301300,
        AdjClose: 21.559771
    },
    {
        Date: "2014-06-13",
        Open: 23.24,
        High: 23.280001,
        Low: 23.15,
        Close: 23.27,
        Volume: 2489200,
        AdjClose: 21.559771
    },
    {
        Date: "2014-06-12",
        Open: 23.309999,
        High: 23.4,
        Low: 23.25,
        Close: 23.299999,
        Volume: 2641600,
        AdjClose: 21.587565
    },
    {
        Date: "2014-06-11",
        Open: 23.440001,
        High: 23.49,
        Low: 23.33,
        Close: 23.379999,
        Volume: 1263300,
        AdjClose: 21.661685
    },
    {
        Date: "2014-06-10",
        Open: 23.700001,
        High: 23.809999,
        Low: 23.629999,
        Close: 23.73,
        Volume: 1340800,
        AdjClose: 21.985963
    },
    {
        Date: "2014-06-09",
        Open: 23.59,
        High: 23.76,
        Low: 23.59,
        Close: 23.690001,
        Volume: 1607800,
        AdjClose: 21.948903
    },
    {
        Date: "2014-06-06",
        Open: 23.559999,
        High: 23.780001,
        Low: 23.559999,
        Close: 23.77,
        Volume: 1007300,
        AdjClose: 22.023024
    },
    {
        Date: "2014-06-05",
        Open: 23.450001,
        High: 23.620001,
        Low: 23.42,
        Close: 23.610001,
        Volume: 1725900,
        AdjClose: 21.874783
    },
    {
        Date: "2014-06-04",
        Open: 23.379999,
        High: 23.450001,
        Low: 23.379999,
        Close: 23.41,
        Volume: 1153400,
        AdjClose: 21.689481
    },
    {
        Date: "2014-06-03",
        Open: 23.620001,
        High: 23.620001,
        Low: 23.35,
        Close: 23.48,
        Volume: 3693300,
        AdjClose: 21.754336
    },
    {
        Date: "2014-06-02",
        Open: 23.690001,
        High: 23.700001,
        Low: 23.469999,
        Close: 23.48,
        Volume: 2700600,
        AdjClose: 21.754336
    },
    {
        Date: "2014-05-30",
        Open: 23.75,
        High: 23.77,
        Low: 23.66,
        Close: 23.74,
        Volume: 1407600,
        AdjClose: 21.995228
    },
    {
        Date: "2014-05-29",
        Open: 23.77,
        High: 23.809999,
        Low: 23.66,
        Close: 23.77,
        Volume: 744800,
        AdjClose: 22.023024
    },
    {
        Date: "2014-05-28",
        Open: 23.709999,
        High: 23.82,
        Low: 23.690001,
        Close: 23.75,
        Volume: 937000,
        AdjClose: 22.004493
    },
    {
        Date: "2014-05-27",
        Open: 23.93,
        High: 23.959999,
        Low: 23.75,
        Close: 23.870001,
        Volume: 1841200,
        AdjClose: 22.115674
    },
    {
        Date: "2014-05-23",
        Open: 23.74,
        High: 23.889999,
        Low: 23.709999,
        Close: 23.85,
        Volume: 4992400,
        AdjClose: 22.097144
    },
    {
        Date: "2014-05-22",
        Open: 23.6,
        High: 23.66,
        Low: 23.52,
        Close: 23.620001,
        Volume: 1337000,
        AdjClose: 21.884048
    },
    {
        Date: "2014-05-21",
        Open: 23.530001,
        High: 23.68,
        Low: 23.49,
        Close: 23.620001,
        Volume: 1484300,
        AdjClose: 21.884048
    },
    {
        Date: "2014-05-20",
        Open: 23.84,
        High: 23.85,
        Low: 23.5,
        Close: 23.58,
        Volume: 2557000,
        AdjClose: 21.846987
    },
    {
        Date: "2014-05-19",
        Open: 23.540001,
        High: 23.620001,
        Low: 23.52,
        Close: 23.6,
        Volume: 875900,
        AdjClose: 21.865518
    },
    {
        Date: "2014-05-16",
        Open: 23.57,
        High: 23.620001,
        Low: 23.469999,
        Close: 23.58,
        Volume: 1411700,
        AdjClose: 21.846987
    },
    {
        Date: "2014-05-15",
        Open: 23.629999,
        High: 23.68,
        Low: 23.48,
        Close: 23.57,
        Volume: 2665600,
        AdjClose: 21.837722
    },
    {
        Date: "2014-05-14",
        Open: 23.74,
        High: 23.83,
        Low: 23.709999,
        Close: 23.73,
        Volume: 1799500,
        AdjClose: 21.985963
    },
    {
        Date: "2014-05-13",
        Open: 23.700001,
        High: 23.719999,
        Low: 23.549999,
        Close: 23.59,
        Volume: 2612600,
        AdjClose: 21.856252
    },
    {
        Date: "2014-05-12",
        Open: 23.77,
        High: 23.85,
        Low: 23.709999,
        Close: 23.809999,
        Volume: 2859500,
        AdjClose: 22.060083
    },
    {
        Date: "2014-05-09",
        Open: 23.25,
        High: 23.33,
        Low: 23.120001,
        Close: 23.26,
        Volume: 2290500,
        AdjClose: 21.550506
    },
    {
        Date: "2014-05-08",
        Open: 23.35,
        High: 23.379999,
        Low: 23.09,
        Close: 23.15,
        Volume: 4824600,
        AdjClose: 21.44859
    },
    {
        Date: "2014-05-07",
        Open: 23.4,
        High: 23.450001,
        Low: 23.16,
        Close: 23.34,
        Volume: 3862900,
        AdjClose: 21.624626
    },
    {
        Date: "2014-05-06",
        Open: 23.440001,
        High: 23.469999,
        Low: 23.299999,
        Close: 23.34,
        Volume: 4029000,
        AdjClose: 21.624626
    },
    {
        Date: "2014-05-05",
        Open: 23.309999,
        High: 23.41,
        Low: 23.200001,
        Close: 23.370001,
        Volume: 3381400,
        AdjClose: 21.652422
    },
    {
        Date: "2014-05-02",
        Open: 24.01,
        High: 24.280001,
        Low: 23.99,
        Close: 24.139999,
        Volume: 5687100,
        AdjClose: 21.640375
    },
    {
        Date: "2014-05-01",
        Open: 23.959999,
        High: 24,
        Low: 23.59,
        Close: 23.940001,
        Volume: 3735100,
        AdjClose: 21.461086
    },
    {
        Date: "2014-04-30",
        Open: 24.26,
        High: 24.27,
        Low: 23.809999,
        Close: 23.92,
        Volume: 3024700,
        AdjClose: 21.443156
    },
    {
        Date: "2014-04-29",
        Open: 24.360001,
        High: 24.48,
        Low: 24.09,
        Close: 24.120001,
        Volume: 6843700,
        AdjClose: 21.622448
    },
    {
        Date: "2014-04-28",
        Open: 26.16,
        High: 26.35,
        Low: 25.959999,
        Close: 26.26,
        Volume: 4937800,
        AdjClose: 23.540857
    },
    {
        Date: "2014-04-25",
        Open: 26.040001,
        High: 26.1,
        Low: 25.940001,
        Close: 26,
        Volume: 889300,
        AdjClose: 23.307779
    },
    {
        Date: "2014-04-24",
        Open: 26.049999,
        High: 26.190001,
        Low: 25.91,
        Close: 26.040001,
        Volume: 997500,
        AdjClose: 23.343638
    },
    {
        Date: "2014-04-23",
        Open: 26.07,
        High: 26.07,
        Low: 25.9,
        Close: 25.959999,
        Volume: 1299200,
        AdjClose: 23.27192
    },
    {
        Date: "2014-04-22",
        Open: 26.1,
        High: 26.200001,
        Low: 26.01,
        Close: 26.02,
        Volume: 1448400,
        AdjClose: 23.325708
    },
    {
        Date: "2014-04-21",
        Open: 25.99,
        High: 26.02,
        Low: 25.83,
        Close: 25.91,
        Volume: 967200,
        AdjClose: 23.227098
    },
    {
        Date: "2014-04-17",
        Open: 25.75,
        High: 26,
        Low: 25.73,
        Close: 25.9,
        Volume: 1617500,
        AdjClose: 23.218133
    },
    {
        Date: "2014-04-16",
        Open: 25.51,
        High: 25.65,
        Low: 25.370001,
        Close: 25.629999,
        Volume: 1567500,
        AdjClose: 22.97609
    },
    {
        Date: "2014-04-15",
        Open: 25.41,
        High: 25.540001,
        Low: 25.18,
        Close: 25.49,
        Volume: 1803200,
        AdjClose: 22.850587
    },
    {
        Date: "2014-04-14",
        Open: 25.51,
        High: 25.639999,
        Low: 25.290001,
        Close: 25.52,
        Volume: 1323600,
        AdjClose: 22.877482
    },
    {
        Date: "2014-04-11",
        Open: 25.49,
        High: 25.700001,
        Low: 25.450001,
        Close: 25.51,
        Volume: 1684600,
        AdjClose: 22.868517
    },
    {
        Date: "2014-04-10",
        Open: 26.040001,
        High: 26.049999,
        Low: 25.549999,
        Close: 25.58,
        Volume: 1876800,
        AdjClose: 22.931268
    },
    {
        Date: "2014-04-09",
        Open: 26.02,
        High: 26.17,
        Low: 25.84,
        Close: 26.139999,
        Volume: 1022900,
        AdjClose: 23.433281
    },
    {
        Date: "2014-04-08",
        Open: 25.799999,
        High: 25.959999,
        Low: 25.690001,
        Close: 25.91,
        Volume: 1933700,
        AdjClose: 23.227098
    },
    {
        Date: "2014-04-07",
        Open: 26.01,
        High: 26.040001,
        Low: 25.639999,
        Close: 25.73,
        Volume: 1987500,
        AdjClose: 23.065736
    },
    {
        Date: "2014-04-04",
        Open: 26.09,
        High: 26.17,
        Low: 25.780001,
        Close: 25.84,
        Volume: 1093800,
        AdjClose: 23.164346
    },
    {
        Date: "2014-04-03",
        Open: 26.129999,
        High: 26.17,
        Low: 25.879999,
        Close: 25.98,
        Volume: 766400,
        AdjClose: 23.289849
    },
    {
        Date: "2014-04-02",
        Open: 26.110001,
        High: 26.15,
        Low: 25.99,
        Close: 26.08,
        Volume: 1093800,
        AdjClose: 23.379495
    },
    {
        Date: "2014-04-01",
        Open: 26.139999,
        High: 26.209999,
        Low: 25.98,
        Close: 26.07,
        Volume: 1575800,
        AdjClose: 23.37053
    },
    {
        Date: "2014-03-31",
        Open: 25.85,
        High: 25.889999,
        Low: 25.75,
        Close: 25.790001,
        Volume: 1097400,
        AdjClose: 23.119524
    },
    {
        Date: "2014-03-28",
        Open: 25.49,
        High: 25.66,
        Low: 25.450001,
        Close: 25.58,
        Volume: 1227300,
        AdjClose: 22.931268
    },
    {
        Date: "2014-03-27",
        Open: 25.09,
        High: 25.33,
        Low: 25.049999,
        Close: 25.200001,
        Volume: 2545500,
        AdjClose: 22.590617
    },
    {
        Date: "2014-03-26",
        Open: 25.280001,
        High: 25.360001,
        Low: 25.07,
        Close: 25.1,
        Volume: 1546200,
        AdjClose: 22.500971
    },
    {
        Date: "2014-03-25",
        Open: 24.870001,
        High: 25.110001,
        Low: 24.719999,
        Close: 25.07,
        Volume: 2962300,
        AdjClose: 22.474077
    },
    {
        Date: "2014-03-24",
        Open: 24.950001,
        High: 25.01,
        Low: 24.639999,
        Close: 24.91,
        Volume: 1277900,
        AdjClose: 22.330645
    },
    {
        Date: "2014-03-21",
        Open: 25.200001,
        High: 25.25,
        Low: 24.959999,
        Close: 24.99,
        Volume: 1240800,
        AdjClose: 22.402361
    },
    {
        Date: "2014-03-20",
        Open: 24.809999,
        High: 25.129999,
        Low: 24.74,
        Close: 25.059999,
        Volume: 855700,
        AdjClose: 22.465112
    },
    {
        Date: "2014-03-19",
        Open: 25.379999,
        High: 25.379999,
        Low: 24.76,
        Close: 24.969999,
        Volume: 1504200,
        AdjClose: 22.384431
    },
    {
        Date: "2014-03-18",
        Open: 25.08,
        High: 25.34,
        Low: 25.059999,
        Close: 25.290001,
        Volume: 1093100,
        AdjClose: 22.671298
    },
    {
        Date: "2014-03-17",
        Open: 24.91,
        High: 25.120001,
        Low: 24.91,
        Close: 24.969999,
        Volume: 1488500,
        AdjClose: 22.384431
    },
    {
        Date: "2014-03-14",
        Open: 24.6,
        High: 24.780001,
        Low: 24.58,
        Close: 24.610001,
        Volume: 1587000,
        AdjClose: 22.061709
    },
    {
        Date: "2014-03-13",
        Open: 25.370001,
        High: 25.450001,
        Low: 24.73,
        Close: 24.82,
        Volume: 1265900,
        AdjClose: 22.249964
    },
    {
        Date: "2014-03-12",
        Open: 25.15,
        High: 25.469999,
        Low: 25.09,
        Close: 25.450001,
        Volume: 2692700,
        AdjClose: 22.81473
    },
    {
        Date: "2014-03-11",
        Open: 25.370001,
        High: 25.48,
        Low: 25.09,
        Close: 25.190001,
        Volume: 1679200,
        AdjClose: 22.581652
    },
    {
        Date: "2014-03-10",
        Open: 25.4,
        High: 25.4,
        Low: 25.17,
        Close: 25.34,
        Volume: 1910900,
        AdjClose: 22.71612
    },
    {
        Date: "2014-03-07",
        Open: 25.799999,
        High: 25.809999,
        Low: 25.389999,
        Close: 25.51,
        Volume: 2392200,
        AdjClose: 22.868517
    },
    {
        Date: "2014-03-06",
        Open: 25.950001,
        High: 26,
        Low: 25.870001,
        Close: 25.93,
        Volume: 847200,
        AdjClose: 23.245027
    },
    {
        Date: "2014-03-05",
        Open: 25.700001,
        High: 25.77,
        Low: 25.639999,
        Close: 25.74,
        Volume: 1067100,
        AdjClose: 23.074701
    },
    {
        Date: "2014-03-04",
        Open: 25.610001,
        High: 25.639999,
        Low: 25.450001,
        Close: 25.48,
        Volume: 1606200,
        AdjClose: 22.841623
    },
    {
        Date: "2014-03-03",
        Open: 25.059999,
        High: 25.139999,
        Low: 24.83,
        Close: 24.950001,
        Volume: 1458400,
        AdjClose: 22.366504
    },
    {
        Date: "2014-02-28",
        Open: 25.43,
        High: 25.629999,
        Low: 25.370001,
        Close: 25.48,
        Volume: 1311800,
        AdjClose: 22.841623
    },
    {
        Date: "2014-02-27",
        Open: 25.15,
        High: 25.389999,
        Low: 25.120001,
        Close: 25.33,
        Volume: 1658500,
        AdjClose: 22.707155
    },
    {
        Date: "2014-02-26",
        Open: 25.280001,
        High: 25.370001,
        Low: 25.139999,
        Close: 25.26,
        Volume: 2432500,
        AdjClose: 22.644404
    },
    {
        Date: "2014-02-25",
        Open: 25.219999,
        High: 25.280001,
        Low: 25.040001,
        Close: 25.23,
        Volume: 2702100,
        AdjClose: 22.617509
    },
    {
        Date: "2014-02-24",
        Open: 25.1,
        High: 25.24,
        Low: 25.08,
        Close: 25.1,
        Volume: 2136300,
        AdjClose: 22.500971
    },
    {
        Date: "2014-02-21",
        Open: 25.040001,
        High: 25.139999,
        Low: 24.98,
        Close: 25.030001,
        Volume: 1174900,
        AdjClose: 22.43822
    },
    {
        Date: "2014-02-20",
        Open: 25.01,
        High: 25.25,
        Low: 24.950001,
        Close: 25.040001,
        Volume: 3781700,
        AdjClose: 22.447185
    },
    {
        Date: "2014-02-19",
        Open: 25.35,
        High: 25.6,
        Low: 25.280001,
        Close: 25.309999,
        Volume: 3044800,
        AdjClose: 22.689226
    },
    {
        Date: "2014-02-18",
        Open: 25.15,
        High: 25.34,
        Low: 25.1,
        Close: 25.309999,
        Volume: 2282800,
        AdjClose: 22.689226
    },
    {
        Date: "2014-02-14",
        Open: 25.25,
        High: 25.42,
        Low: 25.23,
        Close: 25.41,
        Volume: 1181900,
        AdjClose: 22.778871
    },
    {
        Date: "2014-02-13",
        Open: 24.959999,
        High: 25.209999,
        Low: 24.950001,
        Close: 25.15,
        Volume: 3330800,
        AdjClose: 22.545793
    },
    {
        Date: "2014-02-12",
        Open: 25.57,
        High: 25.940001,
        Low: 25.49,
        Close: 25.67,
        Volume: 5580700,
        AdjClose: 23.011949
    },
    {
        Date: "2014-02-11",
        Open: 25.52,
        High: 25.799999,
        Low: 25.49,
        Close: 25.700001,
        Volume: 3747100,
        AdjClose: 23.038843
    },
    {
        Date: "2014-02-10",
        Open: 25.290001,
        High: 25.32,
        Low: 25.200001,
        Close: 25.280001,
        Volume: 1412500,
        AdjClose: 22.662333
    },
    {
        Date: "2014-02-07",
        Open: 25.17,
        High: 25.370001,
        Low: 25.110001,
        Close: 25.35,
        Volume: 1770500,
        AdjClose: 22.725084
    },
    {
        Date: "2014-02-06",
        Open: 25,
        High: 25.1,
        Low: 24.969999,
        Close: 25.1,
        Volume: 1574100,
        AdjClose: 22.500971
    },
    {
        Date: "2014-02-05",
        Open: 24.85,
        High: 24.92,
        Low: 24.66,
        Close: 24.76,
        Volume: 2757100,
        AdjClose: 22.196177
    },
    {
        Date: "2014-02-04",
        Open: 24.34,
        High: 24.549999,
        Low: 24.290001,
        Close: 24.51,
        Volume: 1013200,
        AdjClose: 21.972064
    },
    {
        Date: "2014-02-03",
        Open: 24.77,
        High: 24.82,
        Low: 24.26,
        Close: 24.309999,
        Volume: 1975800,
        AdjClose: 21.792773
    },
    {
        Date: "2014-01-31",
        Open: 24.709999,
        High: 25.049999,
        Low: 24.709999,
        Close: 24.84,
        Volume: 1980700,
        AdjClose: 22.267893
    },
    {
        Date: "2014-01-30",
        Open: 25.219999,
        High: 25.25,
        Low: 25.02,
        Close: 25.139999,
        Volume: 1787800,
        AdjClose: 22.536828
    },
    {
        Date: "2014-01-29",
        Open: 25.280001,
        High: 25.379999,
        Low: 25.120001,
        Close: 25.15,
        Volume: 1824800,
        AdjClose: 22.545793
    },
    {
        Date: "2014-01-28",
        Open: 25.33,
        High: 25.49,
        Low: 25.32,
        Close: 25.43,
        Volume: 1494400,
        AdjClose: 22.796801
    },
    {
        Date: "2014-01-27",
        Open: 25.18,
        High: 25.23,
        Low: 24.91,
        Close: 25.01,
        Volume: 2086800,
        AdjClose: 22.42029
    },
    {
        Date: "2014-01-24",
        Open: 25.51,
        High: 25.540001,
        Low: 25.200001,
        Close: 25.200001,
        Volume: 2487200,
        AdjClose: 22.590617
    },
    {
        Date: "2014-01-23",
        Open: 26.01,
        High: 26.040001,
        Low: 25.76,
        Close: 26.02,
        Volume: 3087400,
        AdjClose: 23.325708
    },
    {
        Date: "2014-01-22",
        Open: 25.57,
        High: 25.889999,
        Low: 25.530001,
        Close: 25.59,
        Volume: 2763800,
        AdjClose: 22.940233
    },
    {
        Date: "2014-01-21",
        Open: 26.780001,
        High: 26.799999,
        Low: 26.540001,
        Close: 26.68,
        Volume: 1487800,
        AdjClose: 23.917367
    },
    {
        Date: "2014-01-17",
        Open: 26.950001,
        High: 27.25,
        Low: 26.9,
        Close: 27.09,
        Volume: 1774700,
        AdjClose: 24.284913
    },
    {
        Date: "2014-01-16",
        Open: 26.309999,
        High: 26.77,
        Low: 26.27,
        Close: 26.75,
        Volume: 2727200,
        AdjClose: 23.980118
    },
    {
        Date: "2014-01-15",
        Open: 26.15,
        High: 26.389999,
        Low: 26.120001,
        Close: 26.309999,
        Volume: 1784400,
        AdjClose: 23.585679
    },
    {
        Date: "2014-01-14",
        Open: 26.26,
        High: 26.43,
        Low: 26.16,
        Close: 26.42,
        Volume: 1159500,
        AdjClose: 23.684289
    },
    {
        Date: "2014-01-13",
        Open: 26.23,
        High: 26.299999,
        Low: 26.040001,
        Close: 26.08,
        Volume: 1059600,
        AdjClose: 23.379495
    },
    {
        Date: "2014-01-10",
        Open: 26.1,
        High: 26.299999,
        Low: 26.07,
        Close: 26.299999,
        Volume: 1297500,
        AdjClose: 23.576714
    },
    {
        Date: "2014-01-09",
        Open: 25.93,
        High: 26.040001,
        Low: 25.77,
        Close: 25.940001,
        Volume: 1778300,
        AdjClose: 23.253992
    },
    {
        Date: "2014-01-08",
        Open: 25.799999,
        High: 25.91,
        Low: 25.719999,
        Close: 25.84,
        Volume: 1151200,
        AdjClose: 23.164346
    },
    {
        Date: "2014-01-07",
        Open: 25.709999,
        High: 25.91,
        Low: 25.68,
        Close: 25.870001,
        Volume: 896200,
        AdjClose: 23.19124
    },
    {
        Date: "2014-01-06",
        Open: 25.74,
        High: 25.790001,
        Low: 25.67,
        Close: 25.74,
        Volume: 2434900,
        AdjClose: 23.074701
    },
    {
        Date: "2014-01-03",
        Open: 26.110001,
        High: 26.219999,
        Low: 26.049999,
        Close: 26.139999,
        Volume: 1084900,
        AdjClose: 23.433281
    },
    {
        Date: "2014-01-02",
        Open: 26.17,
        High: 26.17,
        Low: 25.780001,
        Close: 25.93,
        Volume: 2720400,
        AdjClose: 23.245027
    },
    {
        Date: "2013-12-31",
        Open: 26.450001,
        High: 26.629999,
        Low: 26.35,
        Close: 26.559999,
        Volume: 863800,
        AdjClose: 23.809792
    },
    {
        Date: "2013-12-30",
        Open: 26.4,
        High: 26.5,
        Low: 26.24,
        Close: 26.42,
        Volume: 779900,
        AdjClose: 23.684289
    },
    {
        Date: "2013-12-27",
        Open: 26.5,
        High: 26.559999,
        Low: 26.360001,
        Close: 26.440001,
        Volume: 952900,
        AdjClose: 23.702218
    },
    {
        Date: "2013-12-26",
        Open: 26.23,
        High: 26.360001,
        Low: 26.219999,
        Close: 26.280001,
        Volume: 528100,
        AdjClose: 23.558786
    },
    {
        Date: "2013-12-24",
        Open: 26.16,
        High: 26.26,
        Low: 26.110001,
        Close: 26.209999,
        Volume: 535600,
        AdjClose: 23.496033
    },
    {
        Date: "2013-12-23",
        Open: 25.92,
        High: 26.18,
        Low: 25.92,
        Close: 26.1,
        Volume: 2289500,
        AdjClose: 23.397424
    },
    {
        Date: "2013-12-20",
        Open: 25.559999,
        High: 25.83,
        Low: 25.530001,
        Close: 25.76,
        Volume: 3149400,
        AdjClose: 23.09263
    },
    {
        Date: "2013-12-19",
        Open: 25.4,
        High: 25.68,
        Low: 25.4,
        Close: 25.59,
        Volume: 2382500,
        AdjClose: 22.940233
    },
    {
        Date: "2013-12-18",
        Open: 25.23,
        High: 25.42,
        Low: 24.92,
        Close: 25.33,
        Volume: 1804200,
        AdjClose: 22.707155
    },
    {
        Date: "2013-12-17",
        Open: 24.92,
        High: 24.93,
        Low: 24.76,
        Close: 24.83,
        Volume: 888000,
        AdjClose: 22.258928
    },
    {
        Date: "2013-12-16",
        Open: 24.9,
        High: 25.01,
        Low: 24.809999,
        Close: 24.9,
        Volume: 869700,
        AdjClose: 22.32168
    },
    {
        Date: "2013-12-13",
        Open: 24.639999,
        High: 24.65,
        Low: 24.51,
        Close: 24.620001,
        Volume: 1276600,
        AdjClose: 22.070674
    },
    {
        Date: "2013-12-12",
        Open: 24.66,
        High: 24.700001,
        Low: 24.48,
        Close: 24.610001,
        Volume: 1150600,
        AdjClose: 22.061709
    },
    {
        Date: "2013-12-11",
        Open: 25.15,
        High: 25.16,
        Low: 24.82,
        Close: 24.870001,
        Volume: 579000,
        AdjClose: 22.294787
    },
    {
        Date: "2013-12-10",
        Open: 25.280001,
        High: 25.32,
        Low: 25.059999,
        Close: 25.120001,
        Volume: 760000,
        AdjClose: 22.518901
    },
    {
        Date: "2013-12-09",
        Open: 25.32,
        High: 25.379999,
        Low: 25.280001,
        Close: 25.360001,
        Volume: 835300,
        AdjClose: 22.734049
    },
    {
        Date: "2013-12-06",
        Open: 25.24,
        High: 25.360001,
        Low: 25.17,
        Close: 25.360001,
        Volume: 525000,
        AdjClose: 22.734049
    },
    {
        Date: "2013-12-05",
        Open: 25.02,
        High: 25.139999,
        Low: 24.91,
        Close: 24.940001,
        Volume: 874800,
        AdjClose: 22.357539
    },
    {
        Date: "2013-12-04",
        Open: 24.6,
        High: 24.9,
        Low: 24.57,
        Close: 24.85,
        Volume: 1271100,
        AdjClose: 22.276858
    },
    {
        Date: "2013-12-03",
        Open: 25.049999,
        High: 25.08,
        Low: 24.85,
        Close: 24.93,
        Volume: 963600,
        AdjClose: 22.348574
    },
    {
        Date: "2013-12-02",
        Open: 25.43,
        High: 25.469999,
        Low: 25.25,
        Close: 25.26,
        Volume: 783400,
        AdjClose: 22.644404
    },
    {
        Date: "2013-11-29",
        Open: 25.629999,
        High: 25.74,
        Low: 25.51,
        Close: 25.549999,
        Volume: 559100,
        AdjClose: 22.904374
    },
    {
        Date: "2013-11-27",
        Open: 25.620001,
        High: 25.67,
        Low: 25.459999,
        Close: 25.530001,
        Volume: 627300,
        AdjClose: 22.886446
    },
    {
        Date: "2013-11-26",
        Open: 25.389999,
        High: 25.440001,
        Low: 25.299999,
        Close: 25.360001,
        Volume: 924800,
        AdjClose: 22.734049
    },
    {
        Date: "2013-11-25",
        Open: 25.389999,
        High: 25.48,
        Low: 25.25,
        Close: 25.299999,
        Volume: 788400,
        AdjClose: 22.680261
    },
    {
        Date: "2013-11-22",
        Open: 25.360001,
        High: 25.51,
        Low: 25.299999,
        Close: 25.48,
        Volume: 1298600,
        AdjClose: 22.841623
    },
    {
        Date: "2013-11-21",
        Open: 25.059999,
        High: 25.059999,
        Low: 24.9,
        Close: 25.02,
        Volume: 816600,
        AdjClose: 22.429255
    },
    {
        Date: "2013-11-20",
        Open: 25.24,
        High: 25.309999,
        Low: 24.93,
        Close: 25.040001,
        Volume: 1068000,
        AdjClose: 22.447185
    },
    {
        Date: "2013-11-19",
        Open: 25.35,
        High: 25.440001,
        Low: 25.16,
        Close: 25.26,
        Volume: 807600,
        AdjClose: 22.644404
    },
    {
        Date: "2013-11-18",
        Open: 25.57,
        High: 25.6,
        Low: 25.370001,
        Close: 25.43,
        Volume: 1136500,
        AdjClose: 22.796801
    },
    {
        Date: "2013-11-15",
        Open: 25.17,
        High: 25.35,
        Low: 25.15,
        Close: 25.34,
        Volume: 1844100,
        AdjClose: 22.71612
    },
    {
        Date: "2013-11-14",
        Open: 25.08,
        High: 25.26,
        Low: 25.049999,
        Close: 25.26,
        Volume: 1400800,
        AdjClose: 22.644404
    },
    {
        Date: "2013-11-13",
        Open: 24.76,
        High: 25.110001,
        Low: 24.719999,
        Close: 25.09,
        Volume: 1172900,
        AdjClose: 22.492006
    },
    {
        Date: "2013-11-12",
        Open: 25.07,
        High: 25.17,
        Low: 24.9,
        Close: 25.049999,
        Volume: 1101300,
        AdjClose: 22.456148
    },
    {
        Date: "2013-11-11",
        Open: 25.18,
        High: 25.24,
        Low: 25.08,
        Close: 25.219999,
        Volume: 851200,
        AdjClose: 22.608545
    },
    {
        Date: "2013-11-08",
        Open: 24.950001,
        High: 25.27,
        Low: 24.93,
        Close: 25.26,
        Volume: 1715800,
        AdjClose: 22.644404
    },
    {
        Date: "2013-11-07",
        Open: 25.33,
        High: 25.370001,
        Low: 25.120001,
        Close: 25.15,
        Volume: 989500,
        AdjClose: 22.545793
    },
    {
        Date: "2013-11-06",
        Open: 25.42,
        High: 25.49,
        Low: 25.35,
        Close: 25.48,
        Volume: 1264300,
        AdjClose: 22.841623
    },
    {
        Date: "2013-11-05",
        Open: 25.049999,
        High: 25.16,
        Low: 24.950001,
        Close: 25.09,
        Volume: 1159600,
        AdjClose: 22.492006
    },
    {
        Date: "2013-11-04",
        Open: 25.139999,
        High: 25.34,
        Low: 25.110001,
        Close: 25.309999,
        Volume: 864000,
        AdjClose: 22.689226
    },
    {
        Date: "2013-11-01",
        Open: 25.16,
        High: 25.290001,
        Low: 25,
        Close: 25.209999,
        Volume: 1669900,
        AdjClose: 22.59958
    },
    {
        Date: "2013-10-31",
        Open: 25.59,
        High: 25.639999,
        Low: 25.469999,
        Close: 25.469999,
        Volume: 1280700,
        AdjClose: 22.832658
    },
    {
        Date: "2013-10-30",
        Open: 25.780001,
        High: 26,
        Low: 25.65,
        Close: 25.68,
        Volume: 1077300,
        AdjClose: 23.020914
    },
    {
        Date: "2013-10-29",
        Open: 25.65,
        High: 25.68,
        Low: 25.459999,
        Close: 25.6,
        Volume: 2287100,
        AdjClose: 22.949198
    },
    {
        Date: "2013-10-28",
        Open: 25.700001,
        High: 25.879999,
        Low: 25.639999,
        Close: 25.799999,
        Volume: 1755600,
        AdjClose: 23.128487
    },
    {
        Date: "2013-10-25",
        Open: 25.84,
        High: 25.91,
        Low: 25.75,
        Close: 25.84,
        Volume: 2264000,
        AdjClose: 23.164346
    },
    {
        Date: "2013-10-24",
        Open: 25.76,
        High: 25.92,
        Low: 25.719999,
        Close: 25.780001,
        Volume: 3120000,
        AdjClose: 23.11056
    },
    {
        Date: "2013-10-23",
        Open: 24.540001,
        High: 24.68,
        Low: 24.469999,
        Close: 24.6,
        Volume: 1752100,
        AdjClose: 22.052745
    },
    {
        Date: "2013-10-22",
        Open: 24.309999,
        High: 24.540001,
        Low: 24.299999,
        Close: 24.459999,
        Volume: 1890800,
        AdjClose: 21.92724
    },
    {
        Date: "2013-10-21",
        Open: 23.940001,
        High: 24.120001,
        Low: 23.93,
        Close: 24.030001,
        Volume: 2146400,
        AdjClose: 21.541767
    },
    {
        Date: "2013-10-18",
        Open: 23.459999,
        High: 23.73,
        Low: 23.360001,
        Close: 23.68,
        Volume: 1493000,
        AdjClose: 21.228008
    },
    {
        Date: "2013-10-17",
        Open: 23.219999,
        High: 23.440001,
        Low: 23.190001,
        Close: 23.41,
        Volume: 1163200,
        AdjClose: 20.985965
    },
    {
        Date: "2013-10-16",
        Open: 23.110001,
        High: 23.26,
        Low: 23.1,
        Close: 23.23,
        Volume: 1325200,
        AdjClose: 20.824603
    },
    {
        Date: "2013-10-15",
        Open: 23.33,
        High: 23.360001,
        Low: 23.219999,
        Close: 23.290001,
        Volume: 2257900,
        AdjClose: 20.878392
    },
    {
        Date: "2013-10-14",
        Open: 23.16,
        High: 23.48,
        Low: 23.16,
        Close: 23.42,
        Volume: 1043900,
        AdjClose: 20.99493
    },
    {
        Date: "2013-10-11",
        Open: 23.309999,
        High: 23.42,
        Low: 23.280001,
        Close: 23.379999,
        Volume: 712600,
        AdjClose: 20.959071
    },
    {
        Date: "2013-10-10",
        Open: 23.1,
        High: 23.299999,
        Low: 23.08,
        Close: 23.280001,
        Volume: 1358600,
        AdjClose: 20.869427
    },
    {
        Date: "2013-10-09",
        Open: 22.75,
        High: 22.870001,
        Low: 22.66,
        Close: 22.77,
        Volume: 1690700,
        AdjClose: 20.412236
    },
    {
        Date: "2013-10-08",
        Open: 23.370001,
        High: 23.4,
        Low: 23,
        Close: 23,
        Volume: 2446100,
        AdjClose: 20.61842
    },
    {
        Date: "2013-10-07",
        Open: 23.27,
        High: 23.41,
        Low: 23.24,
        Close: 23.24,
        Volume: 1740900,
        AdjClose: 20.833568
    },
    {
        Date: "2013-10-04",
        Open: 23.33,
        High: 23.48,
        Low: 23.280001,
        Close: 23.309999,
        Volume: 1084500,
        AdjClose: 20.896319
    },
    {
        Date: "2013-10-03",
        Open: 23.549999,
        High: 23.58,
        Low: 23.309999,
        Close: 23.370001,
        Volume: 1655000,
        AdjClose: 20.950108
    },
    {
        Date: "2013-10-02",
        Open: 23.559999,
        High: 23.6,
        Low: 23.42,
        Close: 23.6,
        Volume: 696800,
        AdjClose: 21.156292
    },
    {
        Date: "2013-10-01",
        Open: 23.629999,
        High: 23.74,
        Low: 23.51,
        Close: 23.709999,
        Volume: 1095100,
        AdjClose: 21.2549
    },
    {
        Date: "2013-09-30",
        Open: 23.48,
        High: 23.709999,
        Low: 23.450001,
        Close: 23.59,
        Volume: 1160200,
        AdjClose: 21.147327
    },
    {
        Date: "2013-09-27",
        Open: 23.76,
        High: 23.85,
        Low: 23.66,
        Close: 23.809999,
        Volume: 966900,
        AdjClose: 21.344546
    },
    {
        Date: "2013-09-26",
        Open: 23.809999,
        High: 23.91,
        Low: 23.610001,
        Close: 23.74,
        Volume: 1107800,
        AdjClose: 21.281795
    },
    {
        Date: "2013-09-25",
        Open: 23.719999,
        High: 23.889999,
        Low: 23.700001,
        Close: 23.799999,
        Volume: 1054900,
        AdjClose: 21.335581
    },
    {
        Date: "2013-09-24",
        Open: 23.82,
        High: 23.969999,
        Low: 23.67,
        Close: 23.76,
        Volume: 1505700,
        AdjClose: 21.299724
    },
    {
        Date: "2013-09-23",
        Open: 23.870001,
        High: 23.889999,
        Low: 23.610001,
        Close: 23.68,
        Volume: 1925400,
        AdjClose: 21.228008
    },
    {
        Date: "2013-09-20",
        Open: 23.780001,
        High: 23.93,
        Low: 23.75,
        Close: 23.780001,
        Volume: 1461300,
        AdjClose: 21.317654
    },
    {
        Date: "2013-09-19",
        Open: 23.99,
        High: 24.08,
        Low: 23.889999,
        Close: 24.030001,
        Volume: 1382300,
        AdjClose: 21.541767
    },
    {
        Date: "2013-09-18",
        Open: 23.299999,
        High: 23.940001,
        Low: 23.280001,
        Close: 23.93,
        Volume: 1464800,
        AdjClose: 21.452121
    },
    {
        Date: "2013-09-17",
        Open: 23.190001,
        High: 23.25,
        Low: 23.139999,
        Close: 23.24,
        Volume: 589700,
        AdjClose: 20.833568
    },
    {
        Date: "2013-09-16",
        Open: 23.280001,
        High: 23.280001,
        Low: 23.120001,
        Close: 23.129999,
        Volume: 800600,
        AdjClose: 20.734958
    },
    {
        Date: "2013-09-13",
        Open: 22.790001,
        High: 22.959999,
        Low: 22.67,
        Close: 22.91,
        Volume: 712200,
        AdjClose: 20.537739
    },
    {
        Date: "2013-09-12",
        Open: 22.98,
        High: 23.15,
        Low: 22.950001,
        Close: 23.040001,
        Volume: 842500,
        AdjClose: 20.654278
    },
    {
        Date: "2013-09-11",
        Open: 22.950001,
        High: 23.08,
        Low: 22.91,
        Close: 23.049999,
        Volume: 1039000,
        AdjClose: 20.663241
    },
    {
        Date: "2013-09-10",
        Open: 22.67,
        High: 22.84,
        Low: 22.65,
        Close: 22.82,
        Volume: 1448400,
        AdjClose: 20.457058
    },
    {
        Date: "2013-09-09",
        Open: 22.360001,
        High: 22.620001,
        Low: 22.34,
        Close: 22.610001,
        Volume: 1845600,
        AdjClose: 20.268803
    },
    {
        Date: "2013-09-06",
        Open: 22.190001,
        High: 22.33,
        Low: 22.059999,
        Close: 22.23,
        Volume: 1453200,
        AdjClose: 19.92815
    },
    {
        Date: "2013-09-05",
        Open: 21.950001,
        High: 22.09,
        Low: 21.879999,
        Close: 21.99,
        Volume: 1290700,
        AdjClose: 19.713002
    },
    {
        Date: "2013-09-04",
        Open: 21.85,
        High: 22.049999,
        Low: 21.799999,
        Close: 22,
        Volume: 1312800,
        AdjClose: 19.721967
    },
    {
        Date: "2013-09-03",
        Open: 21.6,
        High: 21.68,
        Low: 21.5,
        Close: 21.58,
        Volume: 1307100,
        AdjClose: 19.345456
    },
    {
        Date: "2013-08-30",
        Open: 21.4,
        High: 21.450001,
        Low: 21.309999,
        Close: 21.41,
        Volume: 1129800,
        AdjClose: 19.193059
    },
    {
        Date: "2013-08-29",
        Open: 21.51,
        High: 21.66,
        Low: 21.5,
        Close: 21.610001,
        Volume: 2296600,
        AdjClose: 19.37235
    },
    {
        Date: "2013-08-28",
        Open: 21.77,
        High: 22.02,
        Low: 21.73,
        Close: 21.9,
        Volume: 1241600,
        AdjClose: 19.632321
    },
    {
        Date: "2013-08-27",
        Open: 22.200001,
        High: 22.280001,
        Low: 22.01,
        Close: 22.030001,
        Volume: 906900,
        AdjClose: 19.748861
    },
    {
        Date: "2013-08-26",
        Open: 22.6,
        High: 22.629999,
        Low: 22.450001,
        Close: 22.51,
        Volume: 675200,
        AdjClose: 20.179158
    },
    {
        Date: "2013-08-23",
        Open: 22.540001,
        High: 22.709999,
        Low: 22.48,
        Close: 22.700001,
        Volume: 825500,
        AdjClose: 20.349484
    },
    {
        Date: "2013-08-22",
        Open: 22.469999,
        High: 22.549999,
        Low: 22.42,
        Close: 22.540001,
        Volume: 775700,
        AdjClose: 20.206052
    },
    {
        Date: "2013-08-21",
        Open: 22.32,
        High: 22.379999,
        Low: 22.02,
        Close: 22.110001,
        Volume: 1077900,
        AdjClose: 19.820577
    },
    {
        Date: "2013-08-20",
        Open: 22.389999,
        High: 22.549999,
        Low: 22.360001,
        Close: 22.469999,
        Volume: 751400,
        AdjClose: 20.143299
    },
    {
        Date: "2013-08-19",
        Open: 22.450001,
        High: 22.48,
        Low: 22.33,
        Close: 22.34,
        Volume: 557200,
        AdjClose: 20.026761
    },
    {
        Date: "2013-08-16",
        Open: 22.530001,
        High: 22.629999,
        Low: 22.43,
        Close: 22.51,
        Volume: 600600,
        AdjClose: 20.179158
    },
    {
        Date: "2013-08-15",
        Open: 22.34,
        High: 22.57,
        Low: 22.17,
        Close: 22.530001,
        Volume: 1002200,
        AdjClose: 20.197087
    },
    {
        Date: "2013-08-14",
        Open: 22.5,
        High: 22.65,
        Low: 22.469999,
        Close: 22.620001,
        Volume: 947100,
        AdjClose: 20.277768
    },
    {
        Date: "2013-08-13",
        Open: 22.469999,
        High: 22.58,
        Low: 22.389999,
        Close: 22.52,
        Volume: 690400,
        AdjClose: 20.188122
    },
    {
        Date: "2013-08-12",
        Open: 22.370001,
        High: 22.49,
        Low: 22.34,
        Close: 22.370001,
        Volume: 1070000,
        AdjClose: 20.053655
    },
    {
        Date: "2013-08-09",
        Open: 22.469999,
        High: 22.620001,
        Low: 22.42,
        Close: 22.469999,
        Volume: 1001700,
        AdjClose: 20.143299
    },
    {
        Date: "2013-08-08",
        Open: 22.299999,
        High: 22.58,
        Low: 22.290001,
        Close: 22.52,
        Volume: 1705300,
        AdjClose: 20.188122
    },
    {
        Date: "2013-08-07",
        Open: 22.049999,
        High: 22.15,
        Low: 22.02,
        Close: 22.120001,
        Volume: 718300,
        AdjClose: 19.829542
    },
    {
        Date: "2013-08-06",
        Open: 22.41,
        High: 22.42,
        Low: 22.09,
        Close: 22.190001,
        Volume: 1306900,
        AdjClose: 19.892293
    },
    {
        Date: "2013-08-05",
        Open: 22.120001,
        High: 22.219999,
        Low: 22.059999,
        Close: 22.219999,
        Volume: 709700,
        AdjClose: 19.919186
    },
    {
        Date: "2013-08-02",
        Open: 22.08,
        High: 22.290001,
        Low: 22.01,
        Close: 22.280001,
        Volume: 1125300,
        AdjClose: 19.972974
    },
    {
        Date: "2013-08-01",
        Open: 22.15,
        High: 22.23,
        Low: 22.07,
        Close: 22.129999,
        Volume: 1242000,
        AdjClose: 19.838505
    },
    {
        Date: "2013-07-31",
        Open: 21.889999,
        High: 22.24,
        Low: 21.870001,
        Close: 22.040001,
        Volume: 1181900,
        AdjClose: 19.757825
    },
    {
        Date: "2013-07-30",
        Open: 22.139999,
        High: 22.16,
        Low: 21.950001,
        Close: 21.950001,
        Volume: 753100,
        AdjClose: 19.677145
    },
    {
        Date: "2013-07-29",
        Open: 22.040001,
        High: 22.110001,
        Low: 21.92,
        Close: 21.959999,
        Volume: 889300,
        AdjClose: 19.686108
    },
    {
        Date: "2013-07-26",
        Open: 22.1,
        High: 22.18,
        Low: 21.969999,
        Close: 22.139999,
        Volume: 1199400,
        AdjClose: 19.847469
    },
    {
        Date: "2013-07-25",
        Open: 21.98,
        High: 22.450001,
        Low: 21.98,
        Close: 22.42,
        Volume: 2301000,
        AdjClose: 20.098477
    },
    {
        Date: "2013-07-24",
        Open: 22.969999,
        High: 22.98,
        Low: 22.75,
        Close: 22.959999,
        Volume: 1492200,
        AdjClose: 20.582561
    },
    {
        Date: "2013-07-23",
        Open: 22.809999,
        High: 22.93,
        Low: 22.700001,
        Close: 22.879999,
        Volume: 1338300,
        AdjClose: 20.510844
    },
    {
        Date: "2013-07-22",
        Open: 22.860001,
        High: 22.98,
        Low: 22.84,
        Close: 22.950001,
        Volume: 1421200,
        AdjClose: 20.573598
    },
    {
        Date: "2013-07-19",
        Open: 22.76,
        High: 22.92,
        Low: 22.709999,
        Close: 22.860001,
        Volume: 1023900,
        AdjClose: 20.492917
    },
    {
        Date: "2013-07-18",
        Open: 22.66,
        High: 22.790001,
        Low: 22.620001,
        Close: 22.76,
        Volume: 1329900,
        AdjClose: 20.403271
    },
    {
        Date: "2013-07-17",
        Open: 22.77,
        High: 22.77,
        Low: 22.57,
        Close: 22.690001,
        Volume: 1207500,
        AdjClose: 20.34052
    },
    {
        Date: "2013-07-16",
        Open: 22.639999,
        High: 22.77,
        Low: 22.59,
        Close: 22.719999,
        Volume: 962200,
        AdjClose: 20.367412
    },
    {
        Date: "2013-07-15",
        Open: 22.530001,
        High: 22.610001,
        Low: 22.43,
        Close: 22.549999,
        Volume: 968700,
        AdjClose: 20.215015
    },
    {
        Date: "2013-07-12",
        Open: 22.450001,
        High: 22.530001,
        Low: 22.35,
        Close: 22.530001,
        Volume: 815400,
        AdjClose: 20.197087
    },
    {
        Date: "2013-07-11",
        Open: 22.290001,
        High: 22.57,
        Low: 22.190001,
        Close: 22.49,
        Volume: 1928600,
        AdjClose: 20.161228
    },
    {
        Date: "2013-07-10",
        Open: 21.700001,
        High: 21.99,
        Low: 21.68,
        Close: 21.84,
        Volume: 933500,
        AdjClose: 19.578534
    },
    {
        Date: "2013-07-09",
        Open: 21.73,
        High: 21.75,
        Low: 21.58,
        Close: 21.690001,
        Volume: 1309500,
        AdjClose: 19.444067
    },
    {
        Date: "2013-07-08",
        Open: 21.74,
        High: 21.75,
        Low: 21.57,
        Close: 21.719999,
        Volume: 1051500,
        AdjClose: 19.470959
    },
    {
        Date: "2013-07-05",
        Open: 21.530001,
        High: 21.620001,
        Low: 21.389999,
        Close: 21.620001,
        Volume: 657000,
        AdjClose: 19.381315
    },
    {
        Date: "2013-07-03",
        Open: 21.290001,
        High: 21.5,
        Low: 21.26,
        Close: 21.49,
        Volume: 724800,
        AdjClose: 19.264775
    },
    {
        Date: "2013-07-02",
        Open: 21.459999,
        High: 21.639999,
        Low: 21.309999,
        Close: 21.459999,
        Volume: 1587500,
        AdjClose: 19.237881
    },
    {
        Date: "2013-07-01",
        Open: 21.74,
        High: 21.940001,
        Low: 21.719999,
        Close: 21.860001,
        Volume: 1132200,
        AdjClose: 19.596464
    },
    {
        Date: "2013-06-28",
        Open: 21.68,
        High: 21.74,
        Low: 21.549999,
        Close: 21.66,
        Volume: 1386800,
        AdjClose: 19.417172
    },
    {
        Date: "2013-06-27",
        Open: 21.52,
        High: 21.719999,
        Low: 21.51,
        Close: 21.690001,
        Volume: 2143500,
        AdjClose: 19.444067
    },
    {
        Date: "2013-06-26",
        Open: 21.33,
        High: 21.469999,
        Low: 21.27,
        Close: 21.34,
        Volume: 1295900,
        AdjClose: 19.130308
    },
    {
        Date: "2013-06-25",
        Open: 21.049999,
        High: 21.219999,
        Low: 20.84,
        Close: 21.17,
        Volume: 2453600,
        AdjClose: 18.977911
    },
    {
        Date: "2013-06-24",
        Open: 20.67,
        High: 21.01,
        Low: 20.65,
        Close: 20.889999,
        Volume: 3910100,
        AdjClose: 18.726903
    },
    {
        Date: "2013-06-21",
        Open: 21.4,
        High: 21.450001,
        Low: 21.059999,
        Close: 21.24,
        Volume: 2232500,
        AdjClose: 19.040662
    },
    {
        Date: "2013-06-20",
        Open: 21.870001,
        High: 21.879999,
        Low: 21.41,
        Close: 21.5,
        Volume: 2475000,
        AdjClose: 19.27374
    },
    {
        Date: "2013-06-19",
        Open: 22.690001,
        High: 22.75,
        Low: 22.190001,
        Close: 22.24,
        Volume: 1870400,
        AdjClose: 19.937115
    },
    {
        Date: "2013-06-18",
        Open: 22.43,
        High: 22.59,
        Low: 22.4,
        Close: 22.440001,
        Volume: 2125100,
        AdjClose: 20.116406
    },
    {
        Date: "2013-06-17",
        Open: 22.52,
        High: 22.559999,
        Low: 22.219999,
        Close: 22.299999,
        Volume: 2791400,
        AdjClose: 19.990902
    },
    {
        Date: "2013-06-14",
        Open: 21.860001,
        High: 21.959999,
        Low: 21.73,
        Close: 21.82,
        Volume: 761300,
        AdjClose: 19.560605
    },
    {
        Date: "2013-06-13",
        Open: 21.639999,
        High: 21.969999,
        Low: 21.59,
        Close: 21.92,
        Volume: 1532800,
        AdjClose: 19.65025
    },
    {
        Date: "2013-06-12",
        Open: 21.9,
        High: 21.91,
        Low: 21.65,
        Close: 21.700001,
        Volume: 1517100,
        AdjClose: 19.453031
    },
    {
        Date: "2013-06-11",
        Open: 21.690001,
        High: 21.83,
        Low: 21.629999,
        Close: 21.66,
        Volume: 3042000,
        AdjClose: 19.417172
    },
    {
        Date: "2013-06-10",
        Open: 22.01,
        High: 22.01,
        Low: 21.809999,
        Close: 21.98,
        Volume: 1843800,
        AdjClose: 19.704037
    },
    {
        Date: "2013-06-07",
        Open: 21.879999,
        High: 21.99,
        Low: 21.75,
        Close: 21.85,
        Volume: 1803700,
        AdjClose: 19.587499
    },
    {
        Date: "2013-06-06",
        Open: 21.67,
        High: 21.780001,
        Low: 21.58,
        Close: 21.76,
        Volume: 2313100,
        AdjClose: 19.506818
    },
    {
        Date: "2013-06-05",
        Open: 21.879999,
        High: 21.969999,
        Low: 21.57,
        Close: 21.6,
        Volume: 2949500,
        AdjClose: 19.363386
    },
    {
        Date: "2013-06-04",
        Open: 22.16,
        High: 22.209999,
        Low: 21.92,
        Close: 22.07,
        Volume: 1742300,
        AdjClose: 19.784718
    },
    {
        Date: "2013-06-03",
        Open: 21.91,
        High: 22.16,
        Low: 21.83,
        Close: 22.129999,
        Volume: 2163200,
        AdjClose: 19.838505
    },
    {
        Date: "2013-05-31",
        Open: 22.15,
        High: 22.17,
        Low: 21.77,
        Close: 21.790001,
        Volume: 2970000,
        AdjClose: 19.533712
    },
    {
        Date: "2013-05-30",
        Open: 22.27,
        High: 22.379999,
        Low: 22.23,
        Close: 22.299999,
        Volume: 1518800,
        AdjClose: 19.990902
    },
    {
        Date: "2013-05-29",
        Open: 22.15,
        High: 22.24,
        Low: 22.02,
        Close: 22.1,
        Volume: 1686300,
        AdjClose: 19.811612
    },
    {
        Date: "2013-05-28",
        Open: 22.379999,
        High: 22.42,
        Low: 22.040001,
        Close: 22.07,
        Volume: 1647800,
        AdjClose: 19.784718
    },
    {
        Date: "2013-05-24",
        Open: 22.280001,
        High: 22.309999,
        Low: 22.18,
        Close: 22.25,
        Volume: 2106300,
        AdjClose: 19.94608
    },
    {
        Date: "2013-05-23",
        Open: 22.030001,
        High: 22.110001,
        Low: 21.940001,
        Close: 22.08,
        Volume: 3835100,
        AdjClose: 19.793683
    },
    {
        Date: "2013-05-22",
        Open: 22.559999,
        High: 22.73,
        Low: 22.09,
        Close: 22.219999,
        Volume: 3533000,
        AdjClose: 19.919186
    },
    {
        Date: "2013-05-21",
        Open: 22.469999,
        High: 22.690001,
        Low: 22.389999,
        Close: 22.6,
        Volume: 1712200,
        AdjClose: 20.259839
    },
    {
        Date: "2013-05-20",
        Open: 22.370001,
        High: 22.43,
        Low: 22.309999,
        Close: 22.34,
        Volume: 1206000,
        AdjClose: 20.026761
    },
    {
        Date: "2013-05-17",
        Open: 22.129999,
        High: 22.32,
        Low: 22.1,
        Close: 22.32,
        Volume: 1519700,
        AdjClose: 20.008831
    },
    {
        Date: "2013-05-16",
        Open: 22.139999,
        High: 22.34,
        Low: 22.01,
        Close: 22.030001,
        Volume: 2521900,
        AdjClose: 19.748861
    },
    {
        Date: "2013-05-15",
        Open: 22.209999,
        High: 22.4,
        Low: 22.190001,
        Close: 22.389999,
        Volume: 2525600,
        AdjClose: 20.071583
    },
    {
        Date: "2013-05-14",
        Open: 22.299999,
        High: 22.370001,
        Low: 22.01,
        Close: 22.09,
        Volume: 3132200,
        AdjClose: 19.802647
    },
    {
        Date: "2013-05-13",
        Open: 22.309999,
        High: 22.309999,
        Low: 22.110001,
        Close: 22.17,
        Volume: 2555100,
        AdjClose: 19.874364
    },
    {
        Date: "2013-05-10",
        Open: 22.370001,
        High: 22.629999,
        Low: 22.309999,
        Close: 22.620001,
        Volume: 3376700,
        AdjClose: 20.277768
    },
    {
        Date: "2013-05-09",
        Open: 23.030001,
        High: 23.030001,
        Low: 22.68,
        Close: 22.74,
        Volume: 1282200,
        AdjClose: 20.385342
    },
    {
        Date: "2013-05-08",
        Open: 22.98,
        High: 23.15,
        Low: 22.940001,
        Close: 23.08,
        Volume: 913800,
        AdjClose: 20.690136
    },
    {
        Date: "2013-05-07",
        Open: 22.84,
        High: 22.879999,
        Low: 22.629999,
        Close: 22.76,
        Volume: 1532300,
        AdjClose: 20.403271
    },
    {
        Date: "2013-05-06",
        Open: 22.719999,
        High: 22.75,
        Low: 22.629999,
        Close: 22.700001,
        Volume: 802700,
        AdjClose: 20.349484
    },
    {
        Date: "2013-05-03",
        Open: 22.65,
        High: 22.879999,
        Low: 22.639999,
        Close: 22.66,
        Volume: 1111500,
        AdjClose: 20.313625
    },
    {
        Date: "2013-05-02",
        Open: 22.18,
        High: 22.389999,
        Low: 22.18,
        Close: 22.299999,
        Volume: 2661600,
        AdjClose: 19.990902
    },
    {
        Date: "2013-05-01",
        Open: 22.57,
        High: 22.57,
        Low: 22.17,
        Close: 22.34,
        Volume: 2691300,
        AdjClose: 20.026761
    },
    {
        Date: "2013-04-30",
        Open: 22.459999,
        High: 22.68,
        Low: 22.459999,
        Close: 22.57,
        Volume: 2040700,
        AdjClose: 20.232944
    },
    {
        Date: "2013-04-29",
        Open: 23.120001,
        High: 23.299999,
        Low: 23.110001,
        Close: 23.23,
        Volume: 1158500,
        AdjClose: 20.163021
    },
    {
        Date: "2013-04-26",
        Open: 22.83,
        High: 22.969999,
        Low: 22.68,
        Close: 22.9,
        Volume: 1960500,
        AdjClose: 19.87659
    },
    {
        Date: "2013-04-25",
        Open: 22.66,
        High: 22.950001,
        Low: 22.58,
        Close: 22.83,
        Volume: 1790500,
        AdjClose: 19.815832
    },
    {
        Date: "2013-04-24",
        Open: 22.27,
        High: 22.549999,
        Low: 22.209999,
        Close: 22.43,
        Volume: 1812600,
        AdjClose: 19.468643
    },
    {
        Date: "2013-04-23",
        Open: 21.610001,
        High: 21.9,
        Low: 21.57,
        Close: 21.719999,
        Volume: 2181000,
        AdjClose: 18.852381
    },
    {
        Date: "2013-04-22",
        Open: 21.370001,
        High: 21.42,
        Low: 21.01,
        Close: 21.209999,
        Volume: 2169400,
        AdjClose: 18.409715
    },
    {
        Date: "2013-04-19",
        Open: 21.48,
        High: 21.540001,
        Low: 21.34,
        Close: 21.42,
        Volume: 1289000,
        AdjClose: 18.59199
    },
    {
        Date: "2013-04-18",
        Open: 21.67,
        High: 21.690001,
        Low: 21.360001,
        Close: 21.469999,
        Volume: 2129800,
        AdjClose: 18.635388
    },
    {
        Date: "2013-04-17",
        Open: 21.940001,
        High: 21.950001,
        Low: 21.32,
        Close: 21.52,
        Volume: 3171500,
        AdjClose: 18.678788
    },
    {
        Date: "2013-04-16",
        Open: 22.389999,
        High: 22.559999,
        Low: 22.25,
        Close: 22.51,
        Volume: 1013700,
        AdjClose: 19.538081
    },
    {
        Date: "2013-04-15",
        Open: 22.440001,
        High: 22.459999,
        Low: 21.959999,
        Close: 22.01,
        Volume: 1369600,
        AdjClose: 19.104094
    },
    {
        Date: "2013-04-12",
        Open: 22.540001,
        High: 22.75,
        Low: 22.5,
        Close: 22.73,
        Volume: 998300,
        AdjClose: 19.729035
    },
    {
        Date: "2013-04-11",
        Open: 22.709999,
        High: 22.82,
        Low: 22.59,
        Close: 22.66,
        Volume: 1550200,
        AdjClose: 19.668277
    },
    {
        Date: "2013-04-10",
        Open: 22.450001,
        High: 22.67,
        Low: 22.440001,
        Close: 22.57,
        Volume: 1034200,
        AdjClose: 19.590159
    },
    {
        Date: "2013-04-09",
        Open: 22.049999,
        High: 22.26,
        Low: 21.879999,
        Close: 22.139999,
        Volume: 1199200,
        AdjClose: 19.21693
    },
    {
        Date: "2013-04-08",
        Open: 21.9,
        High: 22.059999,
        Low: 21.780001,
        Close: 21.99,
        Volume: 2139400,
        AdjClose: 19.086735
    },
    {
        Date: "2013-04-05",
        Open: 22.09,
        High: 22.17,
        Low: 21.9,
        Close: 22.16,
        Volume: 1878400,
        AdjClose: 19.23429
    },
    {
        Date: "2013-04-04",
        Open: 22.120001,
        High: 22.42,
        Low: 22.07,
        Close: 22.4,
        Volume: 2190400,
        AdjClose: 19.442604
    },
    {
        Date: "2013-04-03",
        Open: 22.879999,
        High: 22.950001,
        Low: 22.43,
        Close: 22.5,
        Volume: 1506800,
        AdjClose: 19.529401
    },
    {
        Date: "2013-04-02",
        Open: 22.73,
        High: 22.879999,
        Low: 22.709999,
        Close: 22.82,
        Volume: 2095600,
        AdjClose: 19.807152
    },
    {
        Date: "2013-04-01",
        Open: 22.76,
        High: 22.780001,
        Low: 22.389999,
        Close: 22.52,
        Volume: 1124300,
        AdjClose: 19.546761
    },
    {
        Date: "2013-03-28",
        Open: 22.65,
        High: 22.77,
        Low: 22.540001,
        Close: 22.76,
        Volume: 1705500,
        AdjClose: 19.755074
    },
    {
        Date: "2013-03-27",
        Open: 22.49,
        High: 22.629999,
        Low: 22.42,
        Close: 22.629999,
        Volume: 2129100,
        AdjClose: 19.642237
    },
    {
        Date: "2013-03-26",
        Open: 22.629999,
        High: 22.92,
        Low: 22.6,
        Close: 22.92,
        Volume: 1870000,
        AdjClose: 19.89395
    },
    {
        Date: "2013-03-25",
        Open: 22.76,
        High: 22.82,
        Low: 22.389999,
        Close: 22.51,
        Volume: 1435800,
        AdjClose: 19.538081
    },
    {
        Date: "2013-03-22",
        Open: 22.57,
        High: 22.67,
        Low: 22.49,
        Close: 22.629999,
        Volume: 1146400,
        AdjClose: 19.642237
    },
    {
        Date: "2013-03-21",
        Open: 22.440001,
        High: 22.629999,
        Low: 22.370001,
        Close: 22.41,
        Volume: 1354600,
        AdjClose: 19.451283
    },
    {
        Date: "2013-03-20",
        Open: 22.82,
        High: 23.040001,
        Low: 22.82,
        Close: 22.969999,
        Volume: 1091900,
        AdjClose: 19.937348
    },
    {
        Date: "2013-03-19",
        Open: 22.77,
        High: 22.799999,
        Low: 22.450001,
        Close: 22.629999,
        Volume: 1257300,
        AdjClose: 19.642237
    },
    {
        Date: "2013-03-18",
        Open: 22.690001,
        High: 22.93,
        Low: 22.6,
        Close: 22.66,
        Volume: 1610000,
        AdjClose: 19.668277
    },
    {
        Date: "2013-03-15",
        Open: 23.190001,
        High: 23.23,
        Low: 23.07,
        Close: 23.17,
        Volume: 1214200,
        AdjClose: 20.110943
    },
    {
        Date: "2013-03-14",
        Open: 22.709999,
        High: 22.959999,
        Low: 22.700001,
        Close: 22.950001,
        Volume: 1221500,
        AdjClose: 19.91999
    },
    {
        Date: "2013-03-13",
        Open: 22.91,
        High: 22.93,
        Low: 22.709999,
        Close: 22.889999,
        Volume: 1713400,
        AdjClose: 19.86791
    },
    {
        Date: "2013-03-12",
        Open: 22.870001,
        High: 22.92,
        Low: 22.68,
        Close: 22.76,
        Volume: 916400,
        AdjClose: 19.755074
    },
    {
        Date: "2013-03-11",
        Open: 22.719999,
        High: 22.879999,
        Low: 22.68,
        Close: 22.879999,
        Volume: 971300,
        AdjClose: 19.85923
    },
    {
        Date: "2013-03-08",
        Open: 22.780001,
        High: 22.870001,
        Low: 22.629999,
        Close: 22.83,
        Volume: 1550900,
        AdjClose: 19.815832
    },
    {
        Date: "2013-03-07",
        Open: 23.030001,
        High: 23.110001,
        Low: 22.91,
        Close: 22.98,
        Volume: 984700,
        AdjClose: 19.946028
    },
    {
        Date: "2013-03-06",
        Open: 22.99,
        High: 22.99,
        Low: 22.809999,
        Close: 22.85,
        Volume: 955100,
        AdjClose: 19.833192
    },
    {
        Date: "2013-03-05",
        Open: 22.940001,
        High: 23,
        Low: 22.860001,
        Close: 22.879999,
        Volume: 1480100,
        AdjClose: 19.85923
    },
    {
        Date: "2013-03-04",
        Open: 22.4,
        High: 22.690001,
        Low: 22.34,
        Close: 22.66,
        Volume: 2233300,
        AdjClose: 19.668277
    },
    {
        Date: "2013-03-01",
        Open: 22.6,
        High: 22.719999,
        Low: 22.52,
        Close: 22.65,
        Volume: 1879100,
        AdjClose: 19.659597
    },
    {
        Date: "2013-02-28",
        Open: 22.92,
        High: 23.030001,
        Low: 22.709999,
        Close: 22.719999,
        Volume: 2336800,
        AdjClose: 19.720355
    },
    {
        Date: "2013-02-27",
        Open: 22.530001,
        High: 22.92,
        Low: 22.51,
        Close: 22.82,
        Volume: 1569600,
        AdjClose: 19.807152
    },
    {
        Date: "2013-02-26",
        Open: 22.530001,
        High: 22.6,
        Low: 22.25,
        Close: 22.42,
        Volume: 2604700,
        AdjClose: 19.459963
    },
    {
        Date: "2013-02-25",
        Open: 22.99,
        High: 23.02,
        Low: 22.129999,
        Close: 22.139999,
        Volume: 3017900,
        AdjClose: 19.21693
    },
    {
        Date: "2013-02-22",
        Open: 23.01,
        High: 23.1,
        Low: 22.940001,
        Close: 23.08,
        Volume: 1544900,
        AdjClose: 20.032826
    },
    {
        Date: "2013-02-21",
        Open: 22.870001,
        High: 22.950001,
        Low: 22.610001,
        Close: 22.74,
        Volume: 3100100,
        AdjClose: 19.737715
    },
    {
        Date: "2013-02-20",
        Open: 23.34,
        High: 23.370001,
        Low: 22.93,
        Close: 22.93,
        Volume: 2101200,
        AdjClose: 19.90263
    },
    {
        Date: "2013-02-19",
        Open: 22.889999,
        High: 23,
        Low: 22.889999,
        Close: 23,
        Volume: 1387800,
        AdjClose: 19.963388
    },
    {
        Date: "2013-02-15",
        Open: 22.67,
        High: 22.780001,
        Low: 22.639999,
        Close: 22.75,
        Volume: 1413100,
        AdjClose: 19.746395
    },
    {
        Date: "2013-02-14",
        Open: 22.25,
        High: 22.68,
        Low: 22.209999,
        Close: 22.639999,
        Volume: 3537200,
        AdjClose: 19.650917
    },
    {
        Date: "2013-02-13",
        Open: 21.469999,
        High: 21.610001,
        Low: 21.440001,
        Close: 21.610001,
        Volume: 2087400,
        AdjClose: 18.756905
    },
    {
        Date: "2013-02-12",
        Open: 20.93,
        High: 21.200001,
        Low: 20.91,
        Close: 21.110001,
        Volume: 1732400,
        AdjClose: 18.322919
    },
    {
        Date: "2013-02-11",
        Open: 21.08,
        High: 21.08,
        Low: 20.93,
        Close: 21.08,
        Volume: 1177800,
        AdjClose: 18.296879
    },
    {
        Date: "2013-02-08",
        Open: 21.030001,
        High: 21.08,
        Low: 20.98,
        Close: 21.08,
        Volume: 1298500,
        AdjClose: 18.296879
    },
    {
        Date: "2013-02-07",
        Open: 21.17,
        High: 21.200001,
        Low: 20.84,
        Close: 20.93,
        Volume: 1490800,
        AdjClose: 18.166683
    },
    {
        Date: "2013-02-06",
        Open: 21.1,
        High: 21.33,
        Low: 21.08,
        Close: 21.309999,
        Volume: 1175800,
        AdjClose: 18.496512
    },
    {
        Date: "2013-02-05",
        Open: 21.16,
        High: 21.299999,
        Low: 21.129999,
        Close: 21.24,
        Volume: 1134800,
        AdjClose: 18.435755
    },
    {
        Date: "2013-02-04",
        Open: 21.27,
        High: 21.34,
        Low: 21,
        Close: 21.030001,
        Volume: 1892800,
        AdjClose: 18.253481
    },
    {
        Date: "2013-02-01",
        Open: 21.719999,
        High: 21.92,
        Low: 21.700001,
        Close: 21.879999,
        Volume: 1651200,
        AdjClose: 18.991257
    },
    {
        Date: "2013-01-31",
        Open: 21.530001,
        High: 21.57,
        Low: 21.370001,
        Close: 21.41,
        Volume: 1527300,
        AdjClose: 18.58331
    },
    {
        Date: "2013-01-30",
        Open: 21.59,
        High: 21.74,
        Low: 21.540001,
        Close: 21.629999,
        Volume: 2390500,
        AdjClose: 18.774264
    },
    {
        Date: "2013-01-29",
        Open: 21.379999,
        High: 21.549999,
        Low: 21.370001,
        Close: 21.51,
        Volume: 1657800,
        AdjClose: 18.670108
    },
    {
        Date: "2013-01-28",
        Open: 21.379999,
        High: 21.389999,
        Low: 21.290001,
        Close: 21.290001,
        Volume: 1582800,
        AdjClose: 18.479154
    },
    {
        Date: "2013-01-25",
        Open: 21.219999,
        High: 21.360001,
        Low: 21.190001,
        Close: 21.33,
        Volume: 1639700,
        AdjClose: 18.513872
    },
    {
        Date: "2013-01-24",
        Open: 20.950001,
        High: 21.129999,
        Low: 20.950001,
        Close: 21.08,
        Volume: 1963800,
        AdjClose: 18.296879
    },
    {
        Date: "2013-01-23",
        Open: 20.860001,
        High: 21.049999,
        Low: 20.84,
        Close: 20.969999,
        Volume: 4093900,
        AdjClose: 18.201401
    },
    {
        Date: "2013-01-22",
        Open: 20.889999,
        High: 21.07,
        Low: 20.809999,
        Close: 21.049999,
        Volume: 2685900,
        AdjClose: 18.270839
    },
    {
        Date: "2013-01-18",
        Open: 20.99,
        High: 21.17,
        Low: 20.98,
        Close: 21.139999,
        Volume: 1844500,
        AdjClose: 18.348957
    },
    {
        Date: "2013-01-17",
        Open: 21.309999,
        High: 21.370001,
        Low: 21.049999,
        Close: 21.139999,
        Volume: 2914500,
        AdjClose: 18.348957
    },
    {
        Date: "2013-01-16",
        Open: 20.959999,
        High: 21.16,
        Low: 20.93,
        Close: 21.059999,
        Volume: 1817400,
        AdjClose: 18.279519
    },
    {
        Date: "2013-01-15",
        Open: 20.85,
        High: 21.09,
        Low: 20.84,
        Close: 21.030001,
        Volume: 1812800,
        AdjClose: 18.253481
    },
    {
        Date: "2013-01-14",
        Open: 21.27,
        High: 21.32,
        Low: 21.15,
        Close: 21.18,
        Volume: 1305600,
        AdjClose: 18.383677
    },
    {
        Date: "2013-01-11",
        Open: 21.5,
        High: 21.57,
        Low: 21.42,
        Close: 21.540001,
        Volume: 1056100,
        AdjClose: 18.696148
    },
    {
        Date: "2013-01-10",
        Open: 21.379999,
        High: 21.57,
        Low: 21.309999,
        Close: 21.540001,
        Volume: 1222700,
        AdjClose: 18.696148
    },
    {
        Date: "2013-01-09",
        Open: 20.99,
        High: 21.27,
        Low: 20.98,
        Close: 21.24,
        Volume: 1416300,
        AdjClose: 18.435755
    },
    {
        Date: "2013-01-08",
        Open: 20.9,
        High: 20.950001,
        Low: 20.790001,
        Close: 20.870001,
        Volume: 992200,
        AdjClose: 18.114605
    },
    {
        Date: "2013-01-07",
        Open: 20.83,
        High: 20.99,
        Low: 20.780001,
        Close: 20.99,
        Volume: 1362800,
        AdjClose: 18.218761
    },
    {
        Date: "2013-01-04",
        Open: 20.809999,
        High: 21.1,
        Low: 20.790001,
        Close: 21.08,
        Volume: 1355200,
        AdjClose: 18.296879
    },
    {
        Date: "2013-01-03",
        Open: 20.870001,
        High: 21.059999,
        Low: 20.84,
        Close: 20.879999,
        Volume: 1640300,
        AdjClose: 18.123284
    },
    {
        Date: "2013-01-02",
        Open: 21.209999,
        High: 21.32,
        Low: 21.059999,
        Close: 21.190001,
        Volume: 2097200,
        AdjClose: 18.392357
    },
    {
        Date: "2012-12-31",
        Open: 20.530001,
        High: 20.85,
        Low: 20.469999,
        Close: 20.790001,
        Volume: 1548700,
        AdjClose: 18.045168
    },
    {
        Date: "2012-12-28",
        Open: 20.559999,
        High: 20.639999,
        Low: 20.459999,
        Close: 20.49,
        Volume: 1844900,
        AdjClose: 17.784775
    },
    {
        Date: "2012-12-27",
        Open: 20.82,
        High: 20.84,
        Low: 20.58,
        Close: 20.790001,
        Volume: 1308600,
        AdjClose: 18.045168
    },
    {
        Date: "2012-12-26",
        Open: 20.620001,
        High: 20.780001,
        Low: 20.57,
        Close: 20.639999,
        Volume: 888300,
        AdjClose: 17.91497
    },
    {
        Date: "2012-12-24",
        Open: 20.68,
        High: 20.74,
        Low: 20.52,
        Close: 20.57,
        Volume: 648300,
        AdjClose: 17.854212
    },
    {
        Date: "2012-12-21",
        Open: 20.639999,
        High: 20.84,
        Low: 20.610001,
        Close: 20.83,
        Volume: 1730500,
        AdjClose: 18.079886
    },
    {
        Date: "2012-12-20",
        Open: 20.75,
        High: 20.84,
        Low: 20.629999,
        Close: 20.799999,
        Volume: 1400800,
        AdjClose: 18.053846
    },
    {
        Date: "2012-12-19",
        Open: 20.790001,
        High: 20.83,
        Low: 20.65,
        Close: 20.68,
        Volume: 1661600,
        AdjClose: 17.94969
    },
    {
        Date: "2012-12-18",
        Open: 20.57,
        High: 20.719999,
        Low: 20.540001,
        Close: 20.719999,
        Volume: 1503900,
        AdjClose: 17.984408
    },
    {
        Date: "2012-12-17",
        Open: 20.360001,
        High: 20.49,
        Low: 20.360001,
        Close: 20.49,
        Volume: 1725200,
        AdjClose: 17.784775
    },
    {
        Date: "2012-12-14",
        Open: 20.26,
        High: 20.440001,
        Low: 20.24,
        Close: 20.360001,
        Volume: 1621200,
        AdjClose: 17.671939
    },
    {
        Date: "2012-12-13",
        Open: 20.17,
        High: 20.280001,
        Low: 20.08,
        Close: 20.190001,
        Volume: 1420900,
        AdjClose: 17.524383
    },
    {
        Date: "2012-12-12",
        Open: 20.07,
        High: 20.25,
        Low: 20.07,
        Close: 20.09,
        Volume: 1269700,
        AdjClose: 17.437585
    },
    {
        Date: "2012-12-11",
        Open: 19.93,
        High: 20.08,
        Low: 19.92,
        Close: 20.030001,
        Volume: 1489000,
        AdjClose: 17.385508
    },
    {
        Date: "2012-12-10",
        Open: 19.700001,
        High: 19.9,
        Low: 19.690001,
        Close: 19.84,
        Volume: 1385600,
        AdjClose: 17.220592
    },
    {
        Date: "2012-12-07",
        Open: 19.83,
        High: 19.9,
        Low: 19.75,
        Close: 19.889999,
        Volume: 1025500,
        AdjClose: 17.26399
    },
    {
        Date: "2012-12-06",
        Open: 19.85,
        High: 19.879999,
        Low: 19.76,
        Close: 19.879999,
        Volume: 1293800,
        AdjClose: 17.25531
    },
    {
        Date: "2012-12-05",
        Open: 19.75,
        High: 19.940001,
        Low: 19.68,
        Close: 19.870001,
        Volume: 1700900,
        AdjClose: 17.246632
    },
    {
        Date: "2012-12-04",
        Open: 19.67,
        High: 19.719999,
        Low: 19.58,
        Close: 19.66,
        Volume: 1737000,
        AdjClose: 17.064357
    },
    {
        Date: "2012-12-03",
        Open: 19.690001,
        High: 19.709999,
        Low: 19.42,
        Close: 19.43,
        Volume: 1500800,
        AdjClose: 16.864723
    },
    {
        Date: "2012-11-30",
        Open: 19.52,
        High: 19.57,
        Low: 19.35,
        Close: 19.42,
        Volume: 1701400,
        AdjClose: 16.856043
    },
    {
        Date: "2012-11-29",
        Open: 19.389999,
        High: 19.469999,
        Low: 19.24,
        Close: 19.389999,
        Volume: 1874600,
        AdjClose: 16.830003
    },
    {
        Date: "2012-11-28",
        Open: 18.809999,
        High: 19.209999,
        Low: 18.77,
        Close: 19.18,
        Volume: 1841300,
        AdjClose: 16.64773
    },
    {
        Date: "2012-11-27",
        Open: 18.85,
        High: 18.98,
        Low: 18.780001,
        Close: 18.860001,
        Volume: 2167800,
        AdjClose: 16.369979
    },
    {
        Date: "2012-11-26",
        Open: 18.74,
        High: 18.879999,
        Low: 18.73,
        Close: 18.870001,
        Volume: 1517600,
        AdjClose: 16.378659
    },
    {
        Date: "2012-11-23",
        Open: 18.790001,
        High: 18.85,
        Low: 18.700001,
        Close: 18.809999,
        Volume: 1439800,
        AdjClose: 16.326579
    },
    {
        Date: "2012-11-21",
        Open: 18.27,
        High: 18.299999,
        Low: 18.16,
        Close: 18.280001,
        Volume: 2982800,
        AdjClose: 15.866554
    },
    {
        Date: "2012-11-20",
        Open: 18.129999,
        High: 18.139999,
        Low: 17.950001,
        Close: 18,
        Volume: 3151700,
        AdjClose: 15.623521
    },
    {
        Date: "2012-11-19",
        Open: 17.98,
        High: 18.1,
        Low: 17.98,
        Close: 18.049999,
        Volume: 2878700,
        AdjClose: 15.666919
    },
    {
        Date: "2012-11-16",
        Open: 17.65,
        High: 17.74,
        Low: 17.49,
        Close: 17.690001,
        Volume: 2698200,
        AdjClose: 15.35445
    },
    {
        Date: "2012-11-15",
        Open: 17.92,
        High: 17.98,
        Low: 17.700001,
        Close: 17.75,
        Volume: 2668400,
        AdjClose: 15.406528
    },
    {
        Date: "2012-11-14",
        Open: 18.09,
        High: 18.1,
        Low: 17.73,
        Close: 17.799999,
        Volume: 3181000,
        AdjClose: 15.449926
    },
    {
        Date: "2012-11-13",
        Open: 17.9,
        High: 18.17,
        Low: 17.879999,
        Close: 17.99,
        Volume: 3002500,
        AdjClose: 15.614841
    },
    {
        Date: "2012-11-12",
        Open: 18.219999,
        High: 18.32,
        Low: 18.18,
        Close: 18.219999,
        Volume: 960000,
        AdjClose: 15.814475
    },
    {
        Date: "2012-11-09",
        Open: 18.139999,
        High: 18.379999,
        Low: 18.139999,
        Close: 18.190001,
        Volume: 1729900,
        AdjClose: 15.788436
    },
    {
        Date: "2012-11-08",
        Open: 18.440001,
        High: 18.540001,
        Low: 18.219999,
        Close: 18.280001,
        Volume: 2512100,
        AdjClose: 15.866554
    },
    {
        Date: "2012-11-07",
        Open: 18.440001,
        High: 18.459999,
        Low: 18.24,
        Close: 18.35,
        Volume: 1643200,
        AdjClose: 15.927312
    },
    {
        Date: "2012-11-06",
        Open: 18.530001,
        High: 18.67,
        Low: 18.49,
        Close: 18.57,
        Volume: 1316100,
        AdjClose: 16.118266
    },
    {
        Date: "2012-11-05",
        Open: 18.16,
        High: 18.26,
        Low: 18.1,
        Close: 18.24,
        Volume: 1265300,
        AdjClose: 15.831834
    },
    {
        Date: "2012-11-02",
        Open: 18.5,
        High: 18.51,
        Low: 18.16,
        Close: 18.18,
        Volume: 1278400,
        AdjClose: 15.779756
    },
    {
        Date: "2012-11-01",
        Open: 18.17,
        High: 18.33,
        Low: 18.120001,
        Close: 18.299999,
        Volume: 1872900,
        AdjClose: 15.883912
    },
    {
        Date: "2012-10-31",
        Open: 18.280001,
        High: 18.290001,
        Low: 17.98,
        Close: 18.059999,
        Volume: 2117700,
        AdjClose: 15.675599
    },
    {
        Date: "2012-10-26",
        Open: 18.200001,
        High: 18.290001,
        Low: 18.139999,
        Close: 18.190001,
        Volume: 1950400,
        AdjClose: 15.788436
    },
    {
        Date: "2012-10-25",
        Open: 18.66,
        High: 18.709999,
        Low: 18.43,
        Close: 18.5,
        Volume: 3161100,
        AdjClose: 16.057508
    },
    {
        Date: "2012-10-24",
        Open: 18.91,
        High: 18.92,
        Low: 18.74,
        Close: 18.77,
        Volume: 1558300,
        AdjClose: 16.291861
    },
    {
        Date: "2012-10-23",
        Open: 19.030001,
        High: 19.030001,
        Low: 18.809999,
        Close: 18.91,
        Volume: 2371700,
        AdjClose: 16.413377
    },
    {
        Date: "2012-10-22",
        Open: 19.540001,
        High: 19.65,
        Low: 19.41,
        Close: 19.57,
        Volume: 2439300,
        AdjClose: 16.986239
    },
    {
        Date: "2012-10-19",
        Open: 19.629999,
        High: 19.629999,
        Low: 19.27,
        Close: 19.32,
        Volume: 1388100,
        AdjClose: 16.769246
    },
    {
        Date: "2012-10-18",
        Open: 19.66,
        High: 19.860001,
        Low: 19.629999,
        Close: 19.75,
        Volume: 1826300,
        AdjClose: 17.142474
    },
    {
        Date: "2012-10-17",
        Open: 19.5,
        High: 19.629999,
        Low: 19.459999,
        Close: 19.559999,
        Volume: 1512200,
        AdjClose: 16.977559
    },
    {
        Date: "2012-10-16",
        Open: 19.18,
        High: 19.299999,
        Low: 19.15,
        Close: 19.280001,
        Volume: 1141400,
        AdjClose: 16.734528
    },
    {
        Date: "2012-10-15",
        Open: 18.93,
        High: 19.049999,
        Low: 18.799999,
        Close: 19.030001,
        Volume: 1268600,
        AdjClose: 16.517534
    },
    {
        Date: "2012-10-12",
        Open: 19.01,
        High: 19.1,
        Low: 18.84,
        Close: 18.889999,
        Volume: 1519000,
        AdjClose: 16.396017
    },
    {
        Date: "2012-10-11",
        Open: 18.959999,
        High: 19.040001,
        Low: 18.790001,
        Close: 18.809999,
        Volume: 1352700,
        AdjClose: 16.326579
    },
    {
        Date: "2012-10-10",
        Open: 18.66,
        High: 18.68,
        Low: 18.459999,
        Close: 18.559999,
        Volume: 1442000,
        AdjClose: 16.109586
    },
    {
        Date: "2012-10-09",
        Open: 19.059999,
        High: 19.1,
        Low: 18.790001,
        Close: 18.790001,
        Volume: 1465700,
        AdjClose: 16.309221
    },
    {
        Date: "2012-10-08",
        Open: 19.24,
        High: 19.290001,
        Low: 19.17,
        Close: 19.209999,
        Volume: 1754300,
        AdjClose: 16.673768
    },
    {
        Date: "2012-10-05",
        Open: 19.52,
        High: 19.57,
        Low: 19.32,
        Close: 19.4,
        Volume: 1921000,
        AdjClose: 16.838683
    },
    {
        Date: "2012-10-04",
        Open: 19.15,
        High: 19.42,
        Low: 19.139999,
        Close: 19.360001,
        Volume: 1705100,
        AdjClose: 16.803965
    },
    {
        Date: "2012-10-03",
        Open: 19.16,
        High: 19.18,
        Low: 18.940001,
        Close: 19.01,
        Volume: 2025000,
        AdjClose: 16.500174
    },
    {
        Date: "2012-10-02",
        Open: 19.309999,
        High: 19.33,
        Low: 18.99,
        Close: 19.1,
        Volume: 2374000,
        AdjClose: 16.578292
    },
    {
        Date: "2012-10-01",
        Open: 19.02,
        High: 19.16,
        Low: 18.870001,
        Close: 18.940001,
        Volume: 2218600,
        AdjClose: 16.439416
    },
    {
        Date: "2012-09-28",
        Open: 18.99,
        High: 19.01,
        Low: 18.67,
        Close: 18.700001,
        Volume: 2118900,
        AdjClose: 16.231103
    },
    {
        Date: "2012-09-27",
        Open: 19.02,
        High: 19.190001,
        Low: 18.889999,
        Close: 19.18,
        Volume: 1914500,
        AdjClose: 16.64773
    },
    {
        Date: "2012-09-26",
        Open: 18.9,
        High: 18.959999,
        Low: 18.719999,
        Close: 18.879999,
        Volume: 1501000,
        AdjClose: 16.387337
    },
    {
        Date: "2012-09-25",
        Open: 19.379999,
        High: 19.42,
        Low: 19.030001,
        Close: 19.030001,
        Volume: 1467400,
        AdjClose: 16.517534
    },
    {
        Date: "2012-09-24",
        Open: 19.299999,
        High: 19.469999,
        Low: 19.24,
        Close: 19.379999,
        Volume: 1614100,
        AdjClose: 16.821324
    },
    {
        Date: "2012-09-21",
        Open: 19.700001,
        High: 19.709999,
        Low: 19.48,
        Close: 19.5,
        Volume: 1532400,
        AdjClose: 16.925481
    },
    {
        Date: "2012-09-20",
        Open: 19.370001,
        High: 19.530001,
        Low: 19.309999,
        Close: 19.48,
        Volume: 1553200,
        AdjClose: 16.908121
    },
    {
        Date: "2012-09-19",
        Open: 19.629999,
        High: 19.860001,
        Low: 19.530001,
        Close: 19.780001,
        Volume: 1928400,
        AdjClose: 17.168514
    },
    {
        Date: "2012-09-18",
        Open: 19.610001,
        High: 19.700001,
        Low: 19.57,
        Close: 19.610001,
        Volume: 2106300,
        AdjClose: 17.020959
    },
    {
        Date: "2012-09-17",
        Open: 19.889999,
        High: 19.950001,
        Low: 19.780001,
        Close: 19.889999,
        Volume: 2673500,
        AdjClose: 17.26399
    },
    {
        Date: "2012-09-14",
        Open: 20.1,
        High: 20.309999,
        Low: 19.879999,
        Close: 20.030001,
        Volume: 3728100,
        AdjClose: 17.385508
    },
    {
        Date: "2012-09-13",
        Open: 19.209999,
        High: 19.57,
        Low: 19.110001,
        Close: 19.469999,
        Volume: 4038700,
        AdjClose: 16.899441
    },
    {
        Date: "2012-09-12",
        Open: 19.1,
        High: 19.209999,
        Low: 18.99,
        Close: 19.059999,
        Volume: 2232900,
        AdjClose: 16.543572
    },
    {
        Date: "2012-09-11",
        Open: 18.719999,
        High: 18.950001,
        Low: 18.709999,
        Close: 18.9,
        Volume: 3160100,
        AdjClose: 16.404697
    },
    {
        Date: "2012-09-10",
        Open: 18.76,
        High: 18.77,
        Low: 18.51,
        Close: 18.540001,
        Volume: 1920300,
        AdjClose: 16.092227
    },
    {
        Date: "2012-09-07",
        Open: 18.58,
        High: 18.700001,
        Low: 18.51,
        Close: 18.65,
        Volume: 3005200,
        AdjClose: 16.187703
    },
    {
        Date: "2012-09-06",
        Open: 17.690001,
        High: 17.959999,
        Low: 17.68,
        Close: 17.91,
        Volume: 3082300,
        AdjClose: 15.545403
    },
    {
        Date: "2012-09-05",
        Open: 17.219999,
        High: 17.389999,
        Low: 17.16,
        Close: 17.32,
        Volume: 3186600,
        AdjClose: 15.033299
    },
    {
        Date: "2012-09-04",
        Open: 17.34,
        High: 17.370001,
        Low: 17.139999,
        Close: 17.24,
        Volume: 1474300,
        AdjClose: 14.963861
    },
    {
        Date: "2012-08-31",
        Open: 17.469999,
        High: 17.48,
        Low: 17.23,
        Close: 17.32,
        Volume: 1452800,
        AdjClose: 15.033299
    },
    {
        Date: "2012-08-30",
        Open: 17.190001,
        High: 17.200001,
        Low: 17.040001,
        Close: 17.129999,
        Volume: 1525900,
        AdjClose: 14.868383
    },
    {
        Date: "2012-08-29",
        Open: 17.440001,
        High: 17.450001,
        Low: 17.299999,
        Close: 17.379999,
        Volume: 947500,
        AdjClose: 15.085377
    },
    {
        Date: "2012-08-28",
        Open: 17.309999,
        High: 17.41,
        Low: 17.27,
        Close: 17.35,
        Volume: 1850700,
        AdjClose: 15.059339
    },
    {
        Date: "2012-08-27",
        Open: 17.58,
        High: 17.610001,
        Low: 17.48,
        Close: 17.48,
        Volume: 1092100,
        AdjClose: 15.172174
    },
    {
        Date: "2012-08-24",
        Open: 17.51,
        High: 17.67,
        Low: 17.49,
        Close: 17.540001,
        Volume: 1649000,
        AdjClose: 15.224254
    },
    {
        Date: "2012-08-23",
        Open: 17.68,
        High: 17.719999,
        Low: 17.57,
        Close: 17.629999,
        Volume: 1668700,
        AdjClose: 15.30237
    },
    {
        Date: "2012-08-22",
        Open: 17.469999,
        High: 17.77,
        Low: 17.450001,
        Close: 17.74,
        Volume: 2237400,
        AdjClose: 15.397848
    },
    {
        Date: "2012-08-21",
        Open: 17.84,
        High: 17.950001,
        Low: 17.76,
        Close: 17.790001,
        Volume: 2076200,
        AdjClose: 15.441247
    },
    {
        Date: "2012-08-20",
        Open: 17.99,
        High: 18.1,
        Low: 17.91,
        Close: 18.030001,
        Volume: 1464500,
        AdjClose: 15.649561
    },
    {
        Date: "2012-08-17",
        Open: 17.879999,
        High: 17.950001,
        Low: 17.790001,
        Close: 17.950001,
        Volume: 1341300,
        AdjClose: 15.580123
    },
    {
        Date: "2012-08-16",
        Open: 17.719999,
        High: 18,
        Low: 17.68,
        Close: 17.959999,
        Volume: 1612300,
        AdjClose: 15.588801
    },
    {
        Date: "2012-08-15",
        Open: 17.77,
        High: 17.83,
        Low: 17.73,
        Close: 17.77,
        Volume: 1826800,
        AdjClose: 15.423887
    },
    {
        Date: "2012-08-14",
        Open: 17.91,
        High: 17.93,
        Low: 17.67,
        Close: 17.709999,
        Volume: 4367300,
        AdjClose: 15.371808
    },
    {
        Date: "2012-08-13",
        Open: 17.84,
        High: 17.969999,
        Low: 17.77,
        Close: 17.84,
        Volume: 1735600,
        AdjClose: 15.484645
    },
    {
        Date: "2012-08-10",
        Open: 17.77,
        High: 17.940001,
        Low: 17.690001,
        Close: 17.93,
        Volume: 2253100,
        AdjClose: 15.562763
    },
    {
        Date: "2012-08-09",
        Open: 17.799999,
        High: 17.93,
        Low: 17.76,
        Close: 17.879999,
        Volume: 3050700,
        AdjClose: 15.519363
    },
    {
        Date: "2012-08-08",
        Open: 17.83,
        High: 18.059999,
        Low: 17.82,
        Close: 17.99,
        Volume: 2338900,
        AdjClose: 15.614841
    },
    {
        Date: "2012-08-07",
        Open: 18.02,
        High: 18.139999,
        Low: 17.99,
        Close: 18.049999,
        Volume: 2047800,
        AdjClose: 15.666919
    },
    {
        Date: "2012-08-06",
        Open: 18.07,
        High: 18.129999,
        Low: 17.93,
        Close: 17.950001,
        Volume: 3617400,
        AdjClose: 15.580123
    },
    {
        Date: "2012-08-03",
        Open: 17.799999,
        High: 18.040001,
        Low: 17.780001,
        Close: 17.91,
        Volume: 5776300,
        AdjClose: 15.545403
    },
    {
        Date: "2012-08-02",
        Open: 17.26,
        High: 17.41,
        Low: 16.940001,
        Close: 17.120001,
        Volume: 3406600,
        AdjClose: 14.859705
    },
    {
        Date: "2012-08-01",
        Open: 17.57,
        High: 17.610001,
        Low: 17.32,
        Close: 17.360001,
        Volume: 2970700,
        AdjClose: 15.068019
    },
    {
        Date: "2012-07-31",
        Open: 17.299999,
        High: 17.469999,
        Low: 17.280001,
        Close: 17.35,
        Volume: 2546700,
        AdjClose: 15.059339
    },
    {
        Date: "2012-07-30",
        Open: 17.360001,
        High: 17.49,
        Low: 17.299999,
        Close: 17.35,
        Volume: 2435800,
        AdjClose: 15.059339
    },
    {
        Date: "2012-07-27",
        Open: 17.290001,
        High: 17.6,
        Low: 17.25,
        Close: 17.52,
        Volume: 4209100,
        AdjClose: 15.206894
    },
    {
        Date: "2012-07-26",
        Open: 16.9,
        High: 17.110001,
        Low: 16.84,
        Close: 16.969999,
        Volume: 5718600,
        AdjClose: 14.729508
    },
    {
        Date: "2012-07-25",
        Open: 16,
        High: 16,
        Low: 15.73,
        Close: 15.85,
        Volume: 2571900,
        AdjClose: 13.757379
    },
    {
        Date: "2012-07-24",
        Open: 15.92,
        High: 15.93,
        Low: 15.65,
        Close: 15.77,
        Volume: 2584100,
        AdjClose: 13.687941
    },
    {
        Date: "2012-07-23",
        Open: 15.7,
        High: 15.88,
        Low: 15.63,
        Close: 15.83,
        Volume: 3110400,
        AdjClose: 13.740019
    },
    {
        Date: "2012-07-20",
        Open: 16.17,
        High: 16.219999,
        Low: 16.08,
        Close: 16.200001,
        Volume: 2813200,
        AdjClose: 14.06117
    },
    {
        Date: "2012-07-19",
        Open: 16.200001,
        High: 16.23,
        Low: 16,
        Close: 16.049999,
        Volume: 15005100,
        AdjClose: 13.930972
    },
    {
        Date: "2012-07-18",
        Open: 15.9,
        High: 16.23,
        Low: 15.86,
        Close: 16.17,
        Volume: 12692800,
        AdjClose: 14.03513
    },
    {
        Date: "2012-07-17",
        Open: 16.379999,
        High: 16.41,
        Low: 16.209999,
        Close: 16.41,
        Volume: 4548100,
        AdjClose: 14.243443
    },
    {
        Date: "2012-07-16",
        Open: 16.18,
        High: 16.27,
        Low: 16.030001,
        Close: 16.219999,
        Volume: 1517300,
        AdjClose: 14.078528
    },
    {
        Date: "2012-07-13",
        Open: 15.81,
        High: 16.09,
        Low: 15.81,
        Close: 16.08,
        Volume: 1237000,
        AdjClose: 13.957012
    },
    {
        Date: "2012-07-12",
        Open: 15.79,
        High: 15.9,
        Low: 15.67,
        Close: 15.82,
        Volume: 1721300,
        AdjClose: 13.731339
    },
    {
        Date: "2012-07-11",
        Open: 15.95,
        High: 16.040001,
        Low: 15.8,
        Close: 15.95,
        Volume: 1949200,
        AdjClose: 13.844175
    },
    {
        Date: "2012-07-10",
        Open: 16.219999,
        High: 16.25,
        Low: 15.76,
        Close: 15.82,
        Volume: 3191500,
        AdjClose: 13.731339
    },
    {
        Date: "2012-07-09",
        Open: 15.96,
        High: 16.02,
        Low: 15.86,
        Close: 15.97,
        Volume: 2052200,
        AdjClose: 13.861535
    },
    {
        Date: "2012-07-06",
        Open: 16.17,
        High: 16.200001,
        Low: 16.02,
        Close: 16.110001,
        Volume: 1723100,
        AdjClose: 13.983052
    },
    {
        Date: "2012-07-05",
        Open: 16.389999,
        High: 16.459999,
        Low: 16.299999,
        Close: 16.32,
        Volume: 2677100,
        AdjClose: 14.165325
    },
    {
        Date: "2012-07-03",
        Open: 16.440001,
        High: 16.639999,
        Low: 16.4,
        Close: 16.639999,
        Volume: 1187600,
        AdjClose: 14.443077
    },
    {
        Date: "2012-07-02",
        Open: 16.469999,
        High: 16.469999,
        Low: 16.24,
        Close: 16.42,
        Volume: 2609200,
        AdjClose: 14.252123
    },
    {
        Date: "2012-06-29",
        Open: 16.190001,
        High: 16.32,
        Low: 16.139999,
        Close: 16.32,
        Volume: 2976300,
        AdjClose: 14.165325
    },
    {
        Date: "2012-06-28",
        Open: 15.43,
        High: 15.61,
        Low: 15.38,
        Close: 15.59,
        Volume: 2601000,
        AdjClose: 13.531705
    },
    {
        Date: "2012-06-27",
        Open: 15.46,
        High: 15.61,
        Low: 15.4,
        Close: 15.53,
        Volume: 2853100,
        AdjClose: 13.479626
    },
    {
        Date: "2012-06-26",
        Open: 15.4,
        High: 15.55,
        Low: 15.26,
        Close: 15.49,
        Volume: 2601200,
        AdjClose: 13.444908
    },
    {
        Date: "2012-06-25",
        Open: 15.57,
        High: 15.57,
        Low: 15.39,
        Close: 15.42,
        Volume: 7176000,
        AdjClose: 13.38415
    },
    {
        Date: "2012-06-22",
        Open: 16.190001,
        High: 16.200001,
        Low: 15.81,
        Close: 15.81,
        Volume: 5232300,
        AdjClose: 13.72266
    },
    {
        Date: "2012-06-21",
        Open: 16.629999,
        High: 16.65,
        Low: 16.030001,
        Close: 16.07,
        Volume: 2220900,
        AdjClose: 13.948332
    },
    {
        Date: "2012-06-20",
        Open: 16.530001,
        High: 16.690001,
        Low: 16.389999,
        Close: 16.5,
        Volume: 2296200,
        AdjClose: 14.321561
    },
    {
        Date: "2012-06-19",
        Open: 16.290001,
        High: 16.620001,
        Low: 16.290001,
        Close: 16.48,
        Volume: 2575000,
        AdjClose: 14.304201
    },
    {
        Date: "2012-06-18",
        Open: 16,
        High: 16.110001,
        Low: 15.92,
        Close: 16.059999,
        Volume: 2233300,
        AdjClose: 13.939652
    },
    {
        Date: "2012-06-15",
        Open: 15.86,
        High: 16.02,
        Low: 15.81,
        Close: 16,
        Volume: 2553100,
        AdjClose: 13.887574
    },
    {
        Date: "2012-06-14",
        Open: 15.92,
        High: 16.139999,
        Low: 15.91,
        Close: 16.059999,
        Volume: 3244500,
        AdjClose: 13.939652
    },
    {
        Date: "2012-06-13",
        Open: 15.98,
        High: 16.219999,
        Low: 15.88,
        Close: 16.059999,
        Volume: 4513400,
        AdjClose: 13.939652
    },
    {
        Date: "2012-06-12",
        Open: 16.25,
        High: 16.43,
        Low: 16.16,
        Close: 16.43,
        Volume: 2396100,
        AdjClose: 14.260803
    },
    {
        Date: "2012-06-11",
        Open: 16.540001,
        High: 16.559999,
        Low: 16.08,
        Close: 16.110001,
        Volume: 2349400,
        AdjClose: 13.983052
    },
    {
        Date: "2012-06-08",
        Open: 15.97,
        High: 16.27,
        Low: 15.97,
        Close: 16.24,
        Volume: 2123600,
        AdjClose: 14.095888
    },
    {
        Date: "2012-06-07",
        Open: 16.469999,
        High: 16.48,
        Low: 16.33,
        Close: 16.370001,
        Volume: 2862900,
        AdjClose: 14.208725
    },
    {
        Date: "2012-06-06",
        Open: 15.75,
        High: 16.17,
        Low: 15.74,
        Close: 16.129999,
        Volume: 2440400,
        AdjClose: 14.00041
    },
    {
        Date: "2012-06-05",
        Open: 15.49,
        High: 15.62,
        Low: 15.46,
        Close: 15.59,
        Volume: 1731500,
        AdjClose: 13.531705
    },
    {
        Date: "2012-06-04",
        Open: 15.65,
        High: 15.7,
        Low: 15.39,
        Close: 15.53,
        Volume: 2609700,
        AdjClose: 13.479626
    },
    {
        Date: "2012-06-01",
        Open: 15.5,
        High: 15.65,
        Low: 15.39,
        Close: 15.63,
        Volume: 4294300,
        AdjClose: 13.566424
    },
    {
        Date: "2012-05-31",
        Open: 15.83,
        High: 15.89,
        Low: 15.51,
        Close: 15.81,
        Volume: 4069900,
        AdjClose: 13.72266
    },
    {
        Date: "2012-05-30",
        Open: 16.280001,
        High: 16.299999,
        Low: 16,
        Close: 16.139999,
        Volume: 2662700,
        AdjClose: 14.00909
    },
    {
        Date: "2012-05-29",
        Open: 16.469999,
        High: 16.57,
        Low: 16.33,
        Close: 16.48,
        Volume: 2109900,
        AdjClose: 14.304201
    },
    {
        Date: "2012-05-25",
        Open: 16.15,
        High: 16.309999,
        Low: 16.15,
        Close: 16.18,
        Volume: 3349300,
        AdjClose: 14.04381
    },
    {
        Date: "2012-05-24",
        Open: 16.25,
        High: 16.4,
        Low: 16.190001,
        Close: 16.4,
        Volume: 7993300,
        AdjClose: 14.234763
    },
    {
        Date: "2012-05-23",
        Open: 16.299999,
        High: 16.379999,
        Low: 16.030001,
        Close: 16.370001,
        Volume: 2181200,
        AdjClose: 14.208725
    },
    {
        Date: "2012-05-22",
        Open: 16.639999,
        High: 16.74,
        Low: 16.33,
        Close: 16.43,
        Volume: 4224300,
        AdjClose: 14.260803
    },
    {
        Date: "2012-05-21",
        Open: 16.26,
        High: 16.469999,
        Low: 16.16,
        Close: 16.469999,
        Volume: 2250700,
        AdjClose: 14.295521
    },
    {
        Date: "2012-05-18",
        Open: 16.139999,
        High: 16.16,
        Low: 15.92,
        Close: 15.99,
        Volume: 3005700,
        AdjClose: 13.878894
    },
    {
        Date: "2012-05-17",
        Open: 16.299999,
        High: 16.35,
        Low: 16.02,
        Close: 16.07,
        Volume: 3133200,
        AdjClose: 13.948332
    },
    {
        Date: "2012-05-16",
        Open: 16.66,
        High: 16.75,
        Low: 16.25,
        Close: 16.25,
        Volume: 4551000,
        AdjClose: 14.104568
    },
    {
        Date: "2012-05-15",
        Open: 16.440001,
        High: 16.559999,
        Low: 16.309999,
        Close: 16.450001,
        Volume: 5577900,
        AdjClose: 14.278163
    },
    {
        Date: "2012-05-14",
        Open: 16.59,
        High: 16.620001,
        Low: 16.450001,
        Close: 16.469999,
        Volume: 3069100,
        AdjClose: 14.295521
    },
    {
        Date: "2012-05-11",
        Open: 16.76,
        High: 17.040001,
        Low: 16.719999,
        Close: 16.84,
        Volume: 3886100,
        AdjClose: 14.616672
    },
    {
        Date: "2012-05-10",
        Open: 17.299999,
        High: 17.360001,
        Low: 17.08,
        Close: 17.139999,
        Volume: 3692200,
        AdjClose: 14.877063
    },
    {
        Date: "2012-05-09",
        Open: 17.030001,
        High: 17.32,
        Low: 16.99,
        Close: 17.200001,
        Volume: 3195500,
        AdjClose: 14.929143
    },
    {
        Date: "2012-05-08",
        Open: 17.43,
        High: 17.6,
        Low: 17.299999,
        Close: 17.559999,
        Volume: 2536100,
        AdjClose: 15.241612
    },
    {
        Date: "2012-05-07",
        Open: 17.809999,
        High: 17.84,
        Low: 17.68,
        Close: 17.73,
        Volume: 3150000,
        AdjClose: 15.389168
    },
    {
        Date: "2012-05-04",
        Open: 17.889999,
        High: 17.959999,
        Low: 17.639999,
        Close: 17.68,
        Volume: 2354900,
        AdjClose: 15.34577
    },
    {
        Date: "2012-05-03",
        Open: 18.110001,
        High: 18.15,
        Low: 17.82,
        Close: 17.91,
        Volume: 2410300,
        AdjClose: 15.545403
    },
    {
        Date: "2012-05-02",
        Open: 18.1,
        High: 18.27,
        Low: 17.940001,
        Close: 18.27,
        Volume: 3857800,
        AdjClose: 15.857874
    },
    {
        Date: "2012-05-01",
        Open: 18.16,
        High: 18.379999,
        Low: 18.15,
        Close: 18.26,
        Volume: 3022800,
        AdjClose: 15.849194
    },
    {
        Date: "2012-04-30",
        Open: 18.940001,
        High: 18.950001,
        Low: 18.780001,
        Close: 18.870001,
        Volume: 3512900,
        AdjClose: 15.764133
    },
    {
        Date: "2012-04-27",
        Open: 18.959999,
        High: 18.98,
        Low: 18.799999,
        Close: 18.809999,
        Volume: 6163700,
        AdjClose: 15.714008
    },
    {
        Date: "2012-04-26",
        Open: 18.780001,
        High: 19.18,
        Low: 18.75,
        Close: 19.08,
        Volume: 5522600,
        AdjClose: 15.939568
    },
    {
        Date: "2012-04-25",
        Open: 19.51,
        High: 19.559999,
        Low: 19.25,
        Close: 19.35,
        Volume: 5275500,
        AdjClose: 16.165128
    },
    {
        Date: "2012-04-24",
        Open: 19.93,
        High: 20.24,
        Low: 19.889999,
        Close: 20.219999,
        Volume: 3268500,
        AdjClose: 16.891932
    },
    {
        Date: "2012-04-23",
        Open: 19.83,
        High: 19.889999,
        Low: 19.559999,
        Close: 19.860001,
        Volume: 4214800,
        AdjClose: 16.591186
    },
    {
        Date: "2012-04-20",
        Open: 20.41,
        High: 20.66,
        Low: 20.32,
        Close: 20.48,
        Volume: 2652000,
        AdjClose: 17.109138
    },
    {
        Date: "2012-04-19",
        Open: 20.190001,
        High: 20.34,
        Low: 19.889999,
        Close: 20.09,
        Volume: 2643800,
        AdjClose: 16.78333
    },
    {
        Date: "2012-04-18",
        Open: 20.040001,
        High: 20.27,
        Low: 20.030001,
        Close: 20.1,
        Volume: 2271900,
        AdjClose: 16.791684
    },
    {
        Date: "2012-04-17",
        Open: 20.17,
        High: 20.35,
        Low: 20,
        Close: 20.27,
        Volume: 5168700,
        AdjClose: 16.933703
    },
    {
        Date: "2012-04-16",
        Open: 19.68,
        High: 19.74,
        Low: 19.459999,
        Close: 19.66,
        Volume: 2471200,
        AdjClose: 16.424104
    },
    {
        Date: "2012-04-13",
        Open: 19.65,
        High: 19.66,
        Low: 19.35,
        Close: 19.35,
        Volume: 1846800,
        AdjClose: 16.165128
    },
    {
        Date: "2012-04-12",
        Open: 19.719999,
        High: 20.02,
        Low: 19.709999,
        Close: 19.93,
        Volume: 2838900,
        AdjClose: 16.649665
    },
    {
        Date: "2012-04-11",
        Open: 19.690001,
        High: 19.690001,
        Low: 19.469999,
        Close: 19.549999,
        Volume: 3892000,
        AdjClose: 16.332209
    },
    {
        Date: "2012-04-10",
        Open: 19.57,
        High: 19.67,
        Low: 19.23,
        Close: 19.23,
        Volume: 2614400,
        AdjClose: 16.064879
    },
    {
        Date: "2012-04-09",
        Open: 19.690001,
        High: 19.790001,
        Low: 19.5,
        Close: 19.77,
        Volume: 1082400,
        AdjClose: 16.515999
    },
    {
        Date: "2012-04-05",
        Open: 19.690001,
        High: 19.950001,
        Low: 19.67,
        Close: 19.92,
        Volume: 1736000,
        AdjClose: 16.64131
    },
    {
        Date: "2012-04-04",
        Open: 19.950001,
        High: 20.049999,
        Low: 19.860001,
        Close: 19.98,
        Volume: 2881100,
        AdjClose: 16.691434
    },
    {
        Date: "2012-04-03",
        Open: 20.719999,
        High: 20.76,
        Low: 20.290001,
        Close: 20.43,
        Volume: 2217200,
        AdjClose: 17.067368
    },
    {
        Date: "2012-04-02",
        Open: 20.459999,
        High: 20.799999,
        Low: 20.4,
        Close: 20.700001,
        Volume: 1820100,
        AdjClose: 17.292928
    },
    {
        Date: "2012-03-30",
        Open: 20.389999,
        High: 20.530001,
        Low: 20.110001,
        Close: 20.41,
        Volume: 4917400,
        AdjClose: 17.05066
    },
    {
        Date: "2012-03-29",
        Open: 19.959999,
        High: 20.059999,
        Low: 19.879999,
        Close: 20,
        Volume: 5157700,
        AdjClose: 16.708143
    },
    {
        Date: "2012-03-28",
        Open: 20.4,
        High: 20.41,
        Low: 20.030001,
        Close: 20.200001,
        Volume: 1775700,
        AdjClose: 16.875225
    },
    {
        Date: "2012-03-27",
        Open: 20.540001,
        High: 20.57,
        Low: 20.280001,
        Close: 20.290001,
        Volume: 2031800,
        AdjClose: 16.950412
    },
    {
        Date: "2012-03-26",
        Open: 20.559999,
        High: 20.66,
        Low: 20.5,
        Close: 20.610001,
        Volume: 2498500,
        AdjClose: 17.217742
    },
    {
        Date: "2012-03-23",
        Open: 20.129999,
        High: 20.389999,
        Low: 20.02,
        Close: 20.34,
        Volume: 1545100,
        AdjClose: 16.992181
    },
    {
        Date: "2012-03-22",
        Open: 19.969999,
        High: 20.18,
        Low: 19.93,
        Close: 20.09,
        Volume: 2430600,
        AdjClose: 16.78333
    },
    {
        Date: "2012-03-21",
        Open: 20.6,
        High: 20.639999,
        Low: 20.389999,
        Close: 20.450001,
        Volume: 1239500,
        AdjClose: 17.084077
    },
    {
        Date: "2012-03-20",
        Open: 20.65,
        High: 20.73,
        Low: 20.5,
        Close: 20.610001,
        Volume: 1492500,
        AdjClose: 17.217742
    },
    {
        Date: "2012-03-19",
        Open: 21,
        High: 21.08,
        Low: 20.92,
        Close: 20.99,
        Volume: 3734100,
        AdjClose: 17.535196
    },
    {
        Date: "2012-03-16",
        Open: 21.030001,
        High: 21.110001,
        Low: 20.799999,
        Close: 20.879999,
        Volume: 2226000,
        AdjClose: 17.4433
    },
    {
        Date: "2012-03-15",
        Open: 20.83,
        High: 21.059999,
        Low: 20.74,
        Close: 21,
        Volume: 2447900,
        AdjClose: 17.54355
    },
    {
        Date: "2012-03-14",
        Open: 20.549999,
        High: 20.73,
        Low: 20.51,
        Close: 20.66,
        Volume: 2813300,
        AdjClose: 17.259511
    },
    {
        Date: "2012-03-13",
        Open: 20.49,
        High: 20.700001,
        Low: 20.42,
        Close: 20.66,
        Volume: 2212700,
        AdjClose: 17.259511
    },
    {
        Date: "2012-03-12",
        Open: 20.299999,
        High: 20.33,
        Low: 20.120001,
        Close: 20.299999,
        Volume: 1912200,
        AdjClose: 16.958764
    },
    {
        Date: "2012-03-09",
        Open: 20.18,
        High: 20.32,
        Low: 20.110001,
        Close: 20.24,
        Volume: 2636700,
        AdjClose: 16.90864
    },
    {
        Date: "2012-03-08",
        Open: 20.129999,
        High: 20.360001,
        Low: 20,
        Close: 20.280001,
        Volume: 2079600,
        AdjClose: 16.942057
    },
    {
        Date: "2012-03-07",
        Open: 19.790001,
        High: 19.9,
        Low: 19.68,
        Close: 19.82,
        Volume: 2262700,
        AdjClose: 16.557769
    },
    {
        Date: "2012-03-06",
        Open: 19.65,
        High: 19.67,
        Low: 19.32,
        Close: 19.450001,
        Volume: 3391200,
        AdjClose: 16.248669
    },
    {
        Date: "2012-03-05",
        Open: 20.5,
        High: 20.52,
        Low: 20.299999,
        Close: 20.389999,
        Volume: 1855200,
        AdjClose: 17.033951
    },
    {
        Date: "2012-03-02",
        Open: 20.6,
        High: 20.639999,
        Low: 20.42,
        Close: 20.6,
        Volume: 1886800,
        AdjClose: 17.209387
    },
    {
        Date: "2012-03-01",
        Open: 20.690001,
        High: 20.77,
        Low: 20.52,
        Close: 20.700001,
        Volume: 2118900,
        AdjClose: 17.292928
    },
    {
        Date: "2012-02-29",
        Open: 20.860001,
        High: 20.9,
        Low: 20.43,
        Close: 20.49,
        Volume: 2116600,
        AdjClose: 17.117492
    },
    {
        Date: "2012-02-28",
        Open: 20.68,
        High: 20.969999,
        Low: 20.620001,
        Close: 20.889999,
        Volume: 1746800,
        AdjClose: 17.451655
    },
    {
        Date: "2012-02-27",
        Open: 20.51,
        High: 20.870001,
        Low: 20.48,
        Close: 20.799999,
        Volume: 2215300,
        AdjClose: 17.376468
    },
    {
        Date: "2012-02-24",
        Open: 20.950001,
        High: 21.07,
        Low: 20.870001,
        Close: 20.92,
        Volume: 1285900,
        AdjClose: 17.476717
    },
    {
        Date: "2012-02-23",
        Open: 20.629999,
        High: 20.91,
        Low: 20.469999,
        Close: 20.9,
        Volume: 2467700,
        AdjClose: 17.460009
    },
    {
        Date: "2012-02-22",
        Open: 20.629999,
        High: 20.809999,
        Low: 20.5,
        Close: 20.76,
        Volume: 2712400,
        AdjClose: 17.343052
    },
    {
        Date: "2012-02-21",
        Open: 20.870001,
        High: 20.98,
        Low: 20.74,
        Close: 20.83,
        Volume: 3582300,
        AdjClose: 17.401531
    },
    {
        Date: "2012-02-17",
        Open: 21,
        High: 21.120001,
        Low: 20.84,
        Close: 21.030001,
        Volume: 3087700,
        AdjClose: 17.568613
    },
    {
        Date: "2012-02-16",
        Open: 20.639999,
        High: 21.219999,
        Low: 20.610001,
        Close: 21.1,
        Volume: 10822600,
        AdjClose: 17.627091
    },
    {
        Date: "2012-02-15",
        Open: 21.58,
        High: 21.68,
        Low: 21.459999,
        Close: 21.67,
        Volume: 3305800,
        AdjClose: 18.103273
    },
    {
        Date: "2012-02-14",
        Open: 21.49,
        High: 21.540001,
        Low: 21.219999,
        Close: 21.440001,
        Volume: 2116600,
        AdjClose: 17.911129
    },
    {
        Date: "2012-02-13",
        Open: 21.76,
        High: 21.77,
        Low: 21.540001,
        Close: 21.709999,
        Volume: 1331900,
        AdjClose: 18.136688
    },
    {
        Date: "2012-02-10",
        Open: 21.4,
        High: 21.57,
        Low: 21.34,
        Close: 21.48,
        Volume: 1381400,
        AdjClose: 17.944545
    },
    {
        Date: "2012-02-09",
        Open: 21.790001,
        High: 21.93,
        Low: 21.6,
        Close: 21.83,
        Volume: 1875700,
        AdjClose: 18.236938
    },
    {
        Date: "2012-02-08",
        Open: 21.6,
        High: 21.719999,
        Low: 21.41,
        Close: 21.629999,
        Volume: 1910500,
        AdjClose: 18.069856
    },
    {
        Date: "2012-02-07",
        Open: 21.360001,
        High: 21.6,
        Low: 21.26,
        Close: 21.530001,
        Volume: 1749400,
        AdjClose: 17.986316
    },
    {
        Date: "2012-02-06",
        Open: 21.51,
        High: 21.74,
        Low: 21.49,
        Close: 21.66,
        Volume: 1314000,
        AdjClose: 18.094918
    },
    {
        Date: "2012-02-03",
        Open: 21.67,
        High: 21.959999,
        Low: 21.66,
        Close: 21.91,
        Volume: 2050700,
        AdjClose: 18.30377
    },
    {
        Date: "2012-02-02",
        Open: 21.66,
        High: 21.719999,
        Low: 21.59,
        Close: 21.67,
        Volume: 1560200,
        AdjClose: 18.103273
    },
    {
        Date: "2012-02-01",
        Open: 21.34,
        High: 21.75,
        Low: 21.27,
        Close: 21.57,
        Volume: 2839100,
        AdjClose: 18.019732
    },
    {
        Date: "2012-01-31",
        Open: 21.15,
        High: 21.18,
        Low: 20.639999,
        Close: 20.83,
        Volume: 7547500,
        AdjClose: 17.401531
    },
    {
        Date: "2012-01-30",
        Open: 20.629999,
        High: 20.700001,
        Low: 20.440001,
        Close: 20.549999,
        Volume: 5924500,
        AdjClose: 17.167616
    },
    {
        Date: "2012-01-27",
        Open: 21.139999,
        High: 21.309999,
        Low: 21.08,
        Close: 21.24,
        Volume: 1238800,
        AdjClose: 17.744047
    },
    {
        Date: "2012-01-26",
        Open: 21.43,
        High: 21.469999,
        Low: 21.209999,
        Close: 21.299999,
        Volume: 1712500,
        AdjClose: 17.794171
    },
    {
        Date: "2012-01-25",
        Open: 20.530001,
        High: 21.15,
        Low: 20.48,
        Close: 21.08,
        Volume: 2289700,
        AdjClose: 17.610382
    },
    {
        Date: "2012-01-24",
        Open: 20.530001,
        High: 20.889999,
        Low: 20.48,
        Close: 20.889999,
        Volume: 2120600,
        AdjClose: 17.451655
    },
    {
        Date: "2012-01-23",
        Open: 21.33,
        High: 21.459999,
        Low: 21.23,
        Close: 21.379999,
        Volume: 2178200,
        AdjClose: 17.861004
    },
    {
        Date: "2012-01-20",
        Open: 20.98,
        High: 21.15,
        Low: 20.91,
        Close: 21.139999,
        Volume: 1947900,
        AdjClose: 17.660506
    },
    {
        Date: "2012-01-19",
        Open: 21.200001,
        High: 21.4,
        Low: 21.09,
        Close: 21.4,
        Volume: 2977500,
        AdjClose: 17.877712
    },
    {
        Date: "2012-01-18",
        Open: 20.58,
        High: 20.99,
        Low: 20.530001,
        Close: 20.98,
        Volume: 2730200,
        AdjClose: 17.526841
    },
    {
        Date: "2012-01-17",
        Open: 20.389999,
        High: 20.48,
        Low: 20.299999,
        Close: 20.33,
        Volume: 2758100,
        AdjClose: 16.983827
    },
    {
        Date: "2012-01-13",
        Open: 19.84,
        High: 19.98,
        Low: 19.639999,
        Close: 19.93,
        Volume: 2178300,
        AdjClose: 16.649665
    },
    {
        Date: "2012-01-12",
        Open: 20.1,
        High: 20.280001,
        Low: 19.959999,
        Close: 20.23,
        Volume: 2189900,
        AdjClose: 16.900286
    },
    {
        Date: "2012-01-11",
        Open: 19.709999,
        High: 19.9,
        Low: 19.67,
        Close: 19.860001,
        Volume: 1552500,
        AdjClose: 16.591186
    },
    {
        Date: "2012-01-10",
        Open: 19.780001,
        High: 19.83,
        Low: 19.639999,
        Close: 19.77,
        Volume: 2644600,
        AdjClose: 16.515999
    },
    {
        Date: "2012-01-09",
        Open: 19.41,
        High: 19.639999,
        Low: 19.32,
        Close: 19.6,
        Volume: 2739700,
        AdjClose: 16.37398
    },
    {
        Date: "2012-01-06",
        Open: 19.450001,
        High: 19.450001,
        Low: 19.139999,
        Close: 19.24,
        Volume: 2790100,
        AdjClose: 16.073233
    },
    {
        Date: "2012-01-05",
        Open: 19.610001,
        High: 19.66,
        Low: 19.48,
        Close: 19.639999,
        Volume: 1494000,
        AdjClose: 16.407396
    },
    {
        Date: "2012-01-04",
        Open: 19.559999,
        High: 19.780001,
        Low: 19.42,
        Close: 19.709999,
        Volume: 2215300,
        AdjClose: 16.465874
    },
    {
        Date: "2012-01-03",
        Open: 19.66,
        High: 19.91,
        Low: 19.639999,
        Close: 19.82,
        Volume: 3124000,
        AdjClose: 16.557769
    },
    {
        Date: "2011-12-30",
        Open: 18.790001,
        High: 18.99,
        Low: 18.709999,
        Close: 18.83,
        Volume: 1465500,
        AdjClose: 15.730716
    },
    {
        Date: "2011-12-29",
        Open: 18.52,
        High: 18.74,
        Low: 18.48,
        Close: 18.709999,
        Volume: 1606500,
        AdjClose: 15.630467
    },
    {
        Date: "2011-12-28",
        Open: 18.709999,
        High: 18.73,
        Low: 18.42,
        Close: 18.51,
        Volume: 2074500,
        AdjClose: 15.463386
    },
    {
        Date: "2011-12-27",
        Open: 18.59,
        High: 18.719999,
        Low: 18.57,
        Close: 18.66,
        Volume: 1641100,
        AdjClose: 15.588697
    },
    {
        Date: "2011-12-23",
        Open: 18.469999,
        High: 18.75,
        Low: 18.41,
        Close: 18.75,
        Volume: 2647200,
        AdjClose: 15.663884
    },
    {
        Date: "2011-12-22",
        Open: 18.17,
        High: 18.48,
        Low: 18.120001,
        Close: 18.469999,
        Volume: 1775100,
        AdjClose: 15.429969
    },
    {
        Date: "2011-12-21",
        Open: 18.27,
        High: 18.290001,
        Low: 17.99,
        Close: 18.24,
        Volume: 1937100,
        AdjClose: 15.237826
    },
    {
        Date: "2011-12-20",
        Open: 18.17,
        High: 18.379999,
        Low: 18.139999,
        Close: 18.370001,
        Volume: 2302600,
        AdjClose: 15.34643
    },
    {
        Date: "2011-12-19",
        Open: 17.98,
        High: 18.02,
        Low: 17.690001,
        Close: 17.74,
        Volume: 2262200,
        AdjClose: 14.820122
    },
    {
        Date: "2011-12-16",
        Open: 17.879999,
        High: 18.01,
        Low: 17.629999,
        Close: 17.790001,
        Volume: 2341500,
        AdjClose: 14.861894
    },
    {
        Date: "2011-12-15",
        Open: 17.9,
        High: 17.940001,
        Low: 17.559999,
        Close: 17.719999,
        Volume: 1688800,
        AdjClose: 14.803414
    },
    {
        Date: "2011-12-14",
        Open: 17.799999,
        High: 17.9,
        Low: 17.51,
        Close: 17.6,
        Volume: 3377400,
        AdjClose: 14.703166
    },
    {
        Date: "2011-12-13",
        Open: 18.379999,
        High: 18.52,
        Low: 17.85,
        Close: 17.969999,
        Volume: 3839900,
        AdjClose: 15.012266
    },
    {
        Date: "2011-12-12",
        Open: 18.379999,
        High: 18.43,
        Low: 18.15,
        Close: 18.33,
        Volume: 4462600,
        AdjClose: 15.313013
    },
    {
        Date: "2011-12-09",
        Open: 18.549999,
        High: 18.870001,
        Low: 18.540001,
        Close: 18.780001,
        Volume: 1804100,
        AdjClose: 15.688947
    },
    {
        Date: "2011-12-08",
        Open: 18.57,
        High: 18.65,
        Low: 18.200001,
        Close: 18.26,
        Volume: 4103800,
        AdjClose: 15.254535
    },
    {
        Date: "2011-12-07",
        Open: 18.700001,
        High: 19.120001,
        Low: 18.57,
        Close: 19.030001,
        Volume: 3465300,
        AdjClose: 15.897798
    },
    {
        Date: "2011-12-06",
        Open: 18.82,
        High: 18.93,
        Low: 18.690001,
        Close: 18.809999,
        Volume: 1908100,
        AdjClose: 15.714008
    },
    {
        Date: "2011-12-05",
        Open: 19.07,
        High: 19.09,
        Low: 18.67,
        Close: 18.799999,
        Volume: 3375900,
        AdjClose: 15.705654
    },
    {
        Date: "2011-12-02",
        Open: 18.92,
        High: 18.959999,
        Low: 18.58,
        Close: 18.629999,
        Volume: 2415700,
        AdjClose: 15.563634
    },
    {
        Date: "2011-12-01",
        Open: 18.780001,
        High: 19.01,
        Low: 18.629999,
        Close: 18.68,
        Volume: 2404200,
        AdjClose: 15.605406
    },
    {
        Date: "2011-11-30",
        Open: 18.51,
        High: 19,
        Low: 18.459999,
        Close: 18.969999,
        Volume: 4124600,
        AdjClose: 15.847673
    },
    {
        Date: "2011-11-29",
        Open: 17.610001,
        High: 17.91,
        Low: 17.549999,
        Close: 17.700001,
        Volume: 2538300,
        AdjClose: 14.786707
    },
    {
        Date: "2011-11-28",
        Open: 17.629999,
        High: 17.73,
        Low: 17.559999,
        Close: 17.68,
        Volume: 2506500,
        AdjClose: 14.769998
    },
    {
        Date: "2011-11-25",
        Open: 16.82,
        High: 17.049999,
        Low: 16.66,
        Close: 16.68,
        Volume: 1362200,
        AdjClose: 13.934591
    },
    {
        Date: "2011-11-23",
        Open: 16.9,
        High: 16.93,
        Low: 16.57,
        Close: 16.700001,
        Volume: 2882600,
        AdjClose: 13.9513
    },
    {
        Date: "2011-11-22",
        Open: 17.049999,
        High: 17.209999,
        Low: 16.93,
        Close: 17.08,
        Volume: 3065700,
        AdjClose: 14.268754
    },
    {
        Date: "2011-11-21",
        Open: 17.33,
        High: 17.389999,
        Low: 17.059999,
        Close: 17.26,
        Volume: 2622000,
        AdjClose: 14.419127
    },
    {
        Date: "2011-11-18",
        Open: 17.950001,
        High: 18,
        Low: 17.690001,
        Close: 17.76,
        Volume: 1711500,
        AdjClose: 14.836831
    },
    {
        Date: "2011-11-17",
        Open: 18.15,
        High: 18.209999,
        Low: 17.700001,
        Close: 17.82,
        Volume: 2997800,
        AdjClose: 14.886955
    },
    {
        Date: "2011-11-16",
        Open: 18.32,
        High: 18.530001,
        Low: 18.18,
        Close: 18.200001,
        Volume: 2468300,
        AdjClose: 15.204411
    },
    {
        Date: "2011-11-15",
        Open: 18.530001,
        High: 18.67,
        Low: 18.23,
        Close: 18.610001,
        Volume: 2270100,
        AdjClose: 15.546927
    },
    {
        Date: "2011-11-14",
        Open: 18.440001,
        High: 18.49,
        Low: 18.190001,
        Close: 18.280001,
        Volume: 1762900,
        AdjClose: 15.271243
    },
    {
        Date: "2011-11-11",
        Open: 18.57,
        High: 18.82,
        Low: 18.549999,
        Close: 18.639999,
        Volume: 1630600,
        AdjClose: 15.571989
    },
    {
        Date: "2011-11-10",
        Open: 18.34,
        High: 18.34,
        Low: 17.879999,
        Close: 18.1,
        Volume: 2296900,
        AdjClose: 15.12087
    },
    {
        Date: "2011-11-09",
        Open: 18.17,
        High: 18.24,
        Low: 17.690001,
        Close: 17.790001,
        Volume: 3703500,
        AdjClose: 14.861894
    },
    {
        Date: "2011-11-08",
        Open: 18.950001,
        High: 19.17,
        Low: 18.73,
        Close: 19.120001,
        Volume: 2378500,
        AdjClose: 15.972985
    },
    {
        Date: "2011-11-07",
        Open: 18.690001,
        High: 18.809999,
        Low: 18.309999,
        Close: 18.68,
        Volume: 1721200,
        AdjClose: 15.605406
    },
    {
        Date: "2011-11-04",
        Open: 18.76,
        High: 18.860001,
        Low: 18.5,
        Close: 18.780001,
        Volume: 2336600,
        AdjClose: 15.688947
    },
    {
        Date: "2011-11-03",
        Open: 18.57,
        High: 19.09,
        Low: 18.370001,
        Close: 19.02,
        Volume: 3011100,
        AdjClose: 15.889444
    },
    {
        Date: "2011-11-02",
        Open: 18.23,
        High: 18.43,
        Low: 18.049999,
        Close: 18.32,
        Volume: 3482800,
        AdjClose: 15.304659
    },
    {
        Date: "2011-11-01",
        Open: 17.93,
        High: 18.25,
        Low: 17.799999,
        Close: 18.1,
        Volume: 6123900,
        AdjClose: 15.12087
    },
    {
        Date: "2011-10-31",
        Open: 19.58,
        High: 19.59,
        Low: 18.809999,
        Close: 18.809999,
        Volume: 3943700,
        AdjClose: 15.714008
    },
    {
        Date: "2011-10-28",
        Open: 20.23,
        High: 20.280001,
        Low: 19.809999,
        Close: 19.950001,
        Volume: 4302300,
        AdjClose: 16.666373
    },
    {
        Date: "2011-10-27",
        Open: 20.08,
        High: 20.540001,
        Low: 19.93,
        Close: 20.370001,
        Volume: 3294400,
        AdjClose: 17.017244
    },
    {
        Date: "2011-10-26",
        Open: 19.860001,
        High: 19.92,
        Low: 19.299999,
        Close: 19.59,
        Volume: 2313700,
        AdjClose: 16.365626
    },
    {
        Date: "2011-10-25",
        Open: 19.6,
        High: 19.68,
        Low: 19.24,
        Close: 19.4,
        Volume: 2175900,
        AdjClose: 16.206898
    },
    {
        Date: "2011-10-24",
        Open: 19.360001,
        High: 19.719999,
        Low: 19.34,
        Close: 19.629999,
        Volume: 1943800,
        AdjClose: 16.399041
    },
    {
        Date: "2011-10-21",
        Open: 18.799999,
        High: 19.09,
        Low: 18.76,
        Close: 19.08,
        Volume: 1435100,
        AdjClose: 15.939568
    },
    {
        Date: "2011-10-20",
        Open: 18.459999,
        High: 18.58,
        Low: 18.129999,
        Close: 18.49,
        Volume: 3253300,
        AdjClose: 15.446678
    },
    {
        Date: "2011-10-19",
        Open: 18.91,
        High: 18.950001,
        Low: 18.41,
        Close: 18.49,
        Volume: 2665700,
        AdjClose: 15.446678
    },
    {
        Date: "2011-10-18",
        Open: 18.950001,
        High: 19.49,
        Low: 18.700001,
        Close: 19.280001,
        Volume: 2384200,
        AdjClose: 16.10665
    },
    {
        Date: "2011-10-17",
        Open: 19.25,
        High: 19.280001,
        Low: 18.879999,
        Close: 18.98,
        Volume: 2770400,
        AdjClose: 15.856027
    },
    {
        Date: "2011-10-14",
        Open: 19.57,
        High: 19.629999,
        Low: 19.299999,
        Close: 19.48,
        Volume: 2771000,
        AdjClose: 16.273731
    },
    {
        Date: "2011-10-13",
        Open: 19.27,
        High: 19.290001,
        Low: 18.959999,
        Close: 19.23,
        Volume: 2377700,
        AdjClose: 16.064879
    },
    {
        Date: "2011-10-12",
        Open: 19.200001,
        High: 19.74,
        Low: 19.139999,
        Close: 19.540001,
        Volume: 4808900,
        AdjClose: 16.323856
    },
    {
        Date: "2011-10-11",
        Open: 18.58,
        High: 18.83,
        Low: 18.48,
        Close: 18.799999,
        Volume: 2298400,
        AdjClose: 15.705654
    },
    {
        Date: "2011-10-10",
        Open: 18.48,
        High: 18.92,
        Low: 18.459999,
        Close: 18.9,
        Volume: 3033500,
        AdjClose: 15.789195
    },
    {
        Date: "2011-10-07",
        Open: 17.700001,
        High: 17.940001,
        Low: 17.59,
        Close: 17.65,
        Volume: 4812900,
        AdjClose: 14.744936
    },
    {
        Date: "2011-10-06",
        Open: 17.120001,
        High: 17.77,
        Low: 17.049999,
        Close: 17.75,
        Volume: 3248600,
        AdjClose: 14.828477
    },
    {
        Date: "2011-10-05",
        Open: 16.790001,
        High: 17.15,
        Low: 16.620001,
        Close: 17.059999,
        Volume: 4970900,
        AdjClose: 14.252045
    },
    {
        Date: "2011-10-04",
        Open: 16.08,
        High: 16.719999,
        Low: 15.89,
        Close: 16.709999,
        Volume: 5298500,
        AdjClose: 13.959653
    },
    {
        Date: "2011-10-03",
        Open: 16.9,
        High: 17.200001,
        Low: 16.42,
        Close: 16.42,
        Volume: 2347600,
        AdjClose: 13.717385
    },
    {
        Date: "2011-09-30",
        Open: 17.33,
        High: 17.6,
        Low: 17.07,
        Close: 17.08,
        Volume: 2837600,
        AdjClose: 14.268754
    },
    {
        Date: "2011-09-29",
        Open: 18.040001,
        High: 18.23,
        Low: 17.610001,
        Close: 17.950001,
        Volume: 4971200,
        AdjClose: 14.995559
    },
    {
        Date: "2011-09-28",
        Open: 17.719999,
        High: 17.9,
        Low: 17.18,
        Close: 17.219999,
        Volume: 5113600,
        AdjClose: 14.38571
    },
    {
        Date: "2011-09-27",
        Open: 17.790001,
        High: 18.1,
        Low: 17.530001,
        Close: 17.620001,
        Volume: 4444400,
        AdjClose: 14.719874
    },
    {
        Date: "2011-09-26",
        Open: 16.799999,
        High: 17.120001,
        Low: 16.450001,
        Close: 17.1,
        Volume: 2853400,
        AdjClose: 14.285462
    },
    {
        Date: "2011-09-23",
        Open: 16.34,
        High: 16.860001,
        Low: 16.32,
        Close: 16.77,
        Volume: 4726800,
        AdjClose: 14.009778
    },
    {
        Date: "2011-09-22",
        Open: 17.110001,
        High: 17.17,
        Low: 16.59,
        Close: 16.77,
        Volume: 5299500,
        AdjClose: 14.009778
    },
    {
        Date: "2011-09-21",
        Open: 18.309999,
        High: 18.34,
        Low: 17.65,
        Close: 17.66,
        Volume: 2387000,
        AdjClose: 14.75329
    },
    {
        Date: "2011-09-20",
        Open: 18.15,
        High: 18.379999,
        Low: 17.969999,
        Close: 18.059999,
        Volume: 2058000,
        AdjClose: 15.087452
    },
    {
        Date: "2011-09-19",
        Open: 18.07,
        High: 18.33,
        Low: 17.879999,
        Close: 18.209999,
        Volume: 2246700,
        AdjClose: 15.212763
    },
    {
        Date: "2011-09-16",
        Open: 19.049999,
        High: 19.120001,
        Low: 18.77,
        Close: 18.940001,
        Volume: 2282500,
        AdjClose: 15.822612
    },
    {
        Date: "2011-09-15",
        Open: 19.08,
        High: 19.15,
        Low: 18.9,
        Close: 19.110001,
        Volume: 3451400,
        AdjClose: 15.964631
    },
    {
        Date: "2011-09-14",
        Open: 18.200001,
        High: 18.74,
        Low: 17.85,
        Close: 18.5,
        Volume: 2813600,
        AdjClose: 15.455032
    },
    {
        Date: "2011-09-13",
        Open: 17.860001,
        High: 18.190001,
        Low: 17.709999,
        Close: 18.030001,
        Volume: 3008900,
        AdjClose: 15.062391
    },
    {
        Date: "2011-09-12",
        Open: 17.65,
        High: 18.030001,
        Low: 17.52,
        Close: 17.99,
        Volume: 4875700,
        AdjClose: 15.028974
    },
    {
        Date: "2011-09-09",
        Open: 18.27,
        High: 18.469999,
        Low: 18,
        Close: 18.09,
        Volume: 4804200,
        AdjClose: 15.112515
    },
    {
        Date: "2011-09-08",
        Open: 19.040001,
        High: 19.309999,
        Low: 18.780001,
        Close: 18.9,
        Volume: 4158700,
        AdjClose: 15.789195
    },
    {
        Date: "2011-09-07",
        Open: 19.18,
        High: 19.49,
        Low: 19.1,
        Close: 19.459999,
        Volume: 4918600,
        AdjClose: 16.257022
    },
    {
        Date: "2011-09-06",
        Open: 18.57,
        High: 18.870001,
        Low: 18.48,
        Close: 18.83,
        Volume: 5934800,
        AdjClose: 15.730716
    },
    {
        Date: "2011-09-02",
        Open: 20.709999,
        High: 20.77,
        Low: 20.200001,
        Close: 20.27,
        Volume: 5389700,
        AdjClose: 16.933703
    },
    {
        Date: "2011-09-01",
        Open: 21.309999,
        High: 21.4,
        Low: 21.110001,
        Close: 21.190001,
        Volume: 6546600,
        AdjClose: 17.702278
    },
    {
        Date: "2011-08-31",
        Open: 21.370001,
        High: 21.58,
        Low: 21.110001,
        Close: 21.27,
        Volume: 6866700,
        AdjClose: 17.76911
    },
    {
        Date: "2011-08-30",
        Open: 20.76,
        High: 20.950001,
        Low: 20.49,
        Close: 20.83,
        Volume: 2397200,
        AdjClose: 17.401531
    },
    {
        Date: "2011-08-29",
        Open: 20.49,
        High: 20.76,
        Low: 20.34,
        Close: 20.74,
        Volume: 1773500,
        AdjClose: 17.326344
    },
    {
        Date: "2011-08-26",
        Open: 19.85,
        High: 20.34,
        Low: 19.59,
        Close: 20.18,
        Volume: 4269300,
        AdjClose: 16.858516
    },
    {
        Date: "2011-08-25",
        Open: 20.42,
        High: 20.559999,
        Low: 19.799999,
        Close: 19.959999,
        Volume: 2802800,
        AdjClose: 16.674726
    },
    {
        Date: "2011-08-24",
        Open: 20.459999,
        High: 20.82,
        Low: 20.219999,
        Close: 20.59,
        Volume: 2961300,
        AdjClose: 17.201033
    },
    {
        Date: "2011-08-23",
        Open: 19.790001,
        High: 20.34,
        Low: 19.620001,
        Close: 20.34,
        Volume: 2597800,
        AdjClose: 16.992181
    },
    {
        Date: "2011-08-22",
        Open: 20.110001,
        High: 20.129999,
        Low: 19.440001,
        Close: 19.459999,
        Volume: 2861000,
        AdjClose: 16.257022
    },
    {
        Date: "2011-08-19",
        Open: 19.610001,
        High: 20.200001,
        Low: 19.43,
        Close: 19.49,
        Volume: 3822100,
        AdjClose: 16.282085
    },
    {
        Date: "2011-08-18",
        Open: 20.379999,
        High: 20.43,
        Low: 19.75,
        Close: 19.940001,
        Volume: 5206300,
        AdjClose: 16.658019
    },
    {
        Date: "2011-08-17",
        Open: 21.719999,
        High: 21.860001,
        Low: 21.27,
        Close: 21.370001,
        Volume: 3698900,
        AdjClose: 17.852651
    },
    {
        Date: "2011-08-16",
        Open: 21.58,
        High: 21.809999,
        Low: 21.290001,
        Close: 21.48,
        Volume: 2624700,
        AdjClose: 17.944545
    },
    {
        Date: "2011-08-15",
        Open: 21.85,
        High: 22.110001,
        Low: 21.84,
        Close: 22.040001,
        Volume: 2239900,
        AdjClose: 18.412374
    },
    {
        Date: "2011-08-12",
        Open: 21.860001,
        High: 22.219999,
        Low: 21.58,
        Close: 21.860001,
        Volume: 3485700,
        AdjClose: 18.262001
    },
    {
        Date: "2011-08-11",
        Open: 20.4,
        High: 21.809999,
        Low: 20.35,
        Close: 21.59,
        Volume: 5891200,
        AdjClose: 18.03644
    },
    {
        Date: "2011-08-10",
        Open: 21.82,
        High: 21.85,
        Low: 20.57,
        Close: 20.59,
        Volume: 7158800,
        AdjClose: 17.201033
    },
    {
        Date: "2011-08-09",
        Open: 20.84,
        High: 21.9,
        Low: 20.389999,
        Close: 21.879999,
        Volume: 9843800,
        AdjClose: 18.278707
    },
    {
        Date: "2011-08-08",
        Open: 20.6,
        High: 20.889999,
        Low: 19.59,
        Close: 19.59,
        Volume: 7142600,
        AdjClose: 16.365626
    },
    {
        Date: "2011-08-05",
        Open: 22.26,
        High: 22.280001,
        Low: 21.049999,
        Close: 21.59,
        Volume: 13052400,
        AdjClose: 18.03644
    },
    {
        Date: "2011-08-04",
        Open: 22.16,
        High: 22.24,
        Low: 21.02,
        Close: 21.049999,
        Volume: 8839500,
        AdjClose: 17.58532
    },
    {
        Date: "2011-08-03",
        Open: 23.4,
        High: 23.469999,
        Low: 22.719999,
        Close: 23.129999,
        Volume: 7350900,
        AdjClose: 19.322966
    },
    {
        Date: "2011-08-02",
        Open: 23.299999,
        High: 23.68,
        Low: 23.1,
        Close: 23.120001,
        Volume: 6041100,
        AdjClose: 19.314614
    },
    {
        Date: "2011-08-01",
        Open: 24.219999,
        High: 24.299999,
        Low: 23.43,
        Close: 23.74,
        Volume: 3914900,
        AdjClose: 19.832565
    },
    {
        Date: "2011-07-29",
        Open: 23.82,
        High: 24.25,
        Low: 23.690001,
        Close: 23.940001,
        Volume: 4439200,
        AdjClose: 19.999647
    },
    {
        Date: "2011-07-28",
        Open: 23.950001,
        High: 24.15,
        Low: 23.809999,
        Close: 23.83,
        Volume: 3735500,
        AdjClose: 19.907752
    },
    {
        Date: "2011-07-27",
        Open: 24.58,
        High: 24.59,
        Low: 23.969999,
        Close: 24.129999,
        Volume: 3451500,
        AdjClose: 20.158374
    },
    {
        Date: "2011-07-26",
        Open: 25.049999,
        High: 25.07,
        Low: 24.73,
        Close: 24.889999,
        Volume: 3142900,
        AdjClose: 20.793283
    },
    {
        Date: "2011-07-25",
        Open: 24.959999,
        High: 25.1,
        Low: 24.870001,
        Close: 24.950001,
        Volume: 2468100,
        AdjClose: 20.843409
    },
    {
        Date: "2011-07-22",
        Open: 24.76,
        High: 24.84,
        Low: 24.469999,
        Close: 24.57,
        Volume: 2482700,
        AdjClose: 20.525953
    },
    {
        Date: "2011-07-21",
        Open: 24.860001,
        High: 25.15,
        Low: 24.76,
        Close: 25.01,
        Volume: 5632100,
        AdjClose: 20.893533
    },
    {
        Date: "2011-07-20",
        Open: 25.209999,
        High: 25.450001,
        Low: 25.1,
        Close: 25.379999,
        Volume: 3811900,
        AdjClose: 21.202632
    },
    {
        Date: "2011-07-19",
        Open: 24.83,
        High: 25.07,
        Low: 24.809999,
        Close: 25,
        Volume: 2979400,
        AdjClose: 20.885178
    },
    {
        Date: "2011-07-18",
        Open: 24.75,
        High: 24.85,
        Low: 24.49,
        Close: 24.74,
        Volume: 4627400,
        AdjClose: 20.667972
    },
    {
        Date: "2011-07-15",
        Open: 25.469999,
        High: 25.59,
        Low: 25.290001,
        Close: 25.52,
        Volume: 1582600,
        AdjClose: 21.319591
    },
    {
        Date: "2011-07-14",
        Open: 26.1,
        High: 26.24,
        Low: 25.57,
        Close: 25.629999,
        Volume: 4489400,
        AdjClose: 21.411484
    },
    {
        Date: "2011-07-13",
        Open: 25.790001,
        High: 26.389999,
        Low: 25.77,
        Close: 26.1,
        Volume: 4670400,
        AdjClose: 21.804127
    },
    {
        Date: "2011-07-12",
        Open: 25.17,
        High: 25.620001,
        Low: 25.15,
        Close: 25.360001,
        Volume: 3694800,
        AdjClose: 21.185926
    },
    {
        Date: "2011-07-11",
        Open: 25.65,
        High: 25.690001,
        Low: 25.26,
        Close: 25.549999,
        Volume: 3877900,
        AdjClose: 21.344652
    },
    {
        Date: "2011-07-08",
        Open: 26.290001,
        High: 26.459999,
        Low: 26.15,
        Close: 26.34,
        Volume: 1681400,
        AdjClose: 22.004624
    },
    {
        Date: "2011-07-07",
        Open: 26.41,
        High: 26.610001,
        Low: 26.360001,
        Close: 26.610001,
        Volume: 2363800,
        AdjClose: 22.230184
    },
    {
        Date: "2011-07-06",
        Open: 26.139999,
        High: 26.200001,
        Low: 25.860001,
        Close: 25.91,
        Volume: 3222500,
        AdjClose: 21.645399
    },
    {
        Date: "2011-07-05",
        Open: 26.139999,
        High: 26.27,
        Low: 25.940001,
        Close: 26.25,
        Volume: 2325200,
        AdjClose: 21.929437
    },
    {
        Date: "2011-07-01",
        Open: 25.73,
        High: 26.17,
        Low: 25.66,
        Close: 26.08,
        Volume: 2828800,
        AdjClose: 21.787418
    },
    {
        Date: "2011-06-30",
        Open: 25.66,
        High: 26.02,
        Low: 25.629999,
        Close: 25.950001,
        Volume: 2947200,
        AdjClose: 21.678816
    },
    {
        Date: "2011-06-29",
        Open: 25.469999,
        High: 25.75,
        Low: 25.290001,
        Close: 25.620001,
        Volume: 2900200,
        AdjClose: 21.403132
    },
    {
        Date: "2011-06-28",
        Open: 24.99,
        High: 25.139999,
        Low: 24.940001,
        Close: 25.059999,
        Volume: 3191900,
        AdjClose: 20.935302
    },
    {
        Date: "2011-06-27",
        Open: 24.77,
        High: 25.18,
        Low: 24.73,
        Close: 25.01,
        Volume: 4067600,
        AdjClose: 20.893533
    },
    {
        Date: "2011-06-24",
        Open: 25.23,
        High: 25.25,
        Low: 24.68,
        Close: 24.709999,
        Volume: 3170400,
        AdjClose: 20.64291
    },
    {
        Date: "2011-06-23",
        Open: 24.76,
        High: 24.93,
        Low: 24.459999,
        Close: 24.870001,
        Volume: 6047700,
        AdjClose: 20.776576
    },
    {
        Date: "2011-06-22",
        Open: 25.35,
        High: 25.5,
        Low: 25.23,
        Close: 25.24,
        Volume: 2963500,
        AdjClose: 21.085676
    },
    {
        Date: "2011-06-21",
        Open: 25.110001,
        High: 25.780001,
        Low: 25.049999,
        Close: 25.68,
        Volume: 4402400,
        AdjClose: 21.453256
    },
    {
        Date: "2011-06-20",
        Open: 24.74,
        High: 24.870001,
        Low: 24.58,
        Close: 24.629999,
        Volume: 2233300,
        AdjClose: 20.576077
    },
    {
        Date: "2011-06-17",
        Open: 25.219999,
        High: 25.24,
        Low: 24.870001,
        Close: 24.959999,
        Volume: 2343700,
        AdjClose: 20.851761
    },
    {
        Date: "2011-06-16",
        Open: 24.709999,
        High: 24.91,
        Low: 24.57,
        Close: 24.780001,
        Volume: 2789500,
        AdjClose: 20.701389
    },
    {
        Date: "2011-06-15",
        Open: 25.41,
        High: 25.68,
        Low: 25.059999,
        Close: 25.190001,
        Volume: 4611700,
        AdjClose: 21.043906
    },
    {
        Date: "2011-06-14",
        Open: 25.75,
        High: 26.129999,
        Low: 25.719999,
        Close: 25.950001,
        Volume: 4606100,
        AdjClose: 21.678816
    },
    {
        Date: "2011-06-13",
        Open: 25.24,
        High: 25.5,
        Low: 24.950001,
        Close: 25.27,
        Volume: 6027600,
        AdjClose: 21.110739
    },
    {
        Date: "2011-06-10",
        Open: 25.83,
        High: 25.870001,
        Low: 25.41,
        Close: 25.530001,
        Volume: 2348800,
        AdjClose: 21.327945
    },
    {
        Date: "2011-06-09",
        Open: 25.73,
        High: 26.07,
        Low: 25.709999,
        Close: 26.02,
        Volume: 3926400,
        AdjClose: 21.737294
    },
    {
        Date: "2011-06-08",
        Open: 25.870001,
        High: 25.92,
        Low: 25.51,
        Close: 25.57,
        Volume: 2709600,
        AdjClose: 21.36136
    },
    {
        Date: "2011-06-07",
        Open: 26.34,
        High: 26.370001,
        Low: 26.09,
        Close: 26.110001,
        Volume: 2038400,
        AdjClose: 21.812481
    },
    {
        Date: "2011-06-06",
        Open: 26.43,
        High: 26.5,
        Low: 26,
        Close: 26.08,
        Volume: 3135800,
        AdjClose: 21.787418
    },
    {
        Date: "2011-06-03",
        Open: 26.459999,
        High: 26.709999,
        Low: 26.41,
        Close: 26.49,
        Volume: 3524700,
        AdjClose: 22.129935
    },
    {
        Date: "2011-06-02",
        Open: 26.360001,
        High: 26.639999,
        Low: 26.16,
        Close: 26.450001,
        Volume: 2912400,
        AdjClose: 22.096519
    },
    {
        Date: "2011-06-01",
        Open: 27.08,
        High: 27.09,
        Low: 26.219999,
        Close: 26.27,
        Volume: 2728500,
        AdjClose: 21.946146
    },
    {
        Date: "2011-05-31",
        Open: 26.950001,
        High: 26.99,
        Low: 26.709999,
        Close: 26.9,
        Volume: 1918600,
        AdjClose: 22.472452
    },
    {
        Date: "2011-05-27",
        Open: 26.309999,
        High: 26.52,
        Low: 26.280001,
        Close: 26.41,
        Volume: 1395500,
        AdjClose: 22.063102
    },
    {
        Date: "2011-05-26",
        Open: 26,
        High: 26.219999,
        Low: 25.82,
        Close: 26.08,
        Volume: 1943800,
        AdjClose: 21.787418
    },
    {
        Date: "2011-05-25",
        Open: 25.66,
        High: 25.959999,
        Low: 25.540001,
        Close: 25.860001,
        Volume: 2589800,
        AdjClose: 21.603629
    },
    {
        Date: "2011-05-24",
        Open: 25.969999,
        High: 26.049999,
        Low: 25.85,
        Close: 25.889999,
        Volume: 2297100,
        AdjClose: 21.62869
    },
    {
        Date: "2011-05-23",
        Open: 25.52,
        High: 25.77,
        Low: 25.450001,
        Close: 25.690001,
        Volume: 4132300,
        AdjClose: 21.46161
    },
    {
        Date: "2011-05-20",
        Open: 26.469999,
        High: 26.540001,
        Low: 26.18,
        Close: 26.32,
        Volume: 4075000,
        AdjClose: 21.987916
    },
    {
        Date: "2011-05-19",
        Open: 26.41,
        High: 26.559999,
        Low: 26.24,
        Close: 26.48,
        Volume: 3804600,
        AdjClose: 22.121581
    },
    {
        Date: "2011-05-18",
        Open: 26.059999,
        High: 26.469999,
        Low: 26.01,
        Close: 26.32,
        Volume: 5590600,
        AdjClose: 21.987916
    },
    {
        Date: "2011-05-17",
        Open: 25.959999,
        High: 26.24,
        Low: 25.82,
        Close: 26.219999,
        Volume: 7370700,
        AdjClose: 21.904375
    },
    {
        Date: "2011-05-16",
        Open: 25.85,
        High: 26.35,
        Low: 25.82,
        Close: 26.209999,
        Volume: 4313900,
        AdjClose: 21.89602
    },
    {
        Date: "2011-05-13",
        Open: 26.280001,
        High: 26.370001,
        Low: 25.719999,
        Close: 25.860001,
        Volume: 2784600,
        AdjClose: 21.603629
    },
    {
        Date: "2011-05-12",
        Open: 26.040001,
        High: 26.57,
        Low: 25.889999,
        Close: 26.459999,
        Volume: 2061900,
        AdjClose: 22.104872
    },
    {
        Date: "2011-05-11",
        Open: 26.4,
        High: 26.42,
        Low: 25.9,
        Close: 26.059999,
        Volume: 2665700,
        AdjClose: 21.77071
    },
    {
        Date: "2011-05-10",
        Open: 26.549999,
        High: 26.85,
        Low: 26.440001,
        Close: 26.68,
        Volume: 2307700,
        AdjClose: 22.288663
    },
    {
        Date: "2011-05-09",
        Open: 26.1,
        High: 26.450001,
        Low: 26,
        Close: 26.389999,
        Volume: 1404700,
        AdjClose: 22.046394
    },
    {
        Date: "2011-05-06",
        Open: 26.27,
        High: 26.459999,
        Low: 25.889999,
        Close: 26.049999,
        Volume: 2362800,
        AdjClose: 21.762355
    },
    {
        Date: "2011-05-05",
        Open: 26.049999,
        High: 26.190001,
        Low: 25.709999,
        Close: 25.790001,
        Volume: 3498800,
        AdjClose: 21.545151
    },
    {
        Date: "2011-05-04",
        Open: 27,
        High: 27.02,
        Low: 26.41,
        Close: 26.51,
        Volume: 4348200,
        AdjClose: 22.146643
    },
    {
        Date: "2011-05-03",
        Open: 26.52,
        High: 26.709999,
        Low: 26.379999,
        Close: 26.49,
        Volume: 3172800,
        AdjClose: 22.129935
    },
    {
        Date: "2011-05-02",
        Open: 27.280001,
        High: 27.469999,
        Low: 26.940001,
        Close: 26.940001,
        Volume: 2667200,
        AdjClose: 21.971207
    },
    {
        Date: "2011-04-29",
        Open: 27.459999,
        High: 27.58,
        Low: 27.440001,
        Close: 27.49,
        Volume: 2271300,
        AdjClose: 22.419765
    },
    {
        Date: "2011-04-28",
        Open: 27.25,
        High: 27.35,
        Low: 27.1,
        Close: 27.26,
        Volume: 3073600,
        AdjClose: 22.232186
    },
    {
        Date: "2011-04-27",
        Open: 26.41,
        High: 26.58,
        Low: 26.01,
        Close: 26.58,
        Volume: 4313600,
        AdjClose: 21.677605
    },
    {
        Date: "2011-04-26",
        Open: 25.629999,
        High: 25.940001,
        Low: 25.42,
        Close: 25.879999,
        Volume: 3400000,
        AdjClose: 21.106712
    },
    {
        Date: "2011-04-25",
        Open: 25.4,
        High: 25.459999,
        Low: 25.290001,
        Close: 25.33,
        Volume: 1768300,
        AdjClose: 20.658154
    },
    {
        Date: "2011-04-21",
        Open: 25.370001,
        High: 25.389999,
        Low: 25.15,
        Close: 25.34,
        Volume: 2596100,
        AdjClose: 20.66631
    },
    {
        Date: "2011-04-20",
        Open: 24.84,
        High: 24.969999,
        Low: 24.809999,
        Close: 24.969999,
        Volume: 2882900,
        AdjClose: 20.364552
    },
    {
        Date: "2011-04-19",
        Open: 24.200001,
        High: 24.219999,
        Low: 23.93,
        Close: 24.01,
        Volume: 2603400,
        AdjClose: 19.581614
    },
    {
        Date: "2011-04-18",
        Open: 24.059999,
        High: 24.120001,
        Low: 23.690001,
        Close: 23.940001,
        Volume: 3138100,
        AdjClose: 19.524525
    },
    {
        Date: "2011-04-15",
        Open: 24.24,
        High: 24.51,
        Low: 24.190001,
        Close: 24.5,
        Volume: 2475300,
        AdjClose: 19.981239
    },
    {
        Date: "2011-04-14",
        Open: 24.15,
        High: 24.24,
        Low: 24.049999,
        Close: 24.190001,
        Volume: 1378200,
        AdjClose: 19.728415
    },
    {
        Date: "2011-04-13",
        Open: 24.51,
        High: 24.52,
        Low: 24.17,
        Close: 24.24,
        Volume: 1466600,
        AdjClose: 19.769193
    },
    {
        Date: "2011-04-12",
        Open: 24.299999,
        High: 24.35,
        Low: 24.07,
        Close: 24.18,
        Volume: 1998700,
        AdjClose: 19.720259
    },
    {
        Date: "2011-04-11",
        Open: 24.379999,
        High: 24.469999,
        Low: 24.25,
        Close: 24.379999,
        Volume: 1289800,
        AdjClose: 19.883371
    },
    {
        Date: "2011-04-08",
        Open: 24.620001,
        High: 24.67,
        Low: 24.440001,
        Close: 24.57,
        Volume: 1252400,
        AdjClose: 20.038328
    },
    {
        Date: "2011-04-07",
        Open: 24.200001,
        High: 24.33,
        Low: 24.139999,
        Close: 24.290001,
        Volume: 2158200,
        AdjClose: 19.809972
    },
    {
        Date: "2011-04-06",
        Open: 24.370001,
        High: 24.51,
        Low: 24.26,
        Close: 24.33,
        Volume: 2198700,
        AdjClose: 19.842593
    },
    {
        Date: "2011-04-05",
        Open: 23.99,
        High: 24.379999,
        Low: 23.950001,
        Close: 24.290001,
        Volume: 2485700,
        AdjClose: 19.809972
    },
    {
        Date: "2011-04-04",
        Open: 24.559999,
        High: 24.610001,
        Low: 24.360001,
        Close: 24.43,
        Volume: 1402500,
        AdjClose: 19.92415
    },
    {
        Date: "2011-04-01",
        Open: 24.26,
        High: 24.67,
        Low: 24.15,
        Close: 24.65,
        Volume: 3034200,
        AdjClose: 20.103572
    },
    {
        Date: "2011-03-31",
        Open: 24.18,
        High: 24.27,
        Low: 24.02,
        Close: 24.190001,
        Volume: 2124600,
        AdjClose: 19.728415
    },
    {
        Date: "2011-03-30",
        Open: 23.860001,
        High: 24.08,
        Low: 23.809999,
        Close: 24.030001,
        Volume: 1289000,
        AdjClose: 19.597926
    },
    {
        Date: "2011-03-29",
        Open: 23.5,
        High: 23.74,
        Low: 23.4,
        Close: 23.73,
        Volume: 1417500,
        AdjClose: 19.353256
    },
    {
        Date: "2011-03-28",
        Open: 23.77,
        High: 23.85,
        Low: 23.639999,
        Close: 23.639999,
        Volume: 1211600,
        AdjClose: 19.279856
    },
    {
        Date: "2011-03-25",
        Open: 23.67,
        High: 23.799999,
        Low: 23.58,
        Close: 23.620001,
        Volume: 1388500,
        AdjClose: 19.263546
    },
    {
        Date: "2011-03-24",
        Open: 23.85,
        High: 24.01,
        Low: 23.719999,
        Close: 23.91,
        Volume: 1842400,
        AdjClose: 19.500058
    },
    {
        Date: "2011-03-23",
        Open: 23.5,
        High: 23.610001,
        Low: 23.33,
        Close: 23.540001,
        Volume: 1545100,
        AdjClose: 19.198301
    },
    {
        Date: "2011-03-22",
        Open: 23.58,
        High: 23.6,
        Low: 23.360001,
        Close: 23.43,
        Volume: 1946300,
        AdjClose: 19.108589
    },
    {
        Date: "2011-03-21",
        Open: 23.74,
        High: 23.879999,
        Low: 23.66,
        Close: 23.780001,
        Volume: 3145700,
        AdjClose: 19.394035
    },
    {
        Date: "2011-03-18",
        Open: 23.360001,
        High: 23.370001,
        Low: 23.08,
        Close: 23.139999,
        Volume: 2564700,
        AdjClose: 18.872075
    },
    {
        Date: "2011-03-17",
        Open: 22.91,
        High: 22.98,
        Low: 22.76,
        Close: 22.870001,
        Volume: 3276900,
        AdjClose: 18.651875
    },
    {
        Date: "2011-03-16",
        Open: 22.41,
        High: 22.51,
        Low: 21.82,
        Close: 22.08,
        Volume: 3559000,
        AdjClose: 18.007581
    },
    {
        Date: "2011-03-15",
        Open: 21.84,
        High: 22.549999,
        Low: 21.73,
        Close: 22.43,
        Volume: 5295600,
        AdjClose: 18.293028
    },
    {
        Date: "2011-03-14",
        Open: 22.98,
        High: 23.1,
        Low: 22.860001,
        Close: 23.049999,
        Volume: 2094600,
        AdjClose: 18.798675
    },
    {
        Date: "2011-03-11",
        Open: 23.049999,
        High: 23.33,
        Low: 23.049999,
        Close: 23.27,
        Volume: 1610200,
        AdjClose: 18.978099
    },
    {
        Date: "2011-03-10",
        Open: 23.49,
        High: 23.530001,
        Low: 23.290001,
        Close: 23.309999,
        Volume: 1903500,
        AdjClose: 19.010721
    },
    {
        Date: "2011-03-09",
        Open: 23.98,
        High: 24.030001,
        Low: 23.799999,
        Close: 23.950001,
        Volume: 2261300,
        AdjClose: 19.532681
    },
    {
        Date: "2011-03-08",
        Open: 23.85,
        High: 23.969999,
        Low: 23.709999,
        Close: 23.879999,
        Volume: 2638500,
        AdjClose: 19.47559
    },
    {
        Date: "2011-03-07",
        Open: 24.370001,
        High: 24.42,
        Low: 23.860001,
        Close: 23.969999,
        Volume: 2731800,
        AdjClose: 19.548991
    },
    {
        Date: "2011-03-04",
        Open: 24.67,
        High: 24.700001,
        Low: 24.219999,
        Close: 24.450001,
        Volume: 2081600,
        AdjClose: 19.940461
    },
    {
        Date: "2011-03-03",
        Open: 24.440001,
        High: 24.57,
        Low: 24.33,
        Close: 24.459999,
        Volume: 2872100,
        AdjClose: 19.948615
    },
    {
        Date: "2011-03-02",
        Open: 24.24,
        High: 24.42,
        Low: 24.200001,
        Close: 24.27,
        Volume: 2483700,
        AdjClose: 19.79366
    },
    {
        Date: "2011-03-01",
        Open: 24.549999,
        High: 24.549999,
        Low: 24.049999,
        Close: 24.120001,
        Volume: 4347700,
        AdjClose: 19.671326
    },
    {
        Date: "2011-02-28",
        Open: 24.610001,
        High: 24.73,
        Low: 24.309999,
        Close: 24.51,
        Volume: 2538100,
        AdjClose: 19.989394
    },
    {
        Date: "2011-02-25",
        Open: 23.98,
        High: 24.200001,
        Low: 23.93,
        Close: 24.200001,
        Volume: 2450800,
        AdjClose: 19.736571
    },
    {
        Date: "2011-02-24",
        Open: 23.98,
        High: 24.1,
        Low: 23.889999,
        Close: 24.02,
        Volume: 3000000,
        AdjClose: 19.58977
    },
    {
        Date: "2011-02-23",
        Open: 23.76,
        High: 23.84,
        Low: 23.450001,
        Close: 23.59,
        Volume: 3950600,
        AdjClose: 19.239078
    },
    {
        Date: "2011-02-22",
        Open: 23.67,
        High: 23.93,
        Low: 23.540001,
        Close: 23.639999,
        Volume: 1889300,
        AdjClose: 19.279856
    },
    {
        Date: "2011-02-18",
        Open: 24.16,
        High: 24.190001,
        Low: 23.93,
        Close: 24.15,
        Volume: 2135900,
        AdjClose: 19.695792
    },
    {
        Date: "2011-02-17",
        Open: 23.799999,
        High: 23.83,
        Low: 23.5,
        Close: 23.629999,
        Volume: 3695600,
        AdjClose: 19.2717
    },
    {
        Date: "2011-02-16",
        Open: 23.559999,
        High: 24.09,
        Low: 23.559999,
        Close: 24.07,
        Volume: 3471100,
        AdjClose: 19.630547
    },
    {
        Date: "2011-02-15",
        Open: 23.549999,
        High: 23.860001,
        Low: 23.469999,
        Close: 23.82,
        Volume: 1818900,
        AdjClose: 19.426657
    },
    {
        Date: "2011-02-14",
        Open: 23.690001,
        High: 23.91,
        Low: 23.68,
        Close: 23.82,
        Volume: 2600900,
        AdjClose: 19.426657
    },
    {
        Date: "2011-02-11",
        Open: 22.99,
        High: 23.5,
        Low: 22.98,
        Close: 23.389999,
        Volume: 2270700,
        AdjClose: 19.075966
    },
    {
        Date: "2011-02-10",
        Open: 23.15,
        High: 23.309999,
        Low: 22.969999,
        Close: 23.23,
        Volume: 2924100,
        AdjClose: 18.945476
    },
    {
        Date: "2011-02-09",
        Open: 23.58,
        High: 23.700001,
        Low: 23.5,
        Close: 23.68,
        Volume: 2325000,
        AdjClose: 19.312479
    },
    {
        Date: "2011-02-08",
        Open: 23.41,
        High: 23.629999,
        Low: 23.290001,
        Close: 23.58,
        Volume: 3468800,
        AdjClose: 19.230923
    },
    {
        Date: "2011-02-07",
        Open: 23.309999,
        High: 23.65,
        Low: 23.27,
        Close: 23.65,
        Volume: 2906500,
        AdjClose: 19.288012
    },
    {
        Date: "2011-02-04",
        Open: 23.219999,
        High: 23.389999,
        Low: 23.09,
        Close: 23.389999,
        Volume: 2002100,
        AdjClose: 19.075966
    },
    {
        Date: "2011-02-03",
        Open: 23.34,
        High: 23.5,
        Low: 23.120001,
        Close: 23.450001,
        Volume: 2920200,
        AdjClose: 19.1249
    },
    {
        Date: "2011-02-02",
        Open: 23.18,
        High: 23.41,
        Low: 23.18,
        Close: 23.35,
        Volume: 3820100,
        AdjClose: 19.043344
    },
    {
        Date: "2011-02-01",
        Open: 23.5,
        High: 24,
        Low: 23.48,
        Close: 24,
        Volume: 4588400,
        AdjClose: 19.573458
    },
    {
        Date: "2011-01-31",
        Open: 23.610001,
        High: 23.780001,
        Low: 23.58,
        Close: 23.67,
        Volume: 2251900,
        AdjClose: 19.304323
    },
    {
        Date: "2011-01-28",
        Open: 24.16,
        High: 24.209999,
        Low: 23.639999,
        Close: 23.73,
        Volume: 2593900,
        AdjClose: 19.353256
    },
    {
        Date: "2011-01-27",
        Open: 24.219999,
        High: 24.32,
        Low: 24.08,
        Close: 24.280001,
        Volume: 2064500,
        AdjClose: 19.801816
    },
    {
        Date: "2011-01-26",
        Open: 23.860001,
        High: 23.99,
        Low: 23.799999,
        Close: 23.99,
        Volume: 2532900,
        AdjClose: 19.565302
    },
    {
        Date: "2011-01-25",
        Open: 23.629999,
        High: 23.799999,
        Low: 23.42,
        Close: 23.799999,
        Volume: 2346700,
        AdjClose: 19.410345
    },
    {
        Date: "2011-01-24",
        Open: 23.139999,
        High: 23.469999,
        Low: 23.139999,
        Close: 23.35,
        Volume: 1435300,
        AdjClose: 19.043344
    },
    {
        Date: "2011-01-21",
        Open: 23.459999,
        High: 23.469999,
        Low: 23.290001,
        Close: 23.360001,
        Volume: 1879300,
        AdjClose: 19.0515
    },
    {
        Date: "2011-01-20",
        Open: 22.9,
        High: 22.99,
        Low: 22.629999,
        Close: 22.809999,
        Volume: 2171500,
        AdjClose: 18.60294
    },
    {
        Date: "2011-01-19",
        Open: 23.639999,
        High: 23.68,
        Low: 23.219999,
        Close: 23.309999,
        Volume: 1370800,
        AdjClose: 19.010721
    },
    {
        Date: "2011-01-18",
        Open: 23.73,
        High: 23.879999,
        Low: 23.6,
        Close: 23.780001,
        Volume: 1300000,
        AdjClose: 19.394035
    },
    {
        Date: "2011-01-14",
        Open: 23.290001,
        High: 23.620001,
        Low: 23.280001,
        Close: 23.549999,
        Volume: 1896300,
        AdjClose: 19.206455
    },
    {
        Date: "2011-01-13",
        Open: 23.440001,
        High: 23.5,
        Low: 23.23,
        Close: 23.27,
        Volume: 2575600,
        AdjClose: 18.978099
    },
    {
        Date: "2011-01-12",
        Open: 22.73,
        High: 22.9,
        Low: 22.700001,
        Close: 22.9,
        Volume: 1855600,
        AdjClose: 18.676341
    },
    {
        Date: "2011-01-11",
        Open: 22.33,
        High: 22.35,
        Low: 22.16,
        Close: 22.309999,
        Volume: 1344600,
        AdjClose: 18.19516
    },
    {
        Date: "2011-01-10",
        Open: 21.790001,
        High: 21.950001,
        Low: 21.67,
        Close: 21.9,
        Volume: 2157500,
        AdjClose: 17.86078
    },
    {
        Date: "2011-01-07",
        Open: 21.959999,
        High: 22.07,
        Low: 21.84,
        Close: 22,
        Volume: 2601700,
        AdjClose: 17.942337
    },
    {
        Date: "2011-01-06",
        Open: 21.969999,
        High: 21.98,
        Low: 21.68,
        Close: 21.780001,
        Volume: 2792500,
        AdjClose: 17.762914
    },
    {
        Date: "2011-01-05",
        Open: 21.879999,
        High: 22.01,
        Low: 21.75,
        Close: 21.92,
        Volume: 3003900,
        AdjClose: 17.877092
    },
    {
        Date: "2011-01-04",
        Open: 22.75,
        High: 22.77,
        Low: 22.24,
        Close: 22.360001,
        Volume: 3618400,
        AdjClose: 18.235939
    },
    {
        Date: "2011-01-03",
        Open: 22.620001,
        High: 22.75,
        Low: 22.620001,
        Close: 22.719999,
        Volume: 2394600,
        AdjClose: 18.52954
    },
    {
        Date: "2010-12-31",
        Open: 22.16,
        High: 22.58,
        Low: 22.139999,
        Close: 22.450001,
        Volume: 1696000,
        AdjClose: 18.30934
    },
    {
        Date: "2010-12-30",
        Open: 22.26,
        High: 22.35,
        Low: 22.17,
        Close: 22.27,
        Volume: 1116400,
        AdjClose: 18.162538
    },
    {
        Date: "2010-12-29",
        Open: 22.110001,
        High: 22.24,
        Low: 22.030001,
        Close: 22.16,
        Volume: 1161600,
        AdjClose: 18.072826
    },
    {
        Date: "2010-12-28",
        Open: 22.049999,
        High: 22.09,
        Low: 21.92,
        Close: 21.940001,
        Volume: 789200,
        AdjClose: 17.893403
    },
    {
        Date: "2010-12-27",
        Open: 21.790001,
        High: 21.950001,
        Low: 21.74,
        Close: 21.940001,
        Volume: 912900,
        AdjClose: 17.893403
    },
    {
        Date: "2010-12-23",
        Open: 21.969999,
        High: 22.01,
        Low: 21.870001,
        Close: 22,
        Volume: 1333800,
        AdjClose: 17.942337
    },
    {
        Date: "2010-12-22",
        Open: 22.16,
        High: 22.290001,
        Low: 22.120001,
        Close: 22.24,
        Volume: 1711500,
        AdjClose: 18.138071
    },
    {
        Date: "2010-12-21",
        Open: 22.049999,
        High: 22.09,
        Low: 21.9,
        Close: 21.950001,
        Volume: 2432800,
        AdjClose: 17.901559
    },
    {
        Date: "2010-12-20",
        Open: 21.889999,
        High: 21.9,
        Low: 21.67,
        Close: 21.709999,
        Volume: 1362000,
        AdjClose: 17.705823
    },
    {
        Date: "2010-12-17",
        Open: 21.66,
        High: 21.66,
        Low: 21.440001,
        Close: 21.610001,
        Volume: 1499800,
        AdjClose: 17.624268
    },
    {
        Date: "2010-12-16",
        Open: 21.450001,
        High: 21.620001,
        Low: 21.370001,
        Close: 21.610001,
        Volume: 1971400,
        AdjClose: 17.624268
    },
    {
        Date: "2010-12-15",
        Open: 21.48,
        High: 21.610001,
        Low: 21.25,
        Close: 21.26,
        Volume: 1620500,
        AdjClose: 17.338822
    },
    {
        Date: "2010-12-14",
        Open: 21.24,
        High: 21.6,
        Low: 21.219999,
        Close: 21.52,
        Volume: 3288300,
        AdjClose: 17.550868
    },
    {
        Date: "2010-12-13",
        Open: 21,
        High: 21.25,
        Low: 21,
        Close: 21.1,
        Volume: 1781100,
        AdjClose: 17.208332
    },
    {
        Date: "2010-12-10",
        Open: 20.75,
        High: 20.809999,
        Low: 20.68,
        Close: 20.790001,
        Volume: 2539700,
        AdjClose: 16.955509
    },
    {
        Date: "2010-12-09",
        Open: 20.700001,
        High: 20.809999,
        Low: 20.530001,
        Close: 20.809999,
        Volume: 2123700,
        AdjClose: 16.971819
    },
    {
        Date: "2010-12-08",
        Open: 20.639999,
        High: 20.74,
        Low: 20.459999,
        Close: 20.66,
        Volume: 2438800,
        AdjClose: 16.849485
    },
    {
        Date: "2010-12-07",
        Open: 20.92,
        High: 20.969999,
        Low: 20.52,
        Close: 20.52,
        Volume: 1726800,
        AdjClose: 16.735307
    },
    {
        Date: "2010-12-06",
        Open: 20.23,
        High: 20.360001,
        Low: 20.17,
        Close: 20.299999,
        Volume: 2068600,
        AdjClose: 16.555883
    },
    {
        Date: "2010-12-03",
        Open: 20.33,
        High: 20.5,
        Low: 20.23,
        Close: 20.49,
        Volume: 2915700,
        AdjClose: 16.71084
    },
    {
        Date: "2010-12-02",
        Open: 19.790001,
        High: 20.16,
        Low: 19.790001,
        Close: 20.120001,
        Volume: 3797500,
        AdjClose: 16.409083
    },
    {
        Date: "2010-12-01",
        Open: 19.610001,
        High: 19.82,
        Low: 19.540001,
        Close: 19.73,
        Volume: 5663800,
        AdjClose: 16.091013
    },
    {
        Date: "2010-11-30",
        Open: 19.440001,
        High: 19.66,
        Low: 19.34,
        Close: 19.360001,
        Volume: 5757700,
        AdjClose: 15.789257
    },
    {
        Date: "2010-11-29",
        Open: 19.52,
        High: 19.6,
        Low: 19.370001,
        Close: 19.57,
        Volume: 4133200,
        AdjClose: 15.960524
    },
    {
        Date: "2010-11-26",
        Open: 19.620001,
        High: 19.620001,
        Low: 19.469999,
        Close: 19.58,
        Volume: 802400,
        AdjClose: 15.96868
    },
    {
        Date: "2010-11-24",
        Open: 19.84,
        High: 19.99,
        Low: 19.790001,
        Close: 19.98,
        Volume: 5856400,
        AdjClose: 16.294904
    },
    {
        Date: "2010-11-23",
        Open: 19.84,
        High: 19.9,
        Low: 19.6,
        Close: 19.66,
        Volume: 6750900,
        AdjClose: 16.033924
    },
    {
        Date: "2010-11-22",
        Open: 20.24,
        High: 20.389999,
        Low: 20.08,
        Close: 20.360001,
        Volume: 2258800,
        AdjClose: 16.604818
    },
    {
        Date: "2010-11-19",
        Open: 20.41,
        High: 20.620001,
        Low: 20.34,
        Close: 20.620001,
        Volume: 2016200,
        AdjClose: 16.816864
    },
    {
        Date: "2010-11-18",
        Open: 20.59,
        High: 20.66,
        Low: 20.48,
        Close: 20.549999,
        Volume: 1777000,
        AdjClose: 16.759773
    },
    {
        Date: "2010-11-17",
        Open: 20.139999,
        High: 20.25,
        Low: 20.030001,
        Close: 20.08,
        Volume: 2653300,
        AdjClose: 16.37646
    },
    {
        Date: "2010-11-16",
        Open: 20.299999,
        High: 20.299999,
        Low: 19.790001,
        Close: 19.940001,
        Volume: 3110200,
        AdjClose: 16.262282
    },
    {
        Date: "2010-11-15",
        Open: 20.76,
        High: 20.790001,
        Low: 20.559999,
        Close: 20.559999,
        Volume: 1602300,
        AdjClose: 16.767929
    },
    {
        Date: "2010-11-12",
        Open: 20.790001,
        High: 20.889999,
        Low: 20.469999,
        Close: 20.59,
        Volume: 2180300,
        AdjClose: 16.792396
    },
    {
        Date: "2010-11-11",
        Open: 21.02,
        High: 21.129999,
        Low: 20.85,
        Close: 21.09,
        Volume: 2683600,
        AdjClose: 17.200177
    },
    {
        Date: "2010-11-10",
        Open: 21.26,
        High: 21.290001,
        Low: 20.99,
        Close: 21.24,
        Volume: 2420400,
        AdjClose: 17.32251
    },
    {
        Date: "2010-11-09",
        Open: 21.57,
        High: 21.73,
        Low: 21.27,
        Close: 21.379999,
        Volume: 2947900,
        AdjClose: 17.436688
    },
    {
        Date: "2010-11-08",
        Open: 21.219999,
        High: 21.530001,
        Low: 21.190001,
        Close: 21.440001,
        Volume: 3425700,
        AdjClose: 17.485623
    },
    {
        Date: "2010-11-05",
        Open: 21.57,
        High: 21.709999,
        Low: 21.559999,
        Close: 21.67,
        Volume: 1832900,
        AdjClose: 17.673202
    },
    {
        Date: "2010-11-04",
        Open: 21.629999,
        High: 21.75,
        Low: 21.5,
        Close: 21.75,
        Volume: 2172600,
        AdjClose: 17.738446
    },
    {
        Date: "2010-11-03",
        Open: 20.959999,
        High: 21.17,
        Low: 20.799999,
        Close: 21.17,
        Volume: 2097800,
        AdjClose: 17.265421
    },
    {
        Date: "2010-11-02",
        Open: 20.9,
        High: 21.030001,
        Low: 20.85,
        Close: 21,
        Volume: 1507900,
        AdjClose: 17.126776
    },
    {
        Date: "2010-11-01",
        Open: 20.68,
        High: 20.73,
        Low: 20.43,
        Close: 20.549999,
        Volume: 3088100,
        AdjClose: 16.759773
    },
    {
        Date: "2010-10-29",
        Open: 20.74,
        High: 20.799999,
        Low: 20.59,
        Close: 20.690001,
        Volume: 2137300,
        AdjClose: 16.873953
    },
    {
        Date: "2010-10-28",
        Open: 21.139999,
        High: 21.17,
        Low: 20.75,
        Close: 21.01,
        Volume: 3553000,
        AdjClose: 17.134932
    },
    {
        Date: "2010-10-27",
        Open: 22.01,
        High: 22.07,
        Low: 21.690001,
        Close: 21.84,
        Volume: 2519400,
        AdjClose: 17.811847
    },
    {
        Date: "2010-10-26",
        Open: 22.059999,
        High: 22.280001,
        Low: 22,
        Close: 22.23,
        Volume: 1639600,
        AdjClose: 18.129915
    },
    {
        Date: "2010-10-25",
        Open: 22.68,
        High: 22.709999,
        Low: 22.48,
        Close: 22.49,
        Volume: 1752300,
        AdjClose: 18.341961
    },
    {
        Date: "2010-10-22",
        Open: 22.52,
        High: 22.559999,
        Low: 22.309999,
        Close: 22.370001,
        Volume: 1446500,
        AdjClose: 18.244095
    },
    {
        Date: "2010-10-21",
        Open: 22.709999,
        High: 22.91,
        Low: 22.49,
        Close: 22.690001,
        Volume: 2319500,
        AdjClose: 18.505074
    },
    {
        Date: "2010-10-20",
        Open: 22.110001,
        High: 22.6,
        Low: 22.110001,
        Close: 22.450001,
        Volume: 1725100,
        AdjClose: 18.30934
    },
    {
        Date: "2010-10-19",
        Open: 22.08,
        High: 22.219999,
        Low: 21.91,
        Close: 22.07,
        Volume: 2533100,
        AdjClose: 17.999426
    },
    {
        Date: "2010-10-18",
        Open: 22.360001,
        High: 22.459999,
        Low: 22.25,
        Close: 22.35,
        Volume: 1636900,
        AdjClose: 18.227783
    },
    {
        Date: "2010-10-15",
        Open: 22.59,
        High: 22.610001,
        Low: 22.209999,
        Close: 22.309999,
        Volume: 1606300,
        AdjClose: 18.19516
    },
    {
        Date: "2010-10-14",
        Open: 22.629999,
        High: 22.700001,
        Low: 22.440001,
        Close: 22.540001,
        Volume: 2647800,
        AdjClose: 18.38274
    },
    {
        Date: "2010-10-13",
        Open: 22.370001,
        High: 22.540001,
        Low: 22.290001,
        Close: 22.389999,
        Volume: 1873700,
        AdjClose: 18.260405
    },
    {
        Date: "2010-10-12",
        Open: 21.889999,
        High: 22.01,
        Low: 21.690001,
        Close: 21.98,
        Volume: 1690100,
        AdjClose: 17.926025
    },
    {
        Date: "2010-10-11",
        Open: 22.040001,
        High: 22.07,
        Low: 21.889999,
        Close: 21.93,
        Volume: 1294900,
        AdjClose: 17.885248
    },
    {
        Date: "2010-10-08",
        Open: 21.66,
        High: 21.83,
        Low: 21.59,
        Close: 21.790001,
        Volume: 1862700,
        AdjClose: 17.77107
    },
    {
        Date: "2010-10-07",
        Open: 22.030001,
        High: 22.040001,
        Low: 21.59,
        Close: 21.790001,
        Volume: 1683000,
        AdjClose: 17.77107
    },
    {
        Date: "2010-10-06",
        Open: 21.66,
        High: 21.82,
        Low: 21.629999,
        Close: 21.82,
        Volume: 3888100,
        AdjClose: 17.795535
    },
    {
        Date: "2010-10-05",
        Open: 21.370001,
        High: 21.59,
        Low: 21.299999,
        Close: 21.48,
        Volume: 3083600,
        AdjClose: 17.518245
    },
    {
        Date: "2010-10-04",
        Open: 21.200001,
        High: 21.299999,
        Low: 20.879999,
        Close: 21.01,
        Volume: 1920100,
        AdjClose: 17.134932
    },
    {
        Date: "2010-10-01",
        Open: 21.530001,
        High: 21.540001,
        Low: 21.23,
        Close: 21.389999,
        Volume: 3474000,
        AdjClose: 17.444844
    },
    {
        Date: "2010-09-30",
        Open: 21.48,
        High: 21.530001,
        Low: 21,
        Close: 21.120001,
        Volume: 2739900,
        AdjClose: 17.224644
    },
    {
        Date: "2010-09-29",
        Open: 21.33,
        High: 21.440001,
        Low: 21.219999,
        Close: 21.280001,
        Volume: 4161400,
        AdjClose: 17.355133
    },
    {
        Date: "2010-09-28",
        Open: 21.1,
        High: 21.24,
        Low: 20.860001,
        Close: 21.209999,
        Volume: 4407200,
        AdjClose: 17.298043
    },
    {
        Date: "2010-09-27",
        Open: 21.120001,
        High: 21.129999,
        Low: 20.969999,
        Close: 20.969999,
        Volume: 2601400,
        AdjClose: 17.102309
    },
    {
        Date: "2010-09-24",
        Open: 21.040001,
        High: 21.209999,
        Low: 21,
        Close: 21.110001,
        Volume: 4314700,
        AdjClose: 17.216488
    },
    {
        Date: "2010-09-23",
        Open: 20.68,
        High: 20.84,
        Low: 20.58,
        Close: 20.639999,
        Volume: 812000,
        AdjClose: 16.833174
    },
    {
        Date: "2010-09-22",
        Open: 21.16,
        High: 21.209999,
        Low: 20.860001,
        Close: 20.91,
        Volume: 1891900,
        AdjClose: 17.053375
    },
    {
        Date: "2010-09-21",
        Open: 21.120001,
        High: 21.200001,
        Low: 20.83,
        Close: 21.059999,
        Volume: 3279200,
        AdjClose: 17.175709
    },
    {
        Date: "2010-09-20",
        Open: 20.5,
        High: 20.860001,
        Low: 20.43,
        Close: 20.82,
        Volume: 2399900,
        AdjClose: 16.979975
    },
    {
        Date: "2010-09-17",
        Open: 20.67,
        High: 20.700001,
        Low: 20.469999,
        Close: 20.6,
        Volume: 1257600,
        AdjClose: 16.800552
    },
    {
        Date: "2010-09-16",
        Open: 20.5,
        High: 20.58,
        Low: 20.379999,
        Close: 20.530001,
        Volume: 1907300,
        AdjClose: 16.743463
    },
    {
        Date: "2010-09-15",
        Open: 20.469999,
        High: 20.6,
        Low: 20.440001,
        Close: 20.59,
        Volume: 1182600,
        AdjClose: 16.792396
    },
    {
        Date: "2010-09-14",
        Open: 20.4,
        High: 20.690001,
        Low: 20.32,
        Close: 20.540001,
        Volume: 933500,
        AdjClose: 16.751619
    },
    {
        Date: "2010-09-13",
        Open: 20.41,
        High: 20.530001,
        Low: 20.360001,
        Close: 20.459999,
        Volume: 2059400,
        AdjClose: 16.686372
    },
    {
        Date: "2010-09-10",
        Open: 20.389999,
        High: 20.459999,
        Low: 20.290001,
        Close: 20.370001,
        Volume: 1320400,
        AdjClose: 16.612973
    },
    {
        Date: "2010-09-09",
        Open: 20.58,
        High: 20.620001,
        Low: 20.34,
        Close: 20.459999,
        Volume: 1246700,
        AdjClose: 16.686372
    },
    {
        Date: "2010-09-08",
        Open: 20.16,
        High: 20.35,
        Low: 20.15,
        Close: 20.18,
        Volume: 1224400,
        AdjClose: 16.458016
    },
    {
        Date: "2010-09-07",
        Open: 20.27,
        High: 20.280001,
        Low: 20.110001,
        Close: 20.17,
        Volume: 1478500,
        AdjClose: 16.449861
    },
    {
        Date: "2010-09-03",
        Open: 20.370001,
        High: 20.450001,
        Low: 20.18,
        Close: 20.389999,
        Volume: 2668900,
        AdjClose: 16.629283
    },
    {
        Date: "2010-09-02",
        Open: 20.15,
        High: 20.370001,
        Low: 20.09,
        Close: 20.370001,
        Volume: 2001600,
        AdjClose: 16.612973
    },
    {
        Date: "2010-09-01",
        Open: 19.719999,
        High: 19.969999,
        Low: 19.700001,
        Close: 19.84,
        Volume: 1754800,
        AdjClose: 16.180726
    },
    {
        Date: "2010-08-31",
        Open: 18.969999,
        High: 19.41,
        Low: 18.92,
        Close: 19.370001,
        Volume: 2627100,
        AdjClose: 15.797413
    },
    {
        Date: "2010-08-30",
        Open: 19.09,
        High: 19.27,
        Low: 19.049999,
        Close: 19.049999,
        Volume: 950000,
        AdjClose: 15.536432
    },
    {
        Date: "2010-08-27",
        Open: 19.209999,
        High: 19.309999,
        Low: 18.879999,
        Close: 19.299999,
        Volume: 1955700,
        AdjClose: 15.740322
    },
    {
        Date: "2010-08-26",
        Open: 19.059999,
        High: 19.209999,
        Low: 18.73,
        Close: 18.790001,
        Volume: 1768400,
        AdjClose: 15.324387
    },
    {
        Date: "2010-08-25",
        Open: 18.59,
        High: 18.879999,
        Low: 18.530001,
        Close: 18.84,
        Volume: 1914400,
        AdjClose: 15.365165
    },
    {
        Date: "2010-08-24",
        Open: 18.799999,
        High: 19.030001,
        Low: 18.639999,
        Close: 18.870001,
        Volume: 1749300,
        AdjClose: 15.389632
    },
    {
        Date: "2010-08-23",
        Open: 19.17,
        High: 19.309999,
        Low: 18.99,
        Close: 19.02,
        Volume: 1825200,
        AdjClose: 15.511966
    },
    {
        Date: "2010-08-20",
        Open: 19.129999,
        High: 19.17,
        Low: 18.98,
        Close: 19.17,
        Volume: 1204000,
        AdjClose: 15.6343
    },
    {
        Date: "2010-08-19",
        Open: 19.940001,
        High: 19.98,
        Low: 19.4,
        Close: 19.559999,
        Volume: 2205000,
        AdjClose: 15.952368
    },
    {
        Date: "2010-08-18",
        Open: 19.809999,
        High: 19.92,
        Low: 19.66,
        Close: 19.780001,
        Volume: 1010000,
        AdjClose: 16.131792
    },
    {
        Date: "2010-08-17",
        Open: 19.610001,
        High: 19.85,
        Low: 19.540001,
        Close: 19.67,
        Volume: 1656600,
        AdjClose: 16.04208
    },
    {
        Date: "2010-08-16",
        Open: 19.27,
        High: 19.549999,
        Low: 19.23,
        Close: 19.42,
        Volume: 1869300,
        AdjClose: 15.83819
    },
    {
        Date: "2010-08-13",
        Open: 18.940001,
        High: 19.059999,
        Low: 18.889999,
        Close: 18.93,
        Volume: 1120000,
        AdjClose: 15.438565
    },
    {
        Date: "2010-08-12",
        Open: 18.77,
        High: 19.09,
        Low: 18.75,
        Close: 19.02,
        Volume: 2053700,
        AdjClose: 15.511966
    },
    {
        Date: "2010-08-11",
        Open: 19.379999,
        High: 19.379999,
        Low: 18.959999,
        Close: 19.059999,
        Volume: 2258800,
        AdjClose: 15.544588
    },
    {
        Date: "2010-08-10",
        Open: 19.940001,
        High: 20.299999,
        Low: 19.809999,
        Close: 20.209999,
        Volume: 2077300,
        AdjClose: 16.482482
    },
    {
        Date: "2010-08-09",
        Open: 20.469999,
        High: 20.52,
        Low: 20.370001,
        Close: 20.52,
        Volume: 1283600,
        AdjClose: 16.735307
    },
    {
        Date: "2010-08-06",
        Open: 20.57,
        High: 20.75,
        Low: 20.469999,
        Close: 20.74,
        Volume: 2367700,
        AdjClose: 16.91473
    },
    {
        Date: "2010-08-05",
        Open: 20.690001,
        High: 20.799999,
        Low: 20.59,
        Close: 20.700001,
        Volume: 1822900,
        AdjClose: 16.882108
    },
    {
        Date: "2010-08-04",
        Open: 20.65,
        High: 20.84,
        Low: 20.41,
        Close: 20.59,
        Volume: 1796700,
        AdjClose: 16.792396
    },
    {
        Date: "2010-08-03",
        Open: 20.629999,
        High: 20.719999,
        Low: 20.440001,
        Close: 20.66,
        Volume: 2362300,
        AdjClose: 16.849485
    },
    {
        Date: "2010-08-02",
        Open: 20.58,
        High: 20.870001,
        Low: 20.530001,
        Close: 20.73,
        Volume: 3142800,
        AdjClose: 16.906574
    },
    {
        Date: "2010-07-30",
        Open: 19.940001,
        High: 20.23,
        Low: 19.91,
        Close: 20.18,
        Volume: 2953300,
        AdjClose: 16.458016
    },
    {
        Date: "2010-07-29",
        Open: 20.35,
        High: 20.43,
        Low: 19.959999,
        Close: 20.139999,
        Volume: 2898900,
        AdjClose: 16.425393
    },
    {
        Date: "2010-07-28",
        Open: 20.07,
        High: 20.24,
        Low: 19.940001,
        Close: 20.040001,
        Volume: 4144400,
        AdjClose: 16.343838
    },
    {
        Date: "2010-07-27",
        Open: 20.129999,
        High: 20.15,
        Low: 19.639999,
        Close: 19.75,
        Volume: 5756100,
        AdjClose: 16.107325
    },
    {
        Date: "2010-07-26",
        Open: 20.07,
        High: 20.389999,
        Low: 19.959999,
        Close: 20.35,
        Volume: 6259800,
        AdjClose: 16.596662
    },
    {
        Date: "2010-07-23",
        Open: 19.610001,
        High: 20.09,
        Low: 19.459999,
        Close: 20.040001,
        Volume: 5637300,
        AdjClose: 16.343838
    },
    {
        Date: "2010-07-22",
        Open: 19.440001,
        High: 19.799999,
        Low: 19.41,
        Close: 19.57,
        Volume: 8056500,
        AdjClose: 15.960524
    },
    {
        Date: "2010-07-21",
        Open: 18.799999,
        High: 18.84,
        Low: 18.370001,
        Close: 18.450001,
        Volume: 3193900,
        AdjClose: 15.047097
    },
    {
        Date: "2010-07-20",
        Open: 17.950001,
        High: 18.42,
        Low: 17.92,
        Close: 18.389999,
        Volume: 3131900,
        AdjClose: 14.998162
    },
    {
        Date: "2010-07-19",
        Open: 18.27,
        High: 18.35,
        Low: 17.98,
        Close: 18.139999,
        Volume: 1832000,
        AdjClose: 14.794272
    },
    {
        Date: "2010-07-16",
        Open: 18.65,
        High: 18.65,
        Low: 18.110001,
        Close: 18.139999,
        Volume: 2070200,
        AdjClose: 14.794272
    },
    {
        Date: "2010-07-15",
        Open: 18.73,
        High: 18.780001,
        Low: 18.43,
        Close: 18.76,
        Volume: 2885000,
        AdjClose: 15.29992
    },
    {
        Date: "2010-07-14",
        Open: 18.450001,
        High: 18.690001,
        Low: 18.41,
        Close: 18.629999,
        Volume: 1881000,
        AdjClose: 15.193896
    },
    {
        Date: "2010-07-13",
        Open: 18.52,
        High: 18.68,
        Low: 18.51,
        Close: 18.6,
        Volume: 2551800,
        AdjClose: 15.16943
    },
    {
        Date: "2010-07-12",
        Open: 18.129999,
        High: 18.25,
        Low: 18.02,
        Close: 18.139999,
        Volume: 2260200,
        AdjClose: 14.794272
    },
    {
        Date: "2010-07-09",
        Open: 18.610001,
        High: 18.879999,
        Low: 18.540001,
        Close: 18.860001,
        Volume: 2119500,
        AdjClose: 14.990008
    },
    {
        Date: "2010-07-08",
        Open: 18.66,
        High: 18.690001,
        Low: 18.389999,
        Close: 18.66,
        Volume: 3103900,
        AdjClose: 14.831047
    },
    {
        Date: "2010-07-07",
        Open: 17.75,
        High: 18.370001,
        Low: 17.73,
        Close: 18.360001,
        Volume: 2197400,
        AdjClose: 14.592606
    },
    {
        Date: "2010-07-06",
        Open: 17.870001,
        High: 18.030001,
        Low: 17.549999,
        Close: 17.690001,
        Volume: 2955000,
        AdjClose: 14.060087
    },
    {
        Date: "2010-07-02",
        Open: 17.629999,
        High: 17.68,
        Low: 17.25,
        Close: 17.43,
        Volume: 2315300,
        AdjClose: 13.853438
    },
    {
        Date: "2010-07-01",
        Open: 17.690001,
        High: 17.77,
        Low: 17.27,
        Close: 17.59,
        Volume: 6328900,
        AdjClose: 13.980606
    },
    {
        Date: "2010-06-30",
        Open: 17.440001,
        High: 17.65,
        Low: 17.200001,
        Close: 17.280001,
        Volume: 4224600,
        AdjClose: 13.734217
    },
    {
        Date: "2010-06-29",
        Open: 17.73,
        High: 17.73,
        Low: 17.32,
        Close: 17.49,
        Volume: 4731000,
        AdjClose: 13.901126
    },
    {
        Date: "2010-06-28",
        Open: 18.43,
        High: 18.469999,
        Low: 18.18,
        Close: 18.25,
        Volume: 2772800,
        AdjClose: 14.505177
    },
    {
        Date: "2010-06-25",
        Open: 18.219999,
        High: 18.389999,
        Low: 17.959999,
        Close: 18.280001,
        Volume: 3605900,
        AdjClose: 14.529022
    },
    {
        Date: "2010-06-24",
        Open: 18.389999,
        High: 18.4,
        Low: 17.959999,
        Close: 18.049999,
        Volume: 4990200,
        AdjClose: 14.346215
    },
    {
        Date: "2010-06-23",
        Open: 18.709999,
        High: 18.719999,
        Low: 18.290001,
        Close: 18.389999,
        Volume: 6053000,
        AdjClose: 14.616449
    },
    {
        Date: "2010-06-22",
        Open: 18.879999,
        High: 18.940001,
        Low: 18.5,
        Close: 18.549999,
        Volume: 3049300,
        AdjClose: 14.743618
    },
    {
        Date: "2010-06-21",
        Open: 19.24,
        High: 19.280001,
        Low: 18.83,
        Close: 18.93,
        Volume: 4878900,
        AdjClose: 15.045644
    },
    {
        Date: "2010-06-18",
        Open: 18.629999,
        High: 18.74,
        Low: 18.5,
        Close: 18.610001,
        Volume: 2920700,
        AdjClose: 14.791307
    },
    {
        Date: "2010-06-17",
        Open: 18.68,
        High: 18.700001,
        Low: 18.450001,
        Close: 18.639999,
        Volume: 3238300,
        AdjClose: 14.81515
    },
    {
        Date: "2010-06-16",
        Open: 18.35,
        High: 18.51,
        Low: 18.32,
        Close: 18.440001,
        Volume: 3010400,
        AdjClose: 14.65619
    },
    {
        Date: "2010-06-15",
        Open: 18.379999,
        High: 18.639999,
        Low: 18.27,
        Close: 18.639999,
        Volume: 3328200,
        AdjClose: 14.81515
    },
    {
        Date: "2010-06-14",
        Open: 18.17,
        High: 18.35,
        Low: 18,
        Close: 18.01,
        Volume: 3868300,
        AdjClose: 14.314424
    },
    {
        Date: "2010-06-11",
        Open: 17.41,
        High: 17.68,
        Low: 17.389999,
        Close: 17.68,
        Volume: 2707600,
        AdjClose: 14.052139
    },
    {
        Date: "2010-06-10",
        Open: 17.35,
        High: 17.540001,
        Low: 17.209999,
        Close: 17.530001,
        Volume: 4384900,
        AdjClose: 13.932918
    },
    {
        Date: "2010-06-09",
        Open: 16.98,
        High: 17.190001,
        Low: 16.700001,
        Close: 16.799999,
        Volume: 3241400,
        AdjClose: 13.35271
    },
    {
        Date: "2010-06-08",
        Open: 16.66,
        High: 16.870001,
        Low: 16.4,
        Close: 16.73,
        Volume: 7705800,
        AdjClose: 13.297074
    },
    {
        Date: "2010-06-07",
        Open: 16.629999,
        High: 16.73,
        Low: 16.26,
        Close: 16.27,
        Volume: 3050600,
        AdjClose: 12.931465
    },
    {
        Date: "2010-06-04",
        Open: 16.84,
        High: 16.93,
        Low: 16.379999,
        Close: 16.450001,
        Volume: 3278000,
        AdjClose: 13.07453
    },
    {
        Date: "2010-06-03",
        Open: 17.290001,
        High: 17.32,
        Low: 16.91,
        Close: 17.16,
        Volume: 5478300,
        AdjClose: 13.63884
    },
    {
        Date: "2010-06-02",
        Open: 16.93,
        High: 17.200001,
        Low: 16.809999,
        Close: 17.18,
        Volume: 5968700,
        AdjClose: 13.654737
    },
    {
        Date: "2010-06-01",
        Open: 16.83,
        High: 17.34,
        Low: 16.68,
        Close: 16.690001,
        Volume: 3624100,
        AdjClose: 13.265283
    },
    {
        Date: "2010-05-28",
        Open: 17.27,
        High: 17.299999,
        Low: 16.860001,
        Close: 17.01,
        Volume: 3631300,
        AdjClose: 13.51962
    },
    {
        Date: "2010-05-27",
        Open: 17,
        High: 17.389999,
        Low: 16.92,
        Close: 17.389999,
        Volume: 3296200,
        AdjClose: 13.821645
    },
    {
        Date: "2010-05-26",
        Open: 16.540001,
        High: 16.700001,
        Low: 16.25,
        Close: 16.32,
        Volume: 3913800,
        AdjClose: 12.971205
    },
    {
        Date: "2010-05-25",
        Open: 16.1,
        High: 16.58,
        Low: 16.01,
        Close: 16.549999,
        Volume: 5818600,
        AdjClose: 13.154009
    },
    {
        Date: "2010-05-24",
        Open: 16.75,
        High: 16.98,
        Low: 16.540001,
        Close: 16.540001,
        Volume: 4051600,
        AdjClose: 13.146062
    },
    {
        Date: "2010-05-21",
        Open: 16.370001,
        High: 16.99,
        Low: 16.26,
        Close: 16.969999,
        Volume: 6354500,
        AdjClose: 13.487827
    },
    {
        Date: "2010-05-20",
        Open: 16.34,
        High: 16.49,
        Low: 16.049999,
        Close: 16.049999,
        Volume: 10707100,
        AdjClose: 12.756607
    },
    {
        Date: "2010-05-19",
        Open: 17.1,
        High: 17.24,
        Low: 16.68,
        Close: 17.01,
        Volume: 5853100,
        AdjClose: 13.51962
    },
    {
        Date: "2010-05-18",
        Open: 17.860001,
        High: 17.91,
        Low: 17.190001,
        Close: 17.280001,
        Volume: 4053700,
        AdjClose: 13.734217
    },
    {
        Date: "2010-05-17",
        Open: 17.68,
        High: 17.77,
        Low: 17.200001,
        Close: 17.58,
        Volume: 7055800,
        AdjClose: 13.972658
    },
    {
        Date: "2010-05-14",
        Open: 18.35,
        High: 18.43,
        Low: 17.84,
        Close: 18.02,
        Volume: 3583400,
        AdjClose: 14.322372
    },
    {
        Date: "2010-05-13",
        Open: 19.01,
        High: 19.18,
        Low: 18.790001,
        Close: 18.799999,
        Volume: 3298600,
        AdjClose: 14.942319
    },
    {
        Date: "2010-05-12",
        Open: 18.92,
        High: 19.16,
        Low: 18.84,
        Close: 19,
        Volume: 5325500,
        AdjClose: 15.10128
    },
    {
        Date: "2010-05-11",
        Open: 18.42,
        High: 18.84,
        Low: 18.24,
        Close: 18.48,
        Volume: 6840200,
        AdjClose: 14.687982
    },
    {
        Date: "2010-05-10",
        Open: 18.65,
        High: 18.75,
        Low: 18.34,
        Close: 18.559999,
        Volume: 5039300,
        AdjClose: 14.751566
    },
    {
        Date: "2010-05-07",
        Open: 18.07,
        High: 18.23,
        Low: 17.280001,
        Close: 17.440001,
        Volume: 12946600,
        AdjClose: 13.861386
    },
    {
        Date: "2010-05-06",
        Open: 18.379999,
        High: 18.59,
        Low: 16.799999,
        Close: 17.700001,
        Volume: 7278900,
        AdjClose: 14.068035
    },
    {
        Date: "2010-05-05",
        Open: 18.42,
        High: 18.450001,
        Low: 17.860001,
        Close: 18.1,
        Volume: 9057800,
        AdjClose: 14.385957
    },
    {
        Date: "2010-05-04",
        Open: 18.959999,
        High: 18.959999,
        Low: 18.57,
        Close: 18.75,
        Volume: 5009000,
        AdjClose: 14.902579
    },
    {
        Date: "2010-05-03",
        Open: 19.280001,
        High: 19.58,
        Low: 19.219999,
        Close: 19.450001,
        Volume: 2832700,
        AdjClose: 15.458943
    },
    {
        Date: "2010-04-30",
        Open: 19.469999,
        High: 19.48,
        Low: 19.139999,
        Close: 19.16,
        Volume: 4200200,
        AdjClose: 15.228449
    },
    {
        Date: "2010-04-29",
        Open: 19.639999,
        High: 19.65,
        Low: 19.129999,
        Close: 19.34,
        Volume: 8610200,
        AdjClose: 15.371514
    },
    {
        Date: "2010-04-28",
        Open: 19.58,
        High: 19.67,
        Low: 18.75,
        Close: 19.280001,
        Volume: 6472200,
        AdjClose: 15.323826
    },
    {
        Date: "2010-04-27",
        Open: 20.219999,
        High: 20.32,
        Low: 19.58,
        Close: 19.67,
        Volume: 7445400,
        AdjClose: 15.633799
    },
    {
        Date: "2010-04-26",
        Open: 20.950001,
        High: 21.059999,
        Low: 20.360001,
        Close: 20.5,
        Volume: 5767800,
        AdjClose: 16.293486
    },
    {
        Date: "2010-04-23",
        Open: 20.68,
        High: 21.1,
        Low: 20.59,
        Close: 21.08,
        Volume: 4618300,
        AdjClose: 16.754473
    },
    {
        Date: "2010-04-22",
        Open: 20.25,
        High: 20.690001,
        Low: 19.870001,
        Close: 20.67,
        Volume: 15661400,
        AdjClose: 16.428603
    },
    {
        Date: "2010-04-21",
        Open: 22.120001,
        High: 22.299999,
        Low: 22.07,
        Close: 22.280001,
        Volume: 1870800,
        AdjClose: 17.708238
    },
    {
        Date: "2010-04-20",
        Open: 22.540001,
        High: 22.58,
        Low: 22.32,
        Close: 22.459999,
        Volume: 1926000,
        AdjClose: 17.851302
    },
    {
        Date: "2010-04-19",
        Open: 21.84,
        High: 22.26,
        Low: 21.83,
        Close: 22.24,
        Volume: 2348600,
        AdjClose: 17.676446
    },
    {
        Date: "2010-04-16",
        Open: 22.48,
        High: 22.559999,
        Low: 22.110001,
        Close: 22.32,
        Volume: 1876800,
        AdjClose: 17.74003
    },
    {
        Date: "2010-04-15",
        Open: 22.4,
        High: 22.610001,
        Low: 22.370001,
        Close: 22.51,
        Volume: 2328600,
        AdjClose: 17.891043
    },
    {
        Date: "2010-04-14",
        Open: 22.42,
        High: 22.6,
        Low: 22.32,
        Close: 22.559999,
        Volume: 1940600,
        AdjClose: 17.930783
    },
    {
        Date: "2010-04-13",
        Open: 22.24,
        High: 22.379999,
        Low: 22.139999,
        Close: 22.32,
        Volume: 1299600,
        AdjClose: 17.74003
    },
    {
        Date: "2010-04-12",
        Open: 22.360001,
        High: 22.57,
        Low: 22.35,
        Close: 22.440001,
        Volume: 2025800,
        AdjClose: 17.835407
    },
    {
        Date: "2010-04-09",
        Open: 21.969999,
        High: 22.35,
        Low: 21.959999,
        Close: 22.33,
        Volume: 2896900,
        AdjClose: 17.747978
    },
    {
        Date: "2010-04-08",
        Open: 21.690001,
        High: 21.959999,
        Low: 21.620001,
        Close: 21.93,
        Volume: 1919600,
        AdjClose: 17.430057
    },
    {
        Date: "2010-04-07",
        Open: 22.09,
        High: 22.190001,
        Low: 21.93,
        Close: 22,
        Volume: 2606800,
        AdjClose: 17.485693
    },
    {
        Date: "2010-04-06",
        Open: 22.129999,
        High: 22.42,
        Low: 22.040001,
        Close: 22.309999,
        Volume: 2822500,
        AdjClose: 17.732082
    },
    {
        Date: "2010-04-05",
        Open: 22.26,
        High: 22.299999,
        Low: 22.02,
        Close: 22.059999,
        Volume: 1663600,
        AdjClose: 17.533381
    },
    {
        Date: "2010-04-01",
        Open: 22.17,
        High: 22.299999,
        Low: 21.92,
        Close: 22.139999,
        Volume: 2223300,
        AdjClose: 17.596965
    },
    {
        Date: "2010-03-31",
        Open: 21.719999,
        High: 21.959999,
        Low: 21.620001,
        Close: 21.84,
        Volume: 1658600,
        AdjClose: 17.358524
    },
    {
        Date: "2010-03-30",
        Open: 21.67,
        High: 21.719999,
        Low: 21.48,
        Close: 21.639999,
        Volume: 1131900,
        AdjClose: 17.199563
    },
    {
        Date: "2010-03-29",
        Open: 21.450001,
        High: 21.52,
        Low: 21.299999,
        Close: 21.49,
        Volume: 1356600,
        AdjClose: 17.080342
    },
    {
        Date: "2010-03-26",
        Open: 21.51,
        High: 21.700001,
        Low: 21.27,
        Close: 21.49,
        Volume: 1877200,
        AdjClose: 17.080342
    },
    {
        Date: "2010-03-25",
        Open: 21.6,
        High: 21.76,
        Low: 21.360001,
        Close: 21.360001,
        Volume: 1689100,
        AdjClose: 16.977019
    },
    {
        Date: "2010-03-24",
        Open: 21.370001,
        High: 21.5,
        Low: 21.290001,
        Close: 21.379999,
        Volume: 1453500,
        AdjClose: 16.992913
    },
    {
        Date: "2010-03-23",
        Open: 21.559999,
        High: 21.790001,
        Low: 21.52,
        Close: 21.790001,
        Volume: 2004600,
        AdjClose: 17.318785
    },
    {
        Date: "2010-03-22",
        Open: 20.879999,
        High: 21.42,
        Low: 20.870001,
        Close: 21.35,
        Volume: 2588100,
        AdjClose: 16.96907
    },
    {
        Date: "2010-03-19",
        Open: 21.120001,
        High: 21.43,
        Low: 21.049999,
        Close: 21.120001,
        Volume: 2028100,
        AdjClose: 16.786266
    },
    {
        Date: "2010-03-18",
        Open: 21.51,
        High: 21.59,
        Low: 21.33,
        Close: 21.59,
        Volume: 1430100,
        AdjClose: 17.159823
    },
    {
        Date: "2010-03-17",
        Open: 21.610001,
        High: 21.74,
        Low: 21.51,
        Close: 21.559999,
        Volume: 1614200,
        AdjClose: 17.135978
    },
    {
        Date: "2010-03-16",
        Open: 21.110001,
        High: 21.4,
        Low: 21.07,
        Close: 21.360001,
        Volume: 1731400,
        AdjClose: 16.977019
    },
    {
        Date: "2010-03-15",
        Open: 21.120001,
        High: 21.15,
        Low: 20.76,
        Close: 20.940001,
        Volume: 3725600,
        AdjClose: 16.643201
    },
    {
        Date: "2010-03-12",
        Open: 21.200001,
        High: 21.200001,
        Low: 21.040001,
        Close: 21.15,
        Volume: 1718200,
        AdjClose: 16.810109
    },
    {
        Date: "2010-03-11",
        Open: 20.799999,
        High: 20.879999,
        Low: 20.67,
        Close: 20.879999,
        Volume: 1845400,
        AdjClose: 16.595511
    },
    {
        Date: "2010-03-10",
        Open: 20.959999,
        High: 21.129999,
        Low: 20.879999,
        Close: 21.049999,
        Volume: 2873100,
        AdjClose: 16.730628
    },
    {
        Date: "2010-03-09",
        Open: 20.76,
        High: 21.09,
        Low: 20.75,
        Close: 21,
        Volume: 2425600,
        AdjClose: 16.690889
    },
    {
        Date: "2010-03-08",
        Open: 21.129999,
        High: 21.25,
        Low: 21.049999,
        Close: 21.219999,
        Volume: 4431000,
        AdjClose: 16.865745
    },
    {
        Date: "2010-03-05",
        Open: 20.860001,
        High: 21.129999,
        Low: 20.82,
        Close: 21.110001,
        Volume: 2602300,
        AdjClose: 16.778317
    },
    {
        Date: "2010-03-04",
        Open: 20.780001,
        High: 20.879999,
        Low: 20.65,
        Close: 20.76,
        Volume: 2524500,
        AdjClose: 16.500136
    },
    {
        Date: "2010-03-03",
        Open: 20.67,
        High: 20.92,
        Low: 20.610001,
        Close: 20.690001,
        Volume: 2702400,
        AdjClose: 16.4445
    },
    {
        Date: "2010-03-02",
        Open: 20.48,
        High: 20.65,
        Low: 20.4,
        Close: 20.57,
        Volume: 2126200,
        AdjClose: 16.349122
    },
    {
        Date: "2010-03-01",
        Open: 20.32,
        High: 20.5,
        Low: 20.17,
        Close: 20.48,
        Volume: 2121100,
        AdjClose: 16.27759
    },
    {
        Date: "2010-02-26",
        Open: 19.99,
        High: 20.280001,
        Low: 19.799999,
        Close: 20.26,
        Volume: 1782600,
        AdjClose: 16.102734
    },
    {
        Date: "2010-02-25",
        Open: 19.610001,
        High: 20.18,
        Low: 19.559999,
        Close: 20.15,
        Volume: 2367800,
        AdjClose: 16.015305
    },
    {
        Date: "2010-02-24",
        Open: 19.719999,
        High: 19.98,
        Low: 19.620001,
        Close: 19.91,
        Volume: 2146800,
        AdjClose: 15.824552
    },
    {
        Date: "2010-02-23",
        Open: 19.950001,
        High: 20.08,
        Low: 19.559999,
        Close: 19.68,
        Volume: 3977900,
        AdjClose: 15.641747
    },
    {
        Date: "2010-02-22",
        Open: 20.08,
        High: 20.129999,
        Low: 19.959999,
        Close: 20.049999,
        Volume: 2680800,
        AdjClose: 15.935824
    },
    {
        Date: "2010-02-19",
        Open: 19.610001,
        High: 19.91,
        Low: 19.52,
        Close: 19.780001,
        Volume: 3331600,
        AdjClose: 15.721228
    },
    {
        Date: "2010-02-18",
        Open: 19.389999,
        High: 19.959999,
        Low: 19.35,
        Close: 19.91,
        Volume: 6326800,
        AdjClose: 15.824552
    },
    {
        Date: "2010-02-17",
        Open: 18.360001,
        High: 18.58,
        Low: 18.299999,
        Close: 18.51,
        Volume: 2459500,
        AdjClose: 14.711826
    },
    {
        Date: "2010-02-16",
        Open: 17.82,
        High: 18.35,
        Low: 17.809999,
        Close: 18.309999,
        Volume: 2327100,
        AdjClose: 14.552865
    },
    {
        Date: "2010-02-12",
        Open: 17.6,
        High: 17.950001,
        Low: 17.540001,
        Close: 17.870001,
        Volume: 2351700,
        AdjClose: 14.203152
    },
    {
        Date: "2010-02-11",
        Open: 17.92,
        High: 18.190001,
        Low: 17.709999,
        Close: 18.190001,
        Volume: 3117000,
        AdjClose: 14.457489
    },
    {
        Date: "2010-02-10",
        Open: 17.76,
        High: 17.879999,
        Low: 17.48,
        Close: 17.67,
        Volume: 2694800,
        AdjClose: 14.044191
    },
    {
        Date: "2010-02-09",
        Open: 17.889999,
        High: 18.23,
        Low: 17.59,
        Close: 17.969999,
        Volume: 4882800,
        AdjClose: 14.282631
    },
    {
        Date: "2010-02-08",
        Open: 17.620001,
        High: 17.66,
        Low: 17.299999,
        Close: 17.299999,
        Volume: 2018000,
        AdjClose: 13.750112
    },
    {
        Date: "2010-02-05",
        Open: 17.77,
        High: 17.83,
        Low: 17.08,
        Close: 17.57,
        Volume: 5869800,
        AdjClose: 13.96471
    },
    {
        Date: "2010-02-04",
        Open: 18.389999,
        High: 18.43,
        Low: 17.9,
        Close: 17.93,
        Volume: 4635900,
        AdjClose: 14.25084
    },
    {
        Date: "2010-02-03",
        Open: 18.82,
        High: 18.91,
        Low: 18.610001,
        Close: 18.66,
        Volume: 2977500,
        AdjClose: 14.831047
    },
    {
        Date: "2010-02-02",
        Open: 18.74,
        High: 19.01,
        Low: 18.66,
        Close: 18.950001,
        Volume: 2635200,
        AdjClose: 15.06154
    },
    {
        Date: "2010-02-01",
        Open: 18.33,
        High: 18.48,
        Low: 18.23,
        Close: 18.48,
        Volume: 3192100,
        AdjClose: 14.687982
    },
    {
        Date: "2010-01-29",
        Open: 18.33,
        High: 18.51,
        Low: 17.98,
        Close: 18.030001,
        Volume: 3879500,
        AdjClose: 14.330321
    },
    {
        Date: "2010-01-28",
        Open: 18.610001,
        High: 18.629999,
        Low: 18.1,
        Close: 18.190001,
        Volume: 6987600,
        AdjClose: 14.457489
    },
    {
        Date: "2010-01-27",
        Open: 18.84,
        High: 18.940001,
        Low: 18.549999,
        Close: 18.879999,
        Volume: 5276600,
        AdjClose: 15.005903
    },
    {
        Date: "2010-01-26",
        Open: 18.42,
        High: 18.76,
        Low: 18.309999,
        Close: 18.51,
        Volume: 5595100,
        AdjClose: 14.711826
    },
    {
        Date: "2010-01-25",
        Open: 18.68,
        High: 18.74,
        Low: 18.34,
        Close: 18.469999,
        Volume: 5457800,
        AdjClose: 14.680033
    },
    {
        Date: "2010-01-22",
        Open: 18.4,
        High: 18.540001,
        Low: 17.940001,
        Close: 17.99,
        Volume: 5739800,
        AdjClose: 14.298528
    },
    {
        Date: "2010-01-21",
        Open: 18.9,
        High: 18.969999,
        Low: 18.290001,
        Close: 18.66,
        Volume: 8110800,
        AdjClose: 14.831047
    },
    {
        Date: "2010-01-20",
        Open: 19.18,
        High: 19.190001,
        Low: 18.860001,
        Close: 19.08,
        Volume: 4321100,
        AdjClose: 15.164864
    },
    {
        Date: "2010-01-19",
        Open: 19.459999,
        High: 19.85,
        Low: 19.43,
        Close: 19.83,
        Volume: 4019400,
        AdjClose: 15.760968
    },
    {
        Date: "2010-01-15",
        Open: 20.219999,
        High: 20.24,
        Low: 19.93,
        Close: 20.040001,
        Volume: 3371400,
        AdjClose: 15.927877
    },
    {
        Date: "2010-01-14",
        Open: 20.23,
        High: 20.32,
        Low: 20.190001,
        Close: 20.24,
        Volume: 2901800,
        AdjClose: 16.086837
    },
    {
        Date: "2010-01-13",
        Open: 20.1,
        High: 20.200001,
        Low: 19.969999,
        Close: 20.16,
        Volume: 2793600,
        AdjClose: 16.023253
    },
    {
        Date: "2010-01-12",
        Open: 20.129999,
        High: 20.280001,
        Low: 19.93,
        Close: 20.08,
        Volume: 3297800,
        AdjClose: 15.959669
    },
    {
        Date: "2010-01-11",
        Open: 20.67,
        High: 20.709999,
        Low: 20.450001,
        Close: 20.48,
        Volume: 6104500,
        AdjClose: 16.27759
    },
    {
        Date: "2010-01-08",
        Open: 20.23,
        High: 20.809999,
        Low: 20.18,
        Close: 20.74,
        Volume: 4833000,
        AdjClose: 16.484239
    },
    {
        Date: "2010-01-07",
        Open: 20.030001,
        High: 20.209999,
        Low: 19.98,
        Close: 20.17,
        Volume: 2158800,
        AdjClose: 16.031201
    },
    {
        Date: "2010-01-06",
        Open: 19.809999,
        High: 20.040001,
        Low: 19.790001,
        Close: 20,
        Volume: 2483800,
        AdjClose: 15.896084
    },
    {
        Date: "2010-01-05",
        Open: 19.6,
        High: 19.799999,
        Low: 19.540001,
        Close: 19.77,
        Volume: 2881700,
        AdjClose: 15.71328
    },
    {
        Date: "2010-01-04",
        Open: 19.59,
        High: 19.709999,
        Low: 19.51,
        Close: 19.639999,
        Volume: 1847200,
        AdjClose: 15.609954
    },
    {
        Date: "2009-12-31",
        Open: 19.290001,
        High: 19.42,
        Low: 19.1,
        Close: 19.1,
        Volume: 1518000,
        AdjClose: 15.180761
    },
    {
        Date: "2009-12-30",
        Open: 19.09,
        High: 19.25,
        Low: 19.030001,
        Close: 19.219999,
        Volume: 2540500,
        AdjClose: 15.276136
    },
    {
        Date: "2009-12-29",
        Open: 19.309999,
        High: 19.4,
        Low: 19.15,
        Close: 19.24,
        Volume: 1773100,
        AdjClose: 15.292033
    },
    {
        Date: "2009-12-28",
        Open: 19.16,
        High: 19.25,
        Low: 19.01,
        Close: 19.1,
        Volume: 1801200,
        AdjClose: 15.180761
    },
    {
        Date: "2009-12-24",
        Open: 19.09,
        High: 19.1,
        Low: 18.92,
        Close: 19.049999,
        Volume: 899800,
        AdjClose: 15.14102
    },
    {
        Date: "2009-12-23",
        Open: 18.9,
        High: 19.09,
        Low: 18.790001,
        Close: 18.950001,
        Volume: 1912000,
        AdjClose: 15.06154
    },
    {
        Date: "2009-12-22",
        Open: 18.719999,
        High: 18.83,
        Low: 18.66,
        Close: 18.74,
        Volume: 2597700,
        AdjClose: 14.894631
    },
    {
        Date: "2009-12-21",
        Open: 18.52,
        High: 18.610001,
        Low: 18.42,
        Close: 18.48,
        Volume: 2505100,
        AdjClose: 14.687982
    },
    {
        Date: "2009-12-18",
        Open: 18.25,
        High: 18.4,
        Low: 18.049999,
        Close: 18.24,
        Volume: 2419600,
        AdjClose: 14.497229
    },
    {
        Date: "2009-12-17",
        Open: 18.299999,
        High: 18.309999,
        Low: 18.09,
        Close: 18.139999,
        Volume: 2879900,
        AdjClose: 14.417748
    },
    {
        Date: "2009-12-16",
        Open: 18.33,
        High: 18.5,
        Low: 18.219999,
        Close: 18.27,
        Volume: 6236400,
        AdjClose: 14.521073
    },
    {
        Date: "2009-12-15",
        Open: 18.17,
        High: 18.27,
        Low: 18.030001,
        Close: 18.07,
        Volume: 2738900,
        AdjClose: 14.362112
    },
    {
        Date: "2009-12-14",
        Open: 18.280001,
        High: 18.450001,
        Low: 18.16,
        Close: 18.389999,
        Volume: 3016000,
        AdjClose: 14.616449
    },
    {
        Date: "2009-12-11",
        Open: 17.99,
        High: 18.059999,
        Low: 17.9,
        Close: 17.940001,
        Volume: 2340000,
        AdjClose: 14.258788
    },
    {
        Date: "2009-12-10",
        Open: 18.049999,
        High: 18.110001,
        Low: 17.790001,
        Close: 17.83,
        Volume: 3250100,
        AdjClose: 14.171359
    },
    {
        Date: "2009-12-09",
        Open: 17.85,
        High: 17.940001,
        Low: 17.610001,
        Close: 17.91,
        Volume: 3952600,
        AdjClose: 14.234943
    },
    {
        Date: "2009-12-08",
        Open: 18.049999,
        High: 18.15,
        Low: 17.9,
        Close: 18.040001,
        Volume: 5464700,
        AdjClose: 14.338269
    },
    {
        Date: "2009-12-07",
        Open: 18.26,
        High: 18.43,
        Low: 18.219999,
        Close: 18.25,
        Volume: 3830100,
        AdjClose: 14.505177
    },
    {
        Date: "2009-12-04",
        Open: 18.76,
        High: 18.870001,
        Low: 18.33,
        Close: 18.540001,
        Volume: 4131400,
        AdjClose: 14.735671
    },
    {
        Date: "2009-12-03",
        Open: 18.98,
        High: 19.08,
        Low: 18.629999,
        Close: 18.65,
        Volume: 3563500,
        AdjClose: 14.823098
    },
    {
        Date: "2009-12-02",
        Open: 18.799999,
        High: 18.950001,
        Low: 18.700001,
        Close: 18.83,
        Volume: 3278800,
        AdjClose: 14.966163
    },
    {
        Date: "2009-12-01",
        Open: 18.75,
        High: 18.860001,
        Low: 18.58,
        Close: 18.719999,
        Volume: 4834300,
        AdjClose: 14.878734
    },
    {
        Date: "2009-11-30",
        Open: 18.290001,
        High: 18.450001,
        Low: 18.120001,
        Close: 18.360001,
        Volume: 6124700,
        AdjClose: 14.592606
    },
    {
        Date: "2009-11-27",
        Open: 18.25,
        High: 18.620001,
        Low: 18.18,
        Close: 18.5,
        Volume: 1979500,
        AdjClose: 14.703878
    },
    {
        Date: "2009-11-25",
        Open: 19.110001,
        High: 19.219999,
        Low: 19.040001,
        Close: 19.209999,
        Volume: 3561200,
        AdjClose: 15.268188
    },
    {
        Date: "2009-11-24",
        Open: 19.209999,
        High: 19.209999,
        Low: 18.969999,
        Close: 19.01,
        Volume: 5733500,
        AdjClose: 15.109228
    },
    {
        Date: "2009-11-23",
        Open: 19.290001,
        High: 19.360001,
        Low: 18.98,
        Close: 19.07,
        Volume: 3985500,
        AdjClose: 15.156916
    },
    {
        Date: "2009-11-20",
        Open: 18.48,
        High: 18.76,
        Low: 18.459999,
        Close: 18.700001,
        Volume: 3334200,
        AdjClose: 14.862839
    },
    {
        Date: "2009-11-19",
        Open: 18.73,
        High: 18.74,
        Low: 18.43,
        Close: 18.629999,
        Volume: 3468800,
        AdjClose: 14.807202
    },
    {
        Date: "2009-11-18",
        Open: 19.24,
        High: 19.309999,
        Low: 19.040001,
        Close: 19.17,
        Volume: 3688700,
        AdjClose: 15.236397
    },
    {
        Date: "2009-11-17",
        Open: 19.120001,
        High: 19.219999,
        Low: 18.93,
        Close: 19.209999,
        Volume: 4317700,
        AdjClose: 15.268188
    },
    {
        Date: "2009-11-16",
        Open: 19.440001,
        High: 19.85,
        Low: 19.43,
        Close: 19.639999,
        Volume: 5056900,
        AdjClose: 15.609954
    },
    {
        Date: "2009-11-13",
        Open: 19.360001,
        High: 19.719999,
        Low: 19.290001,
        Close: 19.67,
        Volume: 5348700,
        AdjClose: 15.633799
    },
    {
        Date: "2009-11-12",
        Open: 19.860001,
        High: 19.92,
        Low: 19.450001,
        Close: 19.51,
        Volume: 4830900,
        AdjClose: 15.50663
    },
    {
        Date: "2009-11-11",
        Open: 20.16,
        High: 20.33,
        Low: 19.889999,
        Close: 19.99,
        Volume: 3904100,
        AdjClose: 15.888136
    },
    {
        Date: "2009-11-10",
        Open: 19.75,
        High: 20.049999,
        Low: 19.700001,
        Close: 20.040001,
        Volume: 4794300,
        AdjClose: 15.927877
    },
    {
        Date: "2009-11-09",
        Open: 19.809999,
        High: 20.02,
        Low: 19.799999,
        Close: 19.92,
        Volume: 2943000,
        AdjClose: 15.8325
    },
    {
        Date: "2009-11-06",
        Open: 19.299999,
        High: 19.68,
        Low: 19.24,
        Close: 19.52,
        Volume: 2458900,
        AdjClose: 15.514579
    },
    {
        Date: "2009-11-05",
        Open: 19.18,
        High: 19.42,
        Low: 19.1,
        Close: 19.33,
        Volume: 2116000,
        AdjClose: 15.363565
    },
    {
        Date: "2009-11-04",
        Open: 19.059999,
        High: 19.27,
        Low: 18.91,
        Close: 18.92,
        Volume: 3893900,
        AdjClose: 15.037696
    },
    {
        Date: "2009-11-03",
        Open: 18.360001,
        High: 18.799999,
        Low: 18.360001,
        Close: 18.799999,
        Volume: 4144700,
        AdjClose: 14.942319
    },
    {
        Date: "2009-11-02",
        Open: 18.82,
        High: 19.209999,
        Low: 18.41,
        Close: 18.75,
        Volume: 6444500,
        AdjClose: 14.902579
    },
    {
        Date: "2009-10-30",
        Open: 19.16,
        High: 19.209999,
        Low: 18.299999,
        Close: 18.530001,
        Volume: 8939700,
        AdjClose: 14.727723
    },
    {
        Date: "2009-10-29",
        Open: 19.370001,
        High: 19.68,
        Low: 19.120001,
        Close: 19.559999,
        Volume: 9832900,
        AdjClose: 15.54637
    },
    {
        Date: "2009-10-28",
        Open: 20.040001,
        High: 20.200001,
        Low: 19.42,
        Close: 19.530001,
        Volume: 5713700,
        AdjClose: 15.522527
    },
    {
        Date: "2009-10-27",
        Open: 20.780001,
        High: 20.809999,
        Low: 20.379999,
        Close: 20.559999,
        Volume: 2667100,
        AdjClose: 16.341174
    },
    {
        Date: "2009-10-26",
        Open: 21.209999,
        High: 21.450001,
        Low: 20.6,
        Close: 20.709999,
        Volume: 3521900,
        AdjClose: 16.460395
    },
    {
        Date: "2009-10-23",
        Open: 21.530001,
        High: 21.639999,
        Low: 21.24,
        Close: 21.379999,
        Volume: 3646100,
        AdjClose: 16.992913
    },
    {
        Date: "2009-10-22",
        Open: 21.120001,
        High: 21.700001,
        Low: 20.99,
        Close: 21.620001,
        Volume: 4088300,
        AdjClose: 17.183668
    },
    {
        Date: "2009-10-21",
        Open: 21.24,
        High: 21.67,
        Low: 21.17,
        Close: 21.17,
        Volume: 2234500,
        AdjClose: 16.826005
    },
    {
        Date: "2009-10-20",
        Open: 21.549999,
        High: 21.549999,
        Low: 21.18,
        Close: 21.33,
        Volume: 2156600,
        AdjClose: 16.953174
    },
    {
        Date: "2009-10-19",
        Open: 21.299999,
        High: 21.610001,
        Low: 21.26,
        Close: 21.51,
        Volume: 2006700,
        AdjClose: 17.096239
    },
    {
        Date: "2009-10-16",
        Open: 21.450001,
        High: 21.5,
        Low: 21.200001,
        Close: 21.43,
        Volume: 2398700,
        AdjClose: 17.032655
    },
    {
        Date: "2009-10-15",
        Open: 21.92,
        High: 22.01,
        Low: 21.73,
        Close: 21.9,
        Volume: 2552400,
        AdjClose: 17.406212
    },
    {
        Date: "2009-10-14",
        Open: 21.799999,
        High: 21.83,
        Low: 21.59,
        Close: 21.780001,
        Volume: 4821800,
        AdjClose: 17.310836
    },
    {
        Date: "2009-10-13",
        Open: 21.110001,
        High: 21.129999,
        Low: 20.76,
        Close: 20.92,
        Volume: 1977500,
        AdjClose: 16.627304
    },
    {
        Date: "2009-10-12",
        Open: 21.23,
        High: 21.4,
        Low: 20.99,
        Close: 21.059999,
        Volume: 2488700,
        AdjClose: 16.738576
    },
    {
        Date: "2009-10-09",
        Open: 20.66,
        High: 20.84,
        Low: 20.58,
        Close: 20.719999,
        Volume: 1982100,
        AdjClose: 16.468343
    },
    {
        Date: "2009-10-08",
        Open: 20.530001,
        High: 20.870001,
        Low: 20.360001,
        Close: 20.620001,
        Volume: 3212300,
        AdjClose: 16.388864
    },
    {
        Date: "2009-10-07",
        Open: 19.790001,
        High: 19.9,
        Low: 19.57,
        Close: 19.77,
        Volume: 2516200,
        AdjClose: 15.71328
    },
    {
        Date: "2009-10-06",
        Open: 19.84,
        High: 20.280001,
        Low: 19.84,
        Close: 20.030001,
        Volume: 3031100,
        AdjClose: 15.919929
    },
    {
        Date: "2009-10-05",
        Open: 19.450001,
        High: 19.68,
        Low: 19.370001,
        Close: 19.639999,
        Volume: 1786300,
        AdjClose: 15.609954
    },
    {
        Date: "2009-10-02",
        Open: 19.129999,
        High: 19.48,
        Low: 19.120001,
        Close: 19.42,
        Volume: 3371100,
        AdjClose: 15.435098
    },
    {
        Date: "2009-10-01",
        Open: 19.85,
        High: 19.889999,
        Low: 19.440001,
        Close: 19.459999,
        Volume: 2500900,
        AdjClose: 15.466889
    },
    {
        Date: "2009-09-30",
        Open: 20.15,
        High: 20.32,
        Low: 19.73,
        Close: 20.040001,
        Volume: 3241000,
        AdjClose: 15.927877
    },
    {
        Date: "2009-09-29",
        Open: 20.08,
        High: 20.25,
        Low: 19.959999,
        Close: 20.1,
        Volume: 1982700,
        AdjClose: 15.975565
    },
    {
        Date: "2009-09-28",
        Open: 19.98,
        High: 20.379999,
        Low: 19.950001,
        Close: 20.209999,
        Volume: 1673100,
        AdjClose: 16.062992
    },
    {
        Date: "2009-09-25",
        Open: 20.059999,
        High: 20.26,
        Low: 19.85,
        Close: 20.1,
        Volume: 5042500,
        AdjClose: 15.975565
    },
    {
        Date: "2009-09-24",
        Open: 20.99,
        High: 21.01,
        Low: 20.299999,
        Close: 20.49,
        Volume: 2961500,
        AdjClose: 16.285538
    },
    {
        Date: "2009-09-23",
        Open: 20.92,
        High: 21.290001,
        Low: 20.809999,
        Close: 20.860001,
        Volume: 4002000,
        AdjClose: 16.579616
    },
    {
        Date: "2009-09-22",
        Open: 20.950001,
        High: 20.950001,
        Low: 20.75,
        Close: 20.91,
        Volume: 3042800,
        AdjClose: 16.619356
    },
    {
        Date: "2009-09-21",
        Open: 20.18,
        High: 20.4,
        Low: 20.09,
        Close: 20.379999,
        Volume: 2487500,
        AdjClose: 16.198109
    },
    {
        Date: "2009-09-18",
        Open: 20.370001,
        High: 20.559999,
        Low: 20.280001,
        Close: 20.48,
        Volume: 2548300,
        AdjClose: 16.27759
    },
    {
        Date: "2009-09-17",
        Open: 20.360001,
        High: 20.620001,
        Low: 20.18,
        Close: 20.33,
        Volume: 4518100,
        AdjClose: 16.15837
    },
    {
        Date: "2009-09-16",
        Open: 20.799999,
        High: 21.030001,
        Low: 20.68,
        Close: 21.02,
        Volume: 3010700,
        AdjClose: 16.706785
    },
    {
        Date: "2009-09-15",
        Open: 20.440001,
        High: 20.68,
        Low: 20.290001,
        Close: 20.610001,
        Volume: 2994100,
        AdjClose: 16.380915
    },
    {
        Date: "2009-09-14",
        Open: 20.25,
        High: 20.540001,
        Low: 20.219999,
        Close: 20.52,
        Volume: 2259400,
        AdjClose: 16.309383
    },
    {
        Date: "2009-09-11",
        Open: 20.620001,
        High: 20.68,
        Low: 20.299999,
        Close: 20.540001,
        Volume: 4015500,
        AdjClose: 16.325279
    },
    {
        Date: "2009-09-10",
        Open: 20.719999,
        High: 20.84,
        Low: 20.440001,
        Close: 20.799999,
        Volume: 4574700,
        AdjClose: 16.531927
    },
    {
        Date: "2009-09-09",
        Open: 20.209999,
        High: 20.4,
        Low: 19.99,
        Close: 20.120001,
        Volume: 7483700,
        AdjClose: 15.991461
    },
    {
        Date: "2009-09-08",
        Open: 19.469999,
        High: 19.540001,
        Low: 19.27,
        Close: 19.389999,
        Volume: 3091000,
        AdjClose: 15.411253
    },
    {
        Date: "2009-09-04",
        Open: 18.67,
        High: 19.02,
        Low: 18.549999,
        Close: 18.98,
        Volume: 2129600,
        AdjClose: 15.085384
    },
    {
        Date: "2009-09-03",
        Open: 18.83,
        High: 18.85,
        Low: 18.620001,
        Close: 18.799999,
        Volume: 3426300,
        AdjClose: 14.942319
    },
    {
        Date: "2009-09-02",
        Open: 18.26,
        High: 18.59,
        Low: 18.219999,
        Close: 18.530001,
        Volume: 3882300,
        AdjClose: 14.727723
    },
    {
        Date: "2009-09-01",
        Open: 18.65,
        High: 19.059999,
        Low: 18.299999,
        Close: 18.370001,
        Volume: 5244000,
        AdjClose: 14.600554
    },
    {
        Date: "2009-08-31",
        Open: 19.18,
        High: 19.219999,
        Low: 19.040001,
        Close: 19.17,
        Volume: 2943500,
        AdjClose: 15.236397
    },
    {
        Date: "2009-08-28",
        Open: 19.620001,
        High: 19.66,
        Low: 19.360001,
        Close: 19.49,
        Volume: 2763600,
        AdjClose: 15.490734
    },
    {
        Date: "2009-08-27",
        Open: 19.17,
        High: 19.43,
        Low: 18.870001,
        Close: 19.33,
        Volume: 3281700,
        AdjClose: 15.363565
    },
    {
        Date: "2009-08-26",
        Open: 19.25,
        High: 19.360001,
        Low: 19,
        Close: 19.23,
        Volume: 3227300,
        AdjClose: 15.284085
    },
    {
        Date: "2009-08-25",
        Open: 19.629999,
        High: 19.84,
        Low: 19.49,
        Close: 19.59,
        Volume: 3604900,
        AdjClose: 15.570215
    },
    {
        Date: "2009-08-24",
        Open: 19.459999,
        High: 19.52,
        Low: 19.15,
        Close: 19.23,
        Volume: 3529500,
        AdjClose: 15.284085
    },
    {
        Date: "2009-08-21",
        Open: 18.870001,
        High: 19.08,
        Low: 18.85,
        Close: 18.99,
        Volume: 2342200,
        AdjClose: 15.093332
    },
    {
        Date: "2009-08-20",
        Open: 18.309999,
        High: 18.549999,
        Low: 18.27,
        Close: 18.469999,
        Volume: 1982100,
        AdjClose: 14.680033
    },
    {
        Date: "2009-08-19",
        Open: 17.700001,
        High: 18.33,
        Low: 17.700001,
        Close: 18.219999,
        Volume: 2460400,
        AdjClose: 14.481332
    },
    {
        Date: "2009-08-18",
        Open: 17.82,
        High: 18.01,
        Low: 17.709999,
        Close: 17.91,
        Volume: 2491000,
        AdjClose: 14.234943
    },
    {
        Date: "2009-08-17",
        Open: 17.790001,
        High: 17.85,
        Low: 17.65,
        Close: 17.719999,
        Volume: 3688900,
        AdjClose: 14.08393
    },
    {
        Date: "2009-08-14",
        Open: 18.629999,
        High: 18.65,
        Low: 18.18,
        Close: 18.459999,
        Volume: 3390500,
        AdjClose: 14.672085
    },
    {
        Date: "2009-08-13",
        Open: 18.709999,
        High: 18.780001,
        Low: 18.49,
        Close: 18.690001,
        Volume: 2850300,
        AdjClose: 14.854891
    },
    {
        Date: "2009-08-12",
        Open: 18.040001,
        High: 18.42,
        Low: 18.040001,
        Close: 18.24,
        Volume: 2111500,
        AdjClose: 14.497229
    },
    {
        Date: "2009-08-11",
        Open: 18.049999,
        High: 18.059999,
        Low: 17.85,
        Close: 17.959999,
        Volume: 2164700,
        AdjClose: 14.274683
    },
    {
        Date: "2009-08-10",
        Open: 18.360001,
        High: 18.379999,
        Low: 18.08,
        Close: 18.200001,
        Volume: 2767900,
        AdjClose: 14.465437
    },
    {
        Date: "2009-08-07",
        Open: 18.379999,
        High: 18.5,
        Low: 18.200001,
        Close: 18.33,
        Volume: 2282000,
        AdjClose: 14.568761
    },
    {
        Date: "2009-08-06",
        Open: 18.440001,
        High: 18.57,
        Low: 18.129999,
        Close: 18.309999,
        Volume: 3319700,
        AdjClose: 14.552865
    },
    {
        Date: "2009-08-05",
        Open: 18.530001,
        High: 18.65,
        Low: 18.17,
        Close: 18.5,
        Volume: 3062300,
        AdjClose: 14.703878
    },
    {
        Date: "2009-08-04",
        Open: 18.549999,
        High: 18.76,
        Low: 18.440001,
        Close: 18.74,
        Volume: 4123600,
        AdjClose: 14.894631
    },
    {
        Date: "2009-08-03",
        Open: 18.700001,
        High: 19.030001,
        Low: 18.629999,
        Close: 18.99,
        Volume: 5052900,
        AdjClose: 15.093332
    },
    {
        Date: "2009-07-31",
        Open: 17.9,
        High: 18.4,
        Low: 17.75,
        Close: 18.280001,
        Volume: 4374500,
        AdjClose: 14.529022
    },
    {
        Date: "2009-07-30",
        Open: 17.74,
        High: 18.040001,
        Low: 17.629999,
        Close: 17.91,
        Volume: 5264800,
        AdjClose: 14.234943
    },
    {
        Date: "2009-07-29",
        Open: 17.6,
        High: 17.620001,
        Low: 17.290001,
        Close: 17.459999,
        Volume: 3869700,
        AdjClose: 13.877281
    },
    {
        Date: "2009-07-28",
        Open: 17.49,
        High: 17.610001,
        Low: 17.110001,
        Close: 17.360001,
        Volume: 4814300,
        AdjClose: 13.797802
    },
    {
        Date: "2009-07-27",
        Open: 17.450001,
        High: 17.690001,
        Low: 17.309999,
        Close: 17.639999,
        Volume: 4391000,
        AdjClose: 14.020346
    },
    {
        Date: "2009-07-24",
        Open: 17.33,
        High: 17.66,
        Low: 17.110001,
        Close: 17.639999,
        Volume: 5198800,
        AdjClose: 14.020346
    },
    {
        Date: "2009-07-23",
        Open: 16.82,
        High: 17.299999,
        Low: 16.73,
        Close: 17.16,
        Volume: 9769700,
        AdjClose: 13.63884
    },
    {
        Date: "2009-07-22",
        Open: 16.129999,
        High: 16.42,
        Low: 16.049999,
        Close: 16.129999,
        Volume: 6167200,
        AdjClose: 12.820191
    },
    {
        Date: "2009-07-21",
        Open: 17,
        High: 17.08,
        Low: 16.629999,
        Close: 16.77,
        Volume: 3827200,
        AdjClose: 12.978358
    },
    {
        Date: "2009-07-20",
        Open: 16.65,
        High: 16.790001,
        Low: 16.450001,
        Close: 16.75,
        Volume: 3666100,
        AdjClose: 12.962879
    },
    {
        Date: "2009-07-17",
        Open: 16.23,
        High: 16.379999,
        Low: 16.110001,
        Close: 16.34,
        Volume: 1910400,
        AdjClose: 12.645579
    },
    {
        Date: "2009-07-16",
        Open: 16.209999,
        High: 16.49,
        Low: 16.120001,
        Close: 16.4,
        Volume: 3253100,
        AdjClose: 12.692013
    },
    {
        Date: "2009-07-15",
        Open: 16,
        High: 16.33,
        Low: 15.99,
        Close: 16.309999,
        Volume: 3055100,
        AdjClose: 12.622362
    },
    {
        Date: "2009-07-14",
        Open: 15.46,
        High: 15.61,
        Low: 15.25,
        Close: 15.49,
        Volume: 2261000,
        AdjClose: 11.987761
    },
    {
        Date: "2009-07-13",
        Open: 15.24,
        High: 15.51,
        Low: 15.05,
        Close: 15.47,
        Volume: 3218300,
        AdjClose: 11.972283
    },
    {
        Date: "2009-07-10",
        Open: 14.89,
        High: 15.11,
        Low: 14.86,
        Close: 15.02,
        Volume: 3616500,
        AdjClose: 11.624027
    },
    {
        Date: "2009-07-09",
        Open: 15.3,
        High: 15.41,
        Low: 15.05,
        Close: 15.2,
        Volume: 5096900,
        AdjClose: 11.763329
    },
    {
        Date: "2009-07-08",
        Open: 14.8,
        High: 14.91,
        Low: 14.44,
        Close: 14.69,
        Volume: 4201000,
        AdjClose: 11.368638
    },
    {
        Date: "2009-07-07",
        Open: 15.28,
        High: 15.31,
        Low: 14.81,
        Close: 14.85,
        Volume: 5385800,
        AdjClose: 11.492464
    },
    {
        Date: "2009-07-06",
        Open: 15.13,
        High: 15.26,
        Low: 15,
        Close: 15.24,
        Volume: 3894800,
        AdjClose: 11.794285
    },
    {
        Date: "2009-07-02",
        Open: 15.75,
        High: 15.75,
        Low: 15.48,
        Close: 15.56,
        Volume: 3517200,
        AdjClose: 12.041935
    },
    {
        Date: "2009-07-01",
        Open: 16.120001,
        High: 16.370001,
        Low: 16.120001,
        Close: 16.16,
        Volume: 5504000,
        AdjClose: 12.506276
    },
    {
        Date: "2009-06-30",
        Open: 15.97,
        High: 16.08,
        Low: 15.58,
        Close: 15.78,
        Volume: 3165300,
        AdjClose: 12.212193
    },
    {
        Date: "2009-06-29",
        Open: 15.89,
        High: 16.15,
        Low: 15.8,
        Close: 15.99,
        Volume: 4232900,
        AdjClose: 12.374713
    },
    {
        Date: "2009-06-26",
        Open: 15.4,
        High: 15.51,
        Low: 15.28,
        Close: 15.44,
        Volume: 5150200,
        AdjClose: 11.949066
    },
    {
        Date: "2009-06-25",
        Open: 15.11,
        High: 15.63,
        Low: 15.07,
        Close: 15.58,
        Volume: 3820000,
        AdjClose: 12.057413
    },
    {
        Date: "2009-06-24",
        Open: 15.47,
        High: 15.66,
        Low: 15.24,
        Close: 15.35,
        Volume: 3484000,
        AdjClose: 11.879415
    },
    {
        Date: "2009-06-23",
        Open: 15.06,
        High: 15.24,
        Low: 14.84,
        Close: 15.14,
        Volume: 3638400,
        AdjClose: 11.716895
    },
    {
        Date: "2009-06-22",
        Open: 15.2,
        High: 15.26,
        Low: 14.97,
        Close: 14.99,
        Volume: 6441300,
        AdjClose: 11.60081
    },
    {
        Date: "2009-06-19",
        Open: 15.69,
        High: 15.83,
        Low: 15.61,
        Close: 15.66,
        Volume: 4269600,
        AdjClose: 12.119325
    },
    {
        Date: "2009-06-18",
        Open: 15.56,
        High: 15.81,
        Low: 15.48,
        Close: 15.69,
        Volume: 5480200,
        AdjClose: 12.142542
    },
    {
        Date: "2009-06-17",
        Open: 15.49,
        High: 15.7,
        Low: 15.31,
        Close: 15.57,
        Volume: 4599600,
        AdjClose: 12.049673
    },
    {
        Date: "2009-06-16",
        Open: 16.139999,
        High: 16.209999,
        Low: 15.68,
        Close: 15.74,
        Volume: 3773000,
        AdjClose: 12.181237
    },
    {
        Date: "2009-06-15",
        Open: 16.219999,
        High: 16.23,
        Low: 15.8,
        Close: 15.94,
        Volume: 3745900,
        AdjClose: 12.336017
    },
    {
        Date: "2009-06-12",
        Open: 16.6,
        High: 16.66,
        Low: 16.43,
        Close: 16.639999,
        Volume: 2771800,
        AdjClose: 12.87775
    },
    {
        Date: "2009-06-11",
        Open: 16.58,
        High: 17.08,
        Low: 16.58,
        Close: 16.83,
        Volume: 2981200,
        AdjClose: 13.024792
    },
    {
        Date: "2009-06-10",
        Open: 16.709999,
        High: 16.73,
        Low: 16.18,
        Close: 16.41,
        Volume: 4531200,
        AdjClose: 12.699752
    },
    {
        Date: "2009-06-09",
        Open: 16.190001,
        High: 16.51,
        Low: 16.16,
        Close: 16.4,
        Volume: 2187200,
        AdjClose: 12.692013
    },
    {
        Date: "2009-06-08",
        Open: 15.88,
        High: 16.26,
        Low: 15.67,
        Close: 16.120001,
        Volume: 3757700,
        AdjClose: 12.475321
    },
    {
        Date: "2009-06-05",
        Open: 16.76,
        High: 16.84,
        Low: 16.34,
        Close: 16.51,
        Volume: 3727300,
        AdjClose: 12.777143
    },
    {
        Date: "2009-06-04",
        Open: 16.719999,
        High: 16.860001,
        Low: 16.5,
        Close: 16.76,
        Volume: 2784000,
        AdjClose: 12.970619
    },
    {
        Date: "2009-06-03",
        Open: 16.82,
        High: 16.91,
        Low: 16.459999,
        Close: 16.700001,
        Volume: 4410300,
        AdjClose: 12.924185
    },
    {
        Date: "2009-06-02",
        Open: 16.950001,
        High: 17.440001,
        Low: 16.92,
        Close: 17.370001,
        Volume: 8073500,
        AdjClose: 13.4427
    },
    {
        Date: "2009-06-01",
        Open: 16.9,
        High: 17.389999,
        Low: 16.77,
        Close: 17.18,
        Volume: 5544700,
        AdjClose: 13.295658
    },
    {
        Date: "2009-05-29",
        Open: 16.290001,
        High: 16.48,
        Low: 16.219999,
        Close: 16.459999,
        Volume: 3748900,
        AdjClose: 12.738447
    },
    {
        Date: "2009-05-28",
        Open: 16.09,
        High: 16.290001,
        Low: 15.77,
        Close: 16.23,
        Volume: 3508600,
        AdjClose: 12.560449
    },
    {
        Date: "2009-05-27",
        Open: 16.25,
        High: 16.34,
        Low: 15.92,
        Close: 15.93,
        Volume: 3265200,
        AdjClose: 12.328279
    },
    {
        Date: "2009-05-26",
        Open: 15.53,
        High: 16.280001,
        Low: 15.5,
        Close: 16.18,
        Volume: 5321300,
        AdjClose: 12.521755
    },
    {
        Date: "2009-05-22",
        Open: 16.110001,
        High: 16.200001,
        Low: 15.9,
        Close: 16.01,
        Volume: 2522700,
        AdjClose: 12.390191
    },
    {
        Date: "2009-05-21",
        Open: 15.86,
        High: 16.16,
        Low: 15.59,
        Close: 15.7,
        Volume: 4690600,
        AdjClose: 12.150281
    },
    {
        Date: "2009-05-20",
        Open: 16.07,
        High: 16.4,
        Low: 16.02,
        Close: 16.049999,
        Volume: 4301800,
        AdjClose: 12.421147
    },
    {
        Date: "2009-05-19",
        Open: 15.67,
        High: 16.040001,
        Low: 15.49,
        Close: 15.81,
        Volume: 4101000,
        AdjClose: 12.235411
    },
    {
        Date: "2009-05-18",
        Open: 15.15,
        High: 15.55,
        Low: 15.02,
        Close: 15.5,
        Volume: 3596400,
        AdjClose: 11.9955
    },
    {
        Date: "2009-05-15",
        Open: 14.92,
        High: 15.27,
        Low: 14.77,
        Close: 14.87,
        Volume: 5201200,
        AdjClose: 11.507941
    },
    {
        Date: "2009-05-14",
        Open: 14.86,
        High: 15.03,
        Low: 14.68,
        Close: 14.88,
        Volume: 8156200,
        AdjClose: 11.51568
    },
    {
        Date: "2009-05-13",
        Open: 14.83,
        High: 14.84,
        Low: 14.42,
        Close: 14.44,
        Volume: 5913700,
        AdjClose: 11.175163
    },
    {
        Date: "2009-05-12",
        Open: 16.02,
        High: 16.049999,
        Low: 15.26,
        Close: 15.52,
        Volume: 5237800,
        AdjClose: 12.010979
    },
    {
        Date: "2009-05-11",
        Open: 15.99,
        High: 16.17,
        Low: 15.91,
        Close: 16.02,
        Volume: 3314300,
        AdjClose: 12.39793
    },
    {
        Date: "2009-05-08",
        Open: 16.139999,
        High: 16.629999,
        Low: 15.94,
        Close: 16.52,
        Volume: 4274400,
        AdjClose: 12.784882
    },
    {
        Date: "2009-05-07",
        Open: 16.41,
        High: 16.49,
        Low: 15.8,
        Close: 16.030001,
        Volume: 4288300,
        AdjClose: 12.40567
    },
    {
        Date: "2009-05-06",
        Open: 16.200001,
        High: 16.379999,
        Low: 15.95,
        Close: 16.290001,
        Volume: 5927900,
        AdjClose: 12.606885
    },
    {
        Date: "2009-05-05",
        Open: 15.89,
        High: 15.97,
        Low: 15.59,
        Close: 15.87,
        Volume: 4067000,
        AdjClose: 12.281844
    },
    {
        Date: "2009-05-04",
        Open: 14.96,
        High: 15.98,
        Low: 14.94,
        Close: 15.98,
        Volume: 10402800,
        AdjClose: 12.366974
    },
    {
        Date: "2009-05-01",
        Open: 14.18,
        High: 14.64,
        Low: 14.11,
        Close: 14.38,
        Volume: 4772200,
        AdjClose: 11.128729
    },
    {
        Date: "2009-04-30",
        Open: 14.42,
        High: 14.65,
        Low: 14.04,
        Close: 14.22,
        Volume: 4807200,
        AdjClose: 11.004904
    },
    {
        Date: "2009-04-29",
        Open: 14.02,
        High: 14.45,
        Low: 13.98,
        Close: 14.24,
        Volume: 4974900,
        AdjClose: 11.020382
    },
    {
        Date: "2009-04-28",
        Open: 13.77,
        High: 14.23,
        Low: 13.71,
        Close: 14.06,
        Volume: 3651000,
        AdjClose: 10.88108
    },
    {
        Date: "2009-04-27",
        Open: 14.43,
        High: 14.82,
        Low: 14.4,
        Close: 14.58,
        Volume: 2903900,
        AdjClose: 11.283509
    },
    {
        Date: "2009-04-24",
        Open: 14.77,
        High: 15.18,
        Low: 14.69,
        Close: 15,
        Volume: 4501300,
        AdjClose: 11.608549
    },
    {
        Date: "2009-04-23",
        Open: 14.75,
        High: 14.85,
        Low: 14.44,
        Close: 14.71,
        Volume: 7539900,
        AdjClose: 11.384117
    },
    {
        Date: "2009-04-22",
        Open: 15.03,
        High: 15.61,
        Low: 14.93,
        Close: 15.12,
        Volume: 5667100,
        AdjClose: 11.701417
    },
    {
        Date: "2009-04-21",
        Open: 14.05,
        High: 14.7,
        Low: 13.99,
        Close: 14.65,
        Volume: 2753700,
        AdjClose: 11.337682
    },
    {
        Date: "2009-04-20",
        Open: 14.67,
        High: 14.68,
        Low: 14.31,
        Close: 14.31,
        Volume: 2763000,
        AdjClose: 11.074556
    },
    {
        Date: "2009-04-17",
        Open: 15.23,
        High: 15.5,
        Low: 15.1,
        Close: 15.36,
        Volume: 3624000,
        AdjClose: 11.887154
    },
    {
        Date: "2009-04-16",
        Open: 15.23,
        High: 15.39,
        Low: 14.93,
        Close: 15.28,
        Volume: 4307300,
        AdjClose: 11.825241
    },
    {
        Date: "2009-04-15",
        Open: 14.89,
        High: 15.23,
        Low: 14.82,
        Close: 15.2,
        Volume: 3226500,
        AdjClose: 11.763329
    },
    {
        Date: "2009-04-14",
        Open: 15.08,
        High: 15.3,
        Low: 14.89,
        Close: 14.99,
        Volume: 3192300,
        AdjClose: 11.60081
    },
    {
        Date: "2009-04-13",
        Open: 14.64,
        High: 15.34,
        Low: 14.55,
        Close: 15.24,
        Volume: 2531600,
        AdjClose: 11.794285
    },
    {
        Date: "2009-04-09",
        Open: 14.75,
        High: 15.06,
        Low: 14.63,
        Close: 15.01,
        Volume: 4499300,
        AdjClose: 11.616288
    },
    {
        Date: "2009-04-08",
        Open: 14.2,
        High: 14.48,
        Low: 14.01,
        Close: 14.38,
        Volume: 3009400,
        AdjClose: 11.128729
    },
    {
        Date: "2009-04-07",
        Open: 14.19,
        High: 14.34,
        Low: 14.06,
        Close: 14.12,
        Volume: 3339300,
        AdjClose: 10.927514
    },
    {
        Date: "2009-04-06",
        Open: 14.8,
        High: 14.92,
        Low: 14.55,
        Close: 14.91,
        Volume: 4790100,
        AdjClose: 11.538897
    },
    {
        Date: "2009-04-03",
        Open: 15.15,
        High: 15.23,
        Low: 14.85,
        Close: 15.2,
        Volume: 3521700,
        AdjClose: 11.763329
    },
    {
        Date: "2009-04-02",
        Open: 14.54,
        High: 15.19,
        Low: 14.51,
        Close: 14.78,
        Volume: 5546300,
        AdjClose: 11.43829
    },
    {
        Date: "2009-04-01",
        Open: 13.63,
        High: 14.42,
        Low: 13.63,
        Close: 14.33,
        Volume: 4203300,
        AdjClose: 11.090034
    },
    {
        Date: "2009-03-31",
        Open: 13.95,
        High: 14.23,
        Low: 13.8,
        Close: 13.94,
        Volume: 4572800,
        AdjClose: 10.788211
    },
    {
        Date: "2009-03-30",
        Open: 13.46,
        High: 13.47,
        Low: 13.1,
        Close: 13.29,
        Volume: 3340300,
        AdjClose: 10.285174
    },
    {
        Date: "2009-03-27",
        Open: 13.94,
        High: 14.02,
        Low: 13.72,
        Close: 13.8,
        Volume: 4570200,
        AdjClose: 10.679865
    },
    {
        Date: "2009-03-26",
        Open: 14.31,
        High: 14.68,
        Low: 14.2,
        Close: 14.61,
        Volume: 6337400,
        AdjClose: 11.306726
    },
    {
        Date: "2009-03-25",
        Open: 13.89,
        High: 14.32,
        Low: 13.85,
        Close: 14.26,
        Volume: 7415300,
        AdjClose: 11.035861
    },
    {
        Date: "2009-03-24",
        Open: 14.04,
        High: 14.42,
        Low: 13.97,
        Close: 14.04,
        Volume: 6301400,
        AdjClose: 10.865602
    },
    {
        Date: "2009-03-23",
        Open: 13.9,
        High: 14.69,
        Low: 13.83,
        Close: 14.69,
        Volume: 5512000,
        AdjClose: 11.368638
    },
    {
        Date: "2009-03-20",
        Open: 13.62,
        High: 13.88,
        Low: 13.45,
        Close: 13.51,
        Volume: 3574500,
        AdjClose: 10.455433
    },
    {
        Date: "2009-03-19",
        Open: 14.08,
        High: 14.08,
        Low: 13.69,
        Close: 13.69,
        Volume: 4087200,
        AdjClose: 10.594735
    },
    {
        Date: "2009-03-18",
        Open: 13.45,
        High: 14.09,
        Low: 13.14,
        Close: 13.88,
        Volume: 5384300,
        AdjClose: 10.741777
    },
    {
        Date: "2009-03-17",
        Open: 13.37,
        High: 13.87,
        Low: 13.32,
        Close: 13.87,
        Volume: 3745800,
        AdjClose: 10.734038
    },
    {
        Date: "2009-03-16",
        Open: 13.48,
        High: 13.81,
        Low: 13.37,
        Close: 13.37,
        Volume: 4140800,
        AdjClose: 10.347086
    },
    {
        Date: "2009-03-13",
        Open: 13.21,
        High: 13.45,
        Low: 12.95,
        Close: 13.4,
        Volume: 4765400,
        AdjClose: 10.370303
    },
    {
        Date: "2009-03-12",
        Open: 12.78,
        High: 13.37,
        Low: 12.65,
        Close: 13.32,
        Volume: 4923800,
        AdjClose: 10.308391
    },
    {
        Date: "2009-03-11",
        Open: 13.21,
        High: 13.36,
        Low: 12.95,
        Close: 13.13,
        Volume: 7436800,
        AdjClose: 10.16135
    },
    {
        Date: "2009-03-10",
        Open: 12.27,
        High: 12.85,
        Low: 12.25,
        Close: 12.72,
        Volume: 4836600,
        AdjClose: 9.84405
    },
    {
        Date: "2009-03-09",
        Open: 11.3,
        High: 11.85,
        Low: 11.3,
        Close: 11.49,
        Volume: 5764100,
        AdjClose: 8.892148
    },
    {
        Date: "2009-03-06",
        Open: 11.86,
        High: 12.09,
        Low: 11.45,
        Close: 11.8,
        Volume: 5292700,
        AdjClose: 9.132059
    },
    {
        Date: "2009-03-05",
        Open: 11.8,
        High: 11.96,
        Low: 11.36,
        Close: 11.47,
        Volume: 4184500,
        AdjClose: 8.87667
    },
    {
        Date: "2009-03-04",
        Open: 11.65,
        High: 12.18,
        Low: 11.56,
        Close: 11.92,
        Volume: 6386900,
        AdjClose: 9.224927
    },
    {
        Date: "2009-03-03",
        Open: 11.31,
        High: 11.43,
        Low: 10.97,
        Close: 11.19,
        Volume: 6540300,
        AdjClose: 8.659977
    },
    {
        Date: "2009-03-02",
        Open: 11.49,
        High: 11.57,
        Low: 10.88,
        Close: 10.97,
        Volume: 6144100,
        AdjClose: 8.489719
    },
    {
        Date: "2009-02-27",
        Open: 11.94,
        High: 12.46,
        Low: 11.85,
        Close: 12.05,
        Volume: 5557100,
        AdjClose: 9.325534
    },
    {
        Date: "2009-02-26",
        Open: 11.95,
        High: 12.11,
        Low: 11.68,
        Close: 11.77,
        Volume: 4624200,
        AdjClose: 9.108842
    },
    {
        Date: "2009-02-25",
        Open: 11.67,
        High: 11.82,
        Low: 11.29,
        Close: 11.59,
        Volume: 4632200,
        AdjClose: 8.969539
    },
    {
        Date: "2009-02-24",
        Open: 11.38,
        High: 11.8,
        Low: 11.12,
        Close: 11.68,
        Volume: 5452100,
        AdjClose: 9.03919
    },
    {
        Date: "2009-02-23",
        Open: 11.87,
        High: 11.88,
        Low: 10.96,
        Close: 10.99,
        Volume: 4560600,
        AdjClose: 8.505197
    },
    {
        Date: "2009-02-20",
        Open: 11.45,
        High: 11.86,
        Low: 11.37,
        Close: 11.66,
        Volume: 5962500,
        AdjClose: 9.023712
    },
    {
        Date: "2009-02-19",
        Open: 12.4,
        High: 12.5,
        Low: 11.9,
        Close: 11.95,
        Volume: 7490000,
        AdjClose: 9.248144
    },
    {
        Date: "2009-02-18",
        Open: 12.37,
        High: 12.39,
        Low: 12.03,
        Close: 12.18,
        Volume: 9244600,
        AdjClose: 9.426142
    },
    {
        Date: "2009-02-17",
        Open: 12.62,
        High: 12.63,
        Low: 12.25,
        Close: 12.25,
        Volume: 7744400,
        AdjClose: 9.480315
    },
    {
        Date: "2009-02-13",
        Open: 13.5,
        High: 13.74,
        Low: 13.43,
        Close: 13.54,
        Volume: 4927800,
        AdjClose: 10.47865
    },
    {
        Date: "2009-02-12",
        Open: 12.95,
        High: 13.45,
        Low: 12.63,
        Close: 13.43,
        Volume: 8668600,
        AdjClose: 10.393521
    },
    {
        Date: "2009-02-11",
        Open: 13.66,
        High: 13.78,
        Low: 12.85,
        Close: 13.16,
        Volume: 6705600,
        AdjClose: 10.184567
    },
    {
        Date: "2009-02-10",
        Open: 14.08,
        High: 14.15,
        Low: 13.4,
        Close: 13.5,
        Volume: 5453100,
        AdjClose: 10.447694
    },
    {
        Date: "2009-02-09",
        Open: 14.38,
        High: 14.65,
        Low: 14.22,
        Close: 14.43,
        Volume: 4761600,
        AdjClose: 11.167424
    },
    {
        Date: "2009-02-06",
        Open: 13.73,
        High: 14.31,
        Low: 13.73,
        Close: 14.24,
        Volume: 3815900,
        AdjClose: 11.020382
    },
    {
        Date: "2009-02-05",
        Open: 13.14,
        High: 13.62,
        Low: 12.94,
        Close: 13.5,
        Volume: 4825700,
        AdjClose: 10.447694
    },
    {
        Date: "2009-02-04",
        Open: 13.24,
        High: 13.55,
        Low: 13.07,
        Close: 13.13,
        Volume: 3790700,
        AdjClose: 10.16135
    },
    {
        Date: "2009-02-03",
        Open: 12.63,
        High: 13.1,
        Low: 12.52,
        Close: 13.05,
        Volume: 4799300,
        AdjClose: 10.099438
    },
    {
        Date: "2009-02-02",
        Open: 12.14,
        High: 12.48,
        Low: 12.11,
        Close: 12.34,
        Volume: 4971400,
        AdjClose: 9.549966
    },
    {
        Date: "2009-01-30",
        Open: 13.29,
        High: 13.29,
        Low: 12.88,
        Close: 13.05,
        Volume: 4780900,
        AdjClose: 10.099438
    },
    {
        Date: "2009-01-29",
        Open: 12.97,
        High: 13.23,
        Low: 12.81,
        Close: 12.94,
        Volume: 5119600,
        AdjClose: 10.014308
    },
    {
        Date: "2009-01-28",
        Open: 13.43,
        High: 13.62,
        Low: 13.18,
        Close: 13.43,
        Volume: 4929500,
        AdjClose: 10.393521
    },
    {
        Date: "2009-01-27",
        Open: 12.39,
        High: 12.6,
        Low: 12.27,
        Close: 12.52,
        Volume: 3785700,
        AdjClose: 9.689269
    },
    {
        Date: "2009-01-26",
        Open: 11.97,
        High: 12.45,
        Low: 11.93,
        Close: 12.2,
        Volume: 5279400,
        AdjClose: 9.44162
    },
    {
        Date: "2009-01-23",
        Open: 11.75,
        High: 12.35,
        Low: 11.7,
        Close: 12.21,
        Volume: 3983200,
        AdjClose: 9.449359
    },
    {
        Date: "2009-01-22",
        Open: 12.31,
        High: 12.66,
        Low: 12.16,
        Close: 12.42,
        Volume: 5344400,
        AdjClose: 9.611878
    },
    {
        Date: "2009-01-21",
        Open: 12.43,
        High: 12.6,
        Low: 12.07,
        Close: 12.55,
        Volume: 4987200,
        AdjClose: 9.712486
    },
    {
        Date: "2009-01-20",
        Open: 12.12,
        High: 12.18,
        Low: 11.62,
        Close: 11.69,
        Volume: 5082800,
        AdjClose: 9.046929
    },
    {
        Date: "2009-01-16",
        Open: 12.67,
        High: 12.74,
        Low: 12.07,
        Close: 12.47,
        Volume: 4090600,
        AdjClose: 9.650574
    },
    {
        Date: "2009-01-15",
        Open: 12.34,
        High: 12.68,
        Low: 11.9,
        Close: 12.58,
        Volume: 5829900,
        AdjClose: 9.735703
    },
    {
        Date: "2009-01-14",
        Open: 12.4,
        High: 12.43,
        Low: 11.93,
        Close: 12.22,
        Volume: 5381700,
        AdjClose: 9.457098
    },
    {
        Date: "2009-01-13",
        Open: 12.91,
        High: 13.19,
        Low: 12.88,
        Close: 13.09,
        Volume: 4008800,
        AdjClose: 10.130394
    },
    {
        Date: "2009-01-12",
        Open: 13.89,
        High: 13.91,
        Low: 13.36,
        Close: 13.48,
        Volume: 3128900,
        AdjClose: 10.432215
    },
    {
        Date: "2009-01-09",
        Open: 14.39,
        High: 14.48,
        Low: 14.08,
        Close: 14.11,
        Volume: 3396400,
        AdjClose: 10.919775
    },
    {
        Date: "2009-01-08",
        Open: 14.45,
        High: 14.72,
        Low: 14.29,
        Close: 14.72,
        Volume: 3076400,
        AdjClose: 11.391856
    },
    {
        Date: "2009-01-07",
        Open: 15.07,
        High: 15.08,
        Low: 14.57,
        Close: 14.71,
        Volume: 5235700,
        AdjClose: 11.384117
    },
    {
        Date: "2009-01-06",
        Open: 15.1,
        High: 15.43,
        Low: 14.97,
        Close: 15.24,
        Volume: 5292300,
        AdjClose: 11.794285
    },
    {
        Date: "2009-01-05",
        Open: 14.91,
        High: 15.17,
        Low: 14.8,
        Close: 14.94,
        Volume: 3685300,
        AdjClose: 11.562114
    },
    {
        Date: "2009-01-02",
        Open: 15.19,
        High: 15.35,
        Low: 14.89,
        Close: 15.25,
        Volume: 3310100,
        AdjClose: 11.802025
    },
    {
        Date: "2008-12-31",
        Open: 14.75,
        High: 15.14,
        Low: 14.74,
        Close: 15.01,
        Volume: 2778100,
        AdjClose: 11.616288
    },
    {
        Date: "2008-12-30",
        Open: 14.78,
        High: 14.94,
        Low: 14.55,
        Close: 14.92,
        Volume: 2683500,
        AdjClose: 11.546637
    },
    {
        Date: "2008-12-29",
        Open: 14.76,
        High: 14.82,
        Low: 14.37,
        Close: 14.45,
        Volume: 4018500,
        AdjClose: 11.182902
    },
    {
        Date: "2008-12-26",
        Open: 13.96,
        High: 14.05,
        Low: 13.75,
        Close: 14.01,
        Volume: 1659400,
        AdjClose: 10.842385
    },
    {
        Date: "2008-12-24",
        Open: 13.76,
        High: 13.87,
        Low: 13.6,
        Close: 13.8,
        Volume: 1354500,
        AdjClose: 10.679865
    },
    {
        Date: "2008-12-23",
        Open: 13.88,
        High: 13.93,
        Low: 13.44,
        Close: 13.59,
        Volume: 3403400,
        AdjClose: 10.517345
    },
    {
        Date: "2008-12-22",
        Open: 13.78,
        High: 14,
        Low: 13.26,
        Close: 13.5,
        Volume: 4682100,
        AdjClose: 10.447694
    },
    {
        Date: "2008-12-19",
        Open: 13.73,
        High: 14,
        Low: 13.56,
        Close: 13.78,
        Volume: 7928800,
        AdjClose: 10.664387
    },
    {
        Date: "2008-12-18",
        Open: 15.12,
        High: 15.12,
        Low: 14.3,
        Close: 14.44,
        Volume: 5482500,
        AdjClose: 11.175163
    },
    {
        Date: "2008-12-17",
        Open: 14.38,
        High: 15.38,
        Low: 14.38,
        Close: 15.21,
        Volume: 8152700,
        AdjClose: 11.771068
    },
    {
        Date: "2008-12-16",
        Open: 13.85,
        High: 14.88,
        Low: 13.81,
        Close: 14.8,
        Volume: 5325300,
        AdjClose: 11.453768
    },
    {
        Date: "2008-12-15",
        Open: 13.73,
        High: 13.83,
        Low: 13.38,
        Close: 13.59,
        Volume: 4211400,
        AdjClose: 10.517345
    },
    {
        Date: "2008-12-12",
        Open: 13.17,
        High: 13.94,
        Low: 13.08,
        Close: 13.8,
        Volume: 5912200,
        AdjClose: 10.679865
    },
    {
        Date: "2008-12-11",
        Open: 13.49,
        High: 13.98,
        Low: 13.19,
        Close: 13.32,
        Volume: 7506600,
        AdjClose: 10.308391
    },
    {
        Date: "2008-12-10",
        Open: 13.53,
        High: 13.8,
        Low: 13.38,
        Close: 13.67,
        Volume: 5503900,
        AdjClose: 10.579257
    },
    {
        Date: "2008-12-09",
        Open: 13.15,
        High: 13.69,
        Low: 12.98,
        Close: 13.31,
        Volume: 8190300,
        AdjClose: 10.300653
    },
    {
        Date: "2008-12-08",
        Open: 12.44,
        High: 13,
        Low: 12.41,
        Close: 12.93,
        Volume: 6488600,
        AdjClose: 10.006569
    },
    {
        Date: "2008-12-05",
        Open: 11.33,
        High: 11.98,
        Low: 11.07,
        Close: 11.94,
        Volume: 5794500,
        AdjClose: 9.240404
    },
    {
        Date: "2008-12-04",
        Open: 11.89,
        High: 12.22,
        Low: 11.52,
        Close: 11.74,
        Volume: 4971900,
        AdjClose: 9.085624
    },
    {
        Date: "2008-12-03",
        Open: 11.51,
        High: 12.17,
        Low: 11.34,
        Close: 12.14,
        Volume: 7322200,
        AdjClose: 9.395186
    },
    {
        Date: "2008-12-02",
        Open: 11.81,
        High: 12.33,
        Low: 11.62,
        Close: 12.06,
        Volume: 7087500,
        AdjClose: 9.333274
    },
    {
        Date: "2008-12-01",
        Open: 12.1,
        High: 12.12,
        Low: 11.53,
        Close: 11.53,
        Volume: 4948700,
        AdjClose: 8.923104
    },
    {
        Date: "2008-11-28",
        Open: 12.69,
        High: 12.98,
        Low: 12.56,
        Close: 12.93,
        Volume: 2450100,
        AdjClose: 10.006569
    },
    {
        Date: "2008-11-26",
        Open: 11.88,
        High: 12.82,
        Low: 11.83,
        Close: 12.74,
        Volume: 9936300,
        AdjClose: 9.859527
    },
    {
        Date: "2008-11-25",
        Open: 12.75,
        High: 12.78,
        Low: 11.87,
        Close: 12.4,
        Volume: 11542300,
        AdjClose: 9.5964
    },
    {
        Date: "2008-11-24",
        Open: 10.6,
        High: 11.4,
        Low: 10.46,
        Close: 11.12,
        Volume: 6373900,
        AdjClose: 8.605804
    },
    {
        Date: "2008-11-21",
        Open: 10.09,
        High: 10.09,
        Low: 9.28,
        Close: 9.95,
        Volume: 10008700,
        AdjClose: 7.700337
    },
    {
        Date: "2008-11-20",
        Open: 9.6,
        High: 10.12,
        Low: 9.11,
        Close: 9.12,
        Volume: 8879500,
        AdjClose: 7.057998
    },
    {
        Date: "2008-11-19",
        Open: 10.3,
        High: 10.47,
        Low: 9.69,
        Close: 9.7,
        Volume: 7853900,
        AdjClose: 7.506861
    },
    {
        Date: "2008-11-18",
        Open: 10.6,
        High: 10.81,
        Low: 10.2,
        Close: 10.55,
        Volume: 5260200,
        AdjClose: 8.164679
    },
    {
        Date: "2008-11-17",
        Open: 10.98,
        High: 11.25,
        Low: 10.6,
        Close: 10.83,
        Volume: 4244200,
        AdjClose: 8.381372
    },
    {
        Date: "2008-11-14",
        Open: 11.26,
        High: 11.57,
        Low: 10.81,
        Close: 10.87,
        Volume: 7659400,
        AdjClose: 8.412328
    },
    {
        Date: "2008-11-13",
        Open: 10.9,
        High: 11.64,
        Low: 10.08,
        Close: 11.57,
        Volume: 8794700,
        AdjClose: 8.95406
    },
    {
        Date: "2008-11-12",
        Open: 11,
        High: 11,
        Low: 10.24,
        Close: 10.28,
        Volume: 5324400,
        AdjClose: 7.955725
    },
    {
        Date: "2008-11-11",
        Open: 11.59,
        High: 11.76,
        Low: 11.21,
        Close: 11.41,
        Volume: 4800000,
        AdjClose: 8.830236
    },
    {
        Date: "2008-11-10",
        Open: 12.97,
        High: 13.01,
        Low: 11.88,
        Close: 12.08,
        Volume: 6323900,
        AdjClose: 9.348751
    },
    {
        Date: "2008-11-07",
        Open: 11.56,
        High: 11.81,
        Low: 11.4,
        Close: 11.72,
        Volume: 5611500,
        AdjClose: 9.070146
    },
    {
        Date: "2008-11-06",
        Open: 12.39,
        High: 12.5,
        Low: 11.14,
        Close: 11.22,
        Volume: 8503400,
        AdjClose: 8.683195
    },
    {
        Date: "2008-11-05",
        Open: 13.52,
        High: 13.81,
        Low: 12.91,
        Close: 12.97,
        Volume: 7179300,
        AdjClose: 10.037525
    },
    {
        Date: "2008-11-04",
        Open: 13.23,
        High: 14.16,
        Low: 13.22,
        Close: 13.96,
        Volume: 8461000,
        AdjClose: 10.803689
    },
    {
        Date: "2008-11-03",
        Open: 12.83,
        High: 13.09,
        Low: 12.78,
        Close: 12.98,
        Volume: 5087100,
        AdjClose: 10.045264
    },
    {
        Date: "2008-10-31",
        Open: 12.4,
        High: 13.42,
        Low: 12.3,
        Close: 13.15,
        Volume: 5663900,
        AdjClose: 10.176827
    },
    {
        Date: "2008-10-30",
        Open: 12.97,
        High: 13.05,
        Low: 12.07,
        Close: 12.73,
        Volume: 7811400,
        AdjClose: 9.851788
    },
    {
        Date: "2008-10-29",
        Open: 11.78,
        High: 12.59,
        Low: 11.64,
        Close: 12.11,
        Volume: 9828100,
        AdjClose: 9.371968
    },
    {
        Date: "2008-10-28",
        Open: 10.45,
        High: 11.3,
        Low: 10.12,
        Close: 11.3,
        Volume: 21502700,
        AdjClose: 8.745107
    },
    {
        Date: "2008-10-27",
        Open: 10.22,
        High: 10.67,
        Low: 10,
        Close: 10,
        Volume: 14731700,
        AdjClose: 7.739033
    },
    {
        Date: "2008-10-24",
        Open: 10.71,
        High: 11.21,
        Low: 10.64,
        Close: 11.01,
        Volume: 14916100,
        AdjClose: 8.520675
    },
    {
        Date: "2008-10-23",
        Open: 11.78,
        High: 11.92,
        Low: 10.94,
        Close: 11.5,
        Volume: 25875200,
        AdjClose: 8.899887
    },
    {
        Date: "2008-10-22",
        Open: 14.2,
        High: 14.61,
        Low: 13.53,
        Close: 13.96,
        Volume: 5072400,
        AdjClose: 10.803689
    },
    {
        Date: "2008-10-21",
        Open: 15.71,
        High: 16.129999,
        Low: 15.3,
        Close: 15.52,
        Volume: 6674300,
        AdjClose: 12.010979
    },
    {
        Date: "2008-10-20",
        Open: 15.37,
        High: 16.07,
        Low: 15.24,
        Close: 16.049999,
        Volume: 4558000,
        AdjClose: 12.421147
    },
    {
        Date: "2008-10-17",
        Open: 14.18,
        High: 15.78,
        Low: 14.05,
        Close: 14.95,
        Volume: 6469600,
        AdjClose: 11.569853
    },
    {
        Date: "2008-10-16",
        Open: 14.9,
        High: 15.15,
        Low: 13.81,
        Close: 15.02,
        Volume: 10169600,
        AdjClose: 11.624027
    },
    {
        Date: "2008-10-15",
        Open: 15.49,
        High: 15.56,
        Low: 14.31,
        Close: 14.31,
        Volume: 8497200,
        AdjClose: 11.074556
    },
    {
        Date: "2008-10-14",
        Open: 17.620001,
        High: 17.719999,
        Low: 16.34,
        Close: 16.73,
        Volume: 11598200,
        AdjClose: 12.947401
    },
    {
        Date: "2008-10-13",
        Open: 15.95,
        High: 17.42,
        Low: 15.75,
        Close: 16.889999,
        Volume: 8000100,
        AdjClose: 13.071225
    },
    {
        Date: "2008-10-10",
        Open: 14.07,
        High: 14.87,
        Low: 13.16,
        Close: 13.85,
        Volume: 14357100,
        AdjClose: 10.71856
    },
    {
        Date: "2008-10-09",
        Open: 16.059999,
        High: 16.120001,
        Low: 13.99,
        Close: 14.31,
        Volume: 9604900,
        AdjClose: 11.074556
    },
    {
        Date: "2008-10-08",
        Open: 15.19,
        High: 15.64,
        Low: 13.99,
        Close: 15.1,
        Volume: 9266700,
        AdjClose: 11.685939
    },
    {
        Date: "2008-10-07",
        Open: 16.459999,
        High: 16.6,
        Low: 14.72,
        Close: 14.88,
        Volume: 11489800,
        AdjClose: 11.51568
    },
    {
        Date: "2008-10-06",
        Open: 16.68,
        High: 16.74,
        Low: 15.43,
        Close: 16.549999,
        Volume: 10083100,
        AdjClose: 12.808098
    },
    {
        Date: "2008-10-03",
        Open: 17.52,
        High: 18.49,
        Low: 17.48,
        Close: 17.700001,
        Volume: 6066400,
        AdjClose: 13.698088
    },
    {
        Date: "2008-10-02",
        Open: 18.57,
        High: 18.59,
        Low: 17.690001,
        Close: 17.82,
        Volume: 6575900,
        AdjClose: 13.790956
    },
    {
        Date: "2008-10-01",
        Open: 18.77,
        High: 19.18,
        Low: 18.51,
        Close: 19,
        Volume: 4957800,
        AdjClose: 14.704162
    },
    {
        Date: "2008-09-30",
        Open: 18.9,
        High: 19.540001,
        Low: 18.620001,
        Close: 19.4,
        Volume: 7581100,
        AdjClose: 15.013723
    },
    {
        Date: "2008-09-29",
        Open: 19.07,
        High: 19.120001,
        Low: 17.57,
        Close: 17.969999,
        Volume: 8882900,
        AdjClose: 13.907041
    },
    {
        Date: "2008-09-26",
        Open: 20.030001,
        High: 20.219999,
        Low: 19.74,
        Close: 20.16,
        Volume: 4791800,
        AdjClose: 15.601889
    },
    {
        Date: "2008-09-25",
        Open: 20.709999,
        High: 20.809999,
        Low: 20.440001,
        Close: 20.629999,
        Volume: 7615100,
        AdjClose: 15.965623
    },
    {
        Date: "2008-09-24",
        Open: 21,
        High: 21.01,
        Low: 20,
        Close: 20.389999,
        Volume: 7090800,
        AdjClose: 15.779887
    },
    {
        Date: "2008-09-23",
        Open: 21.290001,
        High: 21.469999,
        Low: 20.620001,
        Close: 20.85,
        Volume: 6944600,
        AdjClose: 16.135883
    },
    {
        Date: "2008-09-22",
        Open: 21.690001,
        High: 21.959999,
        Low: 21.17,
        Close: 21.32,
        Volume: 5379400,
        AdjClose: 16.499617
    },
    {
        Date: "2008-09-19",
        Open: 21.450001,
        High: 22.040001,
        Low: 20.950001,
        Close: 21.969999,
        Volume: 10165900,
        AdjClose: 17.002654
    },
    {
        Date: "2008-09-18",
        Open: 20.059999,
        High: 20.41,
        Low: 19.16,
        Close: 20.200001,
        Volume: 11315400,
        AdjClose: 15.632846
    },
    {
        Date: "2008-09-17",
        Open: 20.360001,
        High: 20.4,
        Low: 19.32,
        Close: 19.629999,
        Volume: 9145400,
        AdjClose: 15.19172
    },
    {
        Date: "2008-09-16",
        Open: 20.5,
        High: 20.91,
        Low: 20.01,
        Close: 20.85,
        Volume: 8694900,
        AdjClose: 16.135883
    },
    {
        Date: "2008-09-15",
        Open: 20.35,
        High: 21.049999,
        Low: 20.32,
        Close: 20.67,
        Volume: 7192000,
        AdjClose: 15.99658
    },
    {
        Date: "2008-09-12",
        Open: 21.129999,
        High: 21.43,
        Low: 20.969999,
        Close: 21.33,
        Volume: 5219600,
        AdjClose: 16.507356
    },
    {
        Date: "2008-09-11",
        Open: 20.58,
        High: 21.01,
        Low: 20.440001,
        Close: 20.879999,
        Volume: 10577500,
        AdjClose: 16.159099
    },
    {
        Date: "2008-09-10",
        Open: 21.48,
        High: 21.5,
        Low: 20.940001,
        Close: 21.190001,
        Volume: 7183800,
        AdjClose: 16.39901
    },
    {
        Date: "2008-09-09",
        Open: 21.82,
        High: 21.83,
        Low: 20.68,
        Close: 20.700001,
        Volume: 11239900,
        AdjClose: 16.019798
    },
    {
        Date: "2008-09-08",
        Open: 22.950001,
        High: 23,
        Low: 21.85,
        Close: 22.219999,
        Volume: 9395500,
        AdjClose: 17.19613
    },
    {
        Date: "2008-09-05",
        Open: 23,
        High: 23.01,
        Low: 22.459999,
        Close: 22.690001,
        Volume: 6677000,
        AdjClose: 17.559865
    },
    {
        Date: "2008-09-04",
        Open: 24.01,
        High: 24.049999,
        Low: 22.799999,
        Close: 22.969999,
        Volume: 8380400,
        AdjClose: 17.776557
    },
    {
        Date: "2008-09-03",
        Open: 24.82,
        High: 24.889999,
        Low: 24.299999,
        Close: 24.700001,
        Volume: 8238400,
        AdjClose: 19.115411
    },
    {
        Date: "2008-09-02",
        Open: 25.25,
        High: 25.26,
        Low: 24.35,
        Close: 24.459999,
        Volume: 6257200,
        AdjClose: 18.929673
    },
    {
        Date: "2008-08-29",
        Open: 24.9,
        High: 24.950001,
        Low: 24.549999,
        Close: 24.57,
        Volume: 2162300,
        AdjClose: 19.014803
    },
    {
        Date: "2008-08-28",
        Open: 24.68,
        High: 24.83,
        Low: 24.25,
        Close: 24.51,
        Volume: 5189800,
        AdjClose: 18.968369
    },
    {
        Date: "2008-08-27",
        Open: 23.99,
        High: 24.1,
        Low: 23.709999,
        Close: 23.860001,
        Volume: 5831800,
        AdjClose: 18.465332
    },
    {
        Date: "2008-08-26",
        Open: 23.040001,
        High: 23.42,
        Low: 22.93,
        Close: 23.24,
        Volume: 3035100,
        AdjClose: 17.985511
    },
    {
        Date: "2008-08-25",
        Open: 23.34,
        High: 23.41,
        Low: 22.91,
        Close: 22.99,
        Volume: 2606500,
        AdjClose: 17.792036
    },
    {
        Date: "2008-08-22",
        Open: 23.5,
        High: 23.690001,
        Low: 23.389999,
        Close: 23.559999,
        Volume: 3699800,
        AdjClose: 18.23316
    },
    {
        Date: "2008-08-21",
        Open: 23.34,
        High: 23.610001,
        Low: 23.17,
        Close: 23.42,
        Volume: 7046800,
        AdjClose: 18.124814
    },
    {
        Date: "2008-08-20",
        Open: 23.450001,
        High: 23.48,
        Low: 22.99,
        Close: 23.389999,
        Volume: 6927900,
        AdjClose: 18.101597
    },
    {
        Date: "2008-08-19",
        Open: 23.440001,
        High: 23.610001,
        Low: 23.34,
        Close: 23.59,
        Volume: 2817900,
        AdjClose: 18.256378
    },
    {
        Date: "2008-08-18",
        Open: 24.16,
        High: 24.23,
        Low: 23.790001,
        Close: 23.83,
        Volume: 3035600,
        AdjClose: 18.442114
    },
    {
        Date: "2008-08-15",
        Open: 24.040001,
        High: 24.09,
        Low: 23.860001,
        Close: 24.049999,
        Volume: 2456600,
        AdjClose: 18.612373
    },
    {
        Date: "2008-08-14",
        Open: 23.83,
        High: 24.219999,
        Low: 23.780001,
        Close: 24.07,
        Volume: 3697000,
        AdjClose: 18.627851
    },
    {
        Date: "2008-08-13",
        Open: 24.23,
        High: 24.24,
        Low: 23.620001,
        Close: 24.02,
        Volume: 5736000,
        AdjClose: 18.589156
    },
    {
        Date: "2008-08-12",
        Open: 24.389999,
        High: 24.57,
        Low: 24.23,
        Close: 24.4,
        Volume: 4913400,
        AdjClose: 18.883239
    },
    {
        Date: "2008-08-11",
        Open: 25.17,
        High: 25.33,
        Low: 24.940001,
        Close: 25.07,
        Volume: 3428700,
        AdjClose: 19.401754
    },
    {
        Date: "2008-08-08",
        Open: 25.23,
        High: 25.66,
        Low: 25.16,
        Close: 25.549999,
        Volume: 2820800,
        AdjClose: 19.773227
    },
    {
        Date: "2008-08-07",
        Open: 26,
        High: 26.15,
        Low: 25.700001,
        Close: 25.76,
        Volume: 2190400,
        AdjClose: 19.935748
    },
    {
        Date: "2008-08-06",
        Open: 25.59,
        High: 25.99,
        Low: 25.440001,
        Close: 25.860001,
        Volume: 2814300,
        AdjClose: 20.013139
    },
    {
        Date: "2008-08-05",
        Open: 24.99,
        High: 25.57,
        Low: 24.99,
        Close: 25.5,
        Volume: 3612500,
        AdjClose: 19.734533
    },
    {
        Date: "2008-08-04",
        Open: 25.66,
        High: 25.719999,
        Low: 24.76,
        Close: 24.879999,
        Volume: 8091100,
        AdjClose: 19.254712
    },
    {
        Date: "2008-08-01",
        Open: 26.57,
        High: 26.57,
        Low: 25.540001,
        Close: 25.790001,
        Volume: 5267100,
        AdjClose: 19.958966
    },
    {
        Date: "2008-07-31",
        Open: 26.6,
        High: 26.75,
        Low: 26.190001,
        Close: 26.219999,
        Volume: 5087800,
        AdjClose: 20.291743
    },
    {
        Date: "2008-07-30",
        Open: 26.389999,
        High: 26.75,
        Low: 26.35,
        Close: 26.73,
        Volume: 4566200,
        AdjClose: 20.686434
    },
    {
        Date: "2008-07-29",
        Open: 26.09,
        High: 26.620001,
        Low: 26.07,
        Close: 26.59,
        Volume: 3752100,
        AdjClose: 20.578088
    },
    {
        Date: "2008-07-28",
        Open: 26.799999,
        High: 26.82,
        Low: 25.950001,
        Close: 26.059999,
        Volume: 4857600,
        AdjClose: 20.167918
    },
    {
        Date: "2008-07-25",
        Open: 26.280001,
        High: 26.41,
        Low: 25.82,
        Close: 26.01,
        Volume: 6181100,
        AdjClose: 20.129224
    },
    {
        Date: "2008-07-24",
        Open: 27.32,
        High: 27.4,
        Low: 26.549999,
        Close: 26.74,
        Volume: 10696500,
        AdjClose: 20.694173
    },
    {
        Date: "2008-07-23",
        Open: 28.6,
        High: 28.74,
        Low: 28.35,
        Close: 28.42,
        Volume: 4672800,
        AdjClose: 21.99433
    },
    {
        Date: "2008-07-22",
        Open: 28.059999,
        High: 28.74,
        Low: 27.93,
        Close: 28.690001,
        Volume: 5424300,
        AdjClose: 21.83955
    },
    {
        Date: "2008-07-21",
        Open: 28.450001,
        High: 28.57,
        Low: 28.190001,
        Close: 28.5,
        Volume: 2672300,
        AdjClose: 21.694917
    },
    {
        Date: "2008-07-18",
        Open: 28.26,
        High: 28.700001,
        Low: 28.1,
        Close: 28.27,
        Volume: 3960400,
        AdjClose: 21.519836
    },
    {
        Date: "2008-07-17",
        Open: 28.200001,
        High: 28.459999,
        Low: 27.92,
        Close: 28.16,
        Volume: 4534100,
        AdjClose: 21.436101
    },
    {
        Date: "2008-07-16",
        Open: 27.08,
        High: 27.959999,
        Low: 26.9,
        Close: 27.83,
        Volume: 5402900,
        AdjClose: 21.184896
    },
    {
        Date: "2008-07-15",
        Open: 26.719999,
        High: 27.190001,
        Low: 26.469999,
        Close: 26.809999,
        Volume: 4302000,
        AdjClose: 20.408446
    },
    {
        Date: "2008-07-14",
        Open: 27.299999,
        High: 27.42,
        Low: 26.92,
        Close: 27.209999,
        Volume: 5144000,
        AdjClose: 20.712936
    },
    {
        Date: "2008-07-11",
        Open: 26.15,
        High: 26.940001,
        Low: 26.07,
        Close: 26.620001,
        Volume: 6241800,
        AdjClose: 20.263815
    },
    {
        Date: "2008-07-10",
        Open: 26.68,
        High: 26.85,
        Low: 26.290001,
        Close: 26.799999,
        Volume: 6612700,
        AdjClose: 20.400834
    },
    {
        Date: "2008-07-09",
        Open: 27.49,
        High: 27.610001,
        Low: 26.879999,
        Close: 26.959999,
        Volume: 3140200,
        AdjClose: 20.52263
    },
    {
        Date: "2008-07-08",
        Open: 27.209999,
        High: 27.58,
        Low: 26.690001,
        Close: 27.5,
        Volume: 4880200,
        AdjClose: 20.933692
    },
    {
        Date: "2008-07-07",
        Open: 27.51,
        High: 27.9,
        Low: 26.99,
        Close: 27.370001,
        Volume: 4125200,
        AdjClose: 20.834733
    },
    {
        Date: "2008-07-03",
        Open: 27.370001,
        High: 27.6,
        Low: 26.98,
        Close: 27.370001,
        Volume: 3540600,
        AdjClose: 20.834733
    },
    {
        Date: "2008-07-02",
        Open: 28.01,
        High: 28.120001,
        Low: 26.73,
        Close: 26.92,
        Volume: 5432800,
        AdjClose: 20.492181
    },
    {
        Date: "2008-07-01",
        Open: 27.540001,
        High: 27.99,
        Low: 27.219999,
        Close: 27.969999,
        Volume: 5714400,
        AdjClose: 21.291467
    },
    {
        Date: "2008-06-30",
        Open: 28.360001,
        High: 28.58,
        Low: 28.209999,
        Close: 28.32,
        Volume: 3705600,
        AdjClose: 21.557896
    },
    {
        Date: "2008-06-27",
        Open: 28.42,
        High: 28.959999,
        Low: 28.290001,
        Close: 28.77,
        Volume: 4901700,
        AdjClose: 21.900448
    },
    {
        Date: "2008-06-26",
        Open: 28.41,
        High: 28.860001,
        Low: 28.139999,
        Close: 28.209999,
        Volume: 8304700,
        AdjClose: 21.474161
    },
    {
        Date: "2008-06-25",
        Open: 28.57,
        High: 29.889999,
        Low: 28.450001,
        Close: 29.65,
        Volume: 9778400,
        AdjClose: 22.570326
    },
    {
        Date: "2008-06-24",
        Open: 29.870001,
        High: 30.01,
        Low: 29.469999,
        Close: 29.84,
        Volume: 3474000,
        AdjClose: 22.714959
    },
    {
        Date: "2008-06-23",
        Open: 30.709999,
        High: 30.74,
        Low: 30.4,
        Close: 30.530001,
        Volume: 3603500,
        AdjClose: 23.240205
    },
    {
        Date: "2008-06-20",
        Open: 30.709999,
        High: 31,
        Low: 30.15,
        Close: 30.379999,
        Volume: 3622000,
        AdjClose: 23.12602
    },
    {
        Date: "2008-06-19",
        Open: 30.530001,
        High: 31.1,
        Low: 30.33,
        Close: 31.059999,
        Volume: 3994700,
        AdjClose: 23.643653
    },
    {
        Date: "2008-06-18",
        Open: 30.33,
        High: 30.610001,
        Low: 30.120001,
        Close: 30.52,
        Volume: 3311500,
        AdjClose: 23.232592
    },
    {
        Date: "2008-06-17",
        Open: 30.940001,
        High: 31.040001,
        Low: 30.73,
        Close: 30.799999,
        Volume: 3140400,
        AdjClose: 23.445734
    },
    {
        Date: "2008-06-16",
        Open: 30.030001,
        High: 30.76,
        Low: 29.940001,
        Close: 30.639999,
        Volume: 4973600,
        AdjClose: 23.323939
    },
    {
        Date: "2008-06-13",
        Open: 29.52,
        High: 30.01,
        Low: 29.52,
        Close: 29.940001,
        Volume: 5107400,
        AdjClose: 22.791082
    },
    {
        Date: "2008-06-12",
        Open: 29.65,
        High: 30.02,
        Low: 29.559999,
        Close: 29.67,
        Volume: 5186900,
        AdjClose: 22.585551
    },
    {
        Date: "2008-06-11",
        Open: 29.690001,
        High: 29.870001,
        Low: 28.969999,
        Close: 29.469999,
        Volume: 10649900,
        AdjClose: 22.433305
    },
    {
        Date: "2008-06-10",
        Open: 30.33,
        High: 30.809999,
        Low: 30.280001,
        Close: 30.4,
        Volume: 7506800,
        AdjClose: 23.141245
    },
    {
        Date: "2008-06-09",
        Open: 31.790001,
        High: 31.790001,
        Low: 31,
        Close: 31.370001,
        Volume: 4656300,
        AdjClose: 23.879634
    },
    {
        Date: "2008-06-06",
        Open: 32.139999,
        High: 32.369999,
        Low: 31.68,
        Close: 31.719999,
        Volume: 10209100,
        AdjClose: 24.146062
    },
    {
        Date: "2008-06-05",
        Open: 32.029999,
        High: 32.77,
        Low: 31.92,
        Close: 32.689999,
        Volume: 5257500,
        AdjClose: 24.88445
    },
    {
        Date: "2008-06-04",
        Open: 31.959999,
        High: 32.529999,
        Low: 31.9,
        Close: 32.18,
        Volume: 3427600,
        AdjClose: 24.496226
    },
    {
        Date: "2008-06-03",
        Open: 32.299999,
        High: 32.720001,
        Low: 32.200001,
        Close: 32.470001,
        Volume: 5791400,
        AdjClose: 24.716982
    },
    {
        Date: "2008-06-02",
        Open: 32.09,
        High: 32.259998,
        Low: 32.02,
        Close: 32.25,
        Volume: 2843800,
        AdjClose: 24.549512
    },
    {
        Date: "2008-05-30",
        Open: 32.490002,
        High: 32.66,
        Low: 32.360001,
        Close: 32.48,
        Volume: 2921300,
        AdjClose: 24.724593
    },
    {
        Date: "2008-05-29",
        Open: 32.369999,
        High: 32.66,
        Low: 32.18,
        Close: 32.529999,
        Volume: 4022500,
        AdjClose: 24.762654
    },
    {
        Date: "2008-05-28",
        Open: 32.509998,
        High: 32.82,
        Low: 32.380001,
        Close: 32.709999,
        Volume: 2883300,
        AdjClose: 24.899674
    },
    {
        Date: "2008-05-27",
        Open: 32.259998,
        High: 32.529999,
        Low: 32.119999,
        Close: 32.5,
        Volume: 2404500,
        AdjClose: 24.739818
    },
    {
        Date: "2008-05-23",
        Open: 32.990002,
        High: 33.009998,
        Low: 32.470001,
        Close: 32.759998,
        Volume: 3077300,
        AdjClose: 24.937735
    },
    {
        Date: "2008-05-22",
        Open: 32.52,
        High: 33.209999,
        Low: 32.52,
        Close: 32.950001,
        Volume: 6864600,
        AdjClose: 25.08237
    },
    {
        Date: "2008-05-21",
        Open: 32.439999,
        High: 32.830002,
        Low: 32.200001,
        Close: 32.23,
        Volume: 5433600,
        AdjClose: 24.534287
    },
    {
        Date: "2008-05-20",
        Open: 32.529999,
        High: 32.759998,
        Low: 32.279999,
        Close: 32.57,
        Volume: 4858700,
        AdjClose: 24.793103
    },
    {
        Date: "2008-05-19",
        Open: 33.310001,
        High: 33.389999,
        Low: 32.84,
        Close: 32.939999,
        Volume: 4291400,
        AdjClose: 25.074756
    },
    {
        Date: "2008-05-16",
        Open: 32.43,
        High: 32.84,
        Low: 32.25,
        Close: 32.689999,
        Volume: 3695300,
        AdjClose: 24.88445
    },
    {
        Date: "2008-05-15",
        Open: 32.200001,
        High: 32.5,
        Low: 32,
        Close: 32.439999,
        Volume: 3345300,
        AdjClose: 24.694143
    },
    {
        Date: "2008-05-14",
        Open: 32.32,
        High: 32.5,
        Low: 32.09,
        Close: 32.139999,
        Volume: 4476800,
        AdjClose: 24.465776
    },
    {
        Date: "2008-05-13",
        Open: 31.65,
        High: 32.169998,
        Low: 31.540001,
        Close: 32.060001,
        Volume: 3861100,
        AdjClose: 24.40488
    },
    {
        Date: "2008-05-12",
        Open: 31,
        High: 31.5,
        Low: 30.950001,
        Close: 31.440001,
        Volume: 2348800,
        AdjClose: 23.93292
    },
    {
        Date: "2008-05-09",
        Open: 30.879999,
        High: 31.24,
        Low: 30.860001,
        Close: 31.139999,
        Volume: 1969400,
        AdjClose: 23.704551
    },
    {
        Date: "2008-05-08",
        Open: 31.18,
        High: 31.4,
        Low: 30.969999,
        Close: 31.309999,
        Volume: 2423400,
        AdjClose: 23.833959
    },
    {
        Date: "2008-05-07",
        Open: 30.84,
        High: 31,
        Low: 30.459999,
        Close: 30.549999,
        Volume: 3929300,
        AdjClose: 23.255428
    },
    {
        Date: "2008-05-06",
        Open: 30.76,
        High: 30.870001,
        Low: 30.49,
        Close: 30.809999,
        Volume: 2931700,
        AdjClose: 23.453347
    },
    {
        Date: "2008-05-05",
        Open: 31.059999,
        High: 31.16,
        Low: 30.879999,
        Close: 31.02,
        Volume: 2714200,
        AdjClose: 23.613205
    },
    {
        Date: "2008-05-02",
        Open: 30.76,
        High: 30.879999,
        Low: 30.51,
        Close: 30.879999,
        Volume: 3827400,
        AdjClose: 23.506632
    },
    {
        Date: "2008-05-01",
        Open: 30.440001,
        High: 31.190001,
        Low: 30.200001,
        Close: 31.040001,
        Volume: 4777200,
        AdjClose: 23.62843
    },
    {
        Date: "2008-04-30",
        Open: 30.379999,
        High: 30.83,
        Low: 30.35,
        Close: 30.67,
        Volume: 4393800,
        AdjClose: 23.346776
    },
    {
        Date: "2008-04-29",
        Open: 30.18,
        High: 30.370001,
        Low: 30,
        Close: 30.25,
        Volume: 4582400,
        AdjClose: 23.027061
    },
    {
        Date: "2008-04-28",
        Open: 30.530001,
        High: 30.700001,
        Low: 30.459999,
        Close: 30.549999,
        Volume: 5210000,
        AdjClose: 23.255428
    },
    {
        Date: "2008-04-25",
        Open: 30.030001,
        High: 30.200001,
        Low: 29.85,
        Close: 30.07,
        Volume: 3342900,
        AdjClose: 22.89004
    },
    {
        Date: "2008-04-24",
        Open: 30.01,
        High: 30.6,
        Low: 29.540001,
        Close: 30.25,
        Volume: 9814000,
        AdjClose: 23.027061
    },
    {
        Date: "2008-04-23",
        Open: 28.290001,
        High: 28.959999,
        Low: 28.24,
        Close: 28.450001,
        Volume: 5087600,
        AdjClose: 21.656856
    },
    {
        Date: "2008-04-22",
        Open: 28.1,
        High: 28.200001,
        Low: 27.74,
        Close: 28.030001,
        Volume: 3004800,
        AdjClose: 21.337142
    },
    {
        Date: "2008-04-21",
        Open: 27.809999,
        High: 28.1,
        Low: 27.700001,
        Close: 28.02,
        Volume: 2369400,
        AdjClose: 21.329529
    },
    {
        Date: "2008-04-18",
        Open: 27.700001,
        High: 28.049999,
        Low: 27.629999,
        Close: 27.969999,
        Volume: 2523300,
        AdjClose: 21.291467
    },
    {
        Date: "2008-04-17",
        Open: 27.110001,
        High: 27.35,
        Low: 27.030001,
        Close: 27.200001,
        Volume: 1990300,
        AdjClose: 20.705325
    },
    {
        Date: "2008-04-16",
        Open: 27.200001,
        High: 27.879999,
        Low: 27.190001,
        Close: 27.809999,
        Volume: 3062100,
        AdjClose: 21.169671
    },
    {
        Date: "2008-04-15",
        Open: 26.84,
        High: 26.889999,
        Low: 26.5,
        Close: 26.82,
        Volume: 2591000,
        AdjClose: 20.416059
    },
    {
        Date: "2008-04-14",
        Open: 26.49,
        High: 26.58,
        Low: 26.24,
        Close: 26.379999,
        Volume: 4131200,
        AdjClose: 20.081119
    },
    {
        Date: "2008-04-11",
        Open: 26.620001,
        High: 26.99,
        Low: 26.32,
        Close: 26.58,
        Volume: 4256100,
        AdjClose: 20.233365
    },
    {
        Date: "2008-04-10",
        Open: 27,
        High: 27.23,
        Low: 26.700001,
        Close: 27,
        Volume: 7659000,
        AdjClose: 20.553079
    },
    {
        Date: "2008-04-09",
        Open: 27.5,
        High: 27.549999,
        Low: 27.059999,
        Close: 27.129999,
        Volume: 3503100,
        AdjClose: 20.652038
    },
    {
        Date: "2008-04-08",
        Open: 27.059999,
        High: 27.389999,
        Low: 26.92,
        Close: 27.25,
        Volume: 3523200,
        AdjClose: 20.743386
    },
    {
        Date: "2008-04-07",
        Open: 27.799999,
        High: 27.82,
        Low: 27.219999,
        Close: 27.389999,
        Volume: 3304600,
        AdjClose: 20.849957
    },
    {
        Date: "2008-04-04",
        Open: 27.379999,
        High: 27.6,
        Low: 27.280001,
        Close: 27.389999,
        Volume: 4162200,
        AdjClose: 20.849957
    },
    {
        Date: "2008-04-03",
        Open: 26.91,
        High: 27.27,
        Low: 26.690001,
        Close: 27.16,
        Volume: 3246200,
        AdjClose: 20.674875
    },
    {
        Date: "2008-04-02",
        Open: 26.889999,
        High: 27.190001,
        Low: 26.68,
        Close: 27.120001,
        Volume: 4933000,
        AdjClose: 20.644427
    },
    {
        Date: "2008-04-01",
        Open: 26.99,
        High: 27.540001,
        Low: 26.9,
        Close: 27.49,
        Volume: 4370400,
        AdjClose: 20.92608
    },
    {
        Date: "2008-03-31",
        Open: 26.68,
        High: 27.02,
        Low: 26.610001,
        Close: 26.92,
        Volume: 3234100,
        AdjClose: 20.492181
    },
    {
        Date: "2008-03-28",
        Open: 26.440001,
        High: 26.950001,
        Low: 26.309999,
        Close: 26.709999,
        Volume: 4384400,
        AdjClose: 20.332323
    },
    {
        Date: "2008-03-27",
        Open: 26.83,
        High: 26.870001,
        Low: 26.34,
        Close: 26.379999,
        Volume: 5299400,
        AdjClose: 20.081119
    },
    {
        Date: "2008-03-26",
        Open: 26.16,
        High: 26.629999,
        Low: 25.959999,
        Close: 26.4,
        Volume: 4431300,
        AdjClose: 20.096344
    },
    {
        Date: "2008-03-25",
        Open: 25.049999,
        High: 25.6,
        Low: 24.870001,
        Close: 25.43,
        Volume: 3985800,
        AdjClose: 19.357956
    },
    {
        Date: "2008-03-24",
        Open: 24.02,
        High: 25.459999,
        Low: 24.02,
        Close: 24.91,
        Volume: 4100200,
        AdjClose: 18.962119
    },
    {
        Date: "2008-03-20",
        Open: 23.99,
        High: 24.290001,
        Low: 23.700001,
        Close: 24.209999,
        Volume: 5354800,
        AdjClose: 18.429261
    },
    {
        Date: "2008-03-19",
        Open: 25.309999,
        High: 25.43,
        Low: 24.48,
        Close: 24.49,
        Volume: 3828100,
        AdjClose: 18.642404
    },
    {
        Date: "2008-03-18",
        Open: 25.040001,
        High: 25.41,
        Low: 24.75,
        Close: 25.34,
        Volume: 5172900,
        AdjClose: 19.289446
    },
    {
        Date: "2008-03-17",
        Open: 24.299999,
        High: 24.67,
        Low: 23.690001,
        Close: 24.26,
        Volume: 5971100,
        AdjClose: 18.467323
    },
    {
        Date: "2008-03-14",
        Open: 26.16,
        High: 26.24,
        Low: 25,
        Close: 25.48,
        Volume: 5003300,
        AdjClose: 19.396017
    },
    {
        Date: "2008-03-13",
        Open: 25.43,
        High: 26.299999,
        Low: 25.25,
        Close: 26.110001,
        Volume: 6096100,
        AdjClose: 19.875589
    },
    {
        Date: "2008-03-12",
        Open: 25.99,
        High: 26.200001,
        Low: 25.76,
        Close: 25.91,
        Volume: 3792000,
        AdjClose: 19.723344
    },
    {
        Date: "2008-03-11",
        Open: 25.41,
        High: 25.73,
        Low: 25.01,
        Close: 25.73,
        Volume: 5542400,
        AdjClose: 19.586323
    },
    {
        Date: "2008-03-10",
        Open: 25.190001,
        High: 25.23,
        Low: 24.51,
        Close: 24.629999,
        Volume: 6579600,
        AdjClose: 18.748975
    },
    {
        Date: "2008-03-07",
        Open: 25.25,
        High: 25.870001,
        Low: 25.07,
        Close: 25.41,
        Volume: 5413600,
        AdjClose: 19.342731
    },
    {
        Date: "2008-03-06",
        Open: 25.969999,
        High: 26,
        Low: 25.26,
        Close: 25.35,
        Volume: 6128700,
        AdjClose: 19.297058
    },
    {
        Date: "2008-03-05",
        Open: 25.459999,
        High: 25.73,
        Low: 25.16,
        Close: 25.48,
        Volume: 4247200,
        AdjClose: 19.396017
    },
    {
        Date: "2008-03-04",
        Open: 24.969999,
        High: 25.27,
        Low: 24.610001,
        Close: 25.219999,
        Volume: 4841500,
        AdjClose: 19.198098
    },
    {
        Date: "2008-03-03",
        Open: 25.040001,
        High: 25.25,
        Low: 24.73,
        Close: 25.120001,
        Volume: 5307300,
        AdjClose: 19.121977
    },
    {
        Date: "2008-02-29",
        Open: 25.34,
        High: 25.41,
        Low: 24.940001,
        Close: 25.040001,
        Volume: 4940000,
        AdjClose: 19.061079
    },
    {
        Date: "2008-02-28",
        Open: 25.809999,
        High: 26.07,
        Low: 25.709999,
        Close: 25.780001,
        Volume: 6103600,
        AdjClose: 19.624385
    },
    {
        Date: "2008-02-27",
        Open: 25.1,
        High: 26.209999,
        Low: 25.09,
        Close: 25.98,
        Volume: 9708300,
        AdjClose: 19.776629
    },
    {
        Date: "2008-02-26",
        Open: 24.6,
        High: 25.190001,
        Low: 24.41,
        Close: 24.98,
        Volume: 11741600,
        AdjClose: 19.015404
    },
    {
        Date: "2008-02-25",
        Open: 23.92,
        High: 24.200001,
        Low: 23.75,
        Close: 24.17,
        Volume: 8587800,
        AdjClose: 18.398812
    },
    {
        Date: "2008-02-22",
        Open: 24.139999,
        High: 24.139999,
        Low: 23.35,
        Close: 23.9,
        Volume: 7000500,
        AdjClose: 18.193281
    },
    {
        Date: "2008-02-21",
        Open: 24.08,
        High: 24.1,
        Low: 23.41,
        Close: 23.48,
        Volume: 9892400,
        AdjClose: 17.873567
    },
    {
        Date: "2008-02-20",
        Open: 23,
        High: 23.57,
        Low: 22.879999,
        Close: 23.49,
        Volume: 9239900,
        AdjClose: 17.881179
    },
    {
        Date: "2008-02-19",
        Open: 23.709999,
        High: 23.73,
        Low: 22.99,
        Close: 23.110001,
        Volume: 12010600,
        AdjClose: 17.591914
    },
    {
        Date: "2008-02-15",
        Open: 22.959999,
        High: 23.139999,
        Low: 22.459999,
        Close: 22.93,
        Volume: 13747700,
        AdjClose: 17.454893
    },
    {
        Date: "2008-02-14",
        Open: 24.040001,
        High: 24.040001,
        Low: 22.940001,
        Close: 23,
        Volume: 12573600,
        AdjClose: 17.508179
    },
    {
        Date: "2008-02-13",
        Open: 22.950001,
        High: 24.25,
        Low: 22.73,
        Close: 24.110001,
        Volume: 28694800,
        AdjClose: 18.353139
    },
    {
        Date: "2008-02-12",
        Open: 24.290001,
        High: 25.059999,
        Low: 24.23,
        Close: 24.58,
        Volume: 5480600,
        AdjClose: 18.710914
    },
    {
        Date: "2008-02-11",
        Open: 23.870001,
        High: 23.98,
        Low: 23.360001,
        Close: 23.83,
        Volume: 3588400,
        AdjClose: 18.139996
    },
    {
        Date: "2008-02-08",
        Open: 23.34,
        High: 23.6,
        Low: 23.110001,
        Close: 23.459999,
        Volume: 5782400,
        AdjClose: 17.858342
    },
    {
        Date: "2008-02-07",
        Open: 23.52,
        High: 24.209999,
        Low: 23.33,
        Close: 23.870001,
        Volume: 6121800,
        AdjClose: 18.170445
    },
    {
        Date: "2008-02-06",
        Open: 24.299999,
        High: 24.700001,
        Low: 24.01,
        Close: 24.1,
        Volume: 6142300,
        AdjClose: 18.345527
    },
    {
        Date: "2008-02-05",
        Open: 24.51,
        High: 24.65,
        Low: 23.790001,
        Close: 23.889999,
        Volume: 6893300,
        AdjClose: 18.185669
    },
    {
        Date: "2008-02-04",
        Open: 26.15,
        High: 26.15,
        Low: 25.530001,
        Close: 25.620001,
        Volume: 4928800,
        AdjClose: 19.502589
    },
    {
        Date: "2008-02-01",
        Open: 25.440001,
        High: 26.139999,
        Low: 25.25,
        Close: 26,
        Volume: 4290000,
        AdjClose: 19.791854
    },
    {
        Date: "2008-01-31",
        Open: 23.889999,
        High: 25.309999,
        Low: 23.84,
        Close: 25.02,
        Volume: 6532400,
        AdjClose: 19.045854
    },
    {
        Date: "2008-01-30",
        Open: 24.799999,
        High: 25.110001,
        Low: 24.190001,
        Close: 24.42,
        Volume: 10431200,
        AdjClose: 18.589119
    },
    {
        Date: "2008-01-29",
        Open: 25.120001,
        High: 25.450001,
        Low: 24.860001,
        Close: 25.32,
        Volume: 5077300,
        AdjClose: 19.274221
    },
    {
        Date: "2008-01-28",
        Open: 24.200001,
        High: 24.98,
        Low: 23.950001,
        Close: 24.889999,
        Volume: 4507600,
        AdjClose: 18.946894
    },
    {
        Date: "2008-01-25",
        Open: 25.200001,
        High: 25.34,
        Low: 24.139999,
        Close: 24.25,
        Volume: 8493400,
        AdjClose: 18.45971
    },
    {
        Date: "2008-01-24",
        Open: 24.200001,
        High: 24.76,
        Low: 23.809999,
        Close: 24.549999,
        Volume: 6325500,
        AdjClose: 18.688077
    },
    {
        Date: "2008-01-23",
        Open: 22.07,
        High: 23.959999,
        Low: 21.690001,
        Close: 23.91,
        Volume: 8007200,
        AdjClose: 18.200894
    },
    {
        Date: "2008-01-22",
        Open: 21.93,
        High: 23.33,
        Low: 21.889999,
        Close: 23.280001,
        Volume: 13295700,
        AdjClose: 17.721322
    },
    {
        Date: "2008-01-18",
        Open: 23.99,
        High: 24.200001,
        Low: 23.25,
        Close: 23.68,
        Volume: 7229400,
        AdjClose: 18.025812
    },
    {
        Date: "2008-01-17",
        Open: 24.4,
        High: 24.68,
        Low: 23.299999,
        Close: 23.5,
        Volume: 9079000,
        AdjClose: 17.888791
    },
    {
        Date: "2008-01-16",
        Open: 24.73,
        High: 25,
        Low: 23.66,
        Close: 23.860001,
        Volume: 9407500,
        AdjClose: 18.162833
    },
    {
        Date: "2008-01-15",
        Open: 25.610001,
        High: 25.610001,
        Low: 24.99,
        Close: 25.030001,
        Volume: 9740500,
        AdjClose: 19.053466
    },
    {
        Date: "2008-01-14",
        Open: 26.530001,
        High: 27.18,
        Low: 25.65,
        Close: 26.59,
        Volume: 4889900,
        AdjClose: 20.240977
    },
    {
        Date: "2008-01-11",
        Open: 25.9,
        High: 26.02,
        Low: 25.309999,
        Close: 25.469999,
        Volume: 10897300,
        AdjClose: 19.388404
    },
    {
        Date: "2008-01-10",
        Open: 25.4,
        High: 26.42,
        Low: 25.18,
        Close: 26.18,
        Volume: 7793900,
        AdjClose: 19.928875
    },
    {
        Date: "2008-01-09",
        Open: 25.889999,
        High: 26.18,
        Low: 25.34,
        Close: 26.08,
        Volume: 6256900,
        AdjClose: 19.852752
    },
    {
        Date: "2008-01-08",
        Open: 26.469999,
        High: 26.92,
        Low: 26.01,
        Close: 26.01,
        Volume: 5671500,
        AdjClose: 19.799467
    },
    {
        Date: "2008-01-07",
        Open: 26.290001,
        High: 26.559999,
        Low: 25.030001,
        Close: 25.75,
        Volume: 12192500,
        AdjClose: 19.601548
    },
    {
        Date: "2008-01-04",
        Open: 27.370001,
        High: 27.370001,
        Low: 26.68,
        Close: 26.700001,
        Volume: 6830400,
        AdjClose: 20.324712
    },
    {
        Date: "2008-01-03",
        Open: 27.92,
        High: 28.35,
        Low: 27.790001,
        Close: 28.08,
        Volume: 5240200,
        AdjClose: 21.375203
    },
    {
        Date: "2008-01-02",
        Open: 29,
        High: 29.110001,
        Low: 28.23,
        Close: 28.639999,
        Volume: 4697800,
        AdjClose: 21.801488
    },
    {
        Date: "2007-12-31",
        Open: 29.15,
        High: 29.23,
        Low: 28.799999,
        Close: 28.799999,
        Volume: 2462700,
        AdjClose: 21.923284
    },
    {
        Date: "2007-12-28",
        Open: 29.02,
        High: 29.18,
        Low: 28.85,
        Close: 29.129999,
        Volume: 3807400,
        AdjClose: 22.174488
    },
    {
        Date: "2007-12-27",
        Open: 28.57,
        High: 29.09,
        Low: 28.5,
        Close: 28.799999,
        Volume: 4849900,
        AdjClose: 21.923284
    },
    {
        Date: "2007-12-26",
        Open: 28.629999,
        High: 28.93,
        Low: 28.35,
        Close: 28.76,
        Volume: 2670200,
        AdjClose: 21.892836
    },
    {
        Date: "2007-12-24",
        Open: 28.299999,
        High: 28.469999,
        Low: 28.049999,
        Close: 28.4,
        Volume: 1109300,
        AdjClose: 21.618794
    },
    {
        Date: "2007-12-21",
        Open: 27.639999,
        High: 28.07,
        Low: 27.57,
        Close: 28,
        Volume: 4268900,
        AdjClose: 21.314305
    },
    {
        Date: "2007-12-20",
        Open: 27.1,
        High: 27.110001,
        Low: 26.73,
        Close: 27.01,
        Volume: 2669800,
        AdjClose: 20.560692
    },
    {
        Date: "2007-12-19",
        Open: 27.26,
        High: 27.35,
        Low: 26.799999,
        Close: 27.040001,
        Volume: 2603600,
        AdjClose: 20.583529
    },
    {
        Date: "2007-12-18",
        Open: 27.42,
        High: 27.57,
        Low: 26.67,
        Close: 27.23,
        Volume: 7205900,
        AdjClose: 20.728161
    },
    {
        Date: "2007-12-17",
        Open: 27.040001,
        High: 27.139999,
        Low: 26.51,
        Close: 26.73,
        Volume: 5920400,
        AdjClose: 20.347548
    },
    {
        Date: "2007-12-14",
        Open: 27.450001,
        High: 28.07,
        Low: 27.41,
        Close: 27.68,
        Volume: 3954100,
        AdjClose: 21.070713
    },
    {
        Date: "2007-12-13",
        Open: 28.379999,
        High: 28.440001,
        Low: 27.93,
        Close: 28.370001,
        Volume: 3193200,
        AdjClose: 21.595959
    },
    {
        Date: "2007-12-12",
        Open: 29.59,
        High: 29.690001,
        Low: 28.889999,
        Close: 29.280001,
        Volume: 3397200,
        AdjClose: 22.288673
    },
    {
        Date: "2007-12-11",
        Open: 29.48,
        High: 30.200001,
        Low: 28.969999,
        Close: 29.08,
        Volume: 5237400,
        AdjClose: 22.136428
    },
    {
        Date: "2007-12-10",
        Open: 29.459999,
        High: 29.940001,
        Low: 29.379999,
        Close: 29.77,
        Volume: 2472400,
        AdjClose: 22.661673
    },
    {
        Date: "2007-12-07",
        Open: 29.49,
        High: 29.610001,
        Low: 29.370001,
        Close: 29.5,
        Volume: 2513000,
        AdjClose: 22.456142
    },
    {
        Date: "2007-12-06",
        Open: 29.110001,
        High: 29.530001,
        Low: 29,
        Close: 29.52,
        Volume: 3906400,
        AdjClose: 22.471367
    },
    {
        Date: "2007-12-05",
        Open: 29,
        High: 29.5,
        Low: 28.98,
        Close: 29.24,
        Volume: 3783800,
        AdjClose: 22.258224
    },
    {
        Date: "2007-12-04",
        Open: 28.41,
        High: 28.950001,
        Low: 28.34,
        Close: 28.719999,
        Volume: 3903400,
        AdjClose: 21.862386
    },
    {
        Date: "2007-12-03",
        Open: 29.030001,
        High: 29.040001,
        Low: 28.67,
        Close: 28.85,
        Volume: 9871500,
        AdjClose: 21.961346
    },
    {
        Date: "2007-11-30",
        Open: 29.120001,
        High: 29.66,
        Low: 29.01,
        Close: 29.379999,
        Volume: 4190800,
        AdjClose: 22.364795
    },
    {
        Date: "2007-11-29",
        Open: 28.280001,
        High: 28.950001,
        Low: 28.16,
        Close: 28.67,
        Volume: 3935700,
        AdjClose: 21.824326
    },
    {
        Date: "2007-11-28",
        Open: 27.74,
        High: 28.84,
        Low: 27.74,
        Close: 28.709999,
        Volume: 3676500,
        AdjClose: 21.854774
    },
    {
        Date: "2007-11-27",
        Open: 26.75,
        High: 27.209999,
        Low: 26.51,
        Close: 27.200001,
        Volume: 4378900,
        AdjClose: 20.705325
    },
    {
        Date: "2007-11-26",
        Open: 27.879999,
        High: 28,
        Low: 27,
        Close: 27.07,
        Volume: 4241700,
        AdjClose: 20.606365
    },
    {
        Date: "2007-11-23",
        Open: 26.790001,
        High: 27.209999,
        Low: 26.719999,
        Close: 27.15,
        Volume: 1755100,
        AdjClose: 20.667263
    },
    {
        Date: "2007-11-21",
        Open: 26.889999,
        High: 27.15,
        Low: 26.5,
        Close: 26.84,
        Volume: 6512300,
        AdjClose: 20.431284
    },
    {
        Date: "2007-11-20",
        Open: 27.059999,
        High: 27.690001,
        Low: 26.219999,
        Close: 26.6,
        Volume: 11204400,
        AdjClose: 20.24859
    },
    {
        Date: "2007-11-19",
        Open: 26.92,
        High: 26.940001,
        Low: 25.84,
        Close: 26.059999,
        Volume: 7760900,
        AdjClose: 19.837527
    },
    {
        Date: "2007-11-16",
        Open: 27.700001,
        High: 27.969999,
        Low: 27.24,
        Close: 27.969999,
        Volume: 5005500,
        AdjClose: 21.291467
    },
    {
        Date: "2007-11-15",
        Open: 28.059999,
        High: 28.17,
        Low: 27.43,
        Close: 27.75,
        Volume: 3448100,
        AdjClose: 21.123998
    },
    {
        Date: "2007-11-14",
        Open: 29.219999,
        High: 29.379999,
        Low: 28.790001,
        Close: 28.790001,
        Volume: 5731400,
        AdjClose: 21.915673
    },
    {
        Date: "2007-11-13",
        Open: 27.940001,
        High: 29.040001,
        Low: 27.92,
        Close: 28.969999,
        Volume: 7262000,
        AdjClose: 22.052692
    },
    {
        Date: "2007-11-12",
        Open: 29.16,
        High: 29.370001,
        Low: 28.51,
        Close: 28.559999,
        Volume: 6980700,
        AdjClose: 21.74059
    },
    {
        Date: "2007-11-09",
        Open: 30.110001,
        High: 30.440001,
        Low: 29.709999,
        Close: 29.940001,
        Volume: 3906800,
        AdjClose: 22.791082
    },
    {
        Date: "2007-11-08",
        Open: 31.01,
        High: 31.129999,
        Low: 30,
        Close: 30.780001,
        Volume: 5528500,
        AdjClose: 23.430511
    },
    {
        Date: "2007-11-07",
        Open: 31.83,
        High: 32.080002,
        Low: 31.299999,
        Close: 31.32,
        Volume: 4343000,
        AdjClose: 23.841572
    },
    {
        Date: "2007-11-06",
        Open: 31.99,
        High: 32.040001,
        Low: 31.57,
        Close: 31.809999,
        Volume: 7646200,
        AdjClose: 24.214572
    },
    {
        Date: "2007-11-05",
        Open: 31,
        High: 31.17,
        Low: 30.58,
        Close: 30.92,
        Volume: 4048400,
        AdjClose: 23.537082
    },
    {
        Date: "2007-11-02",
        Open: 30.24,
        High: 30.77,
        Low: 30,
        Close: 30.77,
        Volume: 3993000,
        AdjClose: 23.422899
    },
    {
        Date: "2007-11-01",
        Open: 30.110001,
        High: 30.24,
        Low: 29.530001,
        Close: 29.780001,
        Volume: 4304500,
        AdjClose: 22.669286
    },
    {
        Date: "2007-10-31",
        Open: 29.879999,
        High: 30.33,
        Low: 29.74,
        Close: 30.219999,
        Volume: 2544900,
        AdjClose: 23.004224
    },
    {
        Date: "2007-10-30",
        Open: 29.99,
        High: 30.129999,
        Low: 29.870001,
        Close: 29.9,
        Volume: 3040700,
        AdjClose: 22.760632
    },
    {
        Date: "2007-10-29",
        Open: 30.059999,
        High: 30.18,
        Low: 29.91,
        Close: 30.049999,
        Volume: 3020300,
        AdjClose: 22.874816
    },
    {
        Date: "2007-10-26",
        Open: 29.940001,
        High: 29.969999,
        Low: 29.5,
        Close: 29.67,
        Volume: 3443200,
        AdjClose: 22.585551
    },
    {
        Date: "2007-10-25",
        Open: 28.76,
        High: 29.1,
        Low: 28.610001,
        Close: 29.040001,
        Volume: 13866500,
        AdjClose: 22.105979
    },
    {
        Date: "2007-10-24",
        Open: 27.799999,
        High: 28,
        Low: 27.309999,
        Close: 28,
        Volume: 3058600,
        AdjClose: 21.314305
    },
    {
        Date: "2007-10-23",
        Open: 27.790001,
        High: 27.99,
        Low: 27.610001,
        Close: 27.950001,
        Volume: 3138300,
        AdjClose: 21.276244
    },
    {
        Date: "2007-10-22",
        Open: 26.690001,
        High: 27.139999,
        Low: 26.639999,
        Close: 27.09,
        Volume: 3209500,
        AdjClose: 20.62159
    },
    {
        Date: "2007-10-19",
        Open: 27.950001,
        High: 27.99,
        Low: 27.09,
        Close: 27.139999,
        Volume: 3424300,
        AdjClose: 20.65965
    },
    {
        Date: "2007-10-18",
        Open: 28.02,
        High: 28.219999,
        Low: 27.93,
        Close: 28.16,
        Volume: 1946100,
        AdjClose: 21.436101
    },
    {
        Date: "2007-10-17",
        Open: 28.030001,
        High: 28.139999,
        Low: 27.639999,
        Close: 27.93,
        Volume: 2887400,
        AdjClose: 21.261019
    },
    {
        Date: "2007-10-16",
        Open: 27.469999,
        High: 27.709999,
        Low: 27.360001,
        Close: 27.440001,
        Volume: 2410700,
        AdjClose: 20.888019
    },
    {
        Date: "2007-10-15",
        Open: 28.16,
        High: 28.190001,
        Low: 27.85,
        Close: 27.98,
        Volume: 3015200,
        AdjClose: 21.29908
    },
    {
        Date: "2007-10-12",
        Open: 27.379999,
        High: 27.98,
        Low: 27.32,
        Close: 27.870001,
        Volume: 2631900,
        AdjClose: 21.215346
    },
    {
        Date: "2007-10-11",
        Open: 27.860001,
        High: 28.08,
        Low: 27.41,
        Close: 27.620001,
        Volume: 3640500,
        AdjClose: 21.02504
    },
    {
        Date: "2007-10-10",
        Open: 27.309999,
        High: 27.58,
        Low: 27.24,
        Close: 27.43,
        Volume: 2072700,
        AdjClose: 20.880406
    },
    {
        Date: "2007-10-09",
        Open: 26.950001,
        High: 27.379999,
        Low: 26.92,
        Close: 27.33,
        Volume: 2729700,
        AdjClose: 20.804284
    },
    {
        Date: "2007-10-08",
        Open: 26.950001,
        High: 26.959999,
        Low: 26.76,
        Close: 26.84,
        Volume: 1103200,
        AdjClose: 20.431284
    },
    {
        Date: "2007-10-05",
        Open: 26.809999,
        High: 27.110001,
        Low: 26.799999,
        Close: 26.969999,
        Volume: 2095800,
        AdjClose: 20.530242
    },
    {
        Date: "2007-10-04",
        Open: 26.700001,
        High: 26.940001,
        Low: 26.450001,
        Close: 26.889999,
        Volume: 3134900,
        AdjClose: 20.469344
    },
    {
        Date: "2007-10-03",
        Open: 27,
        High: 27.07,
        Low: 26.629999,
        Close: 26.73,
        Volume: 3616600,
        AdjClose: 20.347548
    },
    {
        Date: "2007-10-02",
        Open: 27.030001,
        High: 27.139999,
        Low: 26.719999,
        Close: 27.059999,
        Volume: 2595700,
        AdjClose: 20.598753
    },
    {
        Date: "2007-10-01",
        Open: 26.620001,
        High: 27.48,
        Low: 26.620001,
        Close: 27.15,
        Volume: 4858200,
        AdjClose: 20.667263
    },
    {
        Date: "2007-09-28",
        Open: 26.110001,
        High: 26.48,
        Low: 25.98,
        Close: 26.23,
        Volume: 6535800,
        AdjClose: 19.966936
    },
    {
        Date: "2007-09-27",
        Open: 25.940001,
        High: 26,
        Low: 25.809999,
        Close: 25.91,
        Volume: 4212900,
        AdjClose: 19.723344
    },
    {
        Date: "2007-09-26",
        Open: 25.85,
        High: 25.969999,
        Low: 25.75,
        Close: 25.940001,
        Volume: 1559200,
        AdjClose: 19.746181
    },
    {
        Date: "2007-09-25",
        Open: 25.540001,
        High: 25.84,
        Low: 25.459999,
        Close: 25.84,
        Volume: 1649600,
        AdjClose: 19.670058
    },
    {
        Date: "2007-09-24",
        Open: 25.75,
        High: 25.99,
        Low: 25.709999,
        Close: 25.84,
        Volume: 2017300,
        AdjClose: 19.670058
    },
    {
        Date: "2007-09-21",
        Open: 25.360001,
        High: 25.65,
        Low: 25.299999,
        Close: 25.58,
        Volume: 1868100,
        AdjClose: 19.47214
    },
    {
        Date: "2007-09-20",
        Open: 24.98,
        High: 25.280001,
        Low: 24.940001,
        Close: 25.08,
        Volume: 1869800,
        AdjClose: 19.091527
    },
    {
        Date: "2007-09-19",
        Open: 24.860001,
        High: 24.92,
        Low: 24.540001,
        Close: 24.690001,
        Volume: 2767700,
        AdjClose: 18.79465
    },
    {
        Date: "2007-09-18",
        Open: 24.059999,
        High: 24.92,
        Low: 23.91,
        Close: 24.799999,
        Volume: 2180300,
        AdjClose: 18.878383
    },
    {
        Date: "2007-09-17",
        Open: 23.59,
        High: 23.67,
        Low: 23.35,
        Close: 23.49,
        Volume: 1118200,
        AdjClose: 17.881179
    },
    {
        Date: "2007-09-14",
        Open: 23.610001,
        High: 23.860001,
        Low: 23.530001,
        Close: 23.799999,
        Volume: 971000,
        AdjClose: 18.117158
    },
    {
        Date: "2007-09-13",
        Open: 23.809999,
        High: 24.17,
        Low: 23.690001,
        Close: 24.040001,
        Volume: 1891500,
        AdjClose: 18.299854
    },
    {
        Date: "2007-09-12",
        Open: 23.5,
        High: 23.82,
        Low: 23.469999,
        Close: 23.51,
        Volume: 4181900,
        AdjClose: 17.896404
    },
    {
        Date: "2007-09-11",
        Open: 23.129999,
        High: 23.459999,
        Low: 23.09,
        Close: 23.360001,
        Volume: 2403600,
        AdjClose: 17.78222
    },
    {
        Date: "2007-09-10",
        Open: 23.16,
        High: 23.16,
        Low: 22.57,
        Close: 22.950001,
        Volume: 2278100,
        AdjClose: 17.470118
    },
    {
        Date: "2007-09-07",
        Open: 23.35,
        High: 23.4,
        Low: 23.040001,
        Close: 23.18,
        Volume: 4761800,
        AdjClose: 17.6452
    },
    {
        Date: "2007-09-06",
        Open: 23.85,
        High: 24.15,
        Low: 23.73,
        Close: 23.98,
        Volume: 1925900,
        AdjClose: 18.254179
    },
    {
        Date: "2007-09-05",
        Open: 23.870001,
        High: 24.030001,
        Low: 23.73,
        Close: 23.93,
        Volume: 3518200,
        AdjClose: 18.216118
    },
    {
        Date: "2007-09-04",
        Open: 24.17,
        High: 24.85,
        Low: 24.110001,
        Close: 24.73,
        Volume: 3634100,
        AdjClose: 18.825098
    },
    {
        Date: "2007-08-31",
        Open: 24.6,
        High: 24.879999,
        Low: 24.370001,
        Close: 24.66,
        Volume: 1695900,
        AdjClose: 18.771812
    },
    {
        Date: "2007-08-30",
        Open: 23.6,
        High: 24.27,
        Low: 23.58,
        Close: 24.049999,
        Volume: 1950100,
        AdjClose: 18.307465
    },
    {
        Date: "2007-08-29",
        Open: 23.74,
        High: 24.25,
        Low: 23.65,
        Close: 24.200001,
        Volume: 2106700,
        AdjClose: 18.42165
    },
    {
        Date: "2007-08-28",
        Open: 23.99,
        High: 24.030001,
        Low: 23.219999,
        Close: 23.280001,
        Volume: 2169100,
        AdjClose: 17.721322
    },
    {
        Date: "2007-08-27",
        Open: 23.85,
        High: 24.09,
        Low: 23.719999,
        Close: 23.85,
        Volume: 1860900,
        AdjClose: 18.15522
    },
    {
        Date: "2007-08-24",
        Open: 23.32,
        High: 23.879999,
        Low: 23.280001,
        Close: 23.84,
        Volume: 1571300,
        AdjClose: 18.147608
    },
    {
        Date: "2007-08-23",
        Open: 23.209999,
        High: 23.209999,
        Low: 22.92,
        Close: 23.049999,
        Volume: 1818700,
        AdjClose: 17.546239
    },
    {
        Date: "2007-08-22",
        Open: 22.82,
        High: 23.16,
        Low: 22.82,
        Close: 23.1,
        Volume: 2323500,
        AdjClose: 17.584302
    },
    {
        Date: "2007-08-21",
        Open: 22.219999,
        High: 22.639999,
        Low: 22.16,
        Close: 22.280001,
        Volume: 2094600,
        AdjClose: 16.960097
    },
    {
        Date: "2007-08-20",
        Open: 22.15,
        High: 22.5,
        Low: 22.01,
        Close: 22.389999,
        Volume: 3490500,
        AdjClose: 17.043831
    },
    {
        Date: "2007-08-17",
        Open: 21.950001,
        High: 22.290001,
        Low: 21.26,
        Close: 21.99,
        Volume: 5184400,
        AdjClose: 16.739341
    },
    {
        Date: "2007-08-16",
        Open: 21.27,
        High: 21.42,
        Low: 20.42,
        Close: 21.280001,
        Volume: 6309600,
        AdjClose: 16.198872
    },
    {
        Date: "2007-08-15",
        Open: 22.09,
        High: 22.52,
        Low: 21.76,
        Close: 21.84,
        Volume: 3833200,
        AdjClose: 16.625158
    },
    {
        Date: "2007-08-14",
        Open: 23.15,
        High: 23.18,
        Low: 22.290001,
        Close: 22.360001,
        Volume: 2514300,
        AdjClose: 17.020995
    },
    {
        Date: "2007-08-13",
        Open: 22.959999,
        High: 23.02,
        Low: 22.59,
        Close: 22.620001,
        Volume: 2405600,
        AdjClose: 17.218914
    },
    {
        Date: "2007-08-10",
        Open: 22.26,
        High: 23.379999,
        Low: 21.780001,
        Close: 22.5,
        Volume: 6331500,
        AdjClose: 17.127566
    },
    {
        Date: "2007-08-09",
        Open: 23.120001,
        High: 23.49,
        Low: 23.030001,
        Close: 23.110001,
        Volume: 3246400,
        AdjClose: 17.591914
    },
    {
        Date: "2007-08-08",
        Open: 24,
        High: 24.32,
        Low: 23.780001,
        Close: 24.08,
        Volume: 3075900,
        AdjClose: 18.330302
    },
    {
        Date: "2007-08-07",
        Open: 23.18,
        High: 23.610001,
        Low: 23.040001,
        Close: 23.4,
        Volume: 3325900,
        AdjClose: 17.812669
    },
    {
        Date: "2007-08-06",
        Open: 23.5,
        High: 23.58,
        Low: 22.950001,
        Close: 23.58,
        Volume: 3775300,
        AdjClose: 17.949689
    },
    {
        Date: "2007-08-03",
        Open: 23.82,
        High: 23.879999,
        Low: 23.23,
        Close: 23.32,
        Volume: 3185700,
        AdjClose: 17.751771
    },
    {
        Date: "2007-08-02",
        Open: 23.860001,
        High: 24.190001,
        Low: 23.620001,
        Close: 23.969999,
        Volume: 2805000,
        AdjClose: 18.246567
    },
    {
        Date: "2007-08-01",
        Open: 23.93,
        High: 24.08,
        Low: 23.290001,
        Close: 23.799999,
        Volume: 5474500,
        AdjClose: 18.117158
    },
    {
        Date: "2007-07-31",
        Open: 24.5,
        High: 24.690001,
        Low: 24.01,
        Close: 24.07,
        Volume: 6910100,
        AdjClose: 18.322689
    },
    {
        Date: "2007-07-30",
        Open: 23.74,
        High: 24.18,
        Low: 23.66,
        Close: 24.1,
        Volume: 4154500,
        AdjClose: 18.345527
    },
    {
        Date: "2007-07-27",
        Open: 23.26,
        High: 23.33,
        Low: 22.68,
        Close: 22.719999,
        Volume: 5732000,
        AdjClose: 17.295035
    },
    {
        Date: "2007-07-26",
        Open: 22.92,
        High: 23.309999,
        Low: 22.370001,
        Close: 22.67,
        Volume: 5473500,
        AdjClose: 17.256975
    },
    {
        Date: "2007-07-25",
        Open: 23.33,
        High: 23.42,
        Low: 22.6,
        Close: 23.26,
        Volume: 5684000,
        AdjClose: 17.706097
    },
    {
        Date: "2007-07-24",
        Open: 23.9,
        High: 24.01,
        Low: 23.450001,
        Close: 23.540001,
        Volume: 2904500,
        AdjClose: 17.919241
    },
    {
        Date: "2007-07-23",
        Open: 24.26,
        High: 24.290001,
        Low: 24.08,
        Close: 24.16,
        Volume: 2157900,
        AdjClose: 18.3912
    },
    {
        Date: "2007-07-20",
        Open: 24.27,
        High: 24.34,
        Low: 23.9,
        Close: 24.09,
        Volume: 3010900,
        AdjClose: 18.337914
    },
    {
        Date: "2007-07-19",
        Open: 24.59,
        High: 24.73,
        Low: 24.440001,
        Close: 24.59,
        Volume: 3166100,
        AdjClose: 18.718527
    },
    {
        Date: "2007-07-18",
        Open: 24.25,
        High: 24.360001,
        Low: 23.860001,
        Close: 24.08,
        Volume: 4230700,
        AdjClose: 18.330302
    },
    {
        Date: "2007-07-17",
        Open: 24.540001,
        High: 24.700001,
        Low: 24.34,
        Close: 24.389999,
        Volume: 1907300,
        AdjClose: 18.566281
    },
    {
        Date: "2007-07-16",
        Open: 24.860001,
        High: 24.93,
        Low: 24.690001,
        Close: 24.75,
        Volume: 1936400,
        AdjClose: 18.840323
    },
    {
        Date: "2007-07-13",
        Open: 24.870001,
        High: 24.9,
        Low: 24.6,
        Close: 24.82,
        Volume: 3085000,
        AdjClose: 18.893608
    },
    {
        Date: "2007-07-12",
        Open: 24.51,
        High: 25.040001,
        Low: 24.51,
        Close: 25.02,
        Volume: 3119900,
        AdjClose: 19.045854
    },
    {
        Date: "2007-07-11",
        Open: 23.85,
        High: 24.4,
        Low: 23.85,
        Close: 24.309999,
        Volume: 4210100,
        AdjClose: 18.505383
    },
    {
        Date: "2007-07-10",
        Open: 23.93,
        High: 24.049999,
        Low: 23.65,
        Close: 23.709999,
        Volume: 2190900,
        AdjClose: 18.048648
    },
    {
        Date: "2007-07-09",
        Open: 23.969999,
        High: 24.110001,
        Low: 23.75,
        Close: 24,
        Volume: 1205500,
        AdjClose: 18.269404
    },
    {
        Date: "2007-07-06",
        Open: 23.620001,
        High: 23.82,
        Low: 23.530001,
        Close: 23.790001,
        Volume: 1093400,
        AdjClose: 18.109547
    },
    {
        Date: "2007-07-05",
        Open: 23.700001,
        High: 23.780001,
        Low: 23.65,
        Close: 23.76,
        Volume: 2867500,
        AdjClose: 18.08671
    },
    {
        Date: "2007-07-03",
        Open: 23.540001,
        High: 23.700001,
        Low: 23.469999,
        Close: 23.65,
        Volume: 1660800,
        AdjClose: 18.002975
    },
    {
        Date: "2007-07-02",
        Open: 23.059999,
        High: 23.4,
        Low: 23.040001,
        Close: 23.379999,
        Volume: 3544700,
        AdjClose: 17.797444
    },
    {
        Date: "2007-06-29",
        Open: 22.559999,
        High: 22.790001,
        Low: 22.43,
        Close: 22.6,
        Volume: 1536600,
        AdjClose: 17.203689
    },
    {
        Date: "2007-06-28",
        Open: 22.33,
        High: 22.59,
        Low: 22.32,
        Close: 22.459999,
        Volume: 2112500,
        AdjClose: 17.097116
    },
    {
        Date: "2007-06-27",
        Open: 21.82,
        High: 22.209999,
        Low: 21.74,
        Close: 22.200001,
        Volume: 3121800,
        AdjClose: 16.899199
    },
    {
        Date: "2007-06-26",
        Open: 22.200001,
        High: 22.440001,
        Low: 22.030001,
        Close: 22.120001,
        Volume: 2191600,
        AdjClose: 16.838301
    },
    {
        Date: "2007-06-25",
        Open: 22.309999,
        High: 22.5,
        Low: 22.08,
        Close: 22.110001,
        Volume: 1633900,
        AdjClose: 16.830689
    },
    {
        Date: "2007-06-22",
        Open: 22.559999,
        High: 22.6,
        Low: 22.25,
        Close: 22.389999,
        Volume: 1382500,
        AdjClose: 17.043831
    },
    {
        Date: "2007-06-21",
        Open: 22.290001,
        High: 22.42,
        Low: 22.059999,
        Close: 22.4,
        Volume: 2662300,
        AdjClose: 17.051443
    },
    {
        Date: "2007-06-20",
        Open: 22.67,
        High: 22.700001,
        Low: 22.15,
        Close: 22.190001,
        Volume: 1700600,
        AdjClose: 16.891587
    },
    {
        Date: "2007-06-19",
        Open: 22.01,
        High: 22.18,
        Low: 21.9,
        Close: 22.15,
        Volume: 2966800,
        AdjClose: 16.861137
    },
    {
        Date: "2007-06-18",
        Open: 22.360001,
        High: 22.41,
        Low: 22.02,
        Close: 22.139999,
        Volume: 1469100,
        AdjClose: 16.853525
    },
    {
        Date: "2007-06-15",
        Open: 22.34,
        High: 22.389999,
        Low: 22.16,
        Close: 22.25,
        Volume: 2467500,
        AdjClose: 16.93726
    },
    {
        Date: "2007-06-14",
        Open: 21.719999,
        High: 22.35,
        Low: 21.700001,
        Close: 22.110001,
        Volume: 3243100,
        AdjClose: 16.830689
    },
    {
        Date: "2007-06-13",
        Open: 21.26,
        High: 21.74,
        Low: 21.200001,
        Close: 21.67,
        Volume: 3415600,
        AdjClose: 16.495749
    },
    {
        Date: "2007-06-12",
        Open: 21.48,
        High: 21.6,
        Low: 21.16,
        Close: 21.24,
        Volume: 2176900,
        AdjClose: 16.168422
    },
    {
        Date: "2007-06-11",
        Open: 21.59,
        High: 21.82,
        Low: 21.440001,
        Close: 21.65,
        Volume: 1675700,
        AdjClose: 16.480525
    },
    {
        Date: "2007-06-08",
        Open: 21.030001,
        High: 21.24,
        Low: 20.879999,
        Close: 21.24,
        Volume: 2312400,
        AdjClose: 16.168422
    },
    {
        Date: "2007-06-07",
        Open: 21.24,
        High: 21.389999,
        Low: 20.780001,
        Close: 20.9,
        Volume: 2088900,
        AdjClose: 15.909606
    },
    {
        Date: "2007-06-06",
        Open: 21.67,
        High: 21.68,
        Low: 21.17,
        Close: 21.309999,
        Volume: 1911300,
        AdjClose: 16.221708
    },
    {
        Date: "2007-06-05",
        Open: 22.15,
        High: 22.17,
        Low: 21.85,
        Close: 21.93,
        Volume: 1418600,
        AdjClose: 16.693668
    },
    {
        Date: "2007-06-04",
        Open: 21.83,
        High: 22,
        Low: 21.82,
        Close: 22,
        Volume: 1202100,
        AdjClose: 16.746954
    },
    {
        Date: "2007-06-01",
        Open: 21.719999,
        High: 21.82,
        Low: 21.629999,
        Close: 21.74,
        Volume: 1358100,
        AdjClose: 16.549035
    },
    {
        Date: "2007-05-31",
        Open: 21.379999,
        High: 21.549999,
        Low: 21.35,
        Close: 21.469999,
        Volume: 2460300,
        AdjClose: 16.343504
    },
    {
        Date: "2007-05-30",
        Open: 20.84,
        High: 21.26,
        Low: 20.799999,
        Close: 21.23,
        Volume: 2670500,
        AdjClose: 16.16081
    },
    {
        Date: "2007-05-29",
        Open: 21.200001,
        High: 21.280001,
        Low: 21.08,
        Close: 21.190001,
        Volume: 1387900,
        AdjClose: 16.130362
    },
    {
        Date: "2007-05-25",
        Open: 20.950001,
        High: 21.040001,
        Low: 20.91,
        Close: 21.02,
        Volume: 1874200,
        AdjClose: 16.000953
    },
    {
        Date: "2007-05-24",
        Open: 21.18,
        High: 21.26,
        Low: 20.76,
        Close: 20.83,
        Volume: 2999100,
        AdjClose: 15.85632
    },
    {
        Date: "2007-05-23",
        Open: 21.120001,
        High: 21.379999,
        Low: 21.120001,
        Close: 21.24,
        Volume: 3765300,
        AdjClose: 16.168422
    },
    {
        Date: "2007-05-22",
        Open: 20.59,
        High: 20.9,
        Low: 20.549999,
        Close: 20.809999,
        Volume: 2135700,
        AdjClose: 15.841095
    },
    {
        Date: "2007-05-21",
        Open: 20.35,
        High: 20.4,
        Low: 20.309999,
        Close: 20.379999,
        Volume: 2721300,
        AdjClose: 15.513768
    },
    {
        Date: "2007-05-18",
        Open: 20.290001,
        High: 20.459999,
        Low: 20.290001,
        Close: 20.41,
        Volume: 669900,
        AdjClose: 15.536605
    },
    {
        Date: "2007-05-17",
        Open: 20.26,
        High: 20.26,
        Low: 20.049999,
        Close: 20.1,
        Volume: 682900,
        AdjClose: 15.300626
    },
    {
        Date: "2007-05-16",
        Open: 20.190001,
        High: 20.26,
        Low: 20.02,
        Close: 20.25,
        Volume: 1884800,
        AdjClose: 15.41481
    },
    {
        Date: "2007-05-15",
        Open: 19.99,
        High: 20.23,
        Low: 19.940001,
        Close: 20,
        Volume: 1796900,
        AdjClose: 15.224503
    },
    {
        Date: "2007-05-14",
        Open: 20.01,
        High: 20.040001,
        Low: 19.84,
        Close: 19.969999,
        Volume: 861600,
        AdjClose: 15.201666
    },
    {
        Date: "2007-05-11",
        Open: 19.58,
        High: 19.889999,
        Low: 19.57,
        Close: 19.889999,
        Volume: 1041900,
        AdjClose: 15.140768
    },
    {
        Date: "2007-05-10",
        Open: 19.85,
        High: 19.959999,
        Low: 19.459999,
        Close: 19.559999,
        Volume: 2604500,
        AdjClose: 14.889564
    },
    {
        Date: "2007-05-09",
        Open: 19.709999,
        High: 19.83,
        Low: 19.700001,
        Close: 19.83,
        Volume: 1550500,
        AdjClose: 15.095095
    },
    {
        Date: "2007-05-08",
        Open: 19.719999,
        High: 19.74,
        Low: 19.540001,
        Close: 19.73,
        Volume: 2165200,
        AdjClose: 15.018972
    },
    {
        Date: "2007-05-07",
        Open: 20.1,
        High: 20.219999,
        Low: 20.040001,
        Close: 20.17,
        Volume: 1188400,
        AdjClose: 15.353912
    },
    {
        Date: "2007-05-04",
        Open: 19.91,
        High: 20.07,
        Low: 19.809999,
        Close: 20.01,
        Volume: 1232400,
        AdjClose: 15.232116
    },
    {
        Date: "2007-05-03",
        Open: 19.99,
        High: 20.07,
        Low: 19.85,
        Close: 20.040001,
        Volume: 2488700,
        AdjClose: 15.254953
    },
    {
        Date: "2007-05-02",
        Open: 20.200001,
        High: 20.34,
        Low: 20.15,
        Close: 20.33,
        Volume: 1691100,
        AdjClose: 15.324985
    },
    {
        Date: "2007-05-01",
        Open: 20.030001,
        High: 20.200001,
        Low: 19.85,
        Close: 20,
        Volume: 3293000,
        AdjClose: 15.076227
    },
    {
        Date: "2007-04-30",
        Open: 20.280001,
        High: 20.450001,
        Low: 19.940001,
        Close: 19.959999,
        Volume: 2127000,
        AdjClose: 15.046074
    },
    {
        Date: "2007-04-27",
        Open: 20.34,
        High: 20.379999,
        Low: 20.16,
        Close: 20.290001,
        Volume: 3201000,
        AdjClose: 15.294833
    },
    {
        Date: "2007-04-26",
        Open: 20.18,
        High: 20.35,
        Low: 20.01,
        Close: 20.299999,
        Volume: 8830200,
        AdjClose: 15.30237
    },
    {
        Date: "2007-04-25",
        Open: 19.190001,
        High: 19.540001,
        Low: 19.17,
        Close: 19.469999,
        Volume: 3782800,
        AdjClose: 14.676707
    },
    {
        Date: "2007-04-24",
        Open: 18.85,
        High: 18.85,
        Low: 18.66,
        Close: 18.75,
        Volume: 2982400,
        AdjClose: 14.133963
    },
    {
        Date: "2007-04-23",
        Open: 18.879999,
        High: 19.07,
        Low: 18.860001,
        Close: 18.9,
        Volume: 2488000,
        AdjClose: 14.247034
    },
    {
        Date: "2007-04-20",
        Open: 18.780001,
        High: 18.83,
        Low: 18.57,
        Close: 18.74,
        Volume: 1780600,
        AdjClose: 14.126425
    },
    {
        Date: "2007-04-19",
        Open: 18.219999,
        High: 18.6,
        Low: 18.209999,
        Close: 18.469999,
        Volume: 2126300,
        AdjClose: 13.922895
    },
    {
        Date: "2007-04-18",
        Open: 18.23,
        High: 18.43,
        Low: 18.219999,
        Close: 18.34,
        Volume: 2544000,
        AdjClose: 13.8249
    },
    {
        Date: "2007-04-17",
        Open: 18.379999,
        High: 18.389999,
        Low: 18.18,
        Close: 18.200001,
        Volume: 5355200,
        AdjClose: 13.719367
    },
    {
        Date: "2007-04-16",
        Open: 18.370001,
        High: 18.440001,
        Low: 18.33,
        Close: 18.42,
        Volume: 1393600,
        AdjClose: 13.885205
    },
    {
        Date: "2007-04-13",
        Open: 18.15,
        High: 18.15,
        Low: 17.9,
        Close: 18.08,
        Volume: 1789900,
        AdjClose: 13.628909
    },
    {
        Date: "2007-04-12",
        Open: 17.719999,
        High: 18.02,
        Low: 17.68,
        Close: 18.02,
        Volume: 1640100,
        AdjClose: 13.583681
    },
    {
        Date: "2007-04-11",
        Open: 17.969999,
        High: 17.98,
        Low: 17.75,
        Close: 17.82,
        Volume: 1887400,
        AdjClose: 13.432918
    },
    {
        Date: "2007-04-10",
        Open: 17.92,
        High: 18.040001,
        Low: 17.91,
        Close: 18.040001,
        Volume: 1551900,
        AdjClose: 13.598758
    },
    {
        Date: "2007-04-09",
        Open: 18,
        High: 18,
        Low: 17.83,
        Close: 17.860001,
        Volume: 1149200,
        AdjClose: 13.463071
    },
    {
        Date: "2007-04-05",
        Open: 18.01,
        High: 18.040001,
        Low: 17.84,
        Close: 17.9,
        Volume: 1747300,
        AdjClose: 13.493223
    },
    {
        Date: "2007-04-04",
        Open: 17.84,
        High: 18.01,
        Low: 17.83,
        Close: 18,
        Volume: 2566100,
        AdjClose: 13.568604
    },
    {
        Date: "2007-04-03",
        Open: 17.5,
        High: 17.700001,
        Low: 17.49,
        Close: 17.58,
        Volume: 3575600,
        AdjClose: 13.252004
    },
    {
        Date: "2007-04-02",
        Open: 17.309999,
        High: 17.35,
        Low: 17.16,
        Close: 17.280001,
        Volume: 1851200,
        AdjClose: 13.025861
    },
    {
        Date: "2007-03-30",
        Open: 17.209999,
        High: 17.309999,
        Low: 17.09,
        Close: 17.18,
        Volume: 1871500,
        AdjClose: 12.950479
    },
    {
        Date: "2007-03-29",
        Open: 17.139999,
        High: 17.23,
        Low: 17.040001,
        Close: 17.18,
        Volume: 1398900,
        AdjClose: 12.950479
    },
    {
        Date: "2007-03-28",
        Open: 16.950001,
        High: 17.15,
        Low: 16.889999,
        Close: 16.940001,
        Volume: 1478800,
        AdjClose: 12.769565
    },
    {
        Date: "2007-03-27",
        Open: 17.280001,
        High: 17.35,
        Low: 17.16,
        Close: 17.26,
        Volume: 877500,
        AdjClose: 13.010784
    },
    {
        Date: "2007-03-26",
        Open: 17.450001,
        High: 17.5,
        Low: 17.190001,
        Close: 17.48,
        Volume: 1947700,
        AdjClose: 13.176622
    },
    {
        Date: "2007-03-23",
        Open: 17.620001,
        High: 17.65,
        Low: 17.52,
        Close: 17.57,
        Volume: 1036400,
        AdjClose: 13.244465
    },
    {
        Date: "2007-03-22",
        Open: 17.799999,
        High: 17.799999,
        Low: 17.41,
        Close: 17.530001,
        Volume: 2044100,
        AdjClose: 13.214314
    },
    {
        Date: "2007-03-21",
        Open: 17.190001,
        High: 17.6,
        Low: 17.1,
        Close: 17.6,
        Volume: 2743200,
        AdjClose: 13.26708
    },
    {
        Date: "2007-03-20",
        Open: 16.92,
        High: 17.07,
        Low: 16.889999,
        Close: 17.040001,
        Volume: 1455200,
        AdjClose: 12.844946
    },
    {
        Date: "2007-03-19",
        Open: 16.99,
        High: 17.17,
        Low: 16.950001,
        Close: 16.99,
        Volume: 2192300,
        AdjClose: 12.807255
    },
    {
        Date: "2007-03-16",
        Open: 16.74,
        High: 16.91,
        Low: 16.67,
        Close: 16.76,
        Volume: 1727600,
        AdjClose: 12.633879
    },
    {
        Date: "2007-03-15",
        Open: 16.530001,
        High: 16.790001,
        Low: 16.52,
        Close: 16.75,
        Volume: 2433400,
        AdjClose: 12.62634
    },
    {
        Date: "2007-03-14",
        Open: 16.370001,
        High: 16.610001,
        Low: 16.190001,
        Close: 16.6,
        Volume: 3507900,
        AdjClose: 12.513269
    },
    {
        Date: "2007-03-13",
        Open: 16.85,
        High: 17,
        Low: 16.65,
        Close: 16.67,
        Volume: 3593900,
        AdjClose: 12.566035
    },
    {
        Date: "2007-03-12",
        Open: 16.85,
        High: 17.120001,
        Low: 16.809999,
        Close: 17.07,
        Volume: 1678600,
        AdjClose: 12.86756
    },
    {
        Date: "2007-03-09",
        Open: 16.98,
        High: 17.040001,
        Low: 16.790001,
        Close: 16.85,
        Volume: 1921500,
        AdjClose: 12.701722
    },
    {
        Date: "2007-03-08",
        Open: 16.879999,
        High: 17.049999,
        Low: 16.809999,
        Close: 16.92,
        Volume: 4507200,
        AdjClose: 12.754488
    },
    {
        Date: "2007-03-07",
        Open: 16.51,
        High: 16.57,
        Low: 16.290001,
        Close: 16.34,
        Volume: 3577100,
        AdjClose: 12.317278
    },
    {
        Date: "2007-03-06",
        Open: 16.23,
        High: 16.299999,
        Low: 16.049999,
        Close: 16.24,
        Volume: 3601800,
        AdjClose: 12.241896
    },
    {
        Date: "2007-03-05",
        Open: 15.95,
        High: 16.209999,
        Low: 15.94,
        Close: 15.96,
        Volume: 4553700,
        AdjClose: 12.030829
    },
    {
        Date: "2007-03-02",
        Open: 16.530001,
        High: 16.75,
        Low: 16.459999,
        Close: 16.52,
        Volume: 2284800,
        AdjClose: 12.452964
    },
    {
        Date: "2007-03-01",
        Open: 16.389999,
        High: 16.68,
        Low: 16.219999,
        Close: 16.58,
        Volume: 3381100,
        AdjClose: 12.498192
    },
    {
        Date: "2007-02-28",
        Open: 16.74,
        High: 16.870001,
        Low: 16.559999,
        Close: 16.709999,
        Volume: 2641400,
        AdjClose: 12.596187
    },
    {
        Date: "2007-02-27",
        Open: 16.780001,
        High: 16.99,
        Low: 16.09,
        Close: 16.379999,
        Volume: 7077000,
        AdjClose: 12.347429
    },
    {
        Date: "2007-02-26",
        Open: 17.99,
        High: 18.02,
        Low: 17.790001,
        Close: 17.879999,
        Volume: 1839500,
        AdjClose: 13.478146
    },
    {
        Date: "2007-02-23",
        Open: 17.790001,
        High: 17.889999,
        Low: 17.77,
        Close: 17.85,
        Volume: 1185200,
        AdjClose: 13.455533
    },
    {
        Date: "2007-02-22",
        Open: 17.870001,
        High: 17.959999,
        Low: 17.82,
        Close: 17.92,
        Volume: 1975700,
        AdjClose: 13.5083
    },
    {
        Date: "2007-02-21",
        Open: 17.85,
        High: 17.879999,
        Low: 17.700001,
        Close: 17.82,
        Volume: 2626900,
        AdjClose: 13.432918
    },
    {
        Date: "2007-02-20",
        Open: 18.01,
        High: 18.049999,
        Low: 17.860001,
        Close: 18.040001,
        Volume: 1894300,
        AdjClose: 13.598758
    },
    {
        Date: "2007-02-16",
        Open: 18.110001,
        High: 18.110001,
        Low: 17.879999,
        Close: 18.110001,
        Volume: 4157200,
        AdjClose: 13.651524
    },
    {
        Date: "2007-02-15",
        Open: 18.35,
        High: 18.57,
        Low: 18.290001,
        Close: 18.48,
        Volume: 10167700,
        AdjClose: 13.930434
    },
    {
        Date: "2007-02-14",
        Open: 18.66,
        High: 19.25,
        Low: 18.65,
        Close: 19.129999,
        Volume: 2398900,
        AdjClose: 14.420411
    },
    {
        Date: "2007-02-13",
        Open: 18.42,
        High: 18.549999,
        Low: 18.379999,
        Close: 18.549999,
        Volume: 1215400,
        AdjClose: 13.9832
    },
    {
        Date: "2007-02-12",
        Open: 18.459999,
        High: 18.49,
        Low: 18.290001,
        Close: 18.35,
        Volume: 1173600,
        AdjClose: 13.832439
    },
    {
        Date: "2007-02-09",
        Open: 18.52,
        High: 18.709999,
        Low: 18.48,
        Close: 18.540001,
        Volume: 1748100,
        AdjClose: 13.975663
    },
    {
        Date: "2007-02-08",
        Open: 18.42,
        High: 18.540001,
        Low: 18.370001,
        Close: 18.48,
        Volume: 1387900,
        AdjClose: 13.930434
    },
    {
        Date: "2007-02-07",
        Open: 18.75,
        High: 18.85,
        Low: 18.67,
        Close: 18.719999,
        Volume: 2111400,
        AdjClose: 14.111348
    },
    {
        Date: "2007-02-06",
        Open: 18.280001,
        High: 18.41,
        Low: 18.23,
        Close: 18.389999,
        Volume: 1569200,
        AdjClose: 13.86259
    },
    {
        Date: "2007-02-05",
        Open: 18.110001,
        High: 18.16,
        Low: 18.040001,
        Close: 18.07,
        Volume: 1340200,
        AdjClose: 13.621371
    },
    {
        Date: "2007-02-02",
        Open: 18.16,
        High: 18.219999,
        Low: 18.09,
        Close: 18.209999,
        Volume: 1561600,
        AdjClose: 13.726904
    },
    {
        Date: "2007-02-01",
        Open: 18.01,
        High: 18.110001,
        Low: 17.9,
        Close: 18.02,
        Volume: 1208600,
        AdjClose: 13.583681
    }
];
