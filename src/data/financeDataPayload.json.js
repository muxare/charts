// type: 'RECEIVE_COMPANIES_ANALYSIS',
//
export const dataRowsPayload = [
    {
        Name: 'Telefonaktiebolaget LM Ericsson (publ)',
        Ticker: 'ERIC-B.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 13099760.059701493,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: false,
            downTrend: true,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Telia Company AB',
        Ticker: 'TELIA.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 10123930.6125,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: true,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Fingerprint Cards AB (publ)',
        Ticker: 'FING-B.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 9741875,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: false,
            downTrend: true,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Nordea Bank AB (publ)',
        Ticker: 'NDA-SEK.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 6988535.611940298,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: true,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'SSAB AB',
        Ticker: 'SSAB-A.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 6617436.313432836,
        TechnicalIndiactors: {
            upTrend: true,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Svenska Cellulosa Aktiebolaget SCA (publ)',
        Ticker: 'SCA-B.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 6031683.884615385,
        TechnicalIndiactors: {
            upTrend: true,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'AB Volvo (publ)',
        Ticker: 'VOLV-B.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 5776312.47761194,
        TechnicalIndiactors: {
            upTrend: true,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'SSAB AB',
        Ticker: 'SSAB-B.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 4978357.566265061,
        TechnicalIndiactors: {
            upTrend: true,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Sandvik AB',
        Ticker: 'SAND.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 4917167.831325301,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: true,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'H & M Hennes & Mauritz AB (publ)',
        Ticker: 'HM-B.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 4677672.694915255,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: true,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Skandinaviska Enskilda Banken AB (publ)',
        Ticker: 'SEB-A.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 4655091.567164179,
        TechnicalIndiactors: {
            upTrend: true,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Swedbank AB (publ)',
        Ticker: 'SWED-A.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 2553094.656716418,
        TechnicalIndiactors: {
            upTrend: true,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Tele2 AB (publ)',
        Ticker: 'TEL2-B.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 2177187.656716418,
        TechnicalIndiactors: {
            upTrend: true,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Atlas Copco AB',
        Ticker: 'ATCO-A.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 2027799.1044776118,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: true,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Boliden AB (publ.)',
        Ticker: 'BOL.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 1928356.78313253,
        TechnicalIndiactors: {
            upTrend: true,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Klövern AB (publ)',
        Ticker: 'KLOV-B.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 1593453.8192771084,
        TechnicalIndiactors: {
            upTrend: true,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Alfa Laval AB (publ)',
        Ticker: 'ALFA.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 1481053.6506024096,
        TechnicalIndiactors: {
            upTrend: true,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Husqvarna AB (publ)',
        Ticker: 'HUSQ-B.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 1297639.328358209,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: true,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Getinge AB',
        Ticker: 'GETI-B.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 1237586.5820895522,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: false,
            downTrend: true,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'NetEnt AB (publ)',
        Ticker: 'NET-B.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 1209615.625,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: false,
            downTrend: true,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'AB Electrolux (publ)',
        Ticker: 'ELUX-B.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 1104985.5074626866,
        TechnicalIndiactors: {
            upTrend: true,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Investor AB',
        Ticker: 'INVE-B.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 1056860.786885246,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: true,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Lundin Petroleum AB (publ)',
        Ticker: 'LUPE.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 869629.0240963856,
        TechnicalIndiactors: {
            upTrend: true,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Castellum AB (publ)',
        Ticker: 'CAST.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 855246.108433735,
        TechnicalIndiactors: {
            upTrend: true,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Ratos AB',
        Ticker: 'RATO-B.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 785664,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: false,
            downTrend: true,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Trelleborg AB',
        Ticker: 'TREL-B.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 739031.9104477612,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: true,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Dometic Group AB (publ)',
        Ticker: 'DOM.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 668475.3012048192,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: true,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'BillerudKorsnäs AB (publ)',
        Ticker: 'BILL.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 647135.7710843374,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: false,
            downTrend: true,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Hexpol AB (Publ)',
        Ticker: 'HPOL-B.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 640896.0843373494,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: false,
            downTrend: true,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Stora Enso Oyj',
        Ticker: 'STE-R.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 590917.5522388059,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: true,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Peab AB',
        Ticker: 'PEAB-B.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 558655.6746987952,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: true,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Hexagon AB',
        Ticker: 'HEXA-B.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 507872.2923076923,
        TechnicalIndiactors: {
            upTrend: true,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Kinnevik AB',
        Ticker: 'KINV-B.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 488688,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: false,
            downTrend: true,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Betsson AB',
        Ticker: 'BETS-B.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 404812,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: false,
            downTrend: true,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Axfood AB',
        Ticker: 'AXFO.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 397833.3373493976,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: false,
            downTrend: true,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Resurs Holding AB (publ)',
        Ticker: 'RESURS.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 394718.3855421687,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: false,
            downTrend: true,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Autoliv, Inc.',
        Ticker: 'ALIV-SDB.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 388154.6567164179,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: false,
            downTrend: true,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Hemfosa Fastigheter AB (publ)',
        Ticker: 'HEMF.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 385787.6385542169,
        TechnicalIndiactors: {
            upTrend: true,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Intrum Justitia AB',
        Ticker: 'IJ.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 385761.9397590361,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: false,
            downTrend: true,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Aktiebolaget Industrivärden',
        Ticker: 'INDU-C.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 377284.43396226416,
        TechnicalIndiactors: {
            upTrend: true,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Com Hem Holding AB (publ)',
        Ticker: 'COMH.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 329077.7710843373,
        TechnicalIndiactors: {
            upTrend: true,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Fabege AB (publ)',
        Ticker: 'FABG.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 320820.421686747,
        TechnicalIndiactors: {
            upTrend: true,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Bravida Holding AB (publ)',
        Ticker: 'BRAV.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 313539.0481927711,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: true,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'ICA Gruppen AB',
        Ticker: 'ICA.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 302861.7228915663,
        TechnicalIndiactors: {
            upTrend: true,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Fastighets AB Balder (publ)',
        Ticker: 'BALD-B.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 299603.0120481928,
        TechnicalIndiactors: {
            upTrend: true,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'NIBE Industrier AB (publ)',
        Ticker: 'NIBE-B.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 285004,
        TechnicalIndiactors: {
            upTrend: true,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Scandic Hotels Group AB',
        Ticker: 'SHOT.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 260912.8674698795,
        TechnicalIndiactors: {
            upTrend: true,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Saab AB (publ)',
        Ticker: 'SAAB-B.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 250323.15384615384,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: true,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Wallenstam AB (publ)',
        Ticker: 'WALL-B.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 242916.73493975904,
        TechnicalIndiactors: {
            upTrend: true,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Hufvudstaden AB (publ)',
        Ticker: 'HUFV-A.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 215888.07228915664,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: true,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Modern Times Group Mtg AB',
        Ticker: 'MTG-B.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 200816.20895522388,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: true,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Indutrade AB (publ)',
        Ticker: 'INDT.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 191524.43373493975,
        TechnicalIndiactors: {
            upTrend: true,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Loomis AB (publ)',
        Ticker: 'LOOM-B.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 184232.77108433735,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: true,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Thule Group AB',
        Ticker: 'THULE.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 182902.8313253012,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: true,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'AB Volvo (publ)',
        Ticker: 'VOLV-A.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 164890.09638554216,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: true,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Aktiebolaget Industrivärden',
        Ticker: 'INDU-A.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 134699.3734939759,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: true,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Wihlborgs Fastigheter AB (publ)',
        Ticker: 'WIHL.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 122283.2530120482,
        TechnicalIndiactors: {
            upTrend: true,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Attendo AB (publ)',
        Ticker: 'ATT.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 121749.85542168675,
        TechnicalIndiactors: {
            upTrend: true,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Millicom International Cellular S.A.',
        Ticker: 'MIC-SDB.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 112124.06153846154,
        TechnicalIndiactors: {
            upTrend: true,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Holmen Aktiebolag (publ)',
        Ticker: 'HOLM-B.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 107076.28813559322,
        TechnicalIndiactors: {
            upTrend: true,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'L E Lundbergföretagen AB (publ)',
        Ticker: 'LUND-B.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 90458.2048192771,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: true,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Investor AB',
        Ticker: 'INVE-A.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 81730.18072289157,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: true,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Evolution Gaming Group AB (publ)',
        Ticker: 'EVO.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 73122.77108433735,
        TechnicalIndiactors: {
            upTrend: true,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Pandox AB (publ)',
        Ticker: 'PNDX-B.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 65134.68674698795,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: true,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Clas Ohlson AB (publ)',
        Ticker: 'CLAS-B.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 64955.4578313253,
        TechnicalIndiactors: {
            upTrend: true,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Klövern AB (publ)',
        Ticker: 'KLOV-A.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 57470.9156626506,
        TechnicalIndiactors: {
            upTrend: true,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Avanza Bank Holding AB (publ)',
        Ticker: 'AZA.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 56068.24096385542,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: false,
            downTrend: true,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Investment AB Latour (publ)',
        Ticker: 'LATO-B.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 53884.09090909091,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: true,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Lifco AB',
        Ticker: 'LIFCO-B.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 47395.48192771084,
        TechnicalIndiactors: {
            upTrend: true,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Telefonaktiebolaget LM Ericsson (publ)',
        Ticker: 'ERIC-A.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 43669.74698795181,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: false,
            downTrend: true,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Svenska Cellulosa Aktiebolaget SCA (publ)',
        Ticker: 'SCA-A.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 39648.31325301205,
        TechnicalIndiactors: {
            upTrend: true,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Melker Schörling AB',
        Ticker: 'MELK.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 30097.5421686747,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: false,
            downTrend: true,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Skandinaviska Enskilda Banken AB (publ)',
        Ticker: 'SEB-C.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 29872.734939759037,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: true,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Vitrolife AB (publ)',
        Ticker: 'VITR.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 28498.819277108432,
        TechnicalIndiactors: {
            upTrend: true,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Akelius Residential Property AB (publ)',
        Ticker: 'AKEL-PREF.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 23861.975308641977,
        TechnicalIndiactors: {
            upTrend: true,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'AB Sagax',
        Ticker: 'SAGA-B.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 21291.614457831325,
        TechnicalIndiactors: {
            upTrend: true,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Klövern AB (publ)',
        Ticker: 'KLOV-PREF.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 20708.698795180724,
        TechnicalIndiactors: {
            upTrend: true,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'AB Fagerhult',
        Ticker: 'FAG.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 14130.333333333334,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: false,
            downTrend: true,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Hemfosa Fastigheter AB (publ)',
        Ticker: 'HEMF-PREF.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 12576.753086419752,
        TechnicalIndiactors: {
            upTrend: true,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Husqvarna AB (publ)',
        Ticker: 'HUSQ-A.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 11171.216867469879,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: true,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Tieto Oyj',
        Ticker: 'TIEN.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 9013.855421686747,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: true,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Stora Enso Oyj',
        Ticker: 'STE-A.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 7454.204819277108,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: true,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Beijer Ref AB (publ)',
        Ticker: 'BEIJ-B.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 6930.156626506024,
        TechnicalIndiactors: {
            upTrend: true,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Old Mutual plc',
        Ticker: 'OLDM.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 6482.240963855422,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: false,
            downTrend: true,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Tele2 AB (publ)',
        Ticker: 'TEL2-A.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 4486.156626506024,
        TechnicalIndiactors: {
            upTrend: true,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'NCC AB',
        Ticker: 'NCC-A.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 4307.204819277108,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: false,
            downTrend: true,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Ratos AB',
        Ticker: 'RATO-A.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 4108.843373493976,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: false,
            downTrend: true,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Fenix Outdoor International AG',
        Ticker: 'FOI-B.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 2858.277108433735,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: true,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Pfizer Inc.',
        Ticker: 'PFE.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 2524.6987951807228,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: false,
            downTrend: true,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'AB Sagax',
        Ticker: 'SAGA-A.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 1535.987951807229,
        TechnicalIndiactors: {
            upTrend: true,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'AB Electrolux (publ)',
        Ticker: 'ELUX-A.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 905.9277108433735,
        TechnicalIndiactors: {
            upTrend: true,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Ferronordic Machines AB',
        Ticker: 'FNMA-PREF.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 710.8375,
        TechnicalIndiactors: {
            upTrend: true,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Holmen Aktiebolag (publ)',
        Ticker: 'HOLM-A.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 586.7710843373494,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: true,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Volati AB',
        Ticker: 'VOLO-PREF.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 506,
        TechnicalIndiactors: {
            upTrend: true,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 1
    },
    {
        Name: 'Svenska Handelsbanken AB (publ)',
        Ticker: 'SHB-A.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 3158530.5223880596,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 0
    },
    {
        Name: 'SKF AB',
        Ticker: 'SKF-B.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 2612950.6716417912,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 0
    },
    {
        Name: 'ASSA ABLOY AB (publ)',
        Ticker: 'ASSA-B.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 2174499.6144578313,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 0
    },
    {
        Name: 'Ahlsell AB (publ)',
        Ticker: 'AHSL.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 1670501.21686747,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 0
    },
    {
        Name: 'Elekta AB (publ)',
        Ticker: 'EKTA-B.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 1447814.6153846155,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 0
    },
    {
        Name: 'Skanska AB (publ)',
        Ticker: 'SKA-B.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 1433033.0298507463,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 0
    },
    {
        Name: 'Securitas AB',
        Ticker: 'SECU-B.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 1315312.8059701493,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 0
    },
    {
        Name: 'ABB Ltd',
        Ticker: 'ABB.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 1146187.6626506024,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 0
    },
    {
        Name: 'Atlas Copco AB',
        Ticker: 'ATCO-B.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 835961.8192771084,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 0
    },
    {
        Name: 'Swedish Match AB (publ)',
        Ticker: 'SWMA.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 715230.5180722892,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 0
    },
    {
        Name: 'Swedish Orphan Biovitrum AB',
        Ticker: 'SOBI.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 643832.7590361446,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 0
    },
    {
        Name: 'Kungsleden Aktiebolag',
        Ticker: 'KLED.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 510723.2168674699,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 0
    },
    {
        Name: 'AstraZeneca PLC',
        Ticker: 'AZN.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 503380.7469879518,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 0
    },
    {
        Name: 'NCC AB',
        Ticker: 'NCC-B.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 345635.26153846155,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 0
    },
    {
        Name: 'Nobia AB (publ)',
        Ticker: 'NOBI.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 314019.5180722892,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 0
    },
    {
        Name: 'JM AB (publ)',
        Ticker: 'JM.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 303281.6987951807,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 0
    },
    {
        Name: 'Oriflame Holding AG',
        Ticker: 'ORI.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 112191.73493975903,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 0
    },
    {
        Name: 'Sweco AB (publ)',
        Ticker: 'SWEC-B.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 84899.80722891567,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 0
    },
    {
        Name: 'ÅF AB (publ)',
        Ticker: 'AF-B.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 71107.93975903615,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 0
    },
    {
        Name: 'Atrium Ljungberg AB (publ)',
        Ticker: 'ATRLJ-B.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 55177.39759036145,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 0
    },
    {
        Name: 'AAK AB (publ.)',
        Ticker: 'AAK.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 49289.180722891564,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 0
    },
    {
        Name: 'Svenska Handelsbanken AB (publ)',
        Ticker: 'SHB-B.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 38977.02409638554,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 0
    },
    {
        Name: 'SKF AB',
        Ticker: 'SKF-A.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 12389.036144578313,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 0
    },
    {
        Name: 'Kinnevik AB',
        Ticker: 'KINV-A.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 1616,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 0
    },
    {
        Name: 'Sweco AB (publ)',
        Ticker: 'SWEC-A.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 626.3855421686746,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 0
    },
    {
        Name: 'Axis AB (publ)',
        Ticker: 'AXIS.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 597.3373493975904,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 0
    },
    {
        Name: 'Modern Times Group Mtg AB',
        Ticker: 'MTG-A.ST',
        Date: '2017-08-14T00:00:00',
        AverageTradeVolume: 419.89156626506025,
        TechnicalIndiactors: {
            upTrend: false,
            upCounterTrend: false,
            downTrend: false,
            downCounterTrend: false,
            emaFanUpContinuation: false,
            emaFanDownContinuation: false,
            emaFanBounceUp: false,
            emaFanBounceDown: false
        },
        TechnicalIndicatorScore: 0
    }
];
